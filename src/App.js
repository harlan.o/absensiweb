
import Login  from './views/login/Login';
import Layout  from './layouts/Layout';
import { useSelector } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import './App.css';
import ProtectedRoute from "./components/ProtectedRoute";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import Page404 from './views/page-error/Page404';
import PageMaintenance from './views/page-error/PageMaintenance';
import { useState } from 'react';
import firebase from './firebase/init-firebase';
import "firebase/database";
// test fcm


function App() {
  const userLogin = useSelector(state => state.userLogin);
  const { token } = userLogin;
  const [block, setBlock]= useState(false);
  const [pesan, setPesan]= useState("Sedang Maintenance");
  console.log = console.warn = console.error = () => {};
  const db= firebase.database();
  // console.log(firebase.database())
  const lockMessage= db.ref('lockMessage');
  lockMessage.on('value', (snp)=> {
    if(snp.val() !== pesan){
      setPesan(snp.val());
      console.log("Pesan" + snp.val())
    }
  })
  const isLock= db.ref('isLock');
  isLock.on('value', (snp)=> {
    if(snp.val() !== block){
      setBlock(snp.val())
    }
    console.log("Maintenance :" + snp.val())
  })
  
  

  return (
    <div className="App">
      <header className="App-header">
        
        <Router>
          <Switch>
              <Route exact path="/">
              {block ? (
                <Redirect to="/maintenance" />
              ) : ""}

              {!!token ? (
                <Redirect to="/dashboard" />
              ) : (
                <Redirect to="/login" />
              )}
            </Route>
            <Route path="/login">
              {block ? (
                <Redirect to="/maintenance" />
              ) : ""}
              {!!token ? (
                <Redirect to="/dashboard" />
              ) : (
                <Login />
              )}
              </Route>

            <ProtectedRoute exact path="/dashboard" kode_ha={["FU005"]} ><Layout page="dashboard"/></ProtectedRoute>
            <ProtectedRoute path="/statistik" kode_ha={["FU007"]}><Layout page="statistik"/></ProtectedRoute>
            <ProtectedRoute exact path="/absensi-pegawai" kode_ha={["FU009"]}><Layout page="absensi-pegawai"/></ProtectedRoute>
            <ProtectedRoute path="/absensi-pegawai/hadir" kode_ha={["FU009"]}><Layout page="absensi-pegawai" sub_page="hadir"/></ProtectedRoute>
            <ProtectedRoute path="/absensi-pegawai/absen" kode_ha={["FU009"]}><Layout page="absensi-pegawai" sub_page="absen"/></ProtectedRoute>
            {/* <ProtectedRoute path="/absensi-pegawai/cuti" kode_ha={["FU009"]}><Layout page="absensi-pegawai" sub_page="cuti"/></ProtectedRoute> */}
            {/* <ProtectedRoute path="/absensi-pegawai/sakit" kode_ha={["FU009"]}><Layout page="absensi-pegawai" sub_page="sakit"/></ProtectedRoute> */}
            <ProtectedRoute path="/absensi-pegawai/tanpa-keterangan" kode_ha={["FU009"]}><Layout page="absensi-pegawai" sub_page="tanpa-keterangan"/></ProtectedRoute>
            {/* <ProtectedRoute path="/dokumen-surat"><Layout page="dokumen-surat"/></ProtectedRoute> */}
            <ProtectedRoute path="/pengambilan-ijin" kode_ha={["FU011"]}><Layout page="pengambilan-ijin"/></ProtectedRoute>
            <ProtectedRoute exact path="/penugasan" kode_ha={["FU015", "FU020"]}><Layout page="penugasan" sub_page=""/></ProtectedRoute>
            <ProtectedRoute path="/penugasan/tugas-luar" kode_ha={["FU015"]}><Layout page="penugasan" sub_page="tugas-luar"/></ProtectedRoute>
            <ProtectedRoute path="/penugasan/melaksanakan-tugas" kode_ha={["FU020"]}><Layout page="penugasan" sub_page="melaksanakan-tugas"/></ProtectedRoute>
            <ProtectedRoute exact path="/laporan-absen" kode_ha={["FU025"]}><Layout page="laporan-absen" sub_page=""/></ProtectedRoute>
            <ProtectedRoute path="/laporan-absen/absen-harian" kode_ha={["FU025"]}><Layout page="laporan-absen" sub_page="absen-harian"/></ProtectedRoute>
            <ProtectedRoute path="/laporan-absen/absen-ringkas" kode_ha={["FU025"]}><Layout page="laporan-absen" sub_page="absen-ringkas"/></ProtectedRoute>
            <ProtectedRoute path="/laporan-absen/tahunan" kode_ha={["FU025"]}><Layout page="laporan-absen" sub_page="tahunan"/></ProtectedRoute>
            <ProtectedRoute exact path="/laporan-tpp" kode_ha={["FU027"]}><Layout page="laporan-tpp"/></ProtectedRoute>
            <ProtectedRoute exact path="/laporan-akhir" kode_ha={["FU030", "FU032"]}><Layout page="laporan-akhir"/></ProtectedRoute>
            <ProtectedRoute path="/laporan-akhir/akhir-1" kode_ha={["FU030"]}><Layout page="laporan-akhir" sub_page="akhir-1"/></ProtectedRoute>
            <ProtectedRoute path="/laporan-akhir/akhir-2" kode_ha={["FU032"]}><Layout page="laporan-akhir" sub_page="akhir-2"/></ProtectedRoute>
            <ProtectedRoute path="/artikel" kode_ha={["FU034"]}><Layout page="artikel"/></ProtectedRoute>
            <ProtectedRoute path="/even" kode_ha={["FU039"]}><Layout page="even"/></ProtectedRoute>
            <ProtectedRoute exact path="/schedule" kode_ha={["FU044", "FU048", "FU053"]}><Layout page="schedule"/></ProtectedRoute>
            <ProtectedRoute path="/schedule/jadwal-pegawai" kode_ha={["FU044"]}><Layout page="schedule" sub_page="jadwal-pegawai"/></ProtectedRoute>
            <ProtectedRoute path="/schedule/jadwal-kerja" kode_ha={["FU048"]}><Layout page="schedule" sub_page="jadwal-kerja"/></ProtectedRoute>
            <ProtectedRoute path="/schedule/hari-libur" kode_ha={["FU053"]}><Layout page="schedule" sub_page="hari-libur"/></ProtectedRoute>
            <ProtectedRoute exact path="/pengaturan" kode_ha={["FU057", "FU063", "FU067", "FU071"]}><Layout page="pengaturan"/></ProtectedRoute>
            <ProtectedRoute path="/pengaturan/pegawai" kode_ha={["FU057"]}><Layout page="pengaturan" sub_page="pegawai"/></ProtectedRoute>
            <ProtectedRoute path="/pengaturan/unit-induk" kode_ha={["FU063"]}><Layout page="pengaturan" sub_page="unit-induk"/></ProtectedRoute>
            <ProtectedRoute path="/pengaturan/unit-kerja" kode_ha={["FU067"]}><Layout page="pengaturan" sub_page="unit-kerja"/></ProtectedRoute>
            {/* <ProtectedRoute path="/pengaturan/aturan-disiplin"><Layout page="pengaturan" sub_page="aturan-disiplin"/></ProtectedRoute> */}
            <ProtectedRoute path="/pengaturan/tpp" kode_ha={["FU071"]}><Layout page="pengaturan" sub_page="tpp"/></ProtectedRoute>
            {/* <ProtectedRoute path="/pengaturan/lokasi-absensi"><Layout page="pengaturan" sub_page="lokasi-absensi"/></ProtectedRoute> */}
            {/* <ProtectedRoute path="/pengaturan/management-waktu"><Layout page="pengaturan" sub_page="management-waktu"/></ProtectedRoute> */}
            {/* <ProtectedRoute path="/pengaturan"><Layout page="pengaturan" sub_page=""/></ProtectedRoute> */}
            <Route path="/maintenance">{block ? (<PageMaintenance message={pesan}/>) : <Redirect to="/" />}</Route>
            <Route path="/*"><Page404/></Route>
            {/* <Route path="/firebase-messaging-sw.js"></Route> */}
          </Switch>
        </Router>
      </header>
    </div>
  );
}

export default App;
