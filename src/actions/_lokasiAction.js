import axios from "axios";
import {
    LIST_LOKASI_KOTA_REQUEST,
    LIST_LOKASI_KOTA_SUCCESS,
    LIST_LOKASI_KOTA_FAIL,
    LIST_LOKASI_KOTA_RESET,
  } from "../constants/_lokasiConstants";
import {
    URL_API,
    API_LIST_LOKASI_KOTA,
  } from "../constants/apiConstants";

  export const getListLokasiKota = () => async (dispatch, getState) =>{
    // console.log("unit Induk");
    try {
        dispatch({
          type: LIST_LOKASI_KOTA_REQUEST,
        })
        
        const {
          userLogin: { token },
        } = getState()
        
        const config = {
          headers: {
            Authorization: `${token}`,
          },
        }
        const { data: {lokasiList} } = await axios.get(URL_API+API_LIST_LOKASI_KOTA, config)
        // console.log(pegawaiList);
        dispatch({
          type: LIST_LOKASI_KOTA_SUCCESS,
          payload: lokasiList,
        })
      } catch (error) {
        console.log("error");
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LIST_LOKASI_KOTA_FAIL,
          payload: message,
        })
      }
  }