import axios from "axios";
import {
    LIST_ABSENSI_INDUK_REQUEST,
    LIST_ABSENSI_INDUK_SUCCESS,
    LIST_ABSENSI_INDUK_FAIL,
    LIST_ABSENSI_INDUK_RESET,
    LIST_ABSENSI_KERJA_REQUEST,
    LIST_ABSENSI_KERJA_SUCCESS,
    LIST_ABSENSI_KERJA_FAIL,
    LIST_ABSENSI_KERJA_RESET,
    TOTAL_HADIR,
    TOTAL_HADIR_REQUEST,
    LIST_ABSENSI_DASHBOARD_REQUEST,
    LIST_ABSENSI_DASHBOARD_SUCCESS,
    LIST_ABSENSI_DASHBOARD_FAIL,
    LIST_ABSENSI_DASHBOARD_RESET,
    LAPORAN_ABSENSI_REQUEST,
    LAPORAN_ABSENSI_SUCCESS,
    LAPORAN_ABSENSI_FAIL,
    LAPORAN_ABSENSI_RESET,
  } from "../constants/absensiConstants";
  import {
    URL_API,
    API_LIST_ABSENSI_INDUK,
    API_LIST_ABSENSI_KERJA,
    API_LIST_ABSENSI_INDUK_ALL,
    API_LIST_ABSENSI_DASHBOARD,
    API_LIST_ABSENSI_DASHBOARD_V2,
    API_LAPORAN_ABSENSI
  } from "../constants/apiConstants";

  import {
    stringArrayId
  } from "./genneralAction";

  export const getListAbsenInduk = (id, startDate, endDate, tipe) => async (dispatch, getState) =>{
    try {
        dispatch({
          type: LIST_ABSENSI_INDUK_REQUEST,
        })
        const {
          userLogin: { token },
        } = getState()
        const config = {
          headers: {
            Authorization: `${token}`,
          },
          params:{
            id:`${id}`,
            startDate:`${startDate}`,
            endDate:`${endDate}`,
            tipe:`${tipe}`,
            },
        }
        const { data: {daftarAbsen} } = await axios.get(URL_API+API_LIST_ABSENSI_INDUK, config)
        dispatch({
          type: LIST_ABSENSI_INDUK_SUCCESS,
          payload: daftarAbsen,
        });
        // console.log(config);
        // console.log(daftarAbsen);
        //  console.log("token: "+token);
        // console.log("Ambil Data Profile "+profilPegawai);
      } catch (error) {
        console.log("error");
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LIST_ABSENSI_INDUK_FAIL,
          payload: message,
        })
      }
  }

  export const getListAbsenIndukAll = ( startDate, endDate, tipe) => async (dispatch, getState) =>{
    try {
        dispatch({
          type: LIST_ABSENSI_INDUK_REQUEST,
        })
        const {
          userLogin: { token },
        } = getState()
        const config = {
          headers: {
            Authorization: `${token}`,
          },
          params:{
            startDate:`${startDate}`,
            endDate:`${endDate}`,
            tipe:`${tipe}`,
            },
        }
        const { data: {daftarAbsen} } = await axios.get(URL_API+API_LIST_ABSENSI_INDUK_ALL, config)
        daftarAbsen.sort((a, b) => new Date(b.jamAbsen) - new Date(a.jamAbsen));
        dispatch({
          type: LIST_ABSENSI_INDUK_SUCCESS,
          payload: daftarAbsen,
        });
        // console.log("filter");
        // console.log(daftarAbsen);
        //  console.log("token: "+token);
        // console.log("Ambil Data Profile "+profilPegawai);
      } catch (error) {
        console.log("error");
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LIST_ABSENSI_INDUK_FAIL,
          payload: message,
        })
      }
  }

  // untuk admin OPD
  // export const getListAbsenDashboard = ( tanggalKerja, unitIndukId, unitKerjaId) => async (dispatch, getState) =>{
  //   try {
  //       dispatch({
  //         type: LIST_ABSENSI_DASHBOARD_REQUEST,
  //       })       
  //       const {
  //         userLogin: { token },
  //       } = getState()
  //       const config = {
  //         headers: {
  //           Authorization: `${token}`,
  //         },
  //         params:{
  //           tanggalKerja:`${tanggalKerja}`,
  //           unitIndukId:`${unitIndukId}`,
  //           unitKerjaId:`${unitKerjaId}`,
  //           },
  //       }
  //       const { data } = await axios.get(URL_API+API_LIST_ABSENSI_DASHBOARD, config)
  //       data.daftarAbsen.sort((a, b) => new Date(b.jamAbsen) - new Date(a.jamAbsen));
  //       dispatch({
  //         type: LIST_ABSENSI_DASHBOARD_SUCCESS,
  //         payload: data,
  //       });
        
  //     } catch (error) {
  //       console.log("error");
  //       const message =
  //         error.response && error.response.data.message
  //           ? error.response.data.message
  //           : error.message
  //       if (message === 'Not authorized, token failed') {
  //       }
  //       dispatch({
  //         type: LIST_ABSENSI_DASHBOARD_FAIL,
  //         payload: message,
  //       })
  //     }
  // }

  export const getListAbsenDashboardV2 = ( tanggalKerja, selectUnitInduk, selectUnitKerja) => async (dispatch, getState) =>{
    try {
      
      dispatch({
        type: LIST_ABSENSI_DASHBOARD_REQUEST,
      })
      
     let unitInduk=stringArrayId(selectUnitInduk);
     let unitKerja=stringArrayId(selectUnitKerja);
      
        const {
          userLogin: { token },
        } = getState()
        let config
        if(unitInduk != null && unitKerja != null){
           config = {
            headers: {
              Authorization: `${token}`,
            },
            params:{
              tanggalKerja:`${tanggalKerja}`,
              unitKerjaId:`${unitKerja}`,
              },
          }
        }else if(unitInduk != null && unitKerja == null){
           config = {
            headers: {
              Authorization: `${token}`,
            },
            params:{
              tanggalKerja:`${tanggalKerja}`,
              unitIndukId:`${unitInduk}`,
              },
          }
        }else{
           config = {
            headers: {
              Authorization: `${token}`,
            },
            params:{
              tanggalKerja:`${tanggalKerja}`
              },
          }
        }
        const { data } = await axios.get(URL_API+API_LIST_ABSENSI_DASHBOARD_V2, config)
        data.daftarAbsen.sort((a, b) => new Date(b.jamAbsen) - new Date(a.jamAbsen));
        dispatch({
          type: LIST_ABSENSI_DASHBOARD_SUCCESS,
          payload: data,
        });
        
      } catch (error) {
        console.log("error");
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LIST_ABSENSI_DASHBOARD_FAIL,
          payload: message,
        })
      }
  }


  export const getListAbsenKerja = (id, startDate, endDate, tipe) => async (dispatch, getState) =>{
    try {
        dispatch({
          type: LIST_ABSENSI_KERJA_REQUEST,
        })
        
        const {
          userLogin: { token },
        } = getState()
        
        const config = {
          headers: {
            Authorization: `${token}`,
          },
          params:{
            id:`${id}`,
            startDate:`${startDate}`,
            endDate:`${endDate}`,
            tipe:`${tipe}`,
            },
        }
        // console.log(token+"token");
        const { data: {daftarAbsen} } = await axios.get(URL_API+API_LIST_ABSENSI_KERJA, config)
        // console.log(profilPegawai+"asd");
        daftarAbsen.sort((a, b) => new Date(b.jamAbsen) - new Date(a.jamAbsen));
        dispatch({
          type: LIST_ABSENSI_KERJA_SUCCESS,
          payload: daftarAbsen,
        })
        //  console.log(daftarAbsen);
        // console.log("Ambil Data Profile "+profilPegawai);
      } catch (error) {
        console.log("error");
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LIST_ABSENSI_KERJA_FAIL,
          payload: message,
        })
      }
  }

  export const setTotalHadir = (jumlah) => async (dispatch, getState) =>{
    try {
      const nilai={
        totalJumlah:20
      };
      dispatch({
        type: TOTAL_HADIR_REQUEST,
      });  

    await dispatch({
      type: TOTAL_HADIR,
      payload: jumlah,
    });  
    // const totalHadir = getState();
    console.log(nilai);
    // console.log(totalHadir);
  }catch (error) {
    console.log("error");}
  }


  export const getLaporanAbsen = (_monthYear, unitInduk, unitKerja) => async (dispatch, getState) =>{
    try {
        dispatch({
          type: LAPORAN_ABSENSI_REQUEST,
        })
        let monthYear=_monthYear;

        const {
          userLogin: { token },
        } = getState()
        const unitIndukId=stringArrayId(unitInduk);
        
        let config = {
          headers: {
            Authorization: `${token}`,
          },params:{
            unitIndukId:`${unitIndukId}`,
            yearMonth:`${monthYear}`,
          },
        }

        if(unitKerja != null && unitKerja.length > 0){
            const unitKerjaId=stringArrayId(unitKerja);
            config = {
                headers: {
                  Authorization: `${token}`,
                }, params:{
                  unitKerjaId:`${unitKerjaId}`,
                  yearMonth:`${monthYear}`,
                },
            }   
        }
        console.log("getLaporan")
        console.log(URL_API+API_LAPORAN_ABSENSI)
        console.log(config)
        const { data: {rekapAbsen} } = await axios.get(URL_API+API_LAPORAN_ABSENSI, config)
        ;
        rekapAbsen.sort((a, b) => b.namaPegawai - a.namaPegawai)
        console.log(rekapAbsen)
        dispatch({
          type: LAPORAN_ABSENSI_SUCCESS,
          payload: rekapAbsen,
        })
      } catch (error) {
        console.log("error")
        console.log(error)
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LAPORAN_ABSENSI_FAIL,
          payload: message,
        })
      }
  }