import axios from "axios";
import {
    LIST_ABSENSI_PEGAWAI_REQUEST,
    LIST_ABSENSI_PEGAWAI_SUCCESS,
    LIST_ABSENSI_PEGAWAI_FAIL,
    LIST_ABSENSI_PEGAWAI_RESET,
  } from "../constants/absensiConstants";
import {
    URL_API,
    API_LIST_ABSENSI_PEGAWAI
} from "../constants/apiConstants";
import {
    stringArrayId
  } from "./genneralAction";

  export const getListAbseniPegawai = ( tanggalKerja, selectUnitInduk, selectUnitKerja) => async (dispatch, getState) =>{
    try {
      
      dispatch({
        type: LIST_ABSENSI_PEGAWAI_REQUEST,
      })
      
     let unitInduk=stringArrayId(selectUnitInduk);
     let unitKerja=stringArrayId(selectUnitKerja);
      
        const {
          userLogin: { token },
        } = getState()
        let config
        if(unitInduk != null && unitKerja != null){
           config = {
            headers: {
              Authorization: `${token}`,
            },
            params:{
              tanggal:`${tanggalKerja}`,
              unitKerjaId:`${unitKerja}`,
              },
          }
        }else if(unitInduk != null && unitKerja == null){
           config = {
            headers: {
              Authorization: `${token}`,
            },
            params:{
              tanggal:`${tanggalKerja}`,
              unitIndukId:`${unitInduk}`,
              },
          }
        }else{
           config = {
            headers: {
              Authorization: `${token}`,
            },
            params:{
              tanggal:`${tanggalKerja}`
              },
          }
        }
        console.log("absensiPegawai2")
        const { data } = await axios.get(URL_API+API_LIST_ABSENSI_PEGAWAI, config)
        dispatch({
          type: LIST_ABSENSI_PEGAWAI_SUCCESS,
          payload: data,
        });
        console.log("absensiPegawai", data)
      } catch (error) {
        console.log("error");
        console.log(error);
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LIST_ABSENSI_PEGAWAI_FAIL,
          payload: message,
        })
      }
  }