import axios from "axios";
import {
    PENG_LIST_ARTIKEL_REQUEST,
    PENG_LIST_ARTIKEL_SUCCESS,
    PENG_LIST_ARTIKEL_FAIL,
    PENG_CREATE_ARTIKEL_REQUEST,
    PENG_CREATE_ARTIKEL_SUCCESS,
    PENG_CREATE_ARTIKEL_FAIL,
    PENG_UPDATE_ARTIKEL_REQUEST,
    PENG_UPDATE_ARTIKEL_SUCCESS,
    PENG_UPDATE_ARTIKEL_FAIL,
    PENG_DELETE_ARTIKEL_REQUEST,
    PENG_DELETE_ARTIKEL_SUCCESS,
    PENG_DELETE_ARTIKEL_FAIL,
    PENG_DELETE_ARTIKEL_RESET,
    PENG_PUSH_NOTIFICATION_ARTIKEL_REQUEST,
    PENG_PUSH_NOTIFICATION_ARTIKEL_SUCCESS,
    PENG_PUSH_NOTIFICATION_ARTIKEL_FAIL
  } from "../constants/artikelConstants";

  import {
    URL_API,
    API_LIST_ARTIKEL,
    API_LIST_ARTIKEL_FILTER_UNIT_INDUK,
    API_LIST_ARTIKEL_FILTER_UNIT_KERJA,
    API_ADD_ARTIKEL,
    API_UPDATE_ARTIKEL,
    API_PUSH_NOTIFICATION,
    API_LIST_FCM_PHONE,
    API_DELETE_ARTIKEL
  } from "../constants/apiConstants";
import { FCM_SERVER_KEY } from "../constants/firebaseConstants";
  const moment = require('moment');


// List Artikel
export const getListArtikel = () => async (dispatch, getState) =>{
  // console.log("unit Induk");
  try {
      dispatch({
        type: PENG_LIST_ARTIKEL_REQUEST,
      })
      
      const {
        userLogin: { token },
      } = getState()
      
      const config = {
        headers: {
          Authorization: `${token}`,
        },
      }
      const { data: {newsList} } = await axios.get(URL_API+API_LIST_ARTIKEL, config)
      ;
      newsList.sort((a, b) => new Date(b.createdDate) - new Date(a.createdDate))
      // console.log(newsList.sort((a, b) => new Date(b.createdDate) - new Date(a.createdDate)));
      dispatch({
        type: PENG_LIST_ARTIKEL_SUCCESS,
        payload: newsList,
      })
    } catch (error) {
      // console.log("error");
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      if (message === 'Not authorized, token failed') {
      }
      dispatch({
        type: PENG_LIST_ARTIKEL_FAIL,
        payload: message,
      })
    }
}
// Add Artikel
export const createArtikel = (dataArtikel, tujuanArtikel, userData) => async (dispatch, getState) => {
//  console.log(dataArtikel)
//  console.log(tujuanArtikel)
//  console.log(userData.profileUser.nama)
 
  try {
      dispatch({
          type: PENG_CREATE_ARTIKEL_REQUEST,
      })

      const {
          userLogin: { token },
      } = getState()

      const config = {
          headers: {
          "Content-Type": "application/json",
          Authorization: `${token}`,
          },
      };
      // console.log("step1")
      let value;
      switch (tujuanArtikel) {
        case "semuaUnit":
          value={
              judul:dataArtikel.judul,
              konten:dataArtikel.konten,
              kontenSingkat:dataArtikel.kontenSingkat,
              forAll:true,
              publishedBy:userData.profileUser.nama,
          }
          break;
        case "khususUnitInduk":
          value={
            judul:dataArtikel.judul,
            konten:dataArtikel.konten,
            kontenSingkat:dataArtikel.kontenSingkat,
            forAll:false,
            publishedBy:userData.profileUser.nama,
            toUnitIndukId:dataArtikel.unitIndukId,
          }
          break;
        case "khususUnitKerja":
          value={
            judul:dataArtikel.judul,
            konten:dataArtikel.konten,
            kontenSingkat:dataArtikel.kontenSingkat,
            forAll:false,
            publishedBy:userData.profileUser.nama,
            toUnitKerjaId:dataArtikel.unitKerjaId,
          }
          break;
        default:
          value={}
          break;
      }
      // console.log("step2")
      // let _tanggalLahir = moment(dataArtikel.tanggalLahir);
      // console.log(value)
      const { data : {data} } = await axios.post(URL_API+API_ADD_ARTIKEL, value, config)
      dispatch({
          type: PENG_CREATE_ARTIKEL_SUCCESS,
          // payload: data,
      })
      dispatch(getListArtikel());
      
  } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      if (message === 'Not authorized, token failed') {
        // dispatch(logout())
        // console.log("token exp");
      }
      dispatch({
        type: PENG_CREATE_ARTIKEL_FAIL,
        payload: message,
      })
  }
};

// Edit Artikel
export const updateArtikel = (dataArtikel, tujuanArtikel, userData) => async (dispatch, getState) => {
 console.log(dataArtikel)
//  console.log(tujuanArtikel)
//  console.log(userData.profileUser.nama)
 
  try {
    // console.log("Step1")
      dispatch({
          type: PENG_UPDATE_ARTIKEL_REQUEST,
      })

      const {
          userLogin: { token },
      } = getState()

      const config = {
          headers: {
          "Content-Type": "application/json",
          Authorization: `${token}`,
          },
      };
      let value={};
      // console.log("Step2")
      switch (tujuanArtikel) {
        case "semuaUnit":
          value={
              id:dataArtikel.id,
              judul:dataArtikel.judul,
              konten:dataArtikel.konten,
              kontenSingkat:dataArtikel.kontenSingkat,
              forAll:true,
              modifiedBy:userData.profileUser.nama,
              toUnitIndukId:null,
              toUnitIndukId:null,
              createdDate:dataArtikel.createdDate,
          }
          break;
        case "khususUnitInduk":
          value={
            id:dataArtikel.id,
            judul:dataArtikel.judul,
            konten:dataArtikel.konten,
            kontenSingkat:dataArtikel.kontenSingkat,
            forAll:false,
            modifiedBy:userData.profileUser.nama,
            toUnitIndukId:dataArtikel.unitIndukId,
            toUnitKerjaId:null,
            createdDate:dataArtikel.createdDate,
          }
          break;
        case "khususUnitKerja":
          value={
            id:dataArtikel.id,
            judul:dataArtikel.judul,
            konten:dataArtikel.konten,
            kontenSingkat:dataArtikel.kontenSingkat,
            forAll:false,
            modifiedBy:userData.profileUser.nama,
            toUnitIndukId:null,
            toUnitKerjaId:dataArtikel.unitKerjaId,
            createdDate:dataArtikel.createdDate,
          }
          break;
        default:
          value={}
          break;
      }
      console.log(value)
      // console.log("Step3 fix")
      // let _tanggalLahir = moment(dataArtikel.tanggalLahir);
      const { data : {data} } = await axios.put(URL_API+API_UPDATE_ARTIKEL, value, config)
      dispatch({
          type: PENG_UPDATE_ARTIKEL_SUCCESS,
          payload: data,
      })
      dispatch(getListArtikel());
    
  } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      if (message === 'Not authorized, token failed') {
        // dispatch(logout())
        console.log("token exp");
      }
      dispatch({
        type: PENG_UPDATE_ARTIKEL_FAIL,
        payload: message,
      })
  }
};

// Delete Artikel
export const deleteArtikel = (dataArtikel) => async (dispatch, getState) => {
  
   try {
       dispatch({
           type: PENG_DELETE_ARTIKEL_REQUEST,
       })
 
       const {
           userLogin: { token },
       } = getState()
 
       const config = {
           headers: {
           "Content-Type": "application/json",
           Authorization: `${token}`,
           },params:{
            id:`${dataArtikel.id}`,
            },
       };

       const { data : {data} } = await axios.delete(URL_API+API_DELETE_ARTIKEL, config)
       dispatch({
           type: PENG_DELETE_ARTIKEL_SUCCESS,
           payload: data,
       })
       dispatch(getListArtikel());
     
   } catch (error) {
       const message =
         error.response && error.response.data.message
           ? error.response.data.message
           : error.message
       if (message === 'Not authorized, token failed') {
         // dispatch(logout())
         console.log("token exp");
       }
       dispatch({
         type: PENG_DELETE_ARTIKEL_FAIL,
         payload: message,
       })
   }
 };

export const getFCMphone = async (token, tujuanArtikel, dataArtikel) => {
  let parasmGet;
  switch (tujuanArtikel) {
    case "semuaUnit":
      parasmGet={ forAll:true };
      break;
    case "khususUnitInduk":
      parasmGet={
        unitIndukId:dataArtikel.unitIndukId
      }
      break;
    case "khususUnitKerja":
      parasmGet={
        unitKerjaId:dataArtikel.unitKerjaId
      }
      break;
    default:
      parasmGet={}
      break;
  }
  const config = {
    headers: {
      Authorization: `${token}`,
    },
    params:parasmGet,
  }
  console.log(config)
  const { data: {fcmList} } = await axios.get(URL_API+API_LIST_FCM_PHONE, config);
  return fcmList;
}

export const notificationArtikel = (dataArtikel, tujuanArtikel, userData) => async (dispatch, getState) => {
    try {
        dispatch({
            type: PENG_PUSH_NOTIFICATION_ARTIKEL_REQUEST,
        })
        

        const {
            userLogin: { token },
        } = getState()

        let fcmList = await getFCMphone(token, tujuanArtikel, dataArtikel);
        console.log(fcmList);

        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `key=${FCM_SERVER_KEY}`,
            },
        };
        console.log("step1")
        console.log(dataArtikel)
        
        let value;
        // value={
        //   id:dataArtikel.id
        // }
        value=dataArtikel.id
        // switch (tujuanArtikel) {
        //   case "semuaUnit":
        //     value={
        //         judul:dataArtikel.judul,
        //         konten:dataArtikel.konten,
        //         kontenSingkat:dataArtikel.kontenSingkat,
        //         forAll:true,
        //         publishedBy:userData.profileUser.nama,
        //     }
        //     break;
        //   case "khususUnitInduk":
        //     value={
        //       judul:dataArtikel.judul,
        //       konten:dataArtikel.konten,
        //       kontenSingkat:dataArtikel.kontenSingkat,
        //       forAll:false,
        //       publishedBy:userData.profileUser.nama,
        //       toUnitIndukId:dataArtikel.unitIndukId,
        //     }
        //     break;
        //   case "khususUnitKerja":
        //     value={
        //       judul:dataArtikel.judul,
        //       konten:dataArtikel.konten,
        //       kontenSingkat:dataArtikel.kontenSingkat,
        //       forAll:false,
        //       publishedBy:userData.profileUser.nama,
        //       toUnitKerjaId:dataArtikel.unitKerjaId,
        //     }
        //     break;
        //   default:
        //     value={}
        //     break;
        // }
        const regex = /(<([^>]+)>)/ig;
        const kontenSingkat = dataArtikel.kontenSingkat.replace(regex, '');
        console.log("step2")
        let body={
            notification:{
              title:dataArtikel.judul,
              body:kontenSingkat
            } ,
            data:{id:dataArtikel.id},
            registration_ids : fcmList
        }
        // let _tanggalLahir = moment(dataArtikel.tanggalLahir);
        console.log(body)
        const data= await axios.post(API_PUSH_NOTIFICATION, body, config)
        dispatch({
            type: PENG_PUSH_NOTIFICATION_ARTIKEL_SUCCESS,
            // payload: data,
        })
        console.log(data)
        // dispatch(getListArtikel());
        
    } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
          // dispatch(logout())
          // console.log("token exp");
        }
        dispatch({
          type: PENG_PUSH_NOTIFICATION_ARTIKEL_FAIL,
          payload: message,
        })
    }
  };