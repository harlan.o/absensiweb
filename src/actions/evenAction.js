import axios from "axios";
import {
    PENG_LIST_EVEN_REQUEST,
    PENG_LIST_EVEN_SUCCESS,
    PENG_LIST_EVEN_FAIL,
    PENG_LIST_EVEN_RESET,
    PENG_CREATE_EVEN_REQUEST,
    PENG_CREATE_EVEN_SUCCESS,
    PENG_CREATE_EVEN_FAIL,
    PENG_CREATE_EVEN_RESET,
    PENG_UPDATE_EVEN_REQUEST,
    PENG_UPDATE_EVEN_SUCCESS,
    PENG_UPDATE_EVEN_FAIL,
    PENG_UPDATE_EVEN_RESET,
    PENG_DELETE_EVEN_REQUEST,
    PENG_DELETE_EVEN_SUCCESS,
    PENG_DELETE_EVEN_FAIL,
    PENG_DELETE_EVEN_RESET,
    PENG_PUSH_NOTIFICATION_EVEN_REQUEST,
    PENG_PUSH_NOTIFICATION_EVEN_SUCCESS,
    PENG_PUSH_NOTIFICATION_EVEN_FAIL,
    PENG_PUSH_NOTIFICATION_EVEN_RESET
  } from "../constants/evenConstants";

  import {
    URL_API,
    API_LIST_EVEN,
    API_ADD_EVEN,
    API_PUSH_NOTIFICATION,
  } from "../constants/apiConstants";
  import { FCM_SERVER_KEY } from "../constants/firebaseConstants";
import { clockConversi, paginate, yearWithMonthOneDigit } from "./genneralAction";

const moment = require('moment');

  // List Even
export const getListEven = (_monthYear, unitInduk) => async (dispatch, getState) =>{
    try {
        dispatch({
          type: PENG_LIST_EVEN_REQUEST,
        })
        
        let monthYear=yearWithMonthOneDigit(_monthYear);
        let unitIndukLink="";
        if (unitInduk != null){
          unitIndukLink=unitInduk;
        }else{
          unitIndukLink="-1";
        }

        const {
          userLogin: { token },
        } = getState()
        
        const config = {
          headers: {
            Authorization: `${token}`,
          },
        }
        const { data: {eventList} } = await axios.get(URL_API+API_LIST_EVEN+unitIndukLink+"?yearMonth="+monthYear, config);
        eventList.sort((a, b) => a.mulaiEvent.localeCompare(b.mulaiEvent))
        console.log(eventList);
        dispatch({
          type: PENG_LIST_EVEN_SUCCESS,
          payload: eventList,
        })
      } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: PENG_LIST_EVEN_FAIL,
          payload: message,
        })
      }
    }
    
    
    export const createEven = (dataEven) => async (dispatch, getState) => {
      console.log(dataEven)
      let value;
      let penyelenggara;
    try{
      dispatch({
        type: PENG_CREATE_EVEN_REQUEST,
      })

          const {
              userLogin: { token },
          } = getState()
    
          const config = {
              headers: {
              "Content-Type": "application/json",
              Authorization: `${token}`,
              },
          };
          console.log("step1 y")
          if(dataEven.unitPenyelenggara == -1){
            penyelenggara=null;
          }else{
            penyelenggara=dataEven.unitPenyelenggara;
          }
          
          console.log(moment(dataEven.tanggal).format('YYYY-MM-DD'))
          let tanggalEven=moment(dataEven.tanggal).format('YYYY-MM-DD')
          console.log(tanggalEven)
          switch (dataEven.jenisEvent) {
            case "NORMAL":
              value={
                nama: dataEven.nama,
                tanggal: tanggalEven,
                jenisEvent: dataEven.jenisEvent,
                geolocation: dataEven.geolocation,
                radius: dataEven.radius,
                tempat: dataEven.tempat,
                mulaiEvent: dataEven.mulaiEvent,
                sebelumMasuk: parseInt(dataEven.sebelumMasuk),
                sesudahMasuk: parseInt(dataEven.sesudahMasuk),
                geolocationPulang: dataEven.geolocationPulang,
                radiusPulang: dataEven.radiusPulang,
                tempatPulang: dataEven.tempatPulang,
                akhirEvent: dataEven.akhirEvent,
                sesudahPulang: parseInt(dataEven.sesudahPulang),
                pegawaiIdList: dataEven.pegawaiIdList,
                unitPenyelenggara: penyelenggara,
              }
              break;
            case "SINGLE_ABSEN":
              value={
                nama: dataEven.nama,
                tanggal: tanggalEven,
                jenisEvent: dataEven.jenisEvent,
                geolocation: dataEven.geolocation,
                radius: dataEven.radius,
                tempat: dataEven.tempat,
                mulaiEvent: dataEven.mulaiEvent,
                sebelumMasuk: parseInt(dataEven.sebelumMasuk),
                sesudahMasuk: parseInt(dataEven.sesudahMasuk),
                geolocationPulang: dataEven.geolocation,
                radiusPulang: dataEven.radius,
                tempatPulang: dataEven.tempat,
                akhirEvent: dataEven.mulaiEvent,
                sesudahPulang: parseInt(dataEven.sesudahMasuk),
                pegawaiIdList: dataEven.pegawaiIdList,
                unitPenyelenggara: penyelenggara,
              }
              break;
            case "PULANG_NORMAL":
              value={
                nama: dataEven.nama,
                tanggal: tanggalEven,
                jenisEvent: dataEven.jenisEvent,
                geolocation: dataEven.geolocation,
                radius: dataEven.radius,
                tempat: dataEven.tempat,
                mulaiEvent: dataEven.mulaiEvent,
                sebelumMasuk: parseInt(dataEven.sebelumMasuk),
                sesudahMasuk: parseInt(dataEven.sesudahMasuk),
                pegawaiIdList: dataEven.pegawaiIdList,
                unitPenyelenggara: penyelenggara,
              }
              break;
            default:
              value={}
              break;
          }
          console.log(value)
          const { data : {fcmList} } = await axios.post(URL_API+API_ADD_EVEN, value, config)
          console.log(fcmList)

          let fcmListSendC= paginate(fcmList, 1000, 1);
          console.log(fcmListSendC)

          let totalFcm=fcmList.length;
          let a=0;
          let b=0;
          let akhirPerulangan=Math.ceil(totalFcm/1000) 
          console.log(akhirPerulangan)
          for(var i = 1; i <= akhirPerulangan; i++) {
            b=a+1;
            a=i*1000;
             let fcmListSend= paginate(fcmList, a, b);
             
             console.log(b);
             console.log(a);
             console.log(fcmListSend);

              sendNotifEven(fcmListSend, value)
          }
          dispatch({
              type: PENG_CREATE_EVEN_SUCCESS,
          })
          dispatch(getListEven());

    } catch (error) {
      console.log(error)
      const message =
            error.response && error.response.data.message
              ? error.response.data.message
              : error.message
          if (message === 'Not authorized, token failed') {
          }
          dispatch({
            type: PENG_CREATE_EVEN_FAIL,
            payload: message,
          })

    }
  }

  export const sendNotifEven = async (listFCM, dataEven)  => {
    try {
        
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `key=${FCM_SERVER_KEY}`,
            },
        };
        console.log("FCM req")
        console.log(listFCM)
        console.log(dataEven)
        const regex = /(<([^>]+)>)/ig;
        const _body= dataEven.tempat+" - "+dataEven.tanggal+","+dataEven.mulaiEvent;
        const bodyV = _body.replace(regex, '');
      
        let body={
            notification:{
              title:dataEven.nama,
              body:bodyV
            } ,
            registration_ids : listFCM
        }
        
        const data= await axios.post(API_PUSH_NOTIFICATION, body, config)
        console.log(data)
    } catch (error) {
        console.log(error)
    }
  };

  export const updateEven = (dataEven) => async (dispatch, getState) => {
    console.log(dataEven)
    let value;
    let penyelenggara;
  try{
    dispatch({
      type: PENG_UPDATE_EVEN_REQUEST,
    })

        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },
        };
        console.log("step1 y")
        if(dataEven.unitPenyelenggara == -1){
          penyelenggara=null;
        }else{
          penyelenggara=dataEven.unitPenyelenggara;
        }
        
        console.log(moment(dataEven.tanggal).format('YYYY-MM-DD'))
        let tanggalEven=moment(dataEven.tanggal).format('YYYY-MM-DD')
        console.log(tanggalEven)
        switch (dataEven.jenisEvent) {
          case "NORMAL":
            value={
              id: dataEven.id,
              nama: dataEven.nama,
              tanggal: tanggalEven,
              jenisEvent: dataEven.jenisEvent,
              geolocation: dataEven.geolocation,
              radius: dataEven.radius,
              tempat: dataEven.tempat,
              mulaiEvent: clockConversi(dataEven.mulaiEvent),
              sebelumMasuk: parseInt(dataEven.sebelumMasuk),
              sesudahMasuk: parseInt(dataEven.sesudahMasuk),
              geolocationPulang: dataEven.geolocationPulang,
              radiusPulang: dataEven.radiusPulang,
              tempatPulang: dataEven.tempatPulang,
              akhirEvent: clockConversi(dataEven.akhirEvent),
              sesudahPulang: parseInt(dataEven.sesudahPulang),
              pegawaiIdList: dataEven.pegawaiIdList,
              unitPenyelenggara: penyelenggara,
            }
            break;
          case "SINGLE_ABSEN":
            value={
              id: dataEven.id,
              nama: dataEven.nama,
              tanggal: tanggalEven,
              jenisEvent: dataEven.jenisEvent,
              geolocation: dataEven.geolocation,
              radius: dataEven.radius,
              tempat: dataEven.tempat,
              mulaiEvent: clockConversi(dataEven.mulaiEvent),
              sebelumMasuk: parseInt(dataEven.sebelumMasuk),
              sesudahMasuk: parseInt(dataEven.sesudahMasuk),
              geolocationPulang: dataEven.geolocation,
              radiusPulang: dataEven.radius,
              tempatPulang: dataEven.tempat,
              akhirEvent: clockConversi(dataEven.mulaiEvent),
              sesudahPulang: parseInt(dataEven.sesudahMasuk),
              pegawaiIdList: dataEven.pegawaiIdList,
              unitPenyelenggara: penyelenggara,
            }
            break;
          case "PULANG_NORMAL":
            value={
              id: dataEven.id,
              nama: dataEven.nama,
              tanggal: tanggalEven,
              jenisEvent: dataEven.jenisEvent,
              geolocation: dataEven.geolocation,
              radius: dataEven.radius,
              tempat: dataEven.tempat,
              mulaiEvent: dataEven.mulaiEvent,
              sebelumMasuk: parseInt(dataEven.sebelumMasuk),
              sesudahMasuk: parseInt(dataEven.sesudahMasuk),
              pegawaiIdList: dataEven.pegawaiIdList,
              unitPenyelenggara: penyelenggara,
            }
            break;
          default:
            value={}
            break;
        }
        console.log(value)
        const { data : {fcmList} } = await axios.put(URL_API+API_ADD_EVEN, value, config)
        console.log(fcmList)

        let fcmListSendC= paginate(fcmList, 1000, 1);
        console.log(fcmListSendC)

        let totalFcm=fcmList.length;
        let a=0;
        let b=0;
        let akhirPerulangan=Math.ceil(totalFcm/1000) 
        console.log(akhirPerulangan)
        for(var i = 1; i <= akhirPerulangan; i++) {
          b=a+1;
          a=i*1000;
           let fcmListSend= paginate(fcmList, a, b);
           
           console.log(b);
           console.log(a);
           console.log(fcmListSend);

            sendNotifEven(fcmListSend, value)
        }
        dispatch({
            type: PENG_UPDATE_EVEN_SUCCESS,
        })
        dispatch(getListEven());

  } catch (error) {
    console.log(error)
    const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: PENG_UPDATE_EVEN_FAIL,
          payload: message,
        })

  }
}