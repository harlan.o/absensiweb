import  { encrypt , decrypt } from 'react-crypt-gsm';
import { useDispatch } from 'react-redux';
import { logout } from './userActions';

const moment = require('moment');
export const paginate= (array, page_size, page_number) => {
    return array.slice((page_number - 1) * page_size, page_number * page_size);
}

export const yearWithMonthOneDigit= (yearMonth) =>{
    let [year, month]=yearMonth.split("-");
    month=parseInt(month);
    return year+"-"+month;
}

export const clockConversi = (timeState) => {
    let [hour, minute]=timeState.split(":");
    let clock = hour+":"+minute;
    return clock;
}

export const padLeadingZero= (timeState) =>{
   let [hour, minute] = timeState.split(":");
   while (hour.length < 2) hour = "0" + hour;
   let clock = hour+":"+minute;
    return clock;
}

export const padLeadingZeroNormal= (number) =>{
    while (number.length < 2) number = "0" + number;
     return number;
 }


export const monthName= (MM) =>{
    switch (MM) {
        case '01':
            return "Januari";
        case '02':
            return "Februari";
        case '03':
            return "Maret";
        case '04':
            return "April";
        case '05':
            return "Mei";
        case '06':
            return "Juni";
        case '07':
            return "Juli";
        case '08':
            return "Agustus";
        case '09':
            return "September";
        case '10':
            return "Oktober";
        case '11':
            return "November";
        case '12':
            return "Desember";    
    
        default:
            return "-";
    }
}

export const dayName= (MM) =>{
    switch (MM) {
        case 0:
            return "Minggu";
        case 1:
            return "Senin";
        case 2:
            return "Selasa";
        case 3:
            return "Rabu";
        case 4:
            return "Kamis";
        case 5:
            return "Jumat";
        case 6:
            return "Sabtu";
        
        default:
            return "-";
    }
}

export const tanggalHariIndo= (_date) => {
    let date= moment(_date)
    let day = dayName(date.day());
    let month= monthName(date.format("MM"))
    return day+", "+date.format("DD")+" "+month+" "+date.format("YYYY")
}

export const tahunBulanIndo = (_date) => {
    let date= moment(_date)
    let month= monthName(date.format("MM"))
    return month+" "+date.format("YYYY")
}

export const openMaps= (geolocation) => {
    window.open("https://www.google.com/maps/search/?api=1&query="+geolocation, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=10,left=20,width=1000,height=700");
}

export const openImage= (src) => {
    window.open(src, "_blank", "toolbar=yes,scrollbars=yes,resizable=yes,top=10,left=20,width=1000,height=700");
}

export const stringArrayId=(arr) => {
      let _arr = [];
      if(arr != null && arr.length > 0 ){
        for(var i = 0; i < arr?.length; i++)
        {
            _arr = _arr.concat(arr[i].id);
        }
        return _arr.toString();
      }else{
          return null
      }
}



// export const encry = (str) => {
//     const encryptedStr = encrypt(str);
//     const encryptedStr2 = encrypt("str");
//     console.log("encryptedStr")
//     console.log(encryptedStr)
//     console.log("encryptedStr2")
//     console.log(encryptedStr2)
    
//     // console.log(decry(JSON.stringify(encryptedStr)))
//     return JSON.stringify(encryptedStr)
// }
// export const decry =  (enc) => {
//     if(enc==null){
//         return null
//     }
//     var _enc = JSON.parse(enc)
//     console.log("decr")
//     console.log(_enc)
//     const decryptedText = decrypt(_enc);
//     console.log(decryptedText)
//     return decryptedText
// }

// export const encryHA = (arrHakAkses) => {
//         let dataWxp = arrHakAkses.join("|77|")
//         var encryptedStr = encry(dataWxp);
//         return encryptedStr
// }

// export const decryHA = (enc) => {
//     if(enc==null){
//         return null
//     }
//     console.log(enc)
//     const decryptedText = decry(enc);
//     const arrText = decryptedText.split("|77|")
//     return arrText
// }



export const encry2 = (arrHakAkses) => {
    let dataWxp = arrHakAkses.join("|77|")
    let dataWxp2 = dataWxp.replaceAll("|77|FU", "$")
    let dataWxp3 = dataWxp2.replaceAll("$00", "P")
    return dataWxp3.replaceAll("$0", "Z")
}

export const decry2 = (enc) => {
    try {
        if(enc == null){
            return null
        }
        let _enc = enc.replaceAll("Z", "$0")
        let _enc2 = _enc.replaceAll("P", "$00")
        let _enc3 = _enc2.replaceAll("$", "|77|FU")
        // console.log(_enc3)
        return _enc3.split("|77|")
    } catch (error) {
        Logout2()
        return []
    }
}

export const Logout2 = () => {
    const dispatch = useDispatch()
    dispatch(logout())
}
// export const cekHA = (str)=> async (getState) =>{
//     const {
//         userLogin: { token },
//       } = getState()
// }

export const encryRE = (string) => {
    let uid = encrypt(string)
    let tagCompres = JSON.stringify(uid.tag)
    let obj = {
        contentT:uid.content,
        tagT:tagCompres
    }
    return obj
}

export const decryRE = (_content, tagCompres) => {
    try {
        let _tag=JSON.parse(tagCompres)
        let obj = {
            content:_content,
            tag:_tag.data
        }
        let final = decrypt(obj)
        return final
    } catch (error) {
        return error
    }
}


