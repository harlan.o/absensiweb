import axios from "axios";
import {
    USER_HAK_AKSES_REQUEST,
    USER_HAK_AKSES_SUCCESS,
    USER_HAK_AKSES_FAIL,
    USER_HAK_AKSES_LOGOUT,

  } from "../constants/hakAksesConstants";
  import {
    URL_API,
    URL_API_LOGIN,
    API_GET_HAK_AKSES,
  } from "../constants/apiConstants";
import { decry2, decryRE } from "./genneralAction";
// import { decryHA } from "./genneralAction";
 
export const getHakAkses = () => async (dispatch, getState) => { 
      try{
            dispatch({
                type: USER_HAK_AKSES_REQUEST,
            })
            const {
                userLogin: { token },
            } = getState()
            const config = {
                headers: {
                  Authorization: `${token}`,
                },
            }
            const { data } = await axios.get(URL_API+API_GET_HAK_AKSES, config)
            dispatch({
                type: USER_HAK_AKSES_SUCCESS,
                payload: data.rightList,
              });

      } catch (error) {
            const message =
            error.response && error.response.data.message
            ? error.response.data.message
            : error.message
            if (message === 'Not authorized, token failed') {
            }
            dispatch({
                type: USER_HAK_AKSES_FAIL,
                payload: message,
            })
      }
}

// export const cekHakAkses = (haks, str) =>{
//     try{
//         // const {
//         //     hakAkses: { haks },
//         // } = getState()
//         // console.log("req hak akses")
//         // console.log(haks)
//         console.log(haks.includes(str))
//         if(haks != null){
//             return haks.includes(str)
//         }else{
//             return false
//         }
//     }catch{
//         console.log("error")
//         // dispatch(getHakAkses())
//         return null
//     }
// }

export const cekHakAksesClient = (str) => {

   let contentT = localStorage.getItem("haks_c")
   let tagT = localStorage.getItem("haks_t")
   let _localS = decryRE(contentT, tagT)
   //  console.log(" cek akses client ",_localS)
   let localS = decry2(_localS)
  //  console.log(" cek akses client ", localS)
   if(localS != null){
       return localS.includes(str)

   }else{
        return false
    }
}

