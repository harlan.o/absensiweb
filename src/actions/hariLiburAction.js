import axios from "axios";

import {
    LIST_HARI_LIBUR_REQUEST,
    LIST_HARI_LIBUR_SUCCESS,
    LIST_HARI_LIBUR_FAIL,
    LIST_HARI_LIBUR_RESET,
    CREATE_HARI_LIBUR_REQUEST,
    CREATE_HARI_LIBUR_SUCCESS,
    CREATE_HARI_LIBUR_FAIL,
    CREATE_HARI_LIBUR_RESET,
    DELETE_HARI_LIBUR_REQUEST,
    DELETE_HARI_LIBUR_SUCCESS,
    DELETE_HARI_LIBUR_FAIL,
    DELETE_HARI_LIBUR_RESET,
  } from "../constants/hariLiburConstants";

  import {
    URL_API,
    API_LIST_HARI_LIBUR,
    API_ADD_HARI_LIBUR,
    API_DELETE_HARI_LIBUR,
  } from "../constants/apiConstants";

  
const moment = require('moment');

export const getListHariLibur = (_year) => async (dispatch, getState) =>{
    try {
        dispatch({
            type: LIST_HARI_LIBUR_REQUEST
        })
        const {
            userLogin: { token },
        } = getState()
        const config = {
            headers: {
            Authorization: `${token}`,
            },params:{
                year:`${_year}`
              },
        }
        const { data: {daftarHariLibur} } = await axios.get(URL_API+API_LIST_HARI_LIBUR, config);
        console.log("API Hari Libur", daftarHariLibur)
        dispatch({
            type: LIST_HARI_LIBUR_SUCCESS,
            payload: daftarHariLibur,
        })
    } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LIST_HARI_LIBUR_FAIL,
          payload: message,
        })
    }
}


export const createHariLibur = (hariLibur) => async (dispatch, getState) => {
    console.log("Create Hari libur",hariLibur);
    try {
        dispatch({
            type: CREATE_HARI_LIBUR_REQUEST,
        })
  
        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },
        };
        const { data : {data} } = await axios.post(URL_API+API_ADD_HARI_LIBUR, { 
            // accountDeposit, amountDeposit, descDeposit 
            tanggal:moment(hariLibur.tanggal).format('YYYY-MM-DD'),
            keterangan:hariLibur.keterangan             
        }, config)
        dispatch({
            type: CREATE_HARI_LIBUR_SUCCESS,
            payload: data,
        })
        console.log(moment(hariLibur.tanggal).format('YYYY'));
        dispatch(getListHariLibur(moment(hariLibur.tanggal).format('YYYY')));
      
    } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
          // dispatch(logout())
          console.log("token exp");
        }
        dispatch({
          type: CREATE_HARI_LIBUR_FAIL,
          payload: message,
        })
    }
  };


  export const deleteHariLibur = (tanggal) => async (dispatch, getState) => {
    // console.log(dataPegawai);
    try {
        dispatch({
            type: DELETE_HARI_LIBUR_REQUEST,
        })
  
        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },params:{
                date:`${tanggal}`
            },
        };
        const { data : {data} } = await axios.delete(URL_API+API_DELETE_HARI_LIBUR, config)
        dispatch({
            type: DELETE_HARI_LIBUR_SUCCESS,
            payload: data,
        })
        dispatch(getListHariLibur(moment(tanggal).format('YYYY')));
      
    } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
          // dispatch(logout())
          console.log("token exp");
        }
        dispatch({
          type: DELETE_HARI_LIBUR_FAIL,
          payload: message,
        })
    }
  };