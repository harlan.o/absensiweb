import axios from "axios";
import {
    LIST_JADWAL_KERJA_REQUEST,
    LIST_JADWAL_KERJA_SUCCESS,
    LIST_JADWAL_KERJA_FAIL,
    LIST_JADWAL_KERJA_RESET,
    CREATE_JADWAL_KERJA_REQUEST,
    CREATE_JADWAL_KERJA_SUCCESS,
    CREATE_JADWAL_KERJA_FAIL,
    CREATE_JADWAL_KERJA_RESET,
    UPDATE_JADWAL_KERJA_REQUEST,
    UPDATE_JADWAL_KERJA_SUCCESS,
    UPDATE_JADWAL_KERJA_FAIL,
    UPDATE_JADWAL_KERJA_RESET,
    DELETE_JADWAL_KERJA_REQUEST,
    DELETE_JADWAL_KERJA_SUCCESS,
    DELETE_JADWAL_KERJA_FAIL,
    DELETE_JADWAL_KERJA_RESET,
  } from "../constants/jadwalKerjaConstants";

  import {
    URL_API,
    API_LIST_JADWAL_KERJA,
    API_ADD_JADWAL_KERJA,
    API_UPDATE_JADWAL_KERJA,
    API_DELETE_JADWAL_KERJA,
  } from "../constants/apiConstants";


  // function umum 
 export const clockConversi = (timeState) => {
    let [hour, minute]=timeState.split(":");
    let clock = hour+":"+minute;
    return clock;
  }


  // List Jadwal Kerja
export const getListJadwalKerja = () => async (dispatch, getState) =>{
    try {
        dispatch({
          type: LIST_JADWAL_KERJA_REQUEST,
        })
        
        const {
          userLogin: { token },
        } = getState()
        
        const config = {
          headers: {
            Authorization: `${token}`,
          },
        }
        const { data: {daftarJadwal} } = await axios.get(URL_API+API_LIST_JADWAL_KERJA, config)
        ;
        daftarJadwal.sort((a, b) => b.checkIn - a.checkIn)
        // console.log(daftarJadwal)
        dispatch({
          type: LIST_JADWAL_KERJA_SUCCESS,
          payload: daftarJadwal,
        })
      } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LIST_JADWAL_KERJA_FAIL,
          payload: message,
        })
      }
  }

//   Add Jadwal Kerja
export const createJadwalKerja = (dataJadwalKerja) => async (dispatch, getState) => {
    // console.log(dataPegawai);
    try {
        dispatch({
            type: CREATE_JADWAL_KERJA_REQUEST,
        })
  
        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },
        };
        const { data : {data} } = await axios.post(URL_API+API_ADD_JADWAL_KERJA, { 
            // accountDeposit, amountDeposit, descDeposit 
            masuk:dataJadwalKerja.checkIn,
            nama:dataJadwalKerja.nama,
            pulang:dataJadwalKerja.checkOut,
            sebelumMasuk:dataJadwalKerja.sebelumMasuk,
            sesudahMasuk:dataJadwalKerja.sesudahMasuk,
            sesudahPulang:dataJadwalKerja.sesudahPulang  
        }, config)
        dispatch({
            type: CREATE_JADWAL_KERJA_SUCCESS,
            payload: data,
        })
        dispatch(getListJadwalKerja());
      
    } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
          // dispatch(logout())
          console.log("token exp");
        }
        dispatch({
          type: CREATE_JADWAL_KERJA_FAIL,
          payload: message,
        })
    }
  };

//   Edit Jadwal Kerja
export const updateJadwalKerja = (dataJadwalKerja) => async (dispatch, getState) => {
    // console.log(dataPegawai);
    try {
        dispatch({
            type: UPDATE_JADWAL_KERJA_REQUEST,
        })
  
        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },
        };
        const { data : {data} } = await axios.put(URL_API+API_UPDATE_JADWAL_KERJA, { 
            // accountDeposit, amountDeposit, descDeposit
            id:dataJadwalKerja.id, 
            masuk:clockConversi(dataJadwalKerja.checkIn),
            nama:dataJadwalKerja.nama,
            pulang:clockConversi(dataJadwalKerja.checkOut),
            sebelumMasuk:dataJadwalKerja.sebelumMasuk,
            sesudahMasuk:dataJadwalKerja.sesudahMasuk,
            sesudahPulang:dataJadwalKerja.sesudahPulang  
        }, config)
        // console.log(data);
        dispatch({
            type: UPDATE_JADWAL_KERJA_SUCCESS,
            payload: data,
        })
        dispatch(getListJadwalKerja());
      
    } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
          // dispatch(logout())
          console.log("token exp");
        }
        dispatch({
          type: UPDATE_JADWAL_KERJA_FAIL,
          payload: message,
        })
    }
  };

//   Delete jadwal Kerja
export const deleteJadwalKerja = (dataJadwalKerja) => async (dispatch, getState) => {
    // console.log(dataPegawai);
    try {
        dispatch({
            type: DELETE_JADWAL_KERJA_REQUEST,
        })
  
        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            }
        };
        const { data : {data} } = await axios.delete(URL_API+API_DELETE_JADWAL_KERJA+'/'+dataJadwalKerja.id, config)
        dispatch({
            type: DELETE_JADWAL_KERJA_SUCCESS,
            payload: data,
        })
        dispatch(getListJadwalKerja());
      
    } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
          // dispatch(logout())
          console.log("token exp");
        }
        dispatch({
          type: DELETE_JADWAL_KERJA_FAIL,
          payload: message,
        })
    }
  };