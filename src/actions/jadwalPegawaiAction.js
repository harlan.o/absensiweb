import axios from "axios";
import {
    LIST_JADWAL_PEGAWAI_REQUEST,
    LIST_JADWAL_PEGAWAI_SUCCESS,
    LIST_JADWAL_PEGAWAI_FAIL,
    LIST_JADWAL_PEGAWAI_RESET,
    CREATE_JADWAL_PEGAWAI_REQUEST,
    CREATE_JADWAL_PEGAWAI_SUCCESS,
    CREATE_JADWAL_PEGAWAI_FAIL,
    CREATE_JADWAL_PEGAWAI_RESET,
    UPDATE_JADWAL_PEGAWAI_REQUEST,
    UPDATE_JADWAL_PEGAWAI_SUCCESS,
    UPDATE_JADWAL_PEGAWAI_FAIL,
    UPDATE_JADWAL_PEGAWAI_RESET,
    DELETE_JADWAL_PEGAWAI_REQUEST,
    DELETE_JADWAL_PEGAWAI_SUCCESS,
    DELETE_JADWAL_PEGAWAI_FAIL,
    DELETE_JADWAL_PEGAWAI_RESET,
  } from "../constants/jadwalPegawaiConstants";

  import {
    URL_API,
    API_LIST_JADWAL_PEGAWAI,
    API_ADD_JADWAL_PEGAWAI,
    API_UPDATE_JADWAL_PEGAWAI,
    API_DELETE_JADWAL_PEGAWAI,
    API_WFH_JADWAL_PEGAWAI,
  } from "../constants/apiConstants";
import { stringArrayId, yearWithMonthOneDigit } from "./genneralAction";


  // function umum 
 export const clockConversi = (timeState) => {
    let [hour, minute]=timeState.split(":");
    let clock = hour+":"+minute;
    return clock;
  }


  // List Jadwal Pegawai
export const getListJadwalPegawai = (_monthYear, unitInduk, unitKerja) => async (dispatch, getState) =>{
    try {
        dispatch({
          type: LIST_JADWAL_PEGAWAI_REQUEST,
        })
        let monthYear=yearWithMonthOneDigit(_monthYear);

        const {
          userLogin: { token },
        } = getState()
        const unitIndukId=stringArrayId(unitInduk);
        
        let config = {
          headers: {
            Authorization: `${token}`,
          },params:{
            unitIndukId:`${unitIndukId}`,
            yearMonth:`${monthYear}`,
          },
        }

        if(unitKerja != null && unitKerja.length > 0){
            const unitKerjaId=stringArrayId(unitKerja);
            config = {
                headers: {
                  Authorization: `${token}`,
                }, params:{
                  unitKerjaId:`${unitKerjaId}`,
                  yearMonth:`${monthYear}`,
                },
            }   
        }

        const { data: {jadwalPegawaiList} } = await axios.get(URL_API+API_LIST_JADWAL_PEGAWAI, config)
        ;
        jadwalPegawaiList.sort((a, b) => b.namaPegawai - a.namaPegawai)
        console.log(jadwalPegawaiList)
        dispatch({
          type: LIST_JADWAL_PEGAWAI_SUCCESS,
          payload: jadwalPegawaiList,
        })
      } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LIST_JADWAL_PEGAWAI_FAIL,
          payload: message,
        })
      }
  }

//   Add Jadwal Pegawai
export const createJadwalPegawai = (dataJadwalPegawai) => async (dispatch, getState) => {
    // console.log(dataPegawai);
    try {
        dispatch({
            type: CREATE_JADWAL_PEGAWAI_REQUEST,
        })
  
        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },
        };
        const { status: status, data : {data} } = await axios.put(URL_API+API_ADD_JADWAL_PEGAWAI, { 
          pegawaiIdList:dataJadwalPegawai.idPegawai,
          jadwalList:dataJadwalPegawai.jadwalPegawai,
        }, config)

        if(dataJadwalPegawai.wfh.length > 0){
          console.log("wfh con", config)
          const { status: status3, data : {data3} } = await axios.put(URL_API+API_WFH_JADWAL_PEGAWAI, dataJadwalPegawai.wfh, config)
          console.log("wfh", status3, data3)
        }

        if(status != 200){
          dispatch({
            type: CREATE_JADWAL_PEGAWAI_FAIL,
            payload: "Gagal, Silakan Coba Lagi",
          })          
        }else{
          dispatch({
              type: CREATE_JADWAL_PEGAWAI_SUCCESS,
              payload: data,
          })
          // dispatch(getListJadwalPegawai());
        }
      
    } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
          // dispatch(logout())
          console.log("token exp");
        }
        dispatch({
          type: CREATE_JADWAL_PEGAWAI_FAIL,
          payload: message,
        })
    }
  };

//   Edit Jadwal Pegawai
export const updateJadwalPegawai = (dataJadwalPegawai) => async (dispatch, getState) => {
    // console.log(dataPegawai);
    try {
        dispatch({
            type: UPDATE_JADWAL_PEGAWAI_REQUEST,
        })
  
        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },
        };
        
        if(dataJadwalPegawai.tanggalHapus.length > 0){
          console.log("delete con", config)
          const { status: status2, data : {data2} } = await axios.delete(URL_API+API_DELETE_JADWAL_PEGAWAI, 
            {headers: {
              "Content-Type": "application/json",
              Authorization: `${token}`,
              },
            data:{ 
              pegawaiId:dataJadwalPegawai.idPegawai,
              tanggal:dataJadwalPegawai.tanggalHapus,
              }})
          console.log("delete", status2, data2)
        }

          const { status: status, data : {data} } = await axios.put(URL_API+API_UPDATE_JADWAL_PEGAWAI, { 
            pegawaiIdList:[dataJadwalPegawai.idPegawai],
              jadwalList:dataJadwalPegawai.jadwalPegawai,
          }, config)

          if(dataJadwalPegawai.wfh.length > 0){
            console.log("wfh con", config)
            const { status: status3, data : {data3} } = await axios.put(URL_API+API_WFH_JADWAL_PEGAWAI, dataJadwalPegawai.wfh, config)
            console.log("wfh", status3, data3)
          }


      if(status != 200){
        dispatch({
          type: UPDATE_JADWAL_PEGAWAI_FAIL,
          payload: "Gagal, Silakan Coba Lagi",
        })          
      }else{
        dispatch({
            type: UPDATE_JADWAL_PEGAWAI_SUCCESS,
            payload: data,
        })
        // dispatch(getListJadwalPegawai());
      }      
    } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
          // dispatch(logout())
          console.log("token exp");
        }
        dispatch({
          type: UPDATE_JADWAL_PEGAWAI_FAIL,
          payload: message,
        })
    }
  };

//   Delete jadwal Pegawai
// export const deleteJadwalPegawai = (dataJadwalPegawai) => async (dispatch, getState) => {
//     // console.log(dataPegawai);
//     try {
//         dispatch({
//             type: DELETE_JADWAL_PEGAWAI_REQUEST,
//         })
  
//         const {
//             userLogin: { token },
//         } = getState()
  
//         const config = {
//             headers: {
//             "Content-Type": "application/json",
//             Authorization: `${token}`,
//             }
//         };
//         const { data : {data} } = await axios.delete(URL_API+API_DELETE_JADWAL_PEGAWAI+'/'+dataJadwalPegawai.id, config)
//         dispatch({
//             type: DELETE_JADWAL_PEGAWAI_SUCCESS,
//             payload: data,
//         })
//         dispatch(getListJadwalPegawai());
      
//     } catch (error) {
//         const message =
//           error.response && error.response.data.message
//             ? error.response.data.message
//             : error.message
//         if (message === 'Not authorized, token failed') {
//           // dispatch(logout())
//           console.log("token exp");
//         }
//         dispatch({
//           type: DELETE_JADWAL_PEGAWAI_FAIL,
//           payload: message,
//         })
//     }
//   };