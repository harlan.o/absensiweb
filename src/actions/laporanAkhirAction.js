import axios from "axios";
import {
    LAPORAN_AKHIR_UMUM_REQUEST,
    LAPORAN_AKHIR_UMUM_SUCCESS,
    LAPORAN_AKHIR_UMUM_FAIL,
    LAPORAN_AKHIR_UMUM_RESET,
    LAPORAN_AKHIR_KHUSUS_REQUEST,
    LAPORAN_AKHIR_KHUSUS_SUCCESS,
    LAPORAN_AKHIR_KHUSUS_FAIL,
    LAPORAN_AKHIR_KHUSUS_RESET,
  } from "../constants/laporanAkhirConstants";
  import {
    URL_API,
    API_LIST_LAPORAN_AKHIR_KHUSUS,
    API_LIST_LAPORAN_AKHIR_UMUM,
  } from "../constants/apiConstants";
import { decH, stringArrayId } from "./genneralAction";


export const getListLaporanAkhirUmum = (_monthYear, unitInduk, unitKerja) => async (dispatch, getState) =>{
    try {
        dispatch({
          type: LAPORAN_AKHIR_UMUM_REQUEST,
        })
        let monthYear=_monthYear;

        const {
          userLogin: { token },
        } = getState()
       

        let config = {
          headers: {
            Authorization: `${token}`,
          },params:{
            // unitKerjaId:`1115,809`,
            yearMonth:`${monthYear}`,
          },
        }

        if(unitInduk != null && unitInduk.length > 0){
            const unitIndukId=stringArrayId(unitInduk);
            config = {
                headers: {
                  Authorization: `${token}`,
                }, params:{
                  unitIndukId:`${unitIndukId}`,
                  yearMonth:`${monthYear}`,
                },
            }     
        }

        if(unitKerja != null && unitKerja.length > 0){
            const unitKerjaId=stringArrayId(unitKerja);
            config = {
                headers: {
                  Authorization: `${token}`,
                }, params:{
                  unitKerjaId:`${unitKerjaId}`,
                  yearMonth:`${monthYear}`,
                },
            }   
        }


        const { data: {rekapAbsen} } = await axios.get(URL_API+API_LIST_LAPORAN_AKHIR_UMUM, config)
        ;
        rekapAbsen.sort((a, b) => b.namaPegawai - a.namaPegawai)
        console.log(rekapAbsen)
        dispatch({
          type: LAPORAN_AKHIR_UMUM_SUCCESS,
          payload: rekapAbsen,
        })
      } catch (error) {
        console.log("error")
        console.log(error)
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LAPORAN_AKHIR_UMUM_FAIL,
          payload: message,
        })
      }
  }

  export const getListLaporanAkhirKhusus = (_monthYear, unitInduk, unitKerja) => async (dispatch, getState) =>{
    try {
        dispatch({
          type: LAPORAN_AKHIR_KHUSUS_REQUEST,
        })
        let monthYear=_monthYear;

        const {
          userLogin: { token },
        } = getState()
        const unitIndukId=stringArrayId(unitInduk);
        
        let config = {
          headers: {
            Authorization: `${token}`,
          },params:{
            unitIndukId:`${unitIndukId}`,
            yearMonth:`${monthYear}`,
          },
        }

        if(unitKerja != null && unitKerja.length > 0){
            const unitKerjaId=stringArrayId(unitKerja);
            config = {
                headers: {
                  Authorization: `${token}`,
                }, params:{
                  unitKerjaId:`${unitKerjaId}`,
                  yearMonth:`${monthYear}`,
                },
            }   
        }
        const { data: {rekapAbsen} } = await axios.get(URL_API+API_LIST_LAPORAN_AKHIR_KHUSUS, config)
        ;
        rekapAbsen.sort((a, b) => b.namaPegawai - a.namaPegawai)
        console.log(rekapAbsen)
        dispatch({
          type: LAPORAN_AKHIR_KHUSUS_SUCCESS,
          payload: rekapAbsen,
        })
      } catch (error) {
        console.log("error")
        console.log(error)
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LAPORAN_AKHIR_KHUSUS_FAIL,
          payload: message,
        })
      }
  }

