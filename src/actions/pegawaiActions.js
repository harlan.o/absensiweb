import axios from "axios";
import {
    PENG_LIST_PEGAWAI_REQUEST,
    PENG_LIST_PEGAWAI_SUCCESS,
    PENG_LIST_PEGAWAI_FAIL,
    SIMPLE_LIST_PEGAWAI_REQUEST,
    SIMPLE_LIST_PEGAWAI_SUCCESS,
    SIMPLE_LIST_PEGAWAI_FAIL,
    PENG_CREATE_PEGAWAI_REQUEST,
    PENG_CREATE_PEGAWAI_SUCCESS,
    PENG_CREATE_PEGAWAI_FAIL,
    PENG_UPDATE_PEGAWAI_REQUEST,
    PENG_UPDATE_PEGAWAI_SUCCESS,
    PENG_UPDATE_PEGAWAI_FAIL,
    PENG_RESET_PASSWORD_PEGAWAI_REQUEST,
    PENG_RESET_PASSWORD_PEGAWAI_SUCCESS,
    PENG_RESET_PASSWORD_PEGAWAI_FAIL,
    PENG_RESET_PASSWORD_PEGAWAI_RESET,
    PENG_RESET_DEVICE_ID_PEGAWAI_REQUEST,
    PENG_RESET_DEVICE_ID_PEGAWAI_SUCCESS,
    PENG_RESET_DEVICE_ID_PEGAWAI_FAIL,
    PENG_RESET_DEVICE_ID_PEGAWAI_RESET,
  } from "../constants/pegawaiConstants";
  import {
    URL_API,
    API_LIST_PEGAWAI_BY_UNIT_KERJA,
    API_SIMPLE_LIST_PEGAWAI,
    API_LIST_PEGAWAI,
    API_ADD_PEGAWAI,
    API_UPDATE_PEGAWAI,
    API_RESET_DEVICE_ID,
    API_RESET_PASSWORD,
    URL_API_LOGIN,
  } from "../constants/apiConstants";
import { cekHakAksesClient } from "./hakAksesActions";
import { REGISTER_FU001 } from "../constants/hakAksesConstants";
import { uIIDP } from "./userActions";

const moment = require('moment');
// list Pegawai
export const getListPegawai = () => async (dispatch, getState) =>{
  // console.log("unit Induk");
  try {
      dispatch({
        type: PENG_LIST_PEGAWAI_REQUEST,
      })
      
      const {
        userLogin: { token },
      } = getState()
      
      const config = {
        headers: {
          Authorization: `${token}`,
        },
      }
      const { data: {pegawaiList} } = await axios.get(URL_API+API_LIST_PEGAWAI, config)
      // console.log(pegawaiList);
      let pgl = pegawaiList
      if(!cekHakAksesClient(REGISTER_FU001) ){
        pgl= pegawaiList.filter(v => v.unitIndukId === uIIDP())
      }
      dispatch({
        type: PENG_LIST_PEGAWAI_SUCCESS,
        payload: pgl,
      })
    } catch (error) {
      console.log("error");
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      if (message === 'Not authorized, token failed') {
      }
      dispatch({
        type: PENG_LIST_PEGAWAI_FAIL,
        payload: message,
      })
    }
}

export const getSimpleListPegawai = (idUnitInduk=null, idUnitKerja=null) => async (dispatch, getState) =>{
  // console.log("unit Induk");
  try {
      dispatch({
        type: SIMPLE_LIST_PEGAWAI_REQUEST,
      })
      
      let linkParam="";
      if(idUnitInduk != null && idUnitInduk != undefined){
        linkParam='?unitIndukId='+idUnitInduk;
      }
      if(idUnitKerja != null && idUnitKerja != undefined){
        linkParam='?unitKerjaId='+idUnitKerja;
      }
      

      const {
        userLogin: { token },
      } = getState()
      
      const config = {
        headers: {
          Authorization: `${token}`,
        },
      }
      const { data: {pegawaiList} } = await axios.get(URL_API+API_SIMPLE_LIST_PEGAWAI+linkParam, config)
      pegawaiList.sort((a, b)=> {
        return a.nama.localeCompare(b.nama);
      });
      console.log("simple list", pegawaiList);
      dispatch({
        type: SIMPLE_LIST_PEGAWAI_SUCCESS,
        payload: pegawaiList,
      })
    } catch (error) {
      console.log("error");
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      if (message === 'Not authorized, token failed') {
      }
      dispatch({
        type: SIMPLE_LIST_PEGAWAI_FAIL,
        payload: message,
      })
    }
}


// Add Pegawai
export const createPegawai = (dataPegawai) => async (dispatch, getState) => {
  // console.log(dataPegawai);
  try {
      dispatch({
          type: PENG_CREATE_PEGAWAI_REQUEST,
      })

      const {
          userLogin: { token },
      } = getState()

      const config = {
          headers: {
          "Content-Type": "application/json",
          Authorization: `${token}`,
          },
      };
      let _tanggalLahir = moment(dataPegawai.tanggalLahir);
      const { data : {data} } = await axios.post(URL_API+API_ADD_PEGAWAI, { 

          nip:dataPegawai.nip,
          noKtp:dataPegawai.noKtp,
          nama:dataPegawai.nama,
          gelarDepan:dataPegawai.gelarDepan,
          gelarBelakang:dataPegawai.gelarBelakang,
          tempatLahir:dataPegawai.tempatLahir,
          tanggalLahir:_tanggalLahir.format('YYYY-MM-DD'),
          jenisKelamin:dataPegawai.jenisKelamin,
          jabatan:dataPegawai.jabatan,
          golongan:dataPegawai.golongan,
          unitKerjaId:dataPegawai.unitKerjaId,
          
      }, config)
      dispatch({
          type: PENG_CREATE_PEGAWAI_SUCCESS,
          payload: data,
      })
      dispatch(getListPegawai());
    
  } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      if (message === 'Not authorized, token failed') {
        // dispatch(logout())
        console.log("token exp");
      }
      dispatch({
        type: PENG_CREATE_PEGAWAI_FAIL,
        payload: message,
      })
  }
};

// Edit Pegawai
export const updatePegawai = (dataPegawai) => async (dispatch, getState) => {
  // console.log(dataPegawai);
  try {
      dispatch({
          type: PENG_UPDATE_PEGAWAI_REQUEST,
      })

      const {
          userLogin: { token },
      } = getState()

      const config = {
          headers: {
          "Content-Type": "application/json",
          Authorization: `${token}`,
          },
      };
      let _tanggalLahir = moment(dataPegawai.tanggalLahir);
      const { data : {data} } = await axios.put(URL_API+API_UPDATE_PEGAWAI, { 
          // accountDeposit, amountDeposit, descDeposit 
          id:dataPegawai.id,
          nama:dataPegawai.nama,
          gelarDepan:dataPegawai.gelarDepan,
          gelarBelakang:dataPegawai.gelarBelakang,
          jenisKelamin:dataPegawai.jenisKelamin,
          tempatLahir:dataPegawai.tempatLahir,
          tanggalLahir:_tanggalLahir.format('YYYY-MM-DD'),
          noKtp:dataPegawai.noKtp,
          jabatan:dataPegawai.jabatan,
          golongan:dataPegawai.golongan,
          unitKerjaId:dataPegawai.unitKerjaId,
      }, config)
      dispatch({
          type: PENG_UPDATE_PEGAWAI_SUCCESS,
          payload: data,
      })
      dispatch(getListPegawai());
    
  } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      if (message === 'Not authorized, token failed') {
        // dispatch(logout())
        console.log("token exp");
      }
      dispatch({
        type: PENG_UPDATE_PEGAWAI_FAIL,
        payload: message,
      })
  }
};

export const resetPasswordPegawai = (dataPegawai) => async (dispatch, getState) =>{
  try {
      dispatch({
          type: PENG_RESET_PASSWORD_PEGAWAI_REQUEST,
      })
      const {
          userLogin: { token },
        } = getState()
        
      const config = {
        headers: {
        "Content-Type": "application/json",
        Authorization: `${token}`,
        },
      };

      const { status: status, data : {data} } = await axios.put(URL_API_LOGIN+API_RESET_PASSWORD,
        { nip:dataPegawai.nip,}, config)
      
      if(status != 200){
          dispatch({
            type: PENG_RESET_PASSWORD_PEGAWAI_FAIL,
            payload: "Gagal, Silakan Coba Lagi",
          })          
      }else{
          dispatch({
              type: PENG_RESET_PASSWORD_PEGAWAI_SUCCESS,
              payload: data,
          })
      }
  } catch (error){
      const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message
      dispatch({
          type: PENG_RESET_PASSWORD_PEGAWAI_FAIL,
          payload: message,
      })
  }
}

export const resetDeviceIdPegawai = (dataPegawai) => async (dispatch, getState) =>{
  try {
      dispatch({
          type: PENG_RESET_DEVICE_ID_PEGAWAI_REQUEST,
      })
      const {
          userLogin: { token },
        } = getState()
        
      const config = {
          headers: {
          "Content-Type": "application/json",
          Authorization: `${token}`,
          },params:{
           nip:`${dataPegawai.nip}`,
           },
      };

      const { status: status, data : {data} } = await axios.put(URL_API_LOGIN+API_RESET_DEVICE_ID, {}, config)
      
      if(status != 200){
          dispatch({
            type: PENG_RESET_DEVICE_ID_PEGAWAI_FAIL,
            payload: "Gagal, Silakan Coba Lagi",
          })          
      }else{
          dispatch({
              type: PENG_RESET_DEVICE_ID_PEGAWAI_SUCCESS,
              payload: data,
          })
      }
  } catch (error){
      const message =
      error.response && error.response.data.message
        ? error.response.data.message
        : error.message
      dispatch({
          type: PENG_RESET_DEVICE_ID_PEGAWAI_FAIL,
          payload: message,
      })
  }
}

