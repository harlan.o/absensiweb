import axios from "axios";
import {
    LIST_PENGAMBILAN_IJIN_REQUEST,
    LIST_PENGAMBILAN_IJIN_SUCCESS,
    LIST_PENGAMBILAN_IJIN_FAIL,
    LIST_PENGAMBILAN_IJIN_RESET,
    CREATE_PENGAMBILAN_IJIN_REQUEST,
    CREATE_PENGAMBILAN_IJIN_SUCCESS,
    CREATE_PENGAMBILAN_IJIN_FAIL,
    CREATE_PENGAMBILAN_IJIN_RESET,
    DELETE_PENGAMBILAN_IJIN_REQUEST,
    DELETE_PENGAMBILAN_IJIN_SUCCESS,
    DELETE_PENGAMBILAN_IJIN_FAIL,
    DELETE_PENGAMBILAN_IJIN_RESET,
    LIST_JENIS_IJIN_REQUEST,
    LIST_JENIS_IJIN_SUCCESS,
    LIST_JENIS_IJIN_FAIL,
    LIST_JENIS_IJIN_RESET,
  } from "../constants/pengambilanIjinConstants";
  import {
    URL_API,
    API_LIST_PENGAMBILAN_IJIN,
    API_ADD_PENGAMBILAN_IJIN,
    API_DELETE_PENGAMBILAN_IJIN,
    API_LIST_JENIS_IJIN,
  } from "../constants/apiConstants";
import { decH, stringArrayId, yearWithMonthOneDigit } from "./genneralAction";

const moment = require('moment');

  export const getListPengambilanIjin = (_monthYear, unitInduk, unitKerja) => async (dispatch, getState) =>{ 
    try {
        dispatch({
            type: LIST_PENGAMBILAN_IJIN_REQUEST,
        })
        // let monthYear=yearWithMonthOneDigit(_monthYear);
        let monthYear=_monthYear;
        const {
            userLogin: { token },
        } = getState()
        let config = {
          headers: {
            Authorization: `${token}`,
          },params:{
            yearMonth:`${monthYear}`,
          },
       }
        if(unitInduk != null && unitInduk.length > 0){
          const unitIndukId=stringArrayId(unitInduk);
          let config = {
              headers: {
                Authorization: `${token}`,
              },params:{
                unitIndukId:`${unitIndukId}`,
                yearMonth:`${monthYear}`,
              },
          }
        }

        if(unitKerja != null && unitKerja.length > 0){
            const unitKerjaId=stringArrayId(unitKerja);
            config = {
                headers: {
                  Authorization: `${token}`,
                }, params:{
                  unitKerjaId:`${unitKerjaId}`,
                  yearMonth:`${monthYear}`,
                },
            }   
        }
        const { data: {ijinList} } = await axios.get(URL_API+API_LIST_PENGAMBILAN_IJIN, config)
        ;
        dispatch({
            type: LIST_PENGAMBILAN_IJIN_SUCCESS,
            payload: ijinList,
        })
    } catch (error){
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LIST_PENGAMBILAN_IJIN_FAIL,
          payload: message,
        })
    }
  }

  export const createPengambilanIjin = (dataIjin) => async (dispatch, getState) => { 
    try {
        dispatch({
            type: CREATE_PENGAMBILAN_IJIN_REQUEST,
        })
  
        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },
        };
        let _startDate =moment(dataIjin.tanggalAwal).format('YYYY-MM-DD')
        let _endDate =moment(dataIjin.tanggalAkhir).format('YYYY-MM-DD')
      
        const { status: status, data : {data} } = await axios.put(URL_API+API_ADD_PENGAMBILAN_IJIN, { 
            ijinId:dataIjin.idIjin,
            pegawaiId:dataIjin.idPegawai,
            startDate:_startDate,
            endDate:_endDate,
            reason:dataIjin.alasan       
        }, config)

        console.log("hasil")
        console.log(status)
        if(status != 200){
          // console.log("here status gagal")
            dispatch({
              type: CREATE_PENGAMBILAN_IJIN_FAIL,
              payload: "Gagal, Silakan Coba Lagi",
            })          
          }else{
            // console.log("here status suces")
            dispatch({
                type: CREATE_PENGAMBILAN_IJIN_SUCCESS,
                payload: data,
            })
            // dispatch(getListJadwalPegawai());
          }

    } catch (error){
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: CREATE_PENGAMBILAN_IJIN_FAIL,
          payload: message,
        })
    }
  }

  export const deletePengambilanIjin = (dataIjin) => async (dispatch, getState) => {
    try {
        dispatch({
            type: DELETE_PENGAMBILAN_IJIN_REQUEST,
        })
        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },params:{
             id:`${dataIjin.id}`,
             },
        };
 
        const { status: status, data : {data} }  = await axios.delete(URL_API+API_DELETE_PENGAMBILAN_IJIN, config)
        console.log("delete pengambilan ijin")
        console.log(status)
        if(status != 200){
            dispatch({
              type: DELETE_PENGAMBILAN_IJIN_FAIL,
              payload: "Gagal, Silakan Coba Lagi",
            })          
          }else{
            dispatch({
                type: DELETE_PENGAMBILAN_IJIN_SUCCESS,
                payload: data,
            })
          }
        
    } catch (error){
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: DELETE_PENGAMBILAN_IJIN_FAIL,
          payload: message,
        })
    }
  }

  export const getListJenisIjin = () => async (dispatch, getState) =>{ 
    try {
        dispatch({
            type: LIST_JENIS_IJIN_REQUEST,
        })
        const {
            userLogin: { token },
        } = getState()
        let config = {
            headers: {
              Authorization: `${token}`,
            },
        }
        const { data: {jenisIjinList} } = await axios.get(URL_API+API_LIST_JENIS_IJIN, config)
        ;
        dispatch({
            type: LIST_JENIS_IJIN_SUCCESS,
            payload: jenisIjinList,
        })

    } catch (error){
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: LIST_JENIS_IJIN_FAIL,
          payload: message,
        })
    }
  }