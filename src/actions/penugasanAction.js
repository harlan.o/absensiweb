import axios from "axios";
import {
    LIST_MELAKSANAKAN_TUGAS_REQUEST,
    LIST_MELAKSANAKAN_TUGAS_SUCCESS,
    LIST_MELAKSANAKAN_TUGAS_FAIL,
    LIST_MELAKSANAKAN_TUGAS_RESET,
    CREATE_MELAKSANAKAN_TUGAS_REQUEST,
    CREATE_MELAKSANAKAN_TUGAS_SUCCESS,
    CREATE_MELAKSANAKAN_TUGAS_FAIL,
    CREATE_MELAKSANAKAN_TUGAS_RESET,
    UPDATE_MELAKSANAKAN_TUGAS_REQUEST,
    UPDATE_MELAKSANAKAN_TUGAS_SUCCESS,
    UPDATE_MELAKSANAKAN_TUGAS_FAIL,
    UPDATE_MELAKSANAKAN_TUGAS_RESET,
    DELETE_MELAKSANAKAN_TUGAS_REQUEST,
    DELETE_MELAKSANAKAN_TUGAS_SUCCESS,
    DELETE_MELAKSANAKAN_TUGAS_FAIL,
    DELETE_MELAKSANAKAN_TUGAS_RESET,
    LIST_TUGAS_LUAR_REQUEST,
    LIST_TUGAS_LUAR_SUCCESS,
    LIST_TUGAS_LUAR_FAIL,
    LIST_TUGAS_LUAR_RESET,
    CREATE_TUGAS_LUAR_REQUEST,
    CREATE_TUGAS_LUAR_SUCCESS,
    CREATE_TUGAS_LUAR_FAIL,
    CREATE_TUGAS_LUAR_RESET,
    UPDATE_TUGAS_LUAR_REQUEST,
    UPDATE_TUGAS_LUAR_SUCCESS,
    UPDATE_TUGAS_LUAR_FAIL,
    UPDATE_TUGAS_LUAR_RESET,
    DELETE_TUGAS_LUAR_REQUEST,
    DELETE_TUGAS_LUAR_SUCCESS,
    DELETE_TUGAS_LUAR_FAIL,
    DELETE_TUGAS_LUAR_RESET,
} from "../constants/penugasanConstants";

import {
    URL_API,
    API_LIST_MELAKSANAKAN_TUGAS,
    API_ADD_MELAKSANAKAN_TUGAS,
    API_UPDATE_MELAKSANAKAN_TUGAS,
    API_DELETE_MELAKSANAKAN_TUGAS,
    API_LIST_TUGAS_LUAR,
    API_ADD_TUGAS_LUAR,
    API_UPDATE_TUGAS_LUAR,
    API_DELETE_TUGAS_LUAR,
  } from "../constants/apiConstants";
import { stringArrayId, yearWithMonthOneDigit } from "./genneralAction";



export const getListMelaksanakanTugas = (yearMonth, unitInduk, unitKerja) => async (dispatch, getState) =>{
    try {
        dispatch({
            type: LIST_MELAKSANAKAN_TUGAS_REQUEST,
        })
        const {
            userLogin: { token },
          } = getState()
          let monthYear=yearWithMonthOneDigit(yearMonth);
          const unitIndukId=stringArrayId(unitInduk);

          let config = {
            headers: {
              Authorization: `${token}`,
            },params:{
              unitIndukId:`${unitIndukId}`,
              yearMonth:`${monthYear}`,
            },
          }
          if(unitKerja != null && unitKerja.length > 0){
            const unitKerjaId=stringArrayId(unitKerja);
            config = {
                headers: {
                  Authorization: `${token}`,
                }, params:{
                  unitKerjaId:`${unitKerjaId}`,
                  yearMonth:`${monthYear}`,
                },
            }   
          }

          const { data: {melaksanakanTugasList} } = await axios.get(URL_API+API_LIST_MELAKSANAKAN_TUGAS, config);

        dispatch({
            type: LIST_MELAKSANAKAN_TUGAS_SUCCESS,
            payload: melaksanakanTugasList,
          })
    } catch (error){
        const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
        dispatch({
            type: LIST_MELAKSANAKAN_TUGAS_FAIL,
            payload: message,
        })
    }
}

export const createMelaksanakanTugas = (dataMelaksanakanTugas) => async (dispatch, getState) =>{
    try {
        dispatch({
            type: CREATE_MELAKSANAKAN_TUGAS_REQUEST,
        })
        const {
            userLogin: { token },
          } = getState()
          
          const config = {
            headers: {
              "Content-Type": "application/json",
              Authorization: `${token}`,
            },
          }
          console.log("create Melaksanakan Tugas", dataMelaksanakanTugas)
          const { status: status, data : {data} } = await axios.put(URL_API+API_ADD_MELAKSANAKAN_TUGAS, { 
            pegawaiStatusList:dataMelaksanakanTugas.pegawaiStatusList,
            startDate:dataMelaksanakanTugas.startDate,
            endDate:dataMelaksanakanTugas.endDate,
          }, config)
        
      


        if(status != 200){
            dispatch({
              type: CREATE_MELAKSANAKAN_TUGAS_FAIL,
              payload: "Gagal, Silakan Coba Lagi",
            })          
        }else{
            dispatch({
                type: CREATE_MELAKSANAKAN_TUGAS_SUCCESS,
                payload: data,
            })
        }

    } catch (error){
        const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
        dispatch({
            type: CREATE_MELAKSANAKAN_TUGAS_FAIL,
            payload: message,
        })
    }
}

export const updateMelaksanakanTugas = (dataMelaksanakanTugas) => async (dispatch, getState) =>{
    try {
        dispatch({
            type: UPDATE_MELAKSANAKAN_TUGAS_REQUEST,
        })
        const {
            userLogin: { token },
          } = getState()
          
         
          const config = {
            headers: {
              "Content-Type": "application/json",
              Authorization: `${token}`,
            },
          }

        const { status: status, data : {data} } = await axios.put(URL_API+API_UPDATE_MELAKSANAKAN_TUGAS, { 
            id:dataMelaksanakanTugas.id,
            idPegawai:dataMelaksanakanTugas.idPegawai,
            mulai:dataMelaksanakanTugas.tanggalAwal,
            selesai:dataMelaksanakanTugas.tanggalAkhir,
        }, config)
        
        if(status != 200){
            dispatch({
              type: UPDATE_MELAKSANAKAN_TUGAS_FAIL,
              payload: "Gagal, Silakan Coba Lagi",
            })          
        }else{
            dispatch({
                type: UPDATE_MELAKSANAKAN_TUGAS_SUCCESS,
                payload: data,
            })
        }

    } catch (error){
        const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
        dispatch({
            type: UPDATE_MELAKSANAKAN_TUGAS_FAIL,
            payload: message,
        })
    }
}

export const deleteMelaksanakanTugas = (dataMelaksanakanTugas) => async (dispatch, getState) =>{
    try {
        dispatch({
            type: DELETE_MELAKSANAKAN_TUGAS_REQUEST,
        })
        const {
            userLogin: { token },
          } = getState()
          
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },params:{
             id:`${dataMelaksanakanTugas.id}`,
             },
        };

        const { status: status, data : {data} } = await axios.delete(URL_API+API_DELETE_MELAKSANAKAN_TUGAS, config)
        
        if(status != 200){
            dispatch({
              type: DELETE_MELAKSANAKAN_TUGAS_FAIL,
              payload: "Gagal, Silakan Coba Lagi",
            })          
        }else{
            dispatch({
                type: DELETE_MELAKSANAKAN_TUGAS_SUCCESS,
                payload: data,
            })
        }

    } catch (error){
        const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
        dispatch({
            type: DELETE_MELAKSANAKAN_TUGAS_FAIL,
            payload: message,
        })
    }
}

export const getListTugasLuar = (yearMonth, unitInduk, unitKerja) => async (dispatch, getState) =>{
    try {
        dispatch({
            type: LIST_TUGAS_LUAR_REQUEST,
        })
        const {
            userLogin: { token },
          } = getState()
          
          let monthYear=yearWithMonthOneDigit(yearMonth);
          const unitIndukId=stringArrayId(unitInduk);
          let config = {
            headers: {
              Authorization: `${token}`,
            },params:{
              unitIndukId:`${unitIndukId}`,
              yearMonth:`${monthYear}`,
            },
          }
          if(unitKerja != null && unitKerja.length > 0){
            const unitKerjaId=stringArrayId(unitKerja);
            config = {
                headers: {
                  Authorization: `${token}`,
                }, params:{
                  unitKerjaId:`${unitKerjaId}`,
                  yearMonth:`${monthYear}`,
                },
            }   
          }

          const { data: {tugasLuarList} } = await axios.get(URL_API+API_LIST_TUGAS_LUAR, config);

        dispatch({
            type: LIST_TUGAS_LUAR_SUCCESS,
            payload: tugasLuarList,
          })
    } catch (error){
        const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
        dispatch({
            type: LIST_TUGAS_LUAR_FAIL,
            payload: message,
        })
    }
}

export const createTugasLuar = (dataTugasLuar) => async (dispatch, getState) =>{
    try {
        dispatch({
            type: CREATE_TUGAS_LUAR_REQUEST,
        })
        const {
            userLogin: { token },
          } = getState()
          console.log("create tugas luar", dataTugasLuar)
          const config = {
            headers: {
              "Content-Type": "application/json",
              Authorization: `${token}`,
            },
          }

          const { status: status, data : {data} } = await axios.put(URL_API+API_ADD_TUGAS_LUAR, { 
            pegawaiStatusList:dataTugasLuar.pegawaiStatusList,
            startDate:dataTugasLuar.startDate,
            endDate:dataTugasLuar.endDate,
        }, config)
        
        if(status != 200){
            dispatch({
              type: CREATE_TUGAS_LUAR_FAIL,
              payload: "Gagal, Silakan Coba Lagi",
            })          
        }else{
            dispatch({
                type: CREATE_TUGAS_LUAR_SUCCESS,
                payload: data,
            })
        }

    } catch (error){
        const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
        dispatch({
            type: CREATE_TUGAS_LUAR_FAIL,
            payload: message,
        })
    }
}

export const updateTugasLuar = (dataTugasLuar) => async (dispatch, getState) =>{
    try {
        dispatch({
            type: UPDATE_TUGAS_LUAR_REQUEST,
        })
        const {
            userLogin: { token },
          } = getState()
          
          const config = {
            headers: {
              "Content-Type": "application/json",
              Authorization: `${token}`,
            },
          }

          const { status: status, data : {data} } = await axios.put(URL_API+API_UPDATE_TUGAS_LUAR, { 
            id:dataTugasLuar.id,
            idPegawai:dataTugasLuar.idPegawai,
            mulai:dataTugasLuar.mulai,
            selesai:dataTugasLuar.selesai,
        }, config)
        
        if(status != 200){
            dispatch({
              type: UPDATE_TUGAS_LUAR_FAIL,
              payload: "Gagal, Silakan Coba Lagi",
            })          
        }else{
            dispatch({
                type: UPDATE_TUGAS_LUAR_SUCCESS,
                payload: data,
            })
        }
    } catch (error){
        const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
        dispatch({
            type: UPDATE_TUGAS_LUAR_FAIL,
            payload: message,
        })
    }
}

export const deleteTugasLuar = (dataTugasLuar) => async (dispatch, getState) =>{
    try {
        dispatch({
            type: DELETE_TUGAS_LUAR_REQUEST,
        })
        const {
            userLogin: { token },
          } = getState()
          
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },params:{
             id:`${dataTugasLuar.id}`,
             },
        };

        const { status: status, data : {data} } = await axios.delete(URL_API+API_DELETE_TUGAS_LUAR, config)
        
        if(status != 200){
            dispatch({
              type: DELETE_TUGAS_LUAR_FAIL,
              payload: "Gagal, Silakan Coba Lagi",
            })          
        }else{
            dispatch({
                type: DELETE_TUGAS_LUAR_SUCCESS,
                payload: data,
            })
        }
    } catch (error){
        const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
        dispatch({
            type: DELETE_TUGAS_LUAR_FAIL,
            payload: message,
        })
    }
}