import axios from "axios";



import {
    PENG_LIST_TPP_REQUEST,
    PENG_LIST_TPP_SUCCESS,
    PENG_LIST_TPP_FAIL,
    PENG_CREATE_TPP_REQUEST,
    PENG_CREATE_TPP_SUCCESS,
    PENG_CREATE_TPP_FAIL,
    PENG_UPDATE_TPP_REQUEST,
    PENG_UPDATE_TPP_SUCCESS,
    PENG_UPDATE_TPP_FAIL,
    LIST_TPP_HISTORY_REQUEST,
    LIST_TPP_HISTORY_SUCCESS,
    LIST_TPP_HISTORY_FAIL,
    LIST_TPP_HISTORY_RESET,
    LIST_TPP_PEGAWAI_HISTORY_REQUEST,
    LIST_TPP_PEGAWAI_HISTORY_SUCCESS,
    LIST_TPP_PEGAWAI_HISTORY_FAIL,
    LIST_TPP_PEGAWAI_HISTORY_RESET,
    REVISI_TPP_HISTORY_REQUEST,
    REVISI_TPP_HISTORY_SUCCESS,
    REVISI_TPP_HISTORY_FAIL,
    REVISI_TPP_HISTORY_RESET,
  } from "../constants/tppConstants";
  import {
    URL_API,
    API_LIST_TPP,
    API_ADD_TPP,
    API_LIST_TPP_HISTORY,
    API_LIST_PEGAWAI_TPP_HISTORY,
    API_REVISI_TPP_HISTORY,
    API_LIST_TPP_PEGAWAI,
    API_UPDATE_TPP_PEGAWAI
  } from "../constants/apiConstants";
import { cekHakAksesClient } from "./hakAksesActions";
import { REGISTER_FU001 } from "../constants/hakAksesConstants";
import { uIIDP } from "./userActions";
// list Tpp
export const getListTpp = () => async (dispatch, getState) =>{
  // console.log("unit Induk");
  try {
      dispatch({
        type: PENG_LIST_TPP_REQUEST,
      })
      
      const {
        userLogin: { token },
      } = getState()
      
      let config = {
        headers: {
          Authorization: `${token}`,
        },
      }

        if(!cekHakAksesClient(REGISTER_FU001)){
          config = {
            headers: {
              Authorization: `${token}`,
            },
            params:{
              unitIndukId:`${uIIDP()}`,
              },
          }
        }

      
      const { data: {tppList} } = await axios.get(URL_API+API_LIST_TPP_PEGAWAI, config)
      tppList.sort((a, b) => {
          let fa = a.namaPegawai.toLowerCase(),
              fb = b.namaPegawai.toLowerCase();
      
          if (fa < fb) {
              return -1;
          }
          if (fa > fb) {
              return 1;
          }
          return 0;
      })
      console.log("tpp", tppList)
      dispatch({
        type: PENG_LIST_TPP_SUCCESS,
        payload: tppList,
      })
    } catch (error) {
      console.log("error");
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      if (message === 'Not authorized, token failed') {
      }
      dispatch({
        type: PENG_LIST_TPP_FAIL,
        payload: message,
      })
    }
}


// Add Tpp
export const createTpp = (dataTpp) => async (dispatch, getState) => {
  // console.log(dataTpp);
  try {
      dispatch({
          type: PENG_CREATE_TPP_REQUEST,
      })

      const {
          userLogin: { token },
      } = getState()

      const config = {
          headers: {
          "Content-Type": "application/json",
          Authorization: `${token}`,
          },
      };

      const { data : {data} } = await axios.post(URL_API+API_ADD_TPP, { 
          // accountDeposit, amountDeposit, descDeposit 
          nip:dataTpp.nip,
          noKtp:dataTpp.noKtp,
          nama:dataTpp.nama,
          gelarDepan:dataTpp.gelarDepan,
          tempatLahir:dataTpp.tempatLahir,
          jenisKelamin:dataTpp.jenisKelamin,
          unitKerjaId:dataTpp.unitKerjaId

      }, config)
      dispatch({
          type: PENG_CREATE_TPP_SUCCESS,
          payload: data,
      })
      dispatch(getListTpp());
    
  } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      if (message === 'Not authorized, token failed') {
        // dispatch(logout())
        console.log("token exp");
      }
      dispatch({
        type: PENG_CREATE_TPP_FAIL,
        payload: message,
      })
  }
};

// Edit Tpp
export const updateTpp = (dataTpp) => async (dispatch, getState) => {
  // console.log(dataTpp);
  try {
      dispatch({
          type: PENG_UPDATE_TPP_REQUEST,
      })

      const {
          userLogin: { token },
      } = getState()

      const config = {
          headers: {
            "Content-Type": "application/json",
          Authorization: `${token}`,
          }
      };

      const { status: status, data : {data} } = await axios.put(URL_API+API_UPDATE_TPP_PEGAWAI+"?pegawaiId="+dataTpp.pegawaiId+"&tpp="+dataTpp.nominalTpp, {},config)
      dispatch({
          type: PENG_UPDATE_TPP_SUCCESS,
          payload: data,
      })

      if(status != 200){
        dispatch({
          type: PENG_UPDATE_TPP_FAIL,
          payload: "Gagal, Silakan Coba Lagi",
        })          
      }else{
        dispatch({
            type: PENG_UPDATE_TPP_SUCCESS,
            payload: data,
        })
        dispatch(getListTpp());
      }



    
  } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      if (message === 'Not authorized, token failed') {
        // dispatch(logout())
        console.log("token exp");
      }
      dispatch({
        type: PENG_UPDATE_TPP_FAIL,
        payload: message,
      })
  }
}


export const padLeadingZero = (yearmonth) => {
  let [year, month] = yearmonth.split("-");
  while (month.length < 2) month = "0" + month;
  let d = year+"-"+month;
   return d;
}
// list Tpp History
export const getListTppHistory = (month, unitIndukId, unitKerjaId) => async (dispatch, getState) =>{
  console.log(unitIndukId+" kerja "+ unitKerjaId);
  try {
      dispatch({
        type: LIST_TPP_HISTORY_REQUEST,
      })
      
      const {
        userLogin: { token },
      } = getState()
      
      const config = {
        headers: {
          Authorization: `${token}`,
        },
        params:{
          month:`${padLeadingZero(month)}`,
          unitIndukId:`${unitIndukId}`,
          unitKerjaId:`${unitKerjaId}`,
          },
      }
      const { data: {historyTppList} } = await axios.get(URL_API+API_LIST_TPP_HISTORY, config)
      console.log(config);
      dispatch({
        type: LIST_TPP_HISTORY_SUCCESS,
        payload: historyTppList,
      })
    } catch (error) {
      console.log("error");
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      if (message === 'Not authorized, token failed') {
      }
      dispatch({
        type: LIST_TPP_HISTORY_FAIL,
        payload: message,
      })
    }
}

// list Tpp Pegawai History
export const getListTppPegawaiHistory = (idPegawai) => async (dispatch, getState) =>{
  // console.log("unit Induk");
  try {
      dispatch({
        type: LIST_TPP_PEGAWAI_HISTORY_REQUEST,
      })
      
      const {
        userLogin: { token },
      } = getState()
      
      const config = {
        headers: {
          Authorization: `${token}`,
        },
      }
      const { data: {historyTppList} } = await axios.get(URL_API+API_LIST_PEGAWAI_TPP_HISTORY+"/"+idPegawai, config)
      dispatch({
        type: LIST_TPP_PEGAWAI_HISTORY_SUCCESS,
        payload: historyTppList,
      })
    } catch (error) {
      console.log("error");
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      if (message === 'Not authorized, token failed') {
      }
      dispatch({
        type: LIST_TPP_PEGAWAI_HISTORY_FAIL,
        payload: message,
      })
    }
}

// Revisi Tpp History
export const revisiTppHistory = (idTpp, nilaiPotongan, filterData) => async (dispatch, getState) => {
  let unitKerjaId, unitIndukId;
  if(filterData?.kodeUnitInduk === "all" || filterData?.kodeUnitInduk === null || filterData?.kodeUnitInduk === undefined){
      unitIndukId = "";
  }else{
      unitIndukId=filterData?.kodeUnitInduk;
      unitKerjaId = "";
  }
  if(filterData?.kodeUnitKerja === "all" || filterData?.kodeUnitKerja === null || filterData?.kodeUnitKerja === undefined){
      unitKerjaId = "";
  }else{
      unitIndukId = "";
      unitKerjaId=filterData?.kodeUnitKerja;
  }
  
  try {
      dispatch({
          type: REVISI_TPP_HISTORY_REQUEST,
      })

      const {
          userLogin: { token },
      } = getState()

      const config = {
          headers: {
          "Content-Type": "application/json",
          Authorization: `${token}`,
          },params:{
            potongan:`${nilaiPotongan}`,
            },
      };

      const { data : {data} } = await axios.put(URL_API+API_REVISI_TPP_HISTORY+"/"+idTpp,{}, config)
      dispatch({
          type: REVISI_TPP_HISTORY_SUCCESS,
          payload: data,
      })
      dispatch(getListTppHistory(filterData?.startDate, unitIndukId, unitKerjaId));
  } catch (error) {
      const message =
        error.response && error.response.data.message
          ? error.response.data.message
          : error.message
      if (message === 'Not authorized, token failed') {
        // dispatch(logout())
        console.log("token exp");
      }
      dispatch({
        type: REVISI_TPP_HISTORY_FAIL,
        payload: message,
      })
  }
}