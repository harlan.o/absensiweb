import axios from "axios";
import {
    PENG_LIST_UNIT_INDUK_REQUEST,
    PENG_LIST_UNIT_INDUK_SUCCESS,
    PENG_LIST_UNIT_INDUK_FAIL,
    PENG_CREATE_UNIT_INDUK_REQUEST,
    PENG_CREATE_UNIT_INDUK_SUCCESS,
    PENG_CREATE_UNIT_INDUK_FAIL,
    PENG_UPDATE_UNIT_INDUK_SUCCESS,
    PENG_UPDATE_UNIT_INDUK_FAIL,
    PENG_UPDATE_UNIT_INDUK_REQUEST
  } from "../constants/unitIndukConstants";
  import {
    URL_API,
    API_LIST_UNIT_INDUK,
    API_ADD_UNIT_INDUK,
    API_UPDATE_UNIT_INDUK
  } from "../constants/apiConstants";
import { cekHakAksesClient } from "./hakAksesActions";
import { REGISTER_FU001 } from "../constants/hakAksesConstants";
import { uIIDP } from "./userActions";

  export const getListUnitInduk = () => async (dispatch, getState) =>{
    // console.log("unit Induk");


    try {
        dispatch({
          type: PENG_LIST_UNIT_INDUK_REQUEST,
        })
        
        const {
          userLogin: { token },
        } = getState()
        
        const config = {
          headers: {
            Authorization: `${token}`,
          },
        }
        // console.log(token+"token");
        const { data: {unitIndukList} } = await axios.get(URL_API+API_LIST_UNIT_INDUK, config)
        // console.log(unitIndukList);
        let unk = unitIndukList
        if(!cekHakAksesClient(REGISTER_FU001) ){
          unk= unitIndukList.filter(v => v.id === uIIDP())
        }
        dispatch({
          type: PENG_LIST_UNIT_INDUK_SUCCESS,
          payload: unk,
        })
        //  console.log("token: "+token);
        // console.log("Ambil Data Profile "+profilPegawai);
      } catch (error) {
        console.log("error");
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: PENG_LIST_UNIT_INDUK_FAIL,
          payload: message,
        })
      }
  }


  
  export const createUnitInduk = (dataUnitInduk) => async (dispatch, getState) => {

    try {
        dispatch({
            type: PENG_CREATE_UNIT_INDUK_REQUEST,
        })
  
        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },
        };
        
        const { data : {data} } = await axios.post(URL_API+API_ADD_UNIT_INDUK, { 
            nama:dataUnitInduk.nama,            
        }, config)
        dispatch({
            type: PENG_CREATE_UNIT_INDUK_SUCCESS,
            payload: data,
        })
        dispatch(getListUnitInduk());
      
    } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
          // dispatch(logout())
          console.log("token exp");
        }
        dispatch({
          type: PENG_CREATE_UNIT_INDUK_FAIL,
          payload: message,
        })
    }
  };


  export const updateUnitInduk = (dataUnitInduk) => async (dispatch, getState) => {

    try {
        dispatch({
            type: PENG_UPDATE_UNIT_INDUK_REQUEST,
        })
  
        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },
        };
        
        const { data : {data} } = await axios.put(URL_API+API_UPDATE_UNIT_INDUK, { 
            id:dataUnitInduk.id,
            nama:dataUnitInduk.nama,            
        }, config)
        dispatch({
            type: PENG_UPDATE_UNIT_INDUK_SUCCESS,
            payload: data,
        })
        dispatch(getListUnitInduk());
      
    } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
          // dispatch(logout())
          console.log("token exp");
        }
        dispatch({
          type: PENG_UPDATE_UNIT_INDUK_FAIL,
          payload: message,
        })
    }
  };