import axios from "axios";
import {
    PENG_LIST_UNIT_KERJA_REQUEST,
    PENG_LIST_UNIT_KERJA_SUCCESS,
    PENG_LIST_UNIT_KERJA_FAIL,
    PENG_CREATE_UNIT_KERJA_REQUEST,
    PENG_CREATE_UNIT_KERJA_SUCCESS,
    PENG_CREATE_UNIT_KERJA_FAIL,
    PENG_UPDATE_UNIT_KERJA_SUCCESS,
    PENG_UPDATE_UNIT_KERJA_FAIL,
    PENG_UPDATE_UNIT_KERJA_REQUEST
  } from "../constants/unitKerjaConstants";
  import {
    URL_API,
    API_LIST_UNIT_KERJA,
    API_ADD_UNIT_KERJA,
    API_UPDATE_UNIT_KERJA,
  } from "../constants/apiConstants";
import { cekHakAksesClient } from "./hakAksesActions";
import { REGISTER_FU001 } from "../constants/hakAksesConstants";
import { uIIDP } from "./userActions";

  export const getListUnitKerja = () => async (dispatch, getState) =>{
    //   console.log("unitKerjaList");
    try {
        dispatch({
          type: PENG_LIST_UNIT_KERJA_REQUEST,
        })
        
        const {
          userLogin: { token },
        } = getState()
        
        const config = {
          headers: {
            Authorization: `${token}`,
          },
        }
        // console.log(token+"token");
        const { data: {unitKerjaList} } = await axios.get(URL_API+API_LIST_UNIT_KERJA, config)
        let unk = unitKerjaList
        if(!cekHakAksesClient(REGISTER_FU001) ){
          unk= unitKerjaList.filter(v => v.unitInduk.id === uIIDP())
        }
        dispatch({
          type: PENG_LIST_UNIT_KERJA_SUCCESS,
          payload: unk,
        })
        //  console.log("token: "+token);
        // console.log(unitKerjaList);
      } catch (error) {
        console.log("error");
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: PENG_LIST_UNIT_KERJA_FAIL,
          payload: message,
        })
      }
  }

  export const createUnitKerja = (dataUnitKerja) => async (dispatch, getState) => {

    try {
        dispatch({
            type: PENG_CREATE_UNIT_KERJA_REQUEST,
        })
  
        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },
        };
        
        const { data : {data} } = await axios.post(URL_API+API_ADD_UNIT_KERJA, { 
  
            nama:dataUnitKerja.nama,
            geolocation:dataUnitKerja.geolocation,
            radius:dataUnitKerja.radius,
            unitIndukId:dataUnitKerja.unitInduk.id,
            fixJadwal:dataUnitKerja.fixJadwal,
            
        }, config)
        dispatch({
            type: PENG_CREATE_UNIT_KERJA_SUCCESS,
            payload: data,
        })
        dispatch(getListUnitKerja());
      
    } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
          // dispatch(logout())
          console.log("token exp");
        }
        dispatch({
          type: PENG_CREATE_UNIT_KERJA_FAIL,
          payload: message,
        })
    }
  };


  export const updateUnitKerja = (dataUnitKerja) => async (dispatch, getState) => {

    try {
        dispatch({
            type: PENG_UPDATE_UNIT_KERJA_REQUEST,
        })
  
        const {
            userLogin: { token },
        } = getState()
  
        const config = {
            headers: {
            "Content-Type": "application/json",
            Authorization: `${token}`,
            },
        };
        
        const { data : {data} } = await axios.put(URL_API+API_UPDATE_UNIT_KERJA, { 
            id:dataUnitKerja.id,
            nama:dataUnitKerja.nama,
            geolocation:dataUnitKerja.geolocation,
            radius:dataUnitKerja.radius,
            unitIndukId:dataUnitKerja.unitInduk.id,
            fixJadwal:dataUnitKerja.fixJadwal,
            
        }, config)
        dispatch({
            type: PENG_UPDATE_UNIT_KERJA_SUCCESS,
            payload: data,
        })
        dispatch(getListUnitKerja());
      
    } catch (error) {
        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
          // dispatch(logout())
          console.log("token exp");
        }
        dispatch({
          type: PENG_UPDATE_UNIT_KERJA_FAIL,
          payload: message,
        })
    }
  };