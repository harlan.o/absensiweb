import axios from "axios";
import {
  USER_LOGIN_FAIL,
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGOUT,
  USER_DATA_PROFIL_REQUEST,
  USER_DATA_PROFIL_SUCCESS,
  USER_DATA_PROFIL_FAIL,
} from "../constants/userConstants";
import {
  URL_API,
  URL_API_LOGIN,
  API_LOGIN,
  API_PROFILE_USER,
} from "../constants/apiConstants";
import {
  USER_HAK_AKSES_REQUEST,
  USER_HAK_AKSES_SUCCESS,
  USER_HAK_AKSES_FAIL,
  USER_HAK_AKSES_LOGOUT,

} from "../constants/hakAksesConstants";
import { decryRE, encry, encry2, encryHA, encryRE } from "./genneralAction";
import { decrypt, encrypt } from "react-crypt-gsm";

export const login = (nip, password) => async (dispatch) => {
  
  try {
    dispatch({
      type: USER_LOGIN_REQUEST,
    });

    dispatch({
      type: USER_HAK_AKSES_REQUEST,
    });

    const config = {
      headers: {
        "Content-Type": "application/json",
        "WEB-CLIENT":"true",
      },
    };
    const { data } = await axios.post(URL_API_LOGIN+API_LOGIN, { nip, password }, config);
   
    let _haks = encry2(data.rightList)
    let haksEnc = encryRE(_haks)
    
    localStorage.setItem("haks_c", haksEnc.contentT);
    localStorage.setItem("haks_t", haksEnc.tagT);

    // localStorage.setItem("haks", _haks);
    
    localStorage.setItem("token", "Bearer "+data.access_token);
    
        

    
    dispatch({
      type: USER_LOGIN_SUCCESS,
      payload: "Bearer "+data.access_token,
    });
    dispatch(dataProfile())
    dispatch({
      type: USER_HAK_AKSES_SUCCESS,
      payload: data.rightList,
    });
    // console.log("ency2")
    // console.log(encry2(data.rightList))
   

  } catch (error) {
    dispatch({
      type: USER_LOGIN_FAIL,
      // @TODO fix the error
      payload: "Tidak dapat terhubung",
    });
    dispatch({
      type: USER_HAK_AKSES_FAIL,
      // @TODO fix the error
      payload: "Tidak dapat terhubung",
    });
  }
};

export const uIIDP = () => {
  // console.log("hasil", JSON.parse(objDecProfil))
        let contentT = localStorage.getItem("uid_c")
        let tagT = localStorage.getItem("uid_t")
        let objDecProfil = decryRE(contentT, tagT)
        objDecProfil=JSON.parse(objDecProfil)
        return objDecProfil.unitIndukId
}

export const dataProfile = () => async (dispatch, getState) =>{
      try {
        dispatch({
          type: USER_DATA_PROFIL_REQUEST,
        })
        
        const {
          userLogin: { token },
        } = getState()
        // console.log(token)
        const config = {
          headers: {
            Authorization: `${token}`,
          },
        }
        // console.log(token+"token");
        const { data: {profilPegawai} } = await axios.get(URL_API+API_PROFILE_USER, config)
        // console.log(profilPegawai+"asd");
        let stringToJson = JSON.stringify(profilPegawai)
        let objProfil = encryRE(stringToJson)
        localStorage.setItem("uid_c", objProfil.contentT);
        localStorage.setItem("uid_t", objProfil.tagT);
        
        
        // let objDecProfil = decryRE(objProfil.contentT, objProfil.tagT)
        // console.log("hasil", JSON.parse(objDecProfil))
        
        
        // ecr
        // let _uid = JSON.stringify(profilPegawai)
        // let uid = encrypt(_uid)

        // var string = new TextDecoder("utf-8").decode(uid.tag)
        // let arrString = JSON.stringify(uid.tag)
        // localStorage.setItem("uid_t", arrString);


// ///////////////////////////////////////////////////////////
// ?console.log(localStorage.getItem("uid_t"))
        // dec
//         let c=JSON.parse(arrString)
//         let obj = {
//           content:uid.content,
//           tag:c.data,
//         }
//         console.log("dec tag", c)
//         console.log("dec", obj)
//         console.log("decri", JSON.parse(decrypt(obj)))
// // //////////////////////////////////////////////////////////
//         let string =JSON.stringify(uid)
//         console.log("awal tag",uid.tag)
//         console.log("awal content",uid.content)
//         console.log("awal",uid)
//         console.log("string",string)
//         console.log("json",JSON.parse(string))
        
        

        dispatch({
          type: USER_DATA_PROFIL_SUCCESS,
          payload: profilPegawai,
        })
        //  console.log("token: "+token);
        // console.log("Ambil Data Profile "+profilPegawai);
      } catch (error) {
        dispatch(logout());

        const message =
          error.response && error.response.data.message
            ? error.response.data.message
            : error.message
        if (message === 'Not authorized, token failed') {
        }
        dispatch({
          type: USER_DATA_PROFIL_FAIL,
          payload: message,
        })
        
      }
}

export const logout = () => (dispatch) => {
  // console.log("logout")
  localStorage.removeItem('token');
  localStorage.removeItem('haks_t');
  localStorage.removeItem('haks_c');
  localStorage.removeItem('uid_c');
  localStorage.removeItem('uid_t');
  dispatch({
    type: USER_LOGOUT,
  })
  dispatch({
    type: USER_HAK_AKSES_LOGOUT,
  })
}
