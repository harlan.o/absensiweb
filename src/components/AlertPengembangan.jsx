import React, { Component } from 'react';
import { Alert, Breadcrumb } from 'react-bootstrap';
class AlertPengembangan extends Component {
 
    render(){
        return (
            <Alert variant="warning">
                <Alert.Heading>Informasi Halaman</Alert.Heading>
                <p>
                    Fitur ini masih dalam pengembangan, secepatnya akan diperbaharui
                </p>
                <hr />
                <p className="mb-0">
                    Data yang ditampilkan bukan merupakan data akhir
                </p>
            </Alert>
        );
    }

}
export default AlertPengembangan;