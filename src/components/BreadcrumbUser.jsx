import React, { Component } from 'react';
// import { Link } from 'react-router-dom';
import { Breadcrumb } from 'react-bootstrap';
class BreadcrumbUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listBreadcrumbUser: [],
        };
    }
    render(){
        return (

            <Breadcrumb>
            {this.props.listBreadcrumbUser.map(({path, name, status}, key) => (
                    <Breadcrumb.Item href={path} key={key} active={status}  >{name}</Breadcrumb.Item>
            ))}
            </Breadcrumb>
            
            //           <ol className="breadcrumb">
            // {this.props.listBreadcrumbUser.map(({path, name, status}, key) => (
            //       <li  key={key} className="breadcrumb-item">
            //             <NavLink to={path} className={status ? "breadcrumb-item":""} >{name}</NavLink>
            //       </li>
        );
    }

}
export default BreadcrumbUser;