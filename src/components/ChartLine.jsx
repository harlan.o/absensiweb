
import React, { Component, useState, useEffect } from 'react';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip } from 'recharts';



class ChartLine extends Component {

    constructor (props){
        super(props)
        // uv : value
        this.state = {
            chartData :[{name: 'Page A', uv: 400, pv: 2400, amt: 2400},{name: 'Page B', uv: 400, pv: 2400, amt: 2400},{name: 'Page B', uv: 40, pv: 2400, amt: 2400}],
        }

    }

    render(){

        return(
            <LineChart width={600} height={300} data={this.state.chartData} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
                <Line type="monotone" dataKey="uv" stroke="#8884d8" />
                <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                <XAxis dataKey="name" />
                <YAxis />
                <Tooltip />
            </LineChart>
        )
    }


}
export default ChartLine;