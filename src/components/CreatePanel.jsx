import React, { Component } from 'react';
import { Button} from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import "./Filter.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import ReactToPrint from 'react-to-print';

import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import {  faPlus, faPrint } from '@fortawesome/free-solid-svg-icons';
import { cekHakAksesClient } from '../actions/hakAksesActions';


class CreatePanel extends Component {
    constructor(props){
        super(props);
        this.state={
            showFormAdd: true,
        };
        this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this); 
    }
    
    handleShowFormModalAdd(){
        this.setState(state => ({      
            showFormAdd:true
        }));
        console.log(this.state.showFormAdd)
        this.props.showForm(this.state.showFormAdd); 
    }
     
render()
{
    return (
        <div className="custom-filter">
        {cekHakAksesClient(this.props.kodeAdd) ?  
        <Button variant="secondary" className="btn-width" onClick={this.handleShowFormModalAdd} > <FontAwesomeIcon icon={faPlus} /> </Button>
       :""}{' '}
       {cekHakAksesClient(this.props.kodeCetak) ?
        <ReactToPrint
                    trigger={() => {
                       
                        return <Button variant="secondary" className="btn-width"> <FontAwesomeIcon icon={faPrint} /></Button>;
                    }}
                    content={this.props.print}
                />
        :""}
        </div>
    );
}


}

export default CreatePanel;