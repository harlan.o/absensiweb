import './css/dataTables.bootstrap4.min.css';
import './DataTable.css';
import React, {Component} from 'react';
import { Table, Card } from "react-bootstrap";
const $ =require('jquery')
$.DataTable = require('datatables.net-bs4')


export class DataTableDinasPadaLokasi extends Component {
    constructor(props){
        super(props);
        this.state={
            header: null,
            dataHTML: null,
        };
    }
    componentDidMount(){
        console.log(this.el)

        this.$el = $(this.el)
        // this.$el.style="asd"
        this.$el.DataTable(
            {
                bLengthChange : false,
                language: {
                    emptyTable:     "Data tidak ditemukan",
                    info:           " _START_ - _END_ : _TOTAL_ Data",
                    infoEmpty:      "Tampil 0 data",
                    lengthMenu:     "asd",
                    search:         "Cari:",
                    paginate: {
                        first:    '«',
                        previous: '‹',
                        next:     '›',
                        last:     '»'
                    },
                    aria: {
                        paginate: {
                            first:    'First',
                            previous: 'Previous',
                            next:     'Next',
                            last:     'Last'
                        }
                    }
                },
                // data: this.props.data,
                // columns:[
                //     { title:"NIP" },
                //     { title:"Name", className:"custom-fit" },
                //     { title:"Dinas" },
                //     { title:"Group" },
                //     { title:"Tipe Absen" },
                //     { title:"Waktu" },
                //     { title:"Ket" }
                // ],
     
                
            }
        )
        console.log(this.el);
    }

 
    render(){
        return (
            <Card className="customCard">
                 <Table ref={el => this.el = el} className="table table-striped table-hover custom-table text-nowrap  table-responsive">
                     <thead>
                         <tr> 
                             <th> NIP </th>
                             <th className="custom-fit" > Name </th>
                             <th> Dinas </th>
                             <th> Group </th>
                             <th> Tipe Absen </th>
                             <th> Waktu </th>
                             <th> Ket </th>
                         </tr>
                     </thead>
                     <tbody>
                        <tr>
                            <td> 1232321 </td>
                            <td> Garrett Winters </td>
                            <td> Dinas </td>
                            <td> Dinas Pariwisata Sitaro</td>
                            <td> Selesai Istirahat </td>
                            <td> 13:30 </td>
                            <td> Tepat waktu </td>
                        </tr>
                        <tr>
                            <td> 1232321 </td>
                            <td> Garrett Winters </td>
                            <td> Dinas </td>
                            <td> Dinas Pariwisata Sitaro </td>
                            <td> Mulai Istirahat </td>
                            <td> 12:30 </td>
                            <td> Tepat waktu </td>
                        </tr>
                        <tr>
                            <td> 1232321 </td>
                            <td> Garrett Winters </td>
                            <td> Dinas </td>
                            <td> Dinas Pariwisata Sitaro </td>
                            <td> Kedatangan </td>
                            <td> 06:45 </td>
                            <td> Tepat waktu </td>
                        </tr>
                        <tr>
                            <td> 1232325 </td>
                            <td> Colleen Hurst </td>
                            <td> Dinas </td>
                            <td> Dinas Pariwisata Sitaro </td>
                            <td> Kedatangan </td>
                            <td> 06:50 </td>
                            <td> Tepat waktu </td>
                        </tr>
                    </tbody>
                 </Table>
            </Card>
        );
    }
}


export default DataTableDinasPadaLokasi;