import React, { Component } from 'react';
import { Button, Card, Col, Form } from 'react-bootstrap';
import "./Filter.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import {  faMap, faPencilAlt, faPlus, faPrint } from '@fortawesome/free-solid-svg-icons';


class GroupButtonLokasiAbsensi extends Component {
  
     
render()
{
   
    return (
        <div className="custom-filter">
        <Button variant="secondary" className="btn-width"> <FontAwesomeIcon icon={faPlus} /></Button>{' '}
        <Button variant="secondary" className="btn-width" > <FontAwesomeIcon icon={faPencilAlt} /> </Button>{' '}
        <Button variant="secondary" className="btn-width" > <FontAwesomeIcon icon={faMap} /> </Button>{' '}
        </div>
    );
}


}

export default GroupButtonLokasiAbsensi;