import './css/dataTables.bootstrap4.min.css';
import './DataTable.css';
import React, {Component} from 'react';
import { ListGroup, Button } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTrash } from '@fortawesome/free-solid-svg-icons';


export class ListGroupDinasPadaLokasi extends Component {

 
    render(){
        return (
            <ListGroup>
            <ListGroup.Item className=" d-flex justify-content-between align-items-center">Dinas Pariwisata
            <span><Button variant="secondary" className="btn-width" size="sm"> <FontAwesomeIcon icon={faTrash} /> </Button></span></ListGroup.Item>
            <ListGroup.Item className=" d-flex justify-content-between align-items-center">Dinas Kesehatan <span><Button variant="secondary" className="btn-width" size="sm"> <FontAwesomeIcon icon={faTrash} /> </Button></span></ListGroup.Item>
            <ListGroup.Item className=" d-flex justify-content-between align-items-center">Dinas Komunikasi Dan Informatika <span><Button variant="secondary" className="btn-width" size="sm"> <FontAwesomeIcon icon={faTrash} /> </Button></span></ListGroup.Item>
            <ListGroup.Item className=" d-flex justify-content-between align-items-center">Porta ac consectetur ac <span><Button variant="secondary" className="btn-width" size="sm"> <FontAwesomeIcon icon={faTrash} /> </Button></span></ListGroup.Item>
            <ListGroup.Item className=" d-flex justify-content-between align-items-center">Vestibulum at eros <span><Button variant="secondary" className="btn-width" size="sm"> <FontAwesomeIcon icon={faTrash} /> </Button></span></ListGroup.Item>
          </ListGroup>
        );
    }
}


export default ListGroupDinasPadaLokasi;