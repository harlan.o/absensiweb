import React, { Component } from 'react';
import { Navbar, Nav } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faUser, faBars, faBell } from '@fortawesome/free-solid-svg-icons';
import {connect} from 'react-redux';
import { logout } from '../actions/userActions';
// import firebase from '../firebase/init-firebase.js';
// import "firebase/messaging";

class SidebarUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isToggleOnSidebar: true,
            isToggleOnUserBar: false,
            page: ""
        };
       this.handleClickShowSidebar = this.handleClickShowSidebar.bind(this);  
       this.handleClickShowUserBar = this.handleClickShowUserBar.bind(this);  
       this.logout = this.logout.bind(this);  

    //    this.firebaseApp = firebase.apps[0];
    //    this.messaging = firebase.messaging();
    }
    
    handleClickShowSidebar() {    
        this.setState(state => ({      
              isToggleOnSidebar: !state.isToggleOnSidebar
        }));
        this.props.onShowSidebar(this.state.isToggleOnSidebar);  
    }
    handleClickShowUserBar() {    
        this.setState(state => ({      
            isToggleOnUserBar: !state.isToggleOnUserBar
        }));  
    }

    logout =  (e) =>  {
        e.preventDefault();
        this.props.logout();
    };

    componentDidMount(){

        // this.messaging.requestPermission().then(() => {
        //   console.log('Have permision');
        //   return this.messaging.getToken();
        // }).then((token) => {
        //   console.log(token);
        // }).catch((err) => {
        //   console.log('Error Occured.')
        // })      
        // // this.messaging.onMessage()
        // console.log(this.firebaseApp)
        // this.messaging.onMessage( (payload) => { 
        //   console.log('onMessage: ', payload);
        // });      
    }

    render(){
        
        return(
            <Navbar variant="dark" className="sb-topnav navbar navbar-expand navbar-darkred bg-darkred">
                <Navbar.Brand href="#home">SIMONGO E-Absensi</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="btn-sm order-1 order-lg-0">
                        <Nav.Link href="#features" className="" onClick={this.handleClickShowSidebar} >
                            <FontAwesomeIcon icon={faBars} />
                            {/* <code>
        <pre>{JSON.stringify(this.firebaseApp.options, null, 2)}</pre>
      </code> */}
                        </Nav.Link>
                    </Nav>
                    <div className="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0"></div>
                    <Nav className="navbar-nav ml-auto ml-md-0">
                        <Nav.Item className="nav-link">
                            <FontAwesomeIcon  icon={faBell} />
                        </Nav.Item>
                        <Nav.Item className={this.state.isToggleOnUserBar ? 'dropdown show' : ' dropdown'}>
                            <Nav.Item href={null} className="nav-link dropdown-toggle" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded={this.state.isToggleOnUserBar ? 'true' : 'false'} onClick={this.handleClickShowUserBar}>
                            <FontAwesomeIcon icon={faUser} />
                            </Nav.Item>
                            <div className={this.state.isToggleOnUserBar ? 'dropdown-menu dropdown-menu-right show' : 'dropdown-menu dropdown-menu-right'} aria-labelledby="userDropdown">
                                <a className="dropdown-item" as={Link} to="/profil" href="/profil">Profile</a>
                                <a className="dropdown-item" as={Link} to="/log" href="/log">Activity Log</a>
                                <div className="dropdown-divider"></div>
                                <button className="dropdown-item"  onClick={this.logout}>Logout</button>
                            </div>
                        </Nav.Item>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
        );
    }
}



const mapStateToProps = (state) => {
    return{
        userDataProfile: state.userDataProfile
    } 
 }

 export default connect(mapStateToProps, {logout})(SidebarUser);