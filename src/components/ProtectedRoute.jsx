import React, { useState }  from "react";
import { useDispatch, useSelector } from 'react-redux';
import { Redirect, Route } from "react-router-dom";
import firebase from '../firebase/init-firebase.js';
import "firebase/database";
import { cekHakAksesClient } from "../actions/hakAksesActions.js";

const cekValidAkses = (arrH) => {
  var b = false
  // console.log("cek hak akses awal page", arrH)
  arrH?.forEach(function(v){
    // console.log("cek hak akses page", cekHakAksesClient(v))
   if (cekHakAksesClient(v)){
     b = true
   }
  })
  return b
  ;
}

const ProtectedRoute = (props) => {
  const userLogin = useSelector(state => state.userLogin);
  const { token } = userLogin;
  const [block, setBlock]= useState(false);
  const isLock= firebase.database().ref('isLock');
  isLock.on('value', (snp)=> {
    if(snp.val() !== block){
      setBlock(snp.val())
    }
    // console.log(snp.val())
  })
  
  if(block){
    return(
      <Redirect to={{ pathname: "/maintenance" }} />
    )
  }else{
    return !!token ? ( <>{cekValidAkses(props.kode_ha) == true ? (<Route {...props} />) : (<Redirect to={{ pathname: "/404" }} />) }</> ) :(<Redirect to={{ pathname: "/login" }} />)
  }

};

export default ProtectedRoute;
