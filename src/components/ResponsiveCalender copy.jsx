import React, { Component } from "react";
import "./ResponsiveCalenderStyle.css"
class ResponsiveCalender extends Component {
render(){
    return(
<table id="calendar">
<caption>August 2014</caption>
<tr className="weekdays">
    <th scope="col">Sunday</th>
    <th scope="col">Monday</th>
    <th scope="col">Tuesday</th>
    <th scope="col">Wednesday</th>
    <th scope="col">Thursday</th>
    <th scope="col">Friday</th>
    <th scope="col">Saturday</th>
  </tr>
  
  <tr className="days">
    <td className="day other-month">
      <div className="date">27</div>
    </td>
    <td className="day other-month">
      <div className="date">28</div>
      <div className="event">
        <div className="event-desc">
          HTML 5 lecture with Brad Traversy from Eduonix
        </div>
        <div className="event-time">
          1:00pm to 3:00pm
        </div>
      </div>
    </td>
    <td className="day other-month">
      <div className="date">29</div>
    </td>
    <td className="day other-month">
      <div className="date">30</div>
    </td>
    <td className="day other-month">
      <div className="date">31</div>
    </td>


    <td className="day">
      <div className="date">1</div>
    </td>
    <td className="day">
      <div className="date">2</div>
      <div className="event">
        <div className="event-desc">
          Career development @ Community College room #402
        </div>

        <div className="event-time">
          2:00pm to 5:00pm
        </div>
      </div>
      <div className="event">
        <div className="event-desc">
          Test event 2
        </div>

        <div className="event-time">
          5:00pm to 6:00pm
        </div>
      </div>
    </td>
  </tr>

  
</table>

    );
    }
}
export default ResponsiveCalender;