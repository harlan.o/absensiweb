import React, { Component } from "react";
import { monthName, padLeadingZero, padLeadingZeroNormal } from "../actions/genneralAction";
import "./ResponsiveCalenderStyle.css";

const moment = require('moment');

class ResponsiveCalender extends Component {
    constructor(props) {
        super(props); 
        this.state = {
          listAbsen:[],
          tahunBulan:null,
        };
    }

    componentDidMount(){
        this.setState(() => ({
            listAbsen:this.props.listAbsen,  
            tahunBulan:this.props.tahunBulan    
           })) 
    }

    week(){
        let w=0; let e=0;
        let day=this.day()
        let mingguArr=[]; let c=[];
        day.map((data, key)=>{
            c.push(data)
            w++; e++;
            if(w==7 || day.length==e){
                mingguArr.push(c); c=[];
                w=0;
            }
        })
        return mingguArr;
    }

    day(){
        let hariArr=[];
        let bulanTahun =moment(this.state.tahunBulan).format('YYYY-MM');
        // console.log(bulanTahun)
        let hariPertama= moment(bulanTahun).startOf("month").format("d")
        let totalTanggal=moment(bulanTahun).daysInMonth();
        for (let i = 0; i < hariPertama; i++) {
            hariArr.push("");
        }
        for (let d = 1; d <= totalTanggal; d++) {
            hariArr.push(d);
        }
        return hariArr
    }

    callenderDay(){
        let callender= this.week()
        // console.log(callender)
        // console.log(this.state.listAbsen)

        return callender.map((data, key)=>{
            // console.log(data)
            return(
                <tr className="days">
                    {data.map((d, k)=>{
                       let _tanggal= this.state.tahunBulan+"-"+padLeadingZeroNormal(d.toString())
                    //    console.log(_tanggal)
                        return(
                        <td className={d=="" ? "day other-month":"day"}>
                            {d=="" ? "":<div className="date ">{d}</div>}
                            {this.keteranganTanggal(_tanggal)}
                        </td>
                        )
                    })}
                </tr>
            )       
        })
    }

    keteranganTanggal(_tanggal){
        let vAbsen = this.state.listAbsen.find(tgl => {
            return tgl.tanggal === _tanggal
        });
        if(vAbsen==undefined){
            return;
        }
        // console.log(vAbsen)
        let status= vAbsen.status.split(" - ")
          switch (status[0]) {
                case "HADIR":
                    if(status[1] === "TERLAMBAT"){
                        return(
                            <div className="event terlambat">
                                <div className="event-desc">
                                <b>Terlambat</b> 
                                </div>
                                <div className="event-time">
                                {vAbsen.jamMasuk != null ? padLeadingZero(vAbsen.jamMasuk):"-"} {" / "}
                                {vAbsen.jamPulang != null ? padLeadingZero(vAbsen.jamPulang):"-"}
                                </div>
                            </div>
                        );
                    }else{
                        return(
                            <div className="event hadir">
                                <div className="event-desc">
                               <b>Hadir</b> 
                                </div>
                                <div className="event-time">
                                {vAbsen.jamMasuk != null ? padLeadingZero(vAbsen.jamMasuk):"-"} {" / "}
                                {vAbsen.jamPulang != null ? padLeadingZero(vAbsen.jamPulang):"-"}
                                </div>
                            </div>
                        );
                    }
                case "IJIN":
                    return(
                        <div className="event ijin">
                                <div className="event-desc">
                                <b>{vAbsen.status}</b> 
                                </div>
                            </div>
                    )
                case "TIDAK HADIR":
                    return(
                        <div className="event tanpa-keterangan">
                                <div className="event-desc">
                                <b>Tidak Hadir</b> 
                                </div>
                            </div>
                    )
              default:
                  break;
          }
    }
    render(){
    this.week()
        return(
    <table id="calendar" >
        <caption>{monthName(moment(this.state.tahunBulan).format('MM'))+' '+moment(this.state.tahunBulan).format('YYYY')}</caption>
        <tr className="weekdays">
            <th scope="col">Minggu</th>
            <th scope="col">Senin</th>
            <th scope="col">Selasa</th>
            <th scope="col">Rabu</th>
            <th scope="col">Kamis</th>
            <th scope="col">Jumat</th>
            <th scope="col">Sabtu</th>
        </tr>
        {this.callenderDay()}

    </table>

    );
    }
}
export default ResponsiveCalender;