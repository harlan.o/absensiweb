import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { Media, Nav} from 'react-bootstrap';
import {connect} from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTachometerAlt, faUserClock, faFile, faChartArea, faBookOpen, faCog, faNewspaper, faCalendar, faFlag, faBriefcase } from '@fortawesome/free-solid-svg-icons'
import LogoKab from '../img/logo-kabupaten.png';
import { cekHakAksesClient } from '../actions/hakAksesActions';
import { REGISTER_FU005, REGISTER_FU007, REGISTER_FU009, REGISTER_FU011, REGISTER_FU015, REGISTER_FU020, REGISTER_FU025, REGISTER_FU027, REGISTER_FU030, REGISTER_FU032, REGISTER_FU034, REGISTER_FU039, REGISTER_FU044, REGISTER_FU048, REGISTER_FU053, REGISTER_FU057, REGISTER_FU063, REGISTER_FU067, REGISTER_FU071 } from '../constants/hakAksesConstants';
// const checkActive = (match, location) => {
//     //some additional logic to verify you are in the home URI
//     if(!location) return false;
//     const {pathname} = location;
//     console.log(pathname);
//     return pathname === "/";
// }
class SidebarUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isToggleOnSidebar: true,
            page: "",
            sub_page: "",
            userLogin:{
                nama:"",
                jabatan:"",
                unit_kerja:"",
            },
        };

    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.userDataProfile !== prevProps.userDataProfile){
            if(this.props.userDataProfile?.profileUser !== undefined) 
            // console.log(this.props.userDataProfile?.profileUser);
            this.setState(() => ({      
                userLogin: this.props.userDataProfile?.profileUser
            }));
        }
    }

render(){
    // let match = useRouteMatch({
    //     path: to,
    //     exact: activeOnlyWhenExact
    //   });
    return(
        <div id="layoutSidenav_nav">
                <nav className="sb-sidenav accordion sb-sidenav-darkred" id="sidenavAccordion">
                <a className="navbar-brand-sidebar" href="index.html">SIMONGO <br/>E-Absensi</a>
                <hr/>
                {/* <a className="navbar-brand-sidebar" href="index.html">Absensi</a> */}
                    <div className="sb-sidenav-menu">
                        <Nav>
                            {cekHakAksesClient(REGISTER_FU005) ? (
                            <Nav.Link  as={NavLink} to="/dashboard" href="/dashboard"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faTachometerAlt} />
                                </div>
                                Dashboard
                            </Nav.Link>
                            ):""} 
                            {cekHakAksesClient(REGISTER_FU007) ? (
                            <Nav.Link as={NavLink} to="/statistik" href="/statistik"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faChartArea} />
                                </div>
                                Statistik
                            </Nav.Link>
                            ):""}
                            {cekHakAksesClient(REGISTER_FU009) ? ( 
                            <Nav.Link as={NavLink} to="/absensi-pegawai" href="/absensi-pegawai"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faUserClock} />
                                </div>
                                Absensi Pegawai
                            </Nav.Link>
                            ):""}
                            {cekHakAksesClient(REGISTER_FU011) ? ( 
                            <Nav.Link as={NavLink} to="/pengambilan-ijin" href="/pengambilan-ijin"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faFile} />
                                </div>
                                {/* Dokumen Surat/ */}
                                 Pengambilan Ijin
                            </Nav.Link>
                            ):""}
                            {cekHakAksesClient(REGISTER_FU015) || cekHakAksesClient(REGISTER_FU020) ? (
                            <Nav.Link as={NavLink} to="/penugasan" href="/penugasan"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faBriefcase} />
                                </div>
                                 Penugasan
                            </Nav.Link>
                            ):""}
                            {cekHakAksesClient(REGISTER_FU025) ? (
                            <Nav.Link as={NavLink} to="/laporan-absen" href="/laporan-absen"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faBookOpen} />
                                </div>
                                Laporan Absen
                            </Nav.Link>
                            ):""}
                            {cekHakAksesClient( REGISTER_FU027) ? (
                            <Nav.Link as={NavLink} to="/laporan-tpp" href="/laporan-tpp"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faBookOpen} />
                                </div>
                                Laporan TPP
                            </Nav.Link>
                            ):""}
                            {cekHakAksesClient(REGISTER_FU030) || cekHakAksesClient(REGISTER_FU032)  ? (
                            <Nav.Link as={NavLink} to="/laporan-akhir" href="/laporan-akhir"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faBookOpen} />
                                </div>
                                Laporan Akhir
                            </Nav.Link>
                            ):""}
                            {cekHakAksesClient(REGISTER_FU034) ? (
                            <Nav.Link as={NavLink} to="/artikel" href="/artikel"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faNewspaper} />
                                </div>
                                Artikel
                            </Nav.Link>
                            ):""}
                            {cekHakAksesClient(REGISTER_FU039) ? (
                            <Nav.Link as={NavLink} to="/even" href="/even"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faFlag} />
                                </div>
                                Even
                            </Nav.Link>
                            ):""}
                            {cekHakAksesClient(REGISTER_FU044) || cekHakAksesClient(REGISTER_FU048) || cekHakAksesClient(REGISTER_FU053) ? (
                            <Nav.Link as={NavLink} to="/schedule" href="/schedule"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faCalendar} />
                                </div>
                                Schedule
                            </Nav.Link>
                            ):""}
                            {cekHakAksesClient(REGISTER_FU057) || cekHakAksesClient(REGISTER_FU063) || cekHakAksesClient(REGISTER_FU067) || cekHakAksesClient(REGISTER_FU071) ? (
                            <Nav.Link as={NavLink} to="/pengaturan" href="/pengaturan"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faCog} />
                                </div>
                                Pengaturan
                            </Nav.Link>
                            ):""}
                        </Nav>
                    </div>
                    
                    <div className="sb-sidenav-footer">
                    <hr/>
                        <Media>
                    <img src={LogoKab} width={64} height={64} className="align-self-center mr-3" alt="Logo"/>
                    <Media.Body>
                    Pemerintah Kabupaten Kepulauan
                     Siau Tagulandang Biaro

                    </Media.Body>
                    </Media>
                        <hr/>
                        <div className="small">Masuk sebagai:</div>
                       {this.props.userDataProfile?.profileUser?.nama}<br/>
                        <div className="small">
                        {this.props.userDataProfile?.profileUser?.jabatan}

                        </div>
                    </div>
                </nav>
            </div>

    );
}
}


const mapStateToProps = (state) => {
    return{
        userDataProfile: state.userDataProfile
    } 
 }
export default connect(mapStateToProps,null)(SidebarUser);