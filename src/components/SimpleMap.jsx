import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
 
const AnyReactComponent = ({ text }) => <div>{text}</div>;
 
class SimpleMap extends Component {
  static defaultProps = {
    center: {
      lat: 2.342731,
      lng: 125.406881
    },
    zoom: 14
  };
  _onChildClick = (key) => {
    // alert(key)
    console.log(key)
    // this.props.onCenterChange([childProps.lat, childProps.lng]);
  }
  render() {
    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '100vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyB8L7AFUamts-nbU2tuFK4LkfDykESmCoM" }}
          yesIWantToUseGoogleMapApiInternals
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
            // center={cordinates.center}
            onClick={this._onChildClick}
        >
          {/* <AnyReactComponent
            lat={2.342731}
            lng={125.406881}
            text="My Marker"
            onChildClick={this._onChildClick}
          /> */}
          {/* <AnyReactComponent onClick={this.onMarkerClick}
                name={'Current location'} /> */}
        </GoogleMapReact>
      </div>
    );
  }
}
 
export default SimpleMap;