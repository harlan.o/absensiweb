import React, { Component } from "react";
import { Table, Card } from "react-bootstrap";
import "./TabelAbsensiPegawai.css";

class TableAbsensiPegawai extends Component {
  constructor(props) {
    super(props); //since we are extending class Table so we have to use super in order to overrnipe Component class constructor
    this.state = {
      //state is by default an object
      students: [
        {"no":"1","nip":"1223231233", "name":"Harlan","dinas":"Dinas Pendapatan Daerah", "group":"Divisi Humas",  "waktu":"10.05", "ket":"Terlambat" },
        {"no":"2","nip":"1223231234", "name":"Harlan","dinas":"Dinas Pendapatan Daerah", "group":"Divisi Humas",  "waktu":"10.05", "ket":"Terlambat" },
        {"no":"3","nip":"1223231235", "name":"Harlan","dinas":"Dinas Pendapatan Daerah", "group":"Divisi Humas",  "waktu":"10.05", "ket":"Terlambat" },
        {"no":"4","nip":"1223231236", "name":"Harlan","dinas":"Dinas Pendapatan Daerah", "group":"Divisi Humas",  "waktu":"10.05", "ket":"Terlambat" },
        {"no":"5","nip":"1223231237", "name":"Harlan","dinas":"Dinas Pendapatan Daerah", "group":"Divisi Humas",  "waktu":"10.05", "ket":"Terlambat" },
        {"no":"6","nip":"1223231238", "name":"Harlan","dinas":"Dinas Pendapatan Daerah", "group":"Divisi Humas",  "waktu":"10.05", "ket":"Terlambat" },
        {"no":"7","nip":"1223231239", "name":"Harlan","dinas":"Dinas Pendapatan Daerah", "group":"Divisi Humas",  "waktu":"10.05", "ket":"Terlambat" }
      ],
    };
  }

  renderTableData() {
    return this.state.students.map((student, index) => {
      const { no, nip, name, dinas, group,  waktu, ket } = student; //destructuring
      return (
        <tr key={nip}>
          <td>{no}</td>
          <td>{nip}</td>
          <td>{name}</td>
          <td>{dinas}</td>
          <td>{group}</td>
          <td>{waktu}</td>
          <td>{ket}</td>
        </tr>
      );
    });
  }

  renderTableHeader() {
    let header = Object.keys(this.state.students[0]);
    return header.map((key, index) => {
      return <th key={index}>{key.toUpperCase()}</th>;
    });
  }

  render() {
    return (

    <Card className="mb-4" bg="dark" text="white">  
        <Card.Header as="h5">Pegawai</Card.Header>
        <Card.Body>
            <Table responsive className="table table-striped table-hover custom-table2 text-nowrap">
                <thead>
                <tr>{this.renderTableHeader()}</tr>
                </thead>
                <tbody >
                    {this.renderTableData()}
                </tbody>
            </Table>
                
        </Card.Body>
    </Card>
    );
  }
}

export default TableAbsensiPegawai;