import React, { Component, Fragment } from "react";
import { Button, Table, Card, Col, Form, Row, Spinner } from "react-bootstrap";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import "./TabelDefault.css";
import { faImage, faMap } from "@fortawesome/free-solid-svg-icons";
import { openImage, openMaps, tanggalHariIndo } from "../../actions/genneralAction";
const moment = require('moment');

class AbsensiPegawaiTabelHadir extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      totalData:0,
        search:'',
        rowData:null,
        loading:true
    };
    this.renderTableData=this.renderTableData.bind(this);    
  }
  
  async callApi(){
    if(this.props.dataRow !== null && this.props.dataRow !== undefined){
      let dataCombin=this.props.dataRow;
      await this.setState(() => ({
       rowData:dataCombin,  
       loading:false    
      })) 
    }
  }

  componentDidMount(){
    if(this.state.loading !== false){
      this.callApi();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.getFilter !== this.props.getFilter || prevProps.dataRow !== this.props.dataRow){
      this.callApi();
    }
  }
    
  renderTableData(){
    if(this.state.loading !== true && this.state.rowData !== undefined && this.state.rowData !== null){
      let datashow = this.state.rowData.filter( rowData =>{
        if(rowData.nama.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1 || rowData.nama.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1){
          return true
        }
        if(rowData.nip.indexOf( this.state.search.toLowerCase() ) !== -1 || rowData.nip.indexOf( this.state.search.toLowerCase() ) !== -1){
          return true
        }
        return false
      });

      return datashow.map( (data, key) => {
        
        let no= key+1;
        this.count=no;
        return(
          <tr key={key}>
              <td>{no}</td>
              <td>{data.nama} <br/><small> {data.nip}</small><br/>
              {data.unitInduk}<br/> <small>{data.unitKerja}</small> </td>
              <td><p>
                  {data.absen ? this.listAbsen(data.absen):"-"}
              </p>
                 
              </td>
          </tr>
        )
      }     
      );
    }else{
      return(
        <tr>
          <td colSpan="4" className="loadingPage2">
            <Spinner animation="border" className="spin"  variant="light" />
          </td>
        </tr>
      )
    }
  }

  listAbsen(absen){
    let tglSebelum;
    return absen.map((data, key) => {
      let dateAbsen = moment(data.waktu);
      let tgl= dateAbsen.format('YYYY-MM-DD');
      if(tglSebelum != tgl){
        tglSebelum = tgl 
        return(<Fragment key={key}><br/>
         {/* {tgl} <br/>  */}

         <Button variant={data.keterangan == "MASUK_TERLAMBAT" ? "danger": "secondary"} disabled={data.fotoAbsen != null ? false : true}  className="btn-width mb-1" size="sm" onClick={ 
          (e)=>{
            e.preventDefault();
            openImage(data.fotoAbsen)
          }  
        } >
         <FontAwesomeIcon icon={faImage} /> </Button>

         {' '} <Button variant={data.keterangan == "MASUK_TERLAMBAT" ? "danger": "secondary"} disabled={data.geolocation != null ? false : true}  className="btn-width mb-1" size="sm" onClick={ 
          (e)=>{
            e.preventDefault();
            openMaps(data.geolocation)
          }  
        } > {dateAbsen.format(' HH:mm')} (<small>{data.type}</small>){' '} 
         <FontAwesomeIcon icon={faMap} /> </Button>{' '} 
         
         
         
          <br/></Fragment>)
      }else{
        return(<Fragment key={key}>
          <Button variant={data.keterangan == "MASUK_TERLAMBAT" ? "danger": "secondary"} disabled={data.fotoAbsen != null ? false : true}  className="btn-width mb-1" size="sm" onClick={ 
          (e)=>{
            e.preventDefault();
            openImage(data.fotoAbsen)
          }  
        } >
         <FontAwesomeIcon icon={faImage} /> </Button>
         
          {' '} <Button disabled={data.geolocation != null ? false : true} variant={data.keterangan == "MASUK_TERLAMBAT" ? "danger": "secondary"} className="btn-width" size="sm" onClick={ 
          (e)=>{
            e.preventDefault();
            openMaps(data.geolocation)
          }  
        } >
          {' '}{dateAbsen.format(' H:mm')} (<small>{data.type}</small>){' '}<FontAwesomeIcon icon={faMap} /></Button>
          
        
         
         </Fragment>)
      }

    });
  }

  render() {
    // console.log("render table");
    return (

    <Card className="mb-4" bg="dark" text="white">  
        <Card.Header>
        <Row>
        <Col xs={6} md={4}>
              <h2 className="title-table"><label>HADIR</label> <small>{this.props.title}</small></h2>
            </Col>
            <Col xs={6} md={{ span: 4, offset: 4 }}>
              <Form.Row className="align-items-center">
                <Form.Label  column xs="auto">
                  Cari
                </Form.Label>
                <Col>
                  <Form.Control type="text" placeholder="Cari Nama/NIP Pegawai" onChange={v => this.setState(({search: v.target.value }))}/>
                </Col>
              </Form.Row>
            </Col> 
            </Row>
        </Card.Header>
        <Card.Body>
            <Table responsive className="table table-striped table-hover custom-table3 text-nowrap">
            <tbody>
                <tr>
                  <th>No.</th>
                  <th className="colomInti">Pegawai</th>
                  <th>Waktu</th>
                </tr>
                    {this.renderTableData()}
                </tbody>
            </Table>
            
        </Card.Body>
    </Card>
    );
  }
}




export default AbsensiPegawaiTabelHadir;