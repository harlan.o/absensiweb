import React, {Component} from 'react';
import { Fragment } from 'react';
import { Form, Col } from 'react-bootstrap';
import ThumArtikel from './ThumArtikel';


export class DaftarArtikel extends Component {
    constructor(props){
        super(props);
        this.state={
            dataRow:null,
        };
    }

    componentDidMount(){
        this.setState(state => ({      
            dataRow:this.props.dataRow
        }));
    }
    componentDidUpdate(prevProps){
        if(this.props.dataRow !== prevProps.dataRow){
            this.setState(state => ({      
                dataRow:this.props.dataRow
            }));
            // console.log("update")
        }
    }
    render(){
        // console.log(this.state.dataRow)
        return (
            <Fragment>
                <div className="row">
                    {
                        this.state.dataRow ?
                        this.state.dataRow.map((data, key) => {
                            return (
                                <div className="col-6 mb-3" key={key}>
                                    <ThumArtikel dataArtikel={data} dataonerow={this.getDataOneRow}/>
                                </div>
                                );
                            }) : ""
                    }   
                </div>
            </Fragment>
        )
    }
    getDataOneRow = (v, s) => {
        // console.log(v);
        this.props.getdataedit(v, s);
    }
}

export default DaftarArtikel;