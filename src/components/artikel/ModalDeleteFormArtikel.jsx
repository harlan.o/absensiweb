import React, { Component } from 'react';
import { Alert, Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import './ModalFormArtikel.css';
// Redux
import {connect} from 'react-redux';
import { deleteArtikel} from "../../actions/artikelAction";

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

class ModalDeleteFormArtikel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"Harap Tunggu",
            tujuanArtikel:"",
           data:{ 
            id:null,
            judul: "",
            kontenSingkat: "ss",
            konten: "ss",
            unitKerjaId:null,
            namaUnitKerja:"testing unit kerja fix jadwal",
            unitIndukId:null,
            namaUnitInduk:"",
            kirimNotifikasi: false,
        },
        edit:false,
        };
        this.MySwal = withReactContent(Swal);
    }
    async update(dataOne){
        await this.setState(({       
            data:dataOne,
            judul:"Hapus Artikel",
            edit:true,
        }));
    }
    componentDidMount(){
        if(this.props.data != null){
            this.update(this.props.data)
        }
    }


    render (){
        return (
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          enforceFocus={false}
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Body>
                      <h1>Perhatian!</h1>
                      <h2>
                      <small>Apakah Yakin ingin menghapus </small>
                            "{this.state.data.judul}" ?</h2>

                  </Card.Body>
              </Card>
        
                  </Form>
              </Modal.Body>
          <Modal.Footer>
              
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleFormArtikel} >
                  Ya
              </Button>
          </Modal.Footer>
        </Modal>
      );
    }
        
    handleFormArtikel = (e) => {
        e.preventDefault();
        // console.log(this.state.data)
        let info="";
        let nVerif=0;
        if(this.state.data.judul === "" || this.state.data.judul === null){
            info+="Judul belum terisi <br/>";
            nVerif=1;
        }

        if(nVerif===1){
            this.MySwal.fire({
                icon: "error",
                title: info,
            });
        }else if(this.state.data.id !== null && nVerif!==1){
            this.props.deleteArtikel(this.state.data).then(()=>{
                const {success} = this.props.infoDeleteArtikel;
                if(success){
                    this.MySwal.fire({
                        icon: "success",
                        title: this.state.data.judul+" berhasil dihapus",
                      });
                    }
                    this.props.onHide();
                });
        }
    }

}

const mapStateToProps = (state) => {
    return{
        infoDeleteArtikel: state.infoDeleteArtikel,
        userDataProfile: state.userDataProfile
    } 
 }
export default connect(mapStateToProps, {deleteArtikel})(ModalDeleteFormArtikel);
// export default ModalFormArtikel;