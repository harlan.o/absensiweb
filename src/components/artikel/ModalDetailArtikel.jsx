import React, { Component } from 'react';
import { Alert, Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';

// import './ModalDetailArtikel.css';
// Redux
import {connect} from 'react-redux';
import { notificationArtikel } from "../../actions/artikelAction";
// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

// CKeditor 5

const moment = require('moment');

class ModalDetailArtikel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"Tambah Artikel",
           data:{ 
            id:null,
            judul: "",
            modifiedDate: "",
            publishedBy: "",
            konten: "ss",
            unitKerjaId:"",
        },
        valueOptionUnitKerja:{
            id:"",
            nama:""
        },
        edit:false,
        // dataoptionUnitKerja:this.props.dataoptionunitkerja,
        
        };
        this.MySwal = withReactContent(Swal);
    }
    async update(dataOne) {    
        console.log(dataOne)
        await this.setState(({       
            data:dataOne,
            // valueOptionUnitKerja:{
            //     id:dataOne.unitKerjaId,
            //     nama:dataOne.unitKerja
            // },
            judul:"Ubah Data Artikel",
            edit:true,
        }));
    }
    componentDidMount(){
        if(this.props.dataedit != null){
            this.update(this.props.dataedit)
        }
    }


    render (){
        let mDate = moment(this.state.data?.modifiedDate);

        let subTitle;
        if(this.props.dataArtikel?.namaUnitKerja){
            subTitle=this.props.dataArtikel?.namaUnitKerja;
        }else if(this.props.dataArtikel?.namaUnitInduk){
            subTitle=this.props.dataArtikel?.namaUnitInduk;
        }else{
            subTitle="Untuk Semua Unit";
        }
        return (
           
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          enforceFocus={false}
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            Detail Artikel
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Header>
                      <h4>{this.state.data.judul}asd</h4>
                      <Card.Subtitle className="mb-2 text-muted"><small>{subTitle}</small></Card.Subtitle>
                  </Card.Header>
                  <Card.Body>
                  
                  <span dangerouslySetInnerHTML={ {__html: this.state.data?.konten} } />
                  
                  <footer className="blockquote-footer text-right">
                    {mDate.format('YYYY-MM-DD HH:mm')} - <cite title="Source Title">{this.state.data?.publishedBy}</cite>
                    </footer>
                  </Card.Body>
              </Card>
        
                  </Form>
               
              </Modal.Body>
          <Modal.Footer>
            <Button variant="primary" className="btn-them-red" onClick={this.handleFormPushNotification} >
                  Kirim Notifikasi
              </Button>
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
          </Modal.Footer>
        </Modal>

      );
    }
        

    handleFormPushNotification = (e) => {
        e.preventDefault();
        let tujuanArtikel;
            if(this.props.dataArtikel?.unitKerjaId){
                tujuanArtikel="khususUnitKerja";
            }else if(this.props.dataArtikel?.unitIndukId){
                tujuanArtikel="khususUnitInduk";
            }else{
                tujuanArtikel="semuaUnit";
            }
            this.props.notificationArtikel(this.state.data, tujuanArtikel, this.props.userDataProfile).then(()=>{
                const {success} = this.props.infoCreateArtikel;
                // console.log(success);
                if(success){
                    this.MySwal.fire({
                        icon: "success",
                        title: this.state.data.judul+" berhasil dikirim",
                      });
                    }
                    this.props.onHide();
                });
    }

}


const mapStateToProps = (state) => {
    return{
        infoCreateArtikel: state.infoCreateArtikel,
        pushNotificationArtikel: state.pushNotificationArtikel
    } 
 }

export default connect(mapStateToProps, {notificationArtikel})(ModalDetailArtikel);