import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import { Alert, Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import SelectUnitKerja from '../unit-kerja/SelectUnitKerja';
import SelectUnitInduk from '../unit-induk/SelectUnitInduk';
import './ModalFormArtikel.css';
// Redux
import {connect} from 'react-redux';
import { createArtikel, updateArtikel } from "../../actions/artikelAction";

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

// CKeditor 5
import { CKEditor } from '@ckeditor/ckeditor5-react';
import ClassicEditor from '@ckeditor/ckeditor5-build-classic';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU001 } from '../../constants/hakAksesConstants';
import SelectSingleUnitInduk_IdSend from '../unit-induk/SelectSingleUnitInduk_IdSend';
import { uIIDP } from '../../actions/userActions';
// import ImageInsert from '@ckeditor/ckeditor5-image/src/imageinsert';

class ModalFormArtikel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"Tambah Artikel",
            tujuanArtikel:"",
            dataoptionUnitKerja:this.props.dataoptionunitkerja,
            dataoptionUnitInduk:this.props.dataoptionunitinduk,
           data:{ 
            id:null,
            judul: "",
            // tanggalBuat: "",
            // tanggalBerakhir: new Date(),
            kontenSingkat: "ss",
            konten: "ss",
            unitKerjaId:null,
            namaUnitKerja:"testing unit kerja fix jadwal",
            unitIndukId:null,
            namaUnitInduk:"",
            kirimNotifikasi: false,
        },
        valueOptionUnitKerja:{
            id:0,
            nama:""
        },
        valueOptionUnitInduk:uIIDP(),
        listUnitKerjaView:[],
        edit:false,
        // dataoptionUnitKerja:this.props.dataoptionunitkerja,
        
        };
        this.MySwal = withReactContent(Swal);
        console.log("induk",this.props.dataoptionunitinduk)
        console.log("kerja",this.props.dataoptionunitkerja)
    }
    async update(dataOne) {    
        // console.log(dataOne)
        // console.log(dataOne.unitIndukId )
        let _tujuanArtikel;
        switch (!null) {
            case dataOne?.unitIndukId !== null:
                _tujuanArtikel="khususUnitInduk";
                break;
            case dataOne?.unitKerjaId !== null:
                _tujuanArtikel="khususUnitKerja";
                break;
            default:
                _tujuanArtikel="semuaUnit";
                break;
        }
      
        // console.log(_tujuanArtikel)
        await this.setState(({       
            data:dataOne,
            tujuanArtikel:_tujuanArtikel,
            valueOptionUnitKerja:{
                id:dataOne.unitKerjaId,
                nama:dataOne.namaUnitKerja
            },
            valueOptionUnitInduk:{
                id:dataOne.unitIndukId,
                nama:dataOne.namaUnitInduk
            },
            judul:"Ubah Data Artikel",
            edit:true,
        }));
    }
    componentDidMount(){
        if(this.props.dataedit != null){
            this.update(this.props.dataedit)
        }
        this.dataUnitKerjaList(this.state.valueOptionUnitInduk)
    }

    dataUnitKerjaList(v) {
          let unitKerjaKecil= this.state.dataoptionUnitKerja.filter( (data, key) => {
            if(data !== undefined){
              if( data.unitInduk?.id == v && (v!==null) ){
                return true;
              }
            }
          });
          console.log("filter kerja", unitKerjaKecil)
          this.setState({ listUnitKerjaView : unitKerjaKecil})
      }

    render (){
        console.log(this.state.dataoptionUnitInduk);
        return (
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
          enforceFocus={false}
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Body>
                      <Form.Row>
                          <Form.Group as={Col} controlId="form_judul">
                          <Form.Label className="textblack">Judul*</Form.Label>
                          <Form.Control type="text" placeholder="Masukan Judul" value={this.state.data.judul} onChange={v => this.setState(prevState => ({data:{...prevState.data, judul : v.target.value}}))}/>
                          </Form.Group>
  
                      </Form.Row>

                      <Form.Group>
                          <Form.Label className="textblack">Tujuan Artikel*</Form.Label>
                          <div>
                              {cekHakAksesClient(REGISTER_FU001) ? 
                              <Form.Check
                                  inline
                                  checked={this.state.tujuanArtikel == "semuaUnit"}  
                                  name="form_tujuan_artikel"
                                  label="Semua Unit"
                                  type="radio"
                                  onChange={v => this.setState({tujuanArtikel : "semuaUnit"})}
                              />
                              
                            : ""}
                              <Form.Check
                                  inline
                                  checked={this.state.tujuanArtikel == "khususUnitInduk"} 
                                  name="form_tujuan_artikel"
                                  label="Unit Induk Khusus"
                                  type="radio"
                                  onChange={v => this.setState({tujuanArtikel : "khususUnitInduk"})}
                              />
                              <Form.Check
                                  inline
                                  checked={this.state.tujuanArtikel == "khususUnitKerja"} 
                                  name="form_tujuan_artikel"
                                  label="Unit Kerja Khusus"
                                  type="radio"
                                  onChange={v => this.setState({tujuanArtikel : "khususUnitKerja"})}
                              />
                          </div>
                      </Form.Group>
           
                      {this.state.tujuanArtikel==="khususUnitKerja" ? 
                        <Card bg="secondary">
                            <Card.Body>
                            <Form.Row>
                                    <Form.Group as={Col} controlId="form_id">
                                    <Form.Label className="textblack">Unit Induk*</Form.Label>
                                    <SelectSingleUnitInduk_IdSend isDisabled={!cekHakAksesClient(REGISTER_FU001)} dataoption={this.state.dataoptionUnitInduk != null && this.state.dataoptionUnitInduk.length > 0 ? this.state.dataoptionUnitInduk : []} valueOption={this.state.valueOptionUnitInduk} valueGetSingle={v => 
                                        {
                                            if (cekHakAksesClient(REGISTER_FU001)){    
                                                this.setState(({valueOptionUnitInduk : v.id }))
                                            }
                                        }
                                        }/>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_id">
                                    <Form.Label className="textblack">Unit Kerja*</Form.Label>
                                    <SelectUnitKerja dataoption={this.state.listUnitKerjaView} valueOption={this.state.valueOptionUnitKerja} valueGet={v => this.setState(prevState => ({data:{...prevState.data, unitKerjaId : v.id}, valueOptionUnitKerja:{id:v.id, nama:v.nama}}))}/>
                                    </Form.Group>
                                </Form.Row>
                            </Card.Body>
                        </Card>
                        : ""}
                        {(this.state.tujuanArtikel==="khususUnitInduk") ? 
                        <Card bg="secondary">
                            <Card.Body>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_id">
                                    <Form.Label className="textblack">Unit Induk*</Form.Label>
                                    <SelectSingleUnitInduk_IdSend isDisabled={!cekHakAksesClient(REGISTER_FU001)} dataoption={this.state.dataoptionUnitInduk != null && this.state.dataoptionUnitInduk.length > 0 ? this.state.dataoptionUnitInduk : []} valueOption={this.state.valueOptionUnitInduk} valueGetSingle={v => 
                                        {
                                            if (cekHakAksesClient(REGISTER_FU001)){    
                                                this.setState(prevState => ({data:{...prevState.data, unitIndukId : v.id}, valueOptionUnitInduk : v.id }))
                                            }
                                        }
                                        }/>
                                    </Form.Group>
                                </Form.Row>
                            </Card.Body>
                        </Card>
                        : ""}


                      {/* <Form.Row>
                          <Form.Group as={Col} controlId="form_tglBerakhir">
                          <Form.Label className="textblack">Tanggal Berakhir</Form.Label>
                              <div>
                                <DatePicker         
                                    className="form-control"
                                    selected={new Date(this.state.data.tanggalBerakhir)}
                                    dateFormat="yyyy-MM-dd"
                                    onChange={v => this.setState(prevState => ({data:{...prevState.data, tanggalBerakhir : v}}))}
                                    placeholderText="Pilih Tanggal"
                                />
                              </div>

                          </Form.Group>
                      </Form.Row> */}
                      {/* <Form.Row>
                          <Form.Group as={Col} controlId="form_unitKerja">
                          <Form.Label className="textblack">Unit Keqwerja*</Form.Label>
                          
                          <SelectUnitKerja dataoption={this.state.dataoptionUnitKerja} valueOption={this.state.valueOptionUnitKerja} valueGet={v => this.setState(prevState => ({data:{...prevState.data, unitKerjaId : v.id}, valueOptionUnitKerja:{id:v.id, nama:v.nama}}))}/>
                          </Form.Group>
                      </Form.Row> */}
                      <Form.Row>
                          <Form.Group as={Col} controlId="form_id_tpp">
                          <Form.Label className="textblack">Konten Singkat*</Form.Label>
                          <CKEditor 
                                config={ {
                                    // toolbar: [ 'bold', 'italic', 'link', 'undo', 'redo', 'numberedList', 'bulletedList' ]
                                    // toolbar: [ 'paragraph','bold', 'italic', 'link', 'undo', 'redo', 'numberedList', 'bulletedList', 'media' ]
                                    toolbar: [
                                        'heading', '|',
                                        'bold', 'italic',  'link', '|',
                                         'blockQuote', '|',
                                        'undo', 'redo'
                                    ],
                                    // plugins: [ Paragraph, Bold, Italic, Essentials ],
                                    // toolbar: [ 'bold', 'italic' ]
                                } }
                                editor={ ClassicEditor }
                                data={this.props.dataedit?.kontenSingkat}
                                onReady={ editor => {
                                    // You can store the "editor" and use when it is needed.
                                    // console.log( 'Editor is ready to use!', this.state.data.konten );
                                } }
                                onChange={ ( event, editor ) => {
                                    const data = editor.getData();
                                    this.setState(prevState => ({data:{...prevState.data, kontenSingkat : data}}))
                                    // console.log( { event, editor, data } );
                                    // console.log( { data } );
                                } }
                                onBlur={ ( event, editor ) => {
                                    // console.log( 'Blur.', editor );
                                } }
                                onFocus={ ( event, editor ) => {
                                    // console.log( 'Focus.', editor );
                                } }
                            />
                          </Form.Group>
                      </Form.Row>

                      <Form.Row>
                          <Form.Group as={Col} controlId="form_id_tpp">
                          <Form.Label className="textblack">Konten*</Form.Label>
                          <CKEditor 
                                config={ {
                                    // toolbar: [ 'bold', 'italic', 'link', 'undo', 'redo', 'numberedList', 'bulletedList' ]
                                    // toolbar: [ 'paragraph','bold', 'italic', 'link', 'undo', 'redo', 'numberedList', 'bulletedList', 'media' ]
                                    toolbar: [
                                        'heading', '|',
                                        'bold', 'italic',  'link', '|',
                                        'outdent', 'indent', '|',
                                        'bulletedList', 'numberedList',  '|', 'imageInsert', '|',
                                         'blockQuote', '|',
                                        'undo', 'redo'
                                    ],
                                    // plugins: [ ImageInsert ],
                                    // plugins: [ Paragraph, Bold, Italic, Essentials ],
                                    // toolbar: [ 'bold', 'italic' ]
                                } }
                                editor={ ClassicEditor }
                                data={this.props.dataedit?.konten}
                                onReady={ editor => {
                                    // You can store the "editor" and use when it is needed.
                                    // console.log( 'Editor is ready to use!', this.state.data.konten );
                                } }
                                onChange={ ( event, editor ) => {
                                    const data = editor.getData();
                                    this.setState(prevState => ({data:{...prevState.data, konten : data}}))
                                    // console.log( { event, editor, data } );
                                    // console.log( { data } );
                                } }
                                onBlur={ ( event, editor ) => {
                                    // console.log( 'Blur.', editor );
                                } }
                                onFocus={ ( event, editor ) => {
                                    // console.log( 'Focus.', editor );
                                } }
                            />
                          </Form.Group>
                      </Form.Row>
                      {/* <Form.Group controlId="formBasicCheckbox">
                            <Form.Check type="checkbox" label="Kirim Notifikasi" checked={this.state.kirimNotifikasi} onChange={v => this.setState(prevState =>({data:{...prevState.data, kirimNotifikasi : v.target.checked}}))}/>
                        </Form.Group> */}
                  </Card.Body>
              </Card>
        
                  </Form>
                  <Alert variant='dark'>
                    Symbol *) menandakan Harus dikonten
                   </Alert>
              </Modal.Body>
          <Modal.Footer>
              
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleFormArtikel} >
                  Simpan
              </Button>
          </Modal.Footer>
        </Modal>
      );
    }
        
    handleFormArtikel = (e) => {
        e.preventDefault();
        // console.log(this.state.data)
        let info="";
        let nVerif=0;
        if(this.state.data.judul === "" || this.state.data.judul === null){
            info+="Judul belum terisi <br/>";
            nVerif=1;
        }
        if(this.state.tujuanArtikel === "" || this.state.tujuanArtikel === null){
            info+="Tujuan Artikel belum terisi <br/>";
            nVerif=1;
        }else{
            if(this.state.tujuanArtikel === "khususUnitInduk"){
                if(this.state.data.unitIndukId === "" || this.state.data.unitIndukId === null){
                    info+="Unit Induk Belum Dipilih <br/>";
                    nVerif=1;
                }
            }else if(this.state.tujuanArtikel === "khususUnitKerja"){
                if(this.state.data.unitKerjaId === "" || this.state.data.unitKerjaId === null){
                    info+="Unit Kerja Belum Dipilih <br/>";
                    nVerif=1;
                }
            }
        }
        if(this.state.data.konten === "" || this.state.data.konten === null){
            info+="Konten belum terisi <br/>";
            nVerif=1;
        }
        if(this.state.data.kontenSingkat === "" || this.state.data.kontenSingkat === null){
            info+="Konten SIngkat belum terisi <br/>";
            nVerif=1;
        }

        if(nVerif===1){
            this.MySwal.fire({
                icon: "error",
                title: info,
              });
        }else if(this.state.data.id === null && nVerif!==1){
            // console.log(this.state.data);
            this.props.createArtikel(this.state.data, this.state.tujuanArtikel, this.props.userDataProfile).then(()=>{
                const {success} = this.props.infoCreateArtikel;
                // console.log(success);
                if(success){
                    this.MySwal.fire({
                        icon: "success",
                        title: this.state.data.judul+" berhasil ditambahkan",
                      });
                    }
                    this.props.onHide();
                });
        }else if(this.state.data.id !== null && nVerif!==1){
            // console.log(this.state.data);
            // console.log("update");
            this.props.updateArtikel(this.state.data, this.state.tujuanArtikel, this.props.userDataProfile).then(()=>{
                const {success} = this.props.infoUpdateArtikel;
                // console.log(success);
                if(success){
                    this.MySwal.fire({
                        icon: "success",
                        title: this.state.data.judul+" berhasil diperbaharui",
                      });
                    }
                    this.props.onHide();
                });
        }
    }

}

const mapStateToProps = (state) => {
    return{
        infoCreateArtikel: state.infoCreateArtikel,
        infoUpdateArtikel: state.infoUpdateArtikel,
        userDataProfile: state.userDataProfile
    } 
 }
export default connect(mapStateToProps, {createArtikel, updateArtikel})(ModalFormArtikel);
// export default ModalFormArtikel;