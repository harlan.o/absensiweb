import { faEye, faPencilAlt, faTrash } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, {Component} from 'react';
import { Card, Button  } from 'react-bootstrap';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU037, REGISTER_FU038 } from '../../constants/hakAksesConstants';
import './ThumArtikel.css';
const moment = require('moment');

export class ThumArtikel extends Component {
  // createMarkup(htmltext) {
  //   return {__html: htmltext};
  // }
    render(){
      let _mDate = moment(this.props.dataArtikel?.modifiedDate);
      let subTitle;
      if(this.props.dataArtikel?.namaUnitKerja){
        subTitle=this.props.dataArtikel?.namaUnitKerja;
      }else if(this.props.dataArtikel?.namaUnitInduk){
        subTitle=this.props.dataArtikel?.namaUnitInduk;
      }else{
        subTitle="Untuk Semua Unit";
      }
        return (
            <Card >
  <Card.Body>
    <Card.Title>{this.props.dataArtikel?.judul}</Card.Title>
    
    <Card.Subtitle className="mb-2 text-muted"><small>{subTitle}</small></Card.Subtitle>
    <Card.Text className="text-thum">
    <span dangerouslySetInnerHTML={ {__html: this.props.dataArtikel?.konten} } />
    </Card.Text>
    <footer className="blockquote-footer text-right">
    {_mDate.format('YYYY-MM-DD HH:mm')} - <cite title="Source Title">{this.props.dataArtikel?.publishedBy}</cite>
      </footer>
    <Card.Link href="#">
      <Button variant="secondary"  size="sm" onClick={() => {this.props.dataonerow(this.props.dataArtikel, "detail")}}><FontAwesomeIcon icon={faEye} /> </Button>{' '}
      {cekHakAksesClient(REGISTER_FU037) ? 
          <Button variant="secondary" onClick={() => {this.props.dataonerow(this.props.dataArtikel, "ubah")}} size="sm"><FontAwesomeIcon icon={faPencilAlt} /> </Button>
      :""}{' '}
      {cekHakAksesClient(REGISTER_FU038) ?
          <Button variant="secondary" onClick={() => {this.props.dataonerow(this.props.dataArtikel, "hapus")}} size="sm"><FontAwesomeIcon icon={faTrash} /> </Button>
      :""}
      
      </Card.Link>
    
  </Card.Body>
</Card>
        )

    }
}

export default ThumArtikel;