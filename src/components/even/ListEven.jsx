import { faAngleDown, faEye, faMap, faMapMarkedAlt } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import React, { Component } from 'react';
import { Accordion, Badge, Button, Card, Col, ListGroup, Row } from 'react-bootstrap';
import { openMaps } from '../../actions/genneralAction';
import './listEven.css';
const moment = require('moment');
class ListEven extends Component {
    constructor(props) {
        super(props);
        this.state = {          
            data:[],
        };
    }
    componentDidMount(){
        this.setState({
            data:this.props.data
        })
    }

    renderPerList(dataList){
        let group = dataList.reduce((r, a) => {
                            r[a.tanggal] = [...r[a.tanggal] || [], a];
                            return r;
                           }, {});
        return Object.keys(group).sort().map(
            (key, index) => (
                <Card>
                    <Accordion.Toggle as={Card.Header} eventKey={key} >
                        <label variant="secondary" className="d-flex label-tgl-kegiatan justify-content-between">{key}
                        
                        <label variant="secondary" className="label-tgl-kegiatan justify-content-end"><label className="jmlhInfo">Total {group[key].length}</label> <FontAwesomeIcon icon={faAngleDown}/></label>
                        </label>
                    </Accordion.Toggle>
                    <Accordion.Collapse eventKey={key}>
                        <Card.Body>
                            <ListGroup variant="flush">
                                {group[key].map((evenData, i) => {
                                let jenisEven;
                                let unitPenyelenggara=evenData.unitPenyelenggara;
                                let b_pulang=true;
                                if(unitPenyelenggara==null || unitPenyelenggara==undefined){
                                    unitPenyelenggara="Pemerintah"
                                }
                                switch (evenData.jenisEvent) {
                                    case "NORMAL":
                                        jenisEven="Even Normal";
                                        break;
                                    case "SINGLE_ABSEN":
                                        jenisEven="Even Sekali Absen";
                                        b_pulang=false;
                                        break;
                                    case "PULANG_NORMAL":
                                        jenisEven="Even Waktu Pulang Sesuai Jadwal";
                                        b_pulang=false;
                                        break;
                                        
                                
                                    default:
                                        jenisEven="Even Belum Tersedia";
                                        
                                        break;
                                }
                                return(
                                <ListGroup.Item>
                                <Row>
                                <Col xs={12} md={6} lg={2}>
                                    <small className="label-ket">Mulai Even</small>
                                    <h2 className="jamMulai">{evenData.mulaiEvent.substring(0, evenData.mulaiEvent.length - 3)}</h2><label className="lokasi">(M) {evenData.tempat} </label>
                                </Col>
                                <Col xs={12} md={6} lg={8}>
                                    <small className="label-ket">{jenisEven}</small>
                                    <h2>{evenData.nama}
                                    </h2>
                                    <label className="label-penyelenggara">{evenData.unitPenyelenggara}</label>
                                </Col>
                                <Col xs={12} md={12} lg={2}>
                                    <small className="label-ket">Selesai</small>
                                    <h4> {b_pulang ? evenData.akhirEvent.substring(0, evenData.akhirEvent.length - 3): "--:--"}</h4><label className="lokasi">(L) {b_pulang ? evenData.tempatPulang : "-"}</label> <br/>
                                    <Button variant="secondary" size="sm" onClick={ 
                                    (e)=>{
                                        e.preventDefault();
                                        this.props.setdataonerow(evenData)
                                    }  
                                    }><FontAwesomeIcon icon={faEye} /></Button>{' '}
                                    <Button variant="secondary" size="sm" onClick={ 
                                    (e)=>{
                                        e.preventDefault();
                                        openMaps(evenData.geolocation)
                                    }  
                                    }>M <FontAwesomeIcon icon={faMap}  /></Button>{' '}
                                    {b_pulang ? 
                                    <Button variant="secondary" size="sm"  onClick={ 
                                        (e)=>{
                                            e.preventDefault();
                                            openMaps(evenData.geolocationPulang)
                                        }  
                                        }>S <FontAwesomeIcon icon={faMap} /></Button> : ""
                                    }
                                    
                                    
                                </Col>        
                                    
                                    {/* <div key={i}>title: {evenData.title}, url: {evenData.url}</div> */}
                                </Row>
                                </ListGroup.Item>
                                )})}
                            </ListGroup>
                        </Card.Body>
                    </Accordion.Collapse>
                </Card>
            )
        )

    }

    render() {
        this.renderPerList(this.props.data)
        return (
            <Accordion defaultActiveKey={moment().format('YYYY-MM-DD')} >
                {this.renderPerList(this.props.data)}
            </Accordion>
            )
    }
}
export default ListEven;