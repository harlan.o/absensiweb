import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import MarkerStartEven from './MarkerStartEven';
import MarkerEndEven from './MarkerEndEven';
import Marker from '../map/Marker';
 
let circleStart;
let circleEnd;
class MapEven extends Component {
  static defaultProps = {
    center: {
      lat: 2.74437333401,
      lng: 125.362329483
    },
    zoom: 11
  };
  constructor(props) {
    super(props);
    this.state = {
      radiusStartEvent: 0,
      positionStartEvent:{
        lat:0,
        lng:0,
      },
      radiusEndEvent: 0,
      positionEndEvent:{
        lat:0,
        lng:0,
      },
      klikMouse:{
        lat:"",
        lng:"",
      },
    };   
}

  setPosition = (key) => {
    this.setState(() => ({
      klikMouse:{
        lat:key.lat,
        lng:key.lng,
      },
    }), ()=>{
      this.props.getPosisiTagBaru(this.state.klikMouse);
    });
  }

  componentDidMount(){
    if(this.props.positionStartEvent.lat!=="" && this.props.positionStartEvent.lat!==null){
      this.setState(() => ({
        positionStartEvent:{
          lat:parseFloat(this.props.positionStartEvent.lat),
          lng:parseFloat(this.props.positionStartEvent.lng),
        },
        radiusStartEvent: this.props.radiusStartEvent,
      }));
    }

    if(this.props.positionEndEvent.lat!=="" && this.props.positionEndEvent.lat!==null){
      this.setState(() => ({
        positionEndEvent:{
          lat:parseFloat(this.props.positionEndEvent.lat),
          lng:parseFloat(this.props.positionEndEvent.lng),
        },
        radiusEndEvent: this.props.radiusEndEvent,
      }));
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if(this.props.positionStartEvent !== prevProps.positionStartEvent){
      if(this.props.positionStartEvent.lat!=="" && this.props.positionStartEvent.lat!==null){
        this.setState(() => ({
          positionStartEvent:{
            lat:parseFloat(this.props.positionStartEvent.lat),
            lng:parseFloat(this.props.positionStartEvent.lng),
          },
        }),() =>{
          if(circleStart !== undefined){
            circleStart.setCenter(this.state.positionStartEvent);
          }
        });
      }
    }
    if(this.props.radiusStartEvent !== prevProps.radiusStartEvent){
        this.setState(() => ({
          radiusStartEvent: this.props.radiusStartEvent,
        }),() =>{
          if(circleStart !== undefined){
            circleStart.setRadius(Number(this.state.radiusStartEvent));
          }
        });
    }

    if(this.props.positionEndEvent !== prevProps.positionEndEvent){
      if(this.props.positionEndEvent.lat!=="" && this.props.positionEndEvent.lat!==null){
        this.setState(() => ({
          positionEndEvent:{
            lat:parseFloat(this.props.positionEndEvent.lat),
            lng:parseFloat(this.props.positionEndEvent.lng),
          },
        }),() =>{
          if(circleEnd !== undefined){
            circleEnd.setCenter(this.state.positionEndEvent);
          }
        });
      }
    }
    if(this.props.radiusEndEvent !== prevProps.radiusEndEvent){
        this.setState(() => ({
          radiusEndEvent: this.props.radiusEndEvent,
        }),() =>{
          if(circleEnd !== undefined){
            circleEnd.setRadius(Number(this.state.radiusEndEvent));
          }
        });
    }

  }

  render() {
    // console.log(this.state.radiusStartEvent);
    const apiIsLoaded = (map, maps) => {
      circleStart = new maps.Circle({
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.3,
        map,
        center: this.state.positionStartEvent,
        radius: this.state.radiusStartEvent,
      });
      circleEnd = new maps.Circle({
        strokeColor: "#6c757d",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#6c757d",
        fillOpacity: 0.3,
        map,
        center: this.state.positionEndEvent,
        radius: this.state.radiusEndEvent,
      });
    };

    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '60vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyB8L7AFUamts-nbU2tuFK4LkfDykESmCoM" }}
          yesIWantToUseGoogleMapApiInternals
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
            onGoogleApiLoaded={({ map, maps }) => apiIsLoaded(map, maps)}
            onClick={this.setPosition}
        >

            {(this.state.klikMouse.lat!=="" && this.state.klikMouse.lng!=="" &&this.state.klikMouse.lng!=this.state.positionStartEvent.lng) ? <Marker lat={this.state.klikMouse.lat} lng={this.state.klikMouse.lng}/> : "" }
            {(this.state.positionEndEvent.lat!=="" && this.state.positionEndEvent.lng!=="") ? <MarkerEndEven lat={this.state.positionEndEvent.lat} lng={this.state.positionEndEvent.lng}/> : "" }
            {(this.state.positionStartEvent.lat!=="" && this.state.positionStartEvent.lng!=="") ? <MarkerStartEven lat={this.state.positionStartEvent.lat} lng={this.state.positionStartEvent.lng}/> : "" }
            
            
        </GoogleMapReact>
      </div>
    );
  }
}
 
export default MapEven;