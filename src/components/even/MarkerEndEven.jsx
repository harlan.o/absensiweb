import React, {Component} from 'react';


import {markerEndEvenStyle} from './MarkerMapEven_style.js';

export default class MarkerEndEven extends Component {


  render() {
    return (
       <div >
           <div style={markerEndEvenStyle}>
           </div>
          {this.props.text}
       </div>
    );
  }
}