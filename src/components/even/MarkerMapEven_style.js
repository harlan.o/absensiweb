import BackgroundStart from '../../img/map/MarkerStartEvent.svg'
import BackgroundEnd from '../../img/map/MarkerEndEvent.svg'
const K_WIDTH = 80;
const K_HEIGHT = 80;

export const markerStartEvenStyle = {
  // initially any map object has left top corner at lat lng coordinates
  // it's on you to set object origin to 0,0 coordinates
  position: 'absolute',
  width: K_WIDTH,
  height: K_HEIGHT,
  left: -K_WIDTH / 2,
  top: -K_HEIGHT+10,


  backgroundImage: `url(${BackgroundStart})`,
  textAlign: 'center',
  color: '#3f51b5',
  fontSize: 16,
  fontWeight: 'bold',
  padding: 4,
  
};

export const markerEndEvenStyle = {
  // initially any map object has left top corner at lat lng coordinates
  // it's on you to set object origin to 0,0 coordinates
  position: 'absolute',
  width: K_WIDTH,
  height: K_HEIGHT,
  left: -K_WIDTH / 2,
  top: -K_HEIGHT+10,


  backgroundImage: `url(${BackgroundEnd})`,
  textAlign: 'center',
  color: '#3f51b5',
  fontSize: 16,
  fontWeight: 'bold',
  padding: 4,
  
};