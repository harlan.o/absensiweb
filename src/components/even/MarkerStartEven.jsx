import React, {Component} from 'react';


import {markerStartEvenStyle} from './MarkerMapEven_style.js';

export default class MarkerStartEven extends Component {


  render() {
    return (
       <div >
           <div style={markerStartEvenStyle}>
           </div>
          {this.props.text}
       </div>
    );
  }
}