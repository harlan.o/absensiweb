import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
// import DatePicker, { Calendar } from "react-multi-date-picker";


// Reducer
import { clockConversi, padLeadingZero } from '../../actions/genneralAction';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU042 } from '../../constants/hakAksesConstants';
import ListPegawaiNamaNip from '../pegawai/ListPegawaiNamaNip';



class ModalComDetailEven extends Component{
    constructor(props) {
        super(props);        
        this.state = {
           judul:"Judul",
           jenisModal:"JENIS",
           data:{
               id:null,
               nama:null,
               tanggal:null,
               jenisEvent:"NORMAL",
               
               geolocation:",",
               radius:0,
               tempat:null,
               mulaiEvent:"00:00:00",
               sebelumMasuk:0,
               sesudahMasuk:0,

               geolocationPulang:",",
               radiusPulang:0,
               tempatPulang:null,
               akhirEvent:"00:00:00",
               sesudahPulang:0,

               pegawaiIdList:[],
               unitPenyelenggara:-1,

               listUnitInduk:[]
           },
           geolocationTagBaru:",",
           readonly:false,
        };
    }


    async modalType() {    
        await this.setState(({       
            data:this.props.dataonerow,
            judul:"Ubah Data Unit Induk",
            readonly:true,
        }));
    }

    async callAPI(){
        await this.setState(({       
            data:this.props.dataonerow,
            readonly:true,
        }));
    }

    componentDidMount(){
        this.callAPI()
    }

    render (){
        return (
            <>
              <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                Detail Event {this.state.data.nama}
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
                
                <Row>
                    <Col xs={12} md={6} lg={4}>
                        <Card className="mb-2">
                            <Card.Body>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_nama">
                                    <Form.Label className="textblack">Judul Kegiatan</Form.Label>
                                    <h4>{this.state.data.nama}</h4>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_nama">
                                    <Form.Label className="textblack">Penyelenggara</Form.Label>
                                    <h4>{this.state.data.unitPenyelenggara}</h4>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_nama">
                                    <Form.Label className="textblack">Tanggal</Form.Label>
                                    <h4>{this.state.data.tanggal}</h4>
                                    </Form.Group>
                                </Form.Row>

                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_nama">
                                    <Form.Label className="textblack">Mulai</Form.Label>
                                    <h4>{this.state.data.tempat} - Pukul {this.state.data.mulaiEvent}</h4>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_nama">
                                    <Form.Label className="textblack">Selesai</Form.Label>
                                    <h4>{this.state.data.tempatPulang} - Pukul {this.state.data.akhirEvent}</h4>
                                    </Form.Group>
                                </Form.Row>
                              
                                
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={12} md={6} lg={8}>
                        <Card className="mb-2">
                            <Card.Body>
                                <ListPegawaiNamaNip datas={this.state.data.daftarPegawai} />
                                
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                  </Modal.Body>
              <Modal.Footer>
                  <Button variant="secondary" onClick={this.props.onHide}>
                      Tutup
                  </Button>
                  {cekHakAksesClient(REGISTER_FU042) ?
                  <Button variant="primary" className="btn-them-red" onClick={(e)=>{ this.props.modalTypeHandle()}}>
                      Ubah
                  </Button>
                    :""}
              </Modal.Footer>
              </>
          );
    }



}

export default ModalComDetailEven;