import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
// import DatePicker, { Calendar } from "react-multi-date-picker";
import { DatePanel } from "react-multi-date-picker/plugins";
import DatePicker from 'react-datepicker';
import ListCheckBoxPegawai from '../pegawai/ListCheckBoxPegawai';
import SelectSingleUnitInduk_IdSend from '../unit-induk/SelectSingleUnitInduk_IdSend';

import TimeKeeper from 'react-timekeeper';
import '../TimeKeeper.css';

// Reducer
import { getListUnitInduk } from "../../actions/unitIndukAction";
import { updateEven } from "../../actions/evenAction";
import {connect} from 'react-redux';
import { clockConversi, padLeadingZero } from '../../actions/genneralAction';

import MapEven from './MapEven';

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import '../css/sweetaler2-style.css';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { uIIDP } from '../../actions/userActions';
import { REGISTER_FU001 } from '../../constants/hakAksesConstants';


const moment = require('moment');
let today =new Date();
class ModalComUpdateEven extends Component{
    constructor(props) {
        super(props);        
        let _unitPenyelenggara = uIIDP()
        if(cekHakAksesClient(REGISTER_FU001)){
            _unitPenyelenggara = this.props.dataonerow.unitPenyelenggaraId
        }       
        this.state = {
           judul:"Judul",
           jenisModal:"JENIS",
           data:{
               id:null,
               nama:null,
               tanggal:null,
               jenisEvent:"NORMAL",
               
               geolocation:",",
               radius:0,
               tempat:null,
               mulaiEvent:"00:00:00",
               sebelumMasuk:0,
               sesudahMasuk:0,

               geolocationPulang:",",
               radiusPulang:0,
               tempatPulang:null,
               akhirEvent:"00:00:00",
               sesudahPulang:0,

               pegawaiIdList:[],
               unitPenyelenggara:_unitPenyelenggara,
            },
            listUnitInduk:[],
           geolocationTagBaru:",",
           readonly:false,
        };
        this.handleGetPosisiTag=this.handleGetPosisiTag.bind(this);
        this.MySwal = withReactContent(Swal);
    }
    handleGetPosisiTag = (e) => {
        this.setState(({       
            geolocationTagBaru:e.lat +", "+e.lng
        }));
    }
    handleChangeGeolocationStartEven = () =>{
        this.setState( prevState => ({       
            data:{...prevState.data, geolocation : this.state.geolocationTagBaru }
        }));
    }
    handleChangeGeolocationEndEven = () =>{
        this.setState( prevState => ({       
            data:{...prevState.data, geolocationPulang : this.state.geolocationTagBaru }
        }));
    }
    getListSelectedPegawai = (v)=>{
        this.setState( prevState => ({       
            data:{...prevState.data, pegawaiIdList : v }
        }));
    }


    checkSemua(dataPegawai){
        console.log("hadir")
        if(dataPegawai !=null && dataPegawai !=undefined){
            let pegawai=dataPegawai
            let checked=[];
            for(var i = 0; i < pegawai.length; i++) {
                let obj = pegawai[i];
                checked.push(obj.id)
            }
            console.log(checked);
            return checked;
        }
        return [];

    }

    async callAPI(){
        if(this.props.listPengUnitInduk.data != undefined && this.props.listPengUnitInduk.data != null){
            this.setState(() => ({
                loading:false,
                listUnitInduk:this.props.listPengUnitInduk?.data,
              }));
        }else{
            await this.props.getListUnitInduk().then(()=>{
                this.setState(() => ({
                  loading:false,
                  listUnitInduk:this.props.listPengUnitInduk?.data,
                }));
              });  
        }


        
        let geoAwal=",";
        let geoPulang=",";
        let mulaiE="00:00:00";
        let akhirE="00:00:00";
        let radiusP=0;
        let radiusA=0;
        let unitPenyelenggaraId=-1;
        if(this.props.dataonerow.geolocation != null && this.props.dataonerow.geolocation != undefined){
               geoAwal=this.props.dataonerow.geolocation; 
        }
        if(this.props.dataonerow.geolocationPulang != null && this.props.dataonerow.geolocationPulang != undefined){
            geoPulang=this.props.dataonerow.geolocationPulang; 
        }
        if(this.props.dataonerow.mulaiEvent != null && this.props.dataonerow.mulaiEvent != undefined){
            mulaiE= this.props.dataonerow.mulaiEvent; 
        }
        if(this.props.dataonerow.akhirEvent != null && this.props.dataonerow.akhirEvent != undefined){
            akhirE= this.props.dataonerow.akhirEvent; 
        }
        if(this.props.dataonerow.radiusPulang != null && this.props.dataonerow.radiusPulang != undefined){
            radiusP=this.props.dataonerow.radiusPulang; 
        }
        if(this.props.dataonerow.radius != null && this.props.dataonerow.radius != undefined){
            radiusA=this.props.dataonerow.radius; 
        }
        if(this.props.dataonerow.unitPenyelenggaraId != null && this.props.dataonerow.unitPenyelenggaraId != undefined){
            unitPenyelenggaraId=this.props.dataonerow.unitPenyelenggaraId; 
        }
        console.log(this.props.dataonerow)
        // console.log(mulaiE)
        // console.log(this.props.dataonerow.mulaiEvent)
        // console.log(moment(this.props.dataonerow.tanggal))
        
        // let pegawaiL= this.checkSemua(this.props.dataonerow.daftarPegawai)
        let pegawaiL=[];
        if(this.props.dataonerow.daftarPegawai !=null && this.props.dataonerow.daftarPegawai !=undefined){
            for(var i = 0; i < this.props.dataonerow.daftarPegawai.length; i++) {
                let obj = this.props.dataonerow.daftarPegawai[i];
                pegawaiL.push(obj.id)
            }
        }
        
        this.setState(prevState =>({
            data:{...prevState.data, 
                id:this.props.dataonerow.id,
                nama:this.props.dataonerow.nama,
                nama2:this.props.dataonerow.nama,
                tanggal: new Date(this.props.dataonerow.tanggal),
                jenisEvent:this.props.dataonerow.jenisEvent,
                geolocation:geoAwal,
                radius:radiusA,
                tempat:this.props.dataonerow.tempat,
                mulaiEvent:mulaiE,
                sebelumMasuk:this.props.dataonerow.sebelumMasuk,
                sesudahMasuk:this.props.dataonerow.sesudahMasuk,
                geolocationPulang:geoPulang,
                radiusPulang:radiusP,
                tempatPulang:this.props.dataonerow.tempatPulang,
                akhirEvent:akhirE,
                sesudahPulang:this.props.dataonerow.sesudahPulang,
                pegawaiIdList:pegawaiL,
                unitPenyelenggara:unitPenyelenggaraId,
            }
        }), () => {
            console.log(this.state.data)
        } );
        // await this.setState(({       
        //     data:{
        //         id:this.props.dataonerow.id,
        //         nama:this.props.dataonerow.nama,
        //         tanggal:this.props.dataonerow.tanggal,
        //         jenisEvent:this.props.dataonerow.jenisEvent,
                
        //         geolocation:geoAwal,
        //         radius:radiusA,
        //         tempat:this.props.tempat,
        //         mulaiEvent:"00:00:00",
        //         sebelumMasuk:0,
        //         sesudahMasuk:0,
 
        //         geolocationPulang:geoPulang,
        //         radiusPulang:radiusP,
        //         tempatPulang:this.props.dataonerow.tempatPulang,
        //         akhirEvent:akhirE,
        //         sesudahPulang:0,
 
        //         pegawaiIdList:[],
        //         unitPenyelenggara:-1, 
        //     },
            
        //     readonly:true,
        // }));
        
    }

    componentDidMount(){
        this.callAPI();
    }

    render (){
        let mulaiEvent = clockConversi(this.state.data.mulaiEvent);
        let akhirEvent = clockConversi(this.state.data.akhirEvent);
        let geolocationAwal = this.state.data.geolocation.split(",");
        let centerAwal= {
            lat: geolocationAwal[0],
            lng: geolocationAwal[1],
        };
        let geolocationAkhir = this.state.data.geolocationPulang.split(",");
        let centerAkhir= {
            lat: geolocationAkhir[0],
            lng: geolocationAkhir[1],
        };

        // console.log(this.state.listUnitInduk);
        // console.log("render");
        return (
            <>
              <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                {/* {this.state.judul} */} Ubah Even Kegiatan
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>

                  {/* daftar pegawai with checklist, , maps, */}
                
                <Row>
                    <Col xs={12} md={6} lg={4}>
                        <Card className="mb-2">
                            <Card.Body>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_nama">
                                    <Form.Label className="textblack">Judul Kegiatan*</Form.Label>
                                    <Form.Control autoComplete="off" type="text" readOnly={this.state.readonly} placeholder="Masukan Judul Kegiatan" value={this.state.data.nama}onChange={v => this.setState(prevState => ({data:{...prevState.data, nama : v.target.value}}))} />
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_tanggal">
                                        <Form.Label className="textblack">Tanggal*</Form.Label>
                                        <br/>
                                        <DatePicker   
                                            className="form-control"
                                            selected={this.state.data.tanggal}
                                            dateFormat="yyyy-MM-dd"
                                            // onChange={this.handleChangeEndDate}
                                            onChange={v => {this.setState(prevState => ({data:{...prevState.data, tanggal : v }})); console.log(v)}}
                                            placeholderText="Pilih Tanggal Kegiatan"
                                        />
                                    </Form.Group>
                                </Form.Row>

                                {
                                   cekHakAksesClient(REGISTER_FU001) ?
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_unitPenyelenggara">
                                    <Form.Label className="textblack">Unit Penyelenggara</Form.Label>
                                    {/* <Form.Control type="text" placeholder="Masukan Nama Induk" value={this.state.data.nama} onChange={v => this.setState(prevState => ({data:{...prevState.data, nama : v.target.value}}))}/> */}
                                    { this.state.listUnitInduk ?
                                    <SelectSingleUnitInduk_IdSend dataoption={this.state.listUnitInduk} valueOption={this.state.data.unitPenyelenggara} valueGetSingle={v => {
                                        let id=-1;
                                        if(v!= null && v!=undefined){
                                            id=v.id
                                        }
                                        this.setState(prevState => ({data:{...prevState.data, unitPenyelenggara : id }})); console.log(id) }}/> : ""}
                                    </Form.Group>
                                </Form.Row>
                                :""
                                }
                                <Form.Group>
                                <Form.Label className="textblack">Jenis Even Kegiatan*</Form.Label>
                                <div>
                                    <Form.Check
                                        inline
                                        checked={this.state.data.jenisEvent == "NORMAL"}  
                                        name="form_jenis_absen"
                                        label="Normal"
                                        type="radio"
                                        onChange={v => this.setState(prevState =>({data:{...prevState.data, jenisEvent : "NORMAL"}}))}
                                    />
                                    <Form.Check
                                        inline
                                        checked={this.state.data.jenisEvent == "SINGLE_ABSEN"} 
                                        name="form_jenis_absen"
                                        label="Sekali Absen"
                                        type="radio"
                                        onChange={v => this.setState(prevState =>({data:{...prevState.data, jenisEvent : "SINGLE_ABSEN"}}))}
                                    />
                                    <Form.Check
                                        inline
                                        checked={this.state.data.jenisEvent == "PULANG_NORMAL"} 
                                        name="form_jenis_absen"
                                        label="Waktu Pulang sesuai jadwal"
                                        type="radio"
                                        onChange={v => this.setState(prevState =>({data:{...prevState.data, jenisEvent : "PULANG_NORMAL"}}))}
                                    />
                                </div>
                            </Form.Group>
                            </Card.Body>
                        </Card>
                        
                        <Card>
                            <Card.Body>
                             <ListCheckBoxPegawai idUnitInduk={this.state.data.unitPenyelenggara} listSelected={this.getListSelectedPegawai} valueList={this.state.data.pegawaiIdList}/>
                            </Card.Body>
                        </Card>
                    </Col>
                    <Col xs={12} md={6} lg={8}>
                        <Card className="mb-2">
                            <Card.Body>
            {/* geolocation:",",
               radius:0,
               tempat:null,
               mulaiEvent:"00:00",
               sebelumMasuk:0,
               sesudahMasuk:0,

               geolocationPulang:",",
               radiusPulang:0,
               tempatPulang:null,
               akhirEvent:"00:00",
               sesudahPulang:0, */}
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_tempat_mulai">
                                    <Form.Label className="textblack">Tempat Mulai</Form.Label>
                                    <Form.Control autoComplete="off" type="text" readOnly={this.state.readonly} placeholder="Nama Lokasi Mulai" value={this.state.data.tempat}onChange={v => this.setState(prevState => ({data:{...prevState.data, tempat : v.target.value}}))} />
                                    </Form.Group>
                                    {this.state.data.jenisEvent === "NORMAL" ?
                                        (
                                        <Form.Group as={Col} xs={12} md={6} controlId="form_tempat_selesai">
                                            <Form.Label className="textblack">Tempat Selesai</Form.Label>
                                            <Form.Control type="text" autoComplete="off" readOnly={this.state.readonly} placeholder="Nama Lokasi Selesai" value={this.state.data.tempatPulang}onChange={v => this.setState(prevState => ({data:{...prevState.data, tempatPulang : v.target.value}}))} />
                                        </Form.Group>
                                        ) : ""
                                    }
                                </Form.Row>
                                
                                <Form.Row>
                                    <Form.Group as={Col}  xs={6} md={6} controlId="form_jam_mulai">
                                    <Form.Label className="textblack">Jam Mulai</Form.Label>
                                    <br/>
                                    <TimeKeeper as={Form.Control}
                         onChange={v => this.setState(prevState => ({data:{...prevState.data, mulaiEvent : padLeadingZero(v.formatted24) }}))}
                         time={mulaiEvent}
                         hour24Mode={true}
                         switchToMinuteOnHourSelect
                         />
                                    </Form.Group>
                                    {this.state.data.jenisEvent === "NORMAL" ?
                                        (
                                    <Form.Group as={Col}  xs={6} md={6} controlId="form_jam_selesai">
                                    <Form.Label className="textblack">Jam Selesai</Form.Label>
                                    <br/>
                                    <TimeKeeper as={Form.Control}
                         onChange={v => this.setState(prevState => ({data:{...prevState.data, akhirEvent : padLeadingZero(v.formatted24) }}))}
                         time={akhirEvent}
                         hour24Mode={true}
                         switchToMinuteOnHourSelect
                         />
                                    </Form.Group>
                                        ) : ""
                                    }
                                </Form.Row>

                                <Form.Row>
                                    <Form.Group as={Col}  xs={4} md={4} controlId="form_sebelum_masuk">
                                    <Form.Label className="textblack">Sebelum Mulai (menit)</Form.Label>
                                    <Form.Control type="number" min='0' readOnly={this.state.readonly} placeholder=" 00 menit" value={this.state.data.sebelumMasuk} onChange={v => {if (v.target.value > -0){  this.setState(prevState => ({data:{...prevState.data, sebelumMasuk : v.target.value}}))} }} />
                                    </Form.Group>
                                    <Form.Group as={Col}  xs={4} md={4} controlId="form_sesudah_masuk">
                                    <Form.Label className="textblack">Sesudah Mulai (menit)</Form.Label>
                                    <Form.Control type="number" min='0' readOnly={this.state.readonly} placeholder="00 menit" value={this.state.data.sesudahMasuk} onChange={v => {if (v.target.value > -0){  this.setState(prevState => ({data:{...prevState.data, sesudahMasuk : v.target.value}}))} }} />
                                    </Form.Group>
                                    {this.state.data.jenisEvent === "NORMAL" ?
                                        (
                                    <Form.Group as={Col}  xs={4} md={4} controlId="form_sesudah_pulang">
                                    <Form.Label className="textblack">Sesudah Pulang (menit)</Form.Label>
                                    <Form.Control type="number" min='0' readOnly={this.state.readonly} placeholder="00 menit" value={this.state.data.sesudahPulang} onChange={v => {if (v.target.value > -0){  this.setState(prevState => ({data:{...prevState.data, sesudahPulang : v.target.value}}))} }} />
                                    </Form.Group>
                                        ) : ""
                                    }
                                </Form.Row>
                               
                            </Card.Body>
                        </Card>
                        <Card>
                            <Card.Body>
                            <Form.Row>
                                    <Form.Group as={Col} xs={12} md={8} controlId="form_geolocation_mulai">
                                    <Form.Label className="textblack">Geolocation Mulai</Form.Label>
                                    <Form.Control type="text" readOnly={true} placeholder="Tekan tombol kunci lokasi" value={this.state.data.geolocation}  />
                                    </Form.Group>
                                    <Form.Group as={Col} xs={12} md={4} controlId="form_geolocation_mulai">
                                    <Form.Label className="textblack">Radius (Geo Mulai)</Form.Label>
                                    <Form.Control type="number" min='0' value={this.state.data.radius} placeholder="00 (meter)" value={this.state.data.radius} onChange={v => {if (v.target.value > -0){  this.setState(prevState => ({data:{...prevState.data, radius : v.target.value}}))} }} />
                                    
                                    </Form.Group>
                                    
                                    {this.state.data.jenisEvent === "NORMAL" ?
                                        (
                                            <>
                                    <Form.Group as={Col} xs={12} md={8} controlId="form_geolocation_selesai">
                                    <Form.Label className="textblack">Geolocation Selesai</Form.Label>
                                    <Form.Control type="text" readOnly={true} placeholder="Tekan tombol kunci lokasi" value={this.state.data.geolocationPulang} />
                                    </Form.Group>
                                    <Form.Group as={Col} xs={12} md={4} controlId="form_geolocation_mulai">
                                    <Form.Label className="textblack">Radius (Geo Selesai)</Form.Label>
                                    <Form.Control type="number" min='0' value={this.state.data.radiusPulang} placeholder="00 (meter)" value={this.state.data.radiusPulang} onChange={v => {if (v.target.value > -0){  this.setState(prevState => ({data:{...prevState.data, radiusPulang : v.target.value}}))} }} />
                                    </Form.Group>
                                    </>
                                        ) : ""
                                    }
                                </Form.Row>
                                <Col>
                                <MapEven  positionStartEvent={centerAwal} positionEndEvent={centerAkhir} radiusStartEvent={this.state.data.radius} radiusEndEvent={this.state.data.radiusPulang} getPosisiTagBaru={this.handleGetPosisiTag}/>
                                </Col>
                                
                                {/* <MapUnitKerja positionUnit={center} rediusUnit={this.state.data.radius} getPosisiTagBaru={this.handleGetPosisiTag}/> */}

                                <Form.Group as={Col} xs={12} controlId="form_geolocation_baru">
                                <Button variant="secondary" onClick={this.handleChangeGeolocationStartEven} >
                                    Kunci Lokasi Mulai
                                </Button> {' '}
                                {this.state.data.jenisEvent === "NORMAL" ?
                                        (
                                <Button variant="secondary" onClick={this.handleChangeGeolocationEndEven} >
                                    Kunci Lokasi Selesai
                                </Button>
                                        ) : ""
                                    }
                            </Form.Group>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
                  </Modal.Body>
              <Modal.Footer>
                  <Button variant="secondary" onClick={(e)=>{ this.props.modalTypeHandle()}}>
                      Kembali
                  </Button>
                  <Button variant="primary" className="btn-them-red" onClick={this.handleCreateEven}>
                      Simpan
                  </Button>
              </Modal.Footer>
              </>
          );
    }


    handleCreateEven =  (e) =>  {
        e.preventDefault();
        console.log(this.state.data)
        let info="";
        let nVerif=0;
        if(this.state.data.nama === "" || this.state.data.nama === null){
            info+="Judul Kegiatan Belum Diisi <br/>";
            nVerif=1;
        }
        if(this.state.data.tanggal === "" || this.state.data.tanggal === null){
            info+="Tanggal Kegiatan Belum dipilih <br/>";
            nVerif=1;
        }
        if(this.state.data.geolocation === "" || this.state.data.geolocation === null){
            info+="Lokasi Mulai Belum ditandai di map <br/>";
            nVerif=1;
        }
        if(this.state.data.radius === 0 || this.state.data.radius === null){
            info+="Radius Mulai masi 0 <br/>";
            nVerif=1;
        }
        if(this.state.data.tempat === "" || this.state.data.tempat === null){
            info+="Nama Tempat Mulai belum diisi <br/>";
            nVerif=1;
        }
        if(this.state.data.mulaiEvent === "00:00:00" || this.state.data.mulaiEvent === null){
            info+="Waktu Mulai belum diatur <br/>";
            nVerif=1;
        }
        if(this.state.data.sebeluMasuk === 0 || this.state.data.sebeluMasuk === null){
            info+="Menit Sebelum Mulai Kegiatan masi 0 <br/>";
            nVerif=1;
        }
        if(this.state.data.sesudahMasuk === 0 || this.state.data.sesudahMasuk === null){
            info+="Menit Sesudah Mulai Kegiatan masi 0 <br/>";
            nVerif=1;
        }

        if( this.state.data.pegawaiIdList === null || this.state.data.pegawaiIdList.length === 0){
            info+="Belum Ada Pegawai yg dipilih <br/>";
            nVerif=1;
        }


        if(this.state.data.jenisEvent === "NORMAL"){
            if(this.state.data.geolocationPulang === "" || this.state.data.geolocationPulang === null){
                info+="Lokasi Selesai Belum ditandai di map <br/>";
                nVerif=1;
            }
            if(this.state.data.radiusPulang === 0 || this.state.data.radiusPulang === null){
                info+="Radius Selesai masi 0 <br/>";
                nVerif=1;
            }
            if(this.state.data.tempatPulang === "" || this.state.data.tempatPulang === null){
                info+="Nama Tempat Selesai belum diisi <br/>";
                nVerif=1;
            }
            if(this.state.data.akhirEvent === "00:00:00" || this.state.data.akhirEvent === null){
                info+="Waktu Selesai belum diatur <br/>";
                nVerif=1;
            }
            if(this.state.data.sesudahPulang === 0 || this.state.data.sesudahPulang === null){
                info+="Menit Sesudah Selesai Kegiatan masi 0 <br/>";
                nVerif=1;
            }
        }
        console.log(nVerif)
        if(nVerif===1){
            this.MySwal.fire({
                icon: "error",
                title: "Perhatian!",
                html: info,
                customClass: 'swal-wide',
            });
        }else{
            this.props.updateEven(this.state.data).then(()=> {
                const {success} = this.props.infoUpdateEven;
                const {loading} = this.props.infoUpdateEven;
                console.log("here")
                console.log(success)
                if(success){
                    this.MySwal.fire({
                        icon: "success",
                        title: "Berhasil!",
                        text: this.state.nama+" telah dibuat",
                        // customClass: 'swal-wide',
                    });
                    this.props.onHide();
                }
                if(loading == false && success == false){
                    this.MySwal.fire({
                        icon: "error",
                        title: "Coba Lagi!",
                        // html: info,
                        // customClass: 'swal-wide',
                    });
                }
            })
        }
        // Jenis Modal TAMBAH, DETAIL, UBAH, HAPUS
        // Jenis Event NORMAL, SINGLE_ABSEN, PULANG_NORMAL
        // NORMAL: diisi semua
        // SINGLE ABSEN: nama, tanggal, jenisEvent, geolocation, radius, tempat, mulai event, sbelum masuk, sesudah masuk, pegawai list, unit penyelenggara
        
        


    }

}

const mapStateToProps = (state) => {
    return{
        listPengUnitInduk: state.listPengUnitInduk,
        infoUpdateEven: state.infoUpdateEven,
    } 
  }
export default connect(mapStateToProps, {getListUnitInduk, updateEven}) (ModalComUpdateEven)