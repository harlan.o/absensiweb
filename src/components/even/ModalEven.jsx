import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import ModalComAddEven from './ModalComAddEven';
import ModalComDetailEven from './ModalComDetailEven';
import ModalComUpdateEven from './ModalComUpdateEven';

class ModalEven extends Component{
    constructor(props) {
        
        super(props);        
        this.state = {
           jenisModal:"JENIS",
           data:null,
        };
        this.handleChangeModeDetailToEdit = this.handleChangeModeDetailToEdit.bind(this);
        this.handleChangeModeEditToDetail = this.handleChangeModeEditToDetail.bind(this);
    }

    async callAPI(modal, data){
        this.setState(() => ({
            jenisModal:modal,
            data:data
        }));
    }

    componentDidMount(){
        console.log("hadir")
        this.callAPI(this.props.jenisModal, this.props.dataonerow);
    }

    // componentDidUpdate(prevProps, prevState){
    //     if(this.state.jenisModal !== prevState.jenisModal){
    //         this.callApi(this.props.jenisModal);
    //     }

    // }

    render (){
        return (
            <Modal 
              {...this.props}
              aria-labelledby="contained-modal-title-vcenter"
              dialogClassName="modal-90w">
                  {this.randerModal(this.state.jenisModal)}
            </Modal>
          );
    }
    randerModal(){
        console.log(this.state.jenisModal)
        switch (this.state.jenisModal) {
            case "TAMBAH":
                return(<ModalComAddEven onHide={()=> {this.props.onHide()}}/>)
            case "DETAIL":
                return(<ModalComDetailEven onHide={()=> {this.props.onHide()}} dataonerow={this.props.dataonerow} modalTypeHandle={this.handleChangeModeDetailToEdit} />)
                
            case "UBAH":
                return(<ModalComUpdateEven onHide={()=> {this.props.onHide()}} dataonerow={this.props.dataonerow} modalTypeHandle={this.handleChangeModeEditToDetail} />)
            default:
                break;
        }
    }

    handleChangeModeDetailToEdit(){
        this.setState({
            jenisModal: "UBAH",
          })
    }
    handleChangeModeEditToDetail(){
        this.setState({
            jenisModal: "DETAIL",
          })
    }

}

export default  ModalEven