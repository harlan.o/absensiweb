import React, { Component } from 'react';
import { Button, Card, Col, Form, Spinner, Row } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import "./Filter.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { faFilter,  faPlus, faPrint } from '@fortawesome/free-solid-svg-icons';

import {getListAbsenInduk, getListAbsenKerja} from "../../actions/absensiAction";
import { getListUnitKerja } from "../../actions/unitKerjaAction";
import { getListUnitInduk } from "../../actions/unitIndukAction";
import {connect} from 'react-redux';
import SelectSingleUnitInduk from '../unit-induk/SelectSingleUnitInduk';
import SelectSingleUnitInduk_IdSend from '../unit-induk/SelectSingleUnitInduk_IdSend';
import SelectSingleUnitKerja_IdSend from '../unit-kerja/SelectSingleUnitKerja_IdSend';
import { cekHakAksesClient } from '../../actions/hakAksesActions';

const moment = require('moment');
let today =new Date();
class CreateFilterPanel extends Component {
    constructor(props){
        super(props);
        this.state={
            showFormAdd: true,
            listUnitInduk:null,
            listUnitKerja:null,
            kodeUnitInduk: null,
            kodeUnitKerja: null,
            startDate: today,
            endDate: today,
            showFilter: false,
            loading:true,
            loadingBtn:false,
            textCariCepat:"",
        };
        this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this); 


        // this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        // this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
        this.handleShowFilter = this.handleShowFilter.bind(this);
        // this.handlerFilter = this.handlerFilter.bind(this);
        this.dataSelecteUnitInduk = this.dataSelecteUnitInduk.bind(this);
        this.dataSelecteUnitKerja = this.dataSelecteUnitKerja.bind(this);
        this.handleChangeUnitInduk = this.handleChangeUnitInduk.bind(this);
        this.handleChangeUnitKerja = this.handleChangeUnitKerja.bind(this);
    }
    
    handleShowFormModalAdd(){
        this.setState(state => ({      
            showFormAdd:true
        }));
        console.log(this.state.showFormAdd)
        this.props.showForm(this.state.showFormAdd); 
    }



    // handleChangeStartDate(start) {
    //     // this.preventDefault();
    //     this.setState({
    //       startDate: start,
    //     })
    //   }
      // handleChangeEndDate(end) {
      //   // console.log(end);
      //   this.setState({
      //     endDate: end,
      //   })
      // }
      handleChangeUnitInduk(v) {
        // console.log(v.id);
        if(v == null){
          v ={id:"all"}
        }
        this.setState({
          kodeUnitInduk: v.id
        },
        () =>{
          let unitKerjaKecil= this.props.listPengUnitKerja?.data.filter( (data, key) => {
            if(data !== undefined){
              if( data.unitInduk?.id == v.id && (v.id!==null || v.id!=="all") ){
                return true;
              }
            }
          });
          this.setState({
            listUnitKerja: unitKerjaKecil,
          }
          );
        } 
          // this.state.listUnitKerja
        )
      }

      handleChangeUnitKerja(v) {
        // console.log(v.target.value);
        if(v == null){
          v= {id:null}
        }
        this.setState({
          kodeUnitKerja: v.id,
        })
      }
      handleShowFilter() {    
        this.setState(state => ({      
            showFilter: !state.showFilter
        }));
      }
      handlerFilter =  (e) =>  {
        e.preventDefault();
        this.setState({
          // loadingBtn:true,
          textCariCepat:"",
         });
        let startDateModif = moment(this.state.startDate);
        let endDateModif = moment(this.state.endDate);
        let dataFilter={
          kodeUnitInduk:this.state.kodeUnitInduk,
          kodeUnitKerja:this.state.kodeUnitKerja,
          startDate:startDateModif.format('YYYY-MM-DD'),
          endDate:endDateModif.format('YYYY-MM-DD'),
        };
        // console.log(this.state.startDate);
        this.props.dataFilter(dataFilter);

    };

    // tinggal di test kemudia update dashboard
    dataSelecteUnitKerja(list, unitInduk){
      if(list !== undefined && this.state.loading !== true){
        return  list.map( (data, key) => {
          if(data !== undefined){
            if(data.unitInduk?.id === unitInduk && (unitInduk!==null || unitInduk!=="all")){
              return <option key={key} value={data.id}>{data.nama}</option>;
              }else{
                return <option key={key} value={data.id} >{data.nama}</option>;
              }
          }
        });
      }
     }

     dataSelecteUnitInduk(list){
      //  console.log(this.state.loading)
      if(list !== undefined && this.state.loading !== true){
        return  list.map( (data, key) => {
          return <option key={key} value={data.id} >{data.nama}</option>;
        });
      }
     }

     async callApi(){
     await this.props.getListUnitInduk().then(()=>{
       this.props.getListUnitKerja().then(()=>{
            this.setState(() => ({
              loading:false,
              listUnitInduk:this.props.listPengUnitInduk?.data,
              listUnitKerja:this.props.listPengUnitKerja?.data,
            }));
            
          });  
      });
      
     }

     componentDidMount(){
      this.callApi();
      
    }
    componentDidUpdate(prevProps, prevState) {
      if(prevProps.listUnitKerja !== this.props.listUnitKerja){
        this.callApi();
        
      }
      const {loading} = this.props.listAbsensiInduk;
      if(this.props.listAbsensiInduk !== prevProps.listAbsensiInduk){
        if (loading===false){
          this.setState({
            loadingBtn:false
          });
        }
      }
    }
     
render()
{
    return (
        <div className="custom-filter">
          <Row>
            <Col xs={6} md={4}>
            {cekHakAksesClient(this.props.kodeAdd) ?
              <Button variant="secondary" className="btn-width" onClick={this.handleShowFormModalAdd} > <FontAwesomeIcon icon={faPlus} /> </Button>:""}{' '}
              <Button variant="secondary" className="btn-width" onClick={this.handleShowFilter} > <FontAwesomeIcon icon={faFilter} /> </Button>{' '}
              {cekHakAksesClient(this.props.kodeCetak) ?
                <Button variant="secondary" className="btn-width"> <FontAwesomeIcon icon={faPrint} /></Button>  
              :""}
            </Col>
            <Col xs={6} md={{ span: 4, offset: 4 }}>
              <Form.Row className="align-items-center">
                <Form.Label  column xs="auto">
                  <h5>Cari</h5>
                </Form.Label>
                <Col>
                  <Form.Control type="text" placeholder="Cari Judul Artikel" onChange={v => this.setState(({textCariCepat: v.target.value }), () =>{this.props.getTextCariCepat(v.target.value)} )}/>
                </Col>
              </Form.Row>
            </Col>            
          </Row>
        
        <div className={this.state.showFilter ? "hidden unhidden" : "hidden"}>
                <Card className="custom-card-filter">
                    <Card.Title>Filter</Card.Title>
                        <Form >
                        <Form.Row className="align-items-center">

                            <Col xs="auto" className="col-md-6 my-1" >
                              {/* <Form.Control
                                  as="select"
                                  className="mr-sm-2"
                                  id="dinasFilter"
                                  custom
                                  onChange={this.handleChangeUnitInduk}

                              >
                                  <option value="all">Pilih Unit Induk...</option>
                                  {this.dataSelecteUnitInduk(this.state.listUnitInduk)}
                              </Form.Control>
                               */}
                               <SelectSingleUnitInduk_IdSend dataoption={this.state.listUnitInduk != null ? this.state.listUnitInduk:[]} valueGetSingle={this.handleChangeUnitInduk} valueOption={this.state.kodeUnitInduk} />
                            </Col>
                                
                          <Col xs="auto" className="col-md-6 my-1" >
                            {/* <Form.Control
                              as="select"
                              className="mr-sm-2"
                              id="groupFilter"
                              custom
                              onChange={this.handleChangeUnitKerja}
                            >
                              <option value="all">Pilih Unit Kerja...</option>
                              {this.dataSelecteUnitKerja(this.state.listUnitKerja, this.state.kodeUnitInduk)}
                            </Form.Control> */}
                            <SelectSingleUnitKerja_IdSend dataoption={this.state.kodeUnitInduk != null && this.state.kodeUnitInduk != "all" ? this.state.listUnitKerja : []} valueOption={this.state.kodeUnitKerja} valueGetSingle={this.handleChangeUnitKerja} placeholder="Pilih Unit Kerja" />
                          </Col>
                    

                {/* <Col xs="auto" className=" my-1" >
                <Form.Label htmlFor="inlineFormInputName" srOnly>
                    Name
                </Form.Label>
                <DatePicker
                                className="form-control"
                                selected={this.state.startDate }
                                onChange={this.handleChangeStartDate}
                                placeholderText="Pilih Tanggal Awal"
                            />
                </Col>
                <Col xs="auto" className="my-1" >
                <Form.Label htmlFor="inlineFormInputName" srOnly>
                    Name
                </Form.Label>
                <DatePicker     
                                className="form-control"
                                selected={this.state.endDate }
                                onChange={this.handleChangeEndDate}
                                placeholderText="Pilih Tanggal Akhir"
                            />
                </Col> */}
                


                <Col xs="auto" className="my-1">

                <Button type="submit" onClick={this.handlerFilter}>
                {this.state.loadingBtn ? (
                            <Spinner animation="border" className="spin" />
                        ) : ( "Filter"
                        )}
                        
                </Button>
                </Col>
            </Form.Row>
            </Form>
            </Card>
        </div>

        </div>
    );
}


}
const mapStateToProps = (state) => {
    return{
        listPengUnitInduk: state.listPengUnitInduk,
        listPengUnitKerja: state.listPengUnitKerja,
        listAbsensiInduk: state.listAbsensiInduk,
        // getListUnitKerja: state.getListUnitKerja,
    } 
  }
  export default connect(mapStateToProps, {getListUnitInduk, getListUnitKerja, getListAbsenInduk, getListAbsenKerja})(CreateFilterPanel);
