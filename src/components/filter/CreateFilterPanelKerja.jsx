import React, { Component } from 'react';
import { Button, Card, Col, Form, Spinner, Row } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import "./Filter.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { faFilter,  faPlus, faPrint } from '@fortawesome/free-solid-svg-icons';

import {getListAbsenInduk, getListAbsenKerja} from "../../actions/absensiAction";
import { getListUnitKerja } from "../../actions/unitKerjaAction";
import { getListUnitInduk } from "../../actions/unitIndukAction";
import {connect} from 'react-redux';
import SelectSingleUnitInduk from '../unit-induk/SelectSingleUnitInduk';
import SelectSingleUnitInduk_IdSend from '../unit-induk/SelectSingleUnitInduk_IdSend';
import SelectSingleUnitKerja_IdSend from '../unit-kerja/SelectSingleUnitKerja_IdSend';
import { uIIDP } from '../../actions/userActions';
import { cekHakAksesClient } from '../../actions/hakAksesActions';

const moment = require('moment');
let today =new Date();
class CreateFilterPanelKerja extends Component {
    constructor(props){
        super(props);
        this.state={
            showFormAdd: true,
            listUnitInduk:null,
            listUnitKerja:null,
            kodeUnitInduk: null,
            kodeUnitKerja: null,
            startDate: today,
            endDate: today,
            showFilter: false,
            loading:true,
            loadingBtn:false,
            textCariCepat:"",
        };
        this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this); 


        this.handleShowFilter = this.handleShowFilter.bind(this);
        this.dataSelecteUnitInduk = this.dataSelecteUnitInduk.bind(this);
        this.dataSelecteUnitKerja = this.dataSelecteUnitKerja.bind(this);
        this.handleChangeUnitInduk = this.handleChangeUnitInduk.bind(this);
        this.handleChangeUnitKerja = this.handleChangeUnitKerja.bind(this);
    }
    
    handleShowFormModalAdd(){
        this.setState(state => ({      
            showFormAdd:true
        }));
        console.log(this.state.showFormAdd)
        this.props.showForm(this.state.showFormAdd); 
    }



      handleChangeUnitInduk(v) {
        if(v == null){
          v ={id:"all"}
        }
        this.setState({
          kodeUnitInduk: v.id
        },
        () =>{
          let unitKerjaKecil= this.props.listPengUnitKerja?.data.filter( (data, key) => {
            if(data !== undefined){
              if( data.unitInduk?.id == v.id && (v.id!==null || v.id!=="all") ){
                return true;
              }
            }
          });
          this.setState({
            listUnitKerja: unitKerjaKecil,
          }
          );
        } 
        )
      }

      handleChangeUnitKerja(v) {
        if(v == null){
          v= {id:null}
        }
        this.setState({
          kodeUnitKerja: v.id,
        })
      }
      handleShowFilter() {    
        this.setState(state => ({      
            showFilter: !state.showFilter
        }));
      }
      handlerFilter =  (e) =>  {
        e.preventDefault();
        this.setState({
          // loadingBtn:true,
          textCariCepat:"",
         });
        let startDateModif = moment(this.state.startDate);
        let endDateModif = moment(this.state.endDate);
        let dataFilter={
          kodeUnitInduk:this.state.kodeUnitInduk,
          kodeUnitKerja:this.state.kodeUnitKerja,
          startDate:startDateModif.format('YYYY-MM-DD'),
          endDate:endDateModif.format('YYYY-MM-DD'),
        };
        this.props.dataFilter(dataFilter);

    };

    dataSelecteUnitKerja(list, unitInduk){
      if(list !== undefined && this.state.loading !== true){
        return  list.map( (data, key) => {
          if(data !== undefined){
            if(data.unitInduk?.id === unitInduk && (unitInduk!==null || unitInduk!=="all")){
              return <option key={key} value={data.id}>{data.nama}</option>;
              }else{
                return <option key={key} value={data.id} >{data.nama}</option>;
              }
          }
        });
      }
     }

     dataSelecteUnitInduk(list){
      if(list !== undefined && this.state.loading !== true){
        return  list.map( (data, key) => {
          return <option key={key} value={data.id} >{data.nama}</option>;
        });
      }
     }

     async callApi(){
     await this.props.getListUnitKerja().then(()=>{
      
            this.setState(() => ({
              loading:false,
              listUnitKerja:this.props.listPengUnitKerja?.data,
            }), () => {
              let v={id:uIIDP()}
              this.handleChangeUnitInduk(v)
            } );
          });  
      
     }

     componentDidMount(){
      this.callApi();
      
    }
    componentDidUpdate(prevProps, prevState) {
      if(prevProps.listUnitKerja !== this.props.listUnitKerja){
        this.callApi();
        
      }
    }
     
render()
{
    return (
        <div className="custom-filter">
          <Row>
            <Col xs={6} md={4}>
            {cekHakAksesClient(this.props.kodeAdd) ?
              <Button variant="secondary" className="btn-width" onClick={this.handleShowFormModalAdd} > <FontAwesomeIcon icon={faPlus} /> </Button>:""}{' '}
              <Button variant="secondary" className="btn-width" onClick={this.handleShowFilter} > <FontAwesomeIcon icon={faFilter} /> </Button>{' '}
              {cekHakAksesClient(this.props.kodeCetak) ?
              <Button variant="secondary" className="btn-width"> <FontAwesomeIcon icon={faPrint} /></Button>
              :""}
            </Col>
            <Col xs={6} md={{ span: 4, offset: 4 }}>
              <Form.Row className="align-items-center">
                <Form.Label  column xs="auto">
                  <h5>Cari</h5>
                </Form.Label>
                <Col>
                  <Form.Control type="text" placeholder="Cari Judul Artikel" onChange={v => this.setState(({textCariCepat: v.target.value }), () =>{this.props.getTextCariCepat(v.target.value)} )}/>
                </Col>
              </Form.Row>
            </Col>            
          </Row>
        
        <div className={this.state.showFilter ? "hidden unhidden" : "hidden"}>
                <Card className="custom-card-filter">
                    <Card.Title>Filter</Card.Title>
                        <Form >
                        <Form.Row className="align-items-center">

                                
                          <Col xs="auto" className="col-md-6 my-1" >
                   
                            <SelectSingleUnitKerja_IdSend dataoption={this.state.kodeUnitInduk != null && this.state.kodeUnitInduk != "all" ? this.state.listUnitKerja : []} valueOption={this.state.kodeUnitKerja} valueGetSingle={this.handleChangeUnitKerja} placeholder="Pilih Unit Kerja" />
                          </Col>
                    

                <Col xs="auto" className="my-1">

                <Button type="submit" onClick={this.handlerFilter}>
                {this.state.loadingBtn ? (
                            <Spinner animation="border" className="spin" />
                        ) : ( "Filter"
                        )}
                        
                </Button>
                </Col>
            </Form.Row>
            </Form>
            </Card>
        </div>

        </div>
    );
}


}
const mapStateToProps = (state) => {
    return{
        listPengUnitKerja: state.listPengUnitKerja,
    } 
  }
  export default connect(mapStateToProps, {getListUnitKerja})(CreateFilterPanelKerja);
