import React, { Component } from 'react';
import { Button, Card, Col, Form, FormCheck, Spinner } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import "./Filter.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { faFilter, faPlus, faPrint } from '@fortawesome/free-solid-svg-icons';

import ReactToPrint from 'react-to-print';

// import {getListAbsenInduk, getListAbsenKerja} from "../../actions/absensiAction";
// import { getListUnitKerja } from "../../actions/unitKerjaAction";
import { getListUnitInduk } from "../../actions/unitIndukAction";
import {connect} from 'react-redux';

// import SelectMultiplyUnitInduk from '../unit-induk/SelectMultiplyUnitInduk';
// import SelectMultiplyUnitKerja from '../unit-kerja/SelectMultiplyUnitKerja';
import SelectSingleUnitInduk from '../unit-induk/SelectSingleUnitInduk';
import { cekHakAksesClient } from '../../actions/hakAksesActions';


const moment = require('moment');

let today = new Date();
class CreateFilterSingleMonthSingleUnitInduk extends Component {
    constructor (props) {
        super(props)
        this.state = {
          filterType:"SUPER_ADMIN",
          listUnitInduk:null,
          startDate:today,
          showFilter: false,
          loading:true,
          loadingBtn:false,
          selectSingleUnitInduk:null,

          showFormAdd: true,
        };
        this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this); 
        this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        this.handleShowFilter = this.handleShowFilter.bind(this);
        this.handleChangeUnitIndukSingle = this.handleChangeUnitIndukSingle.bind(this);
      }


     async callApi(){
     await this.props.getListUnitInduk().then(()=>{
         this.setState(() => ({
           loading:false,
           listUnitInduk:this.props.listPengUnitInduk?.data ,
         }));  
      });
      
     }

     componentDidMount(){
      this.callApi();   
    }
    componentDidUpdate(prevProps, prevState) {
      const loading = this.props.setloading;
      if(this.props.setloading !== prevProps.setloading){
        if (this.props.setloading===false){
          this.setState({
            loadingBtn:false
          });
        }
      }
    }
     
render()
{
    return (
        <div className="custom-filter">
        <Button variant="secondary" className="btn-width" onClick={this.handleShowFilter} > <FontAwesomeIcon icon={faFilter} /> </Button>{' '}
        {cekHakAksesClient(this.props.kodeAdd) ?
        <Button variant="secondary" className="btn-width" onClick={this.handleShowFormModalAdd} > <FontAwesomeIcon icon={faPlus} /> </Button>
        :""}
        {' '}
        {cekHakAksesClient(this.props.kodeCetak) ?
        <ReactToPrint
                    trigger={() => {
                       
                        return <Button variant="secondary" className="btn-width"> <FontAwesomeIcon icon={faPrint} /></Button>;
                    }}
                    content={this.props.print}
                />
        :""}
        <div className={this.state.showFilter ? "hidden unhidden" : "hidden"}>
                <Card className="custom-card-filter">
                    <Card.Title>Filter</Card.Title>
                        <Form >
                        <Form.Row className="align-items-center">
                        <Col xs="auto" className="col-md-6 my-1" >
                          { this.state.listUnitInduk ? <SelectSingleUnitInduk dataoption={this.state.listUnitInduk} valueGetSingle={this.handleChangeUnitIndukSingle} valueOption={this.state.selectSingleUnitInduk}/> : ""}
                        </Col>
                    

                <Col xs="auto" className=" my-1" >
                <Form.Label htmlFor="inlineFormInputName" srOnly>
                    Name
                </Form.Label>
                <DatePicker
                                className="form-control"
                                selected={this.state.startDate }
                                onChange={this.handleChangeStartDate}
                                placeholderText="Pilih Bulan dan Tahun"
                                dateFormat="yyyy-MM"
                                showMonthYearPicker
                                showTwoColumnMonthYearPicker
                  />
                </Col>

         

        
                <Col xs="auto" className="my-1">
                <Button type="submit" onClick={this.handlerFilter}>
                {this.state.loadingBtn ? (
                            <Spinner animation="border" className="spin" />
                        ) : ( "Filter"
                        )}
                        
                </Button>
                </Col>
            </Form.Row>
            </Form>
            </Card>
        </div>
        </div>
    );
}

  handleShowFormModalAdd(){
    this.setState(state => ({      
        showFormAdd:true
    }));
    this.props.showForm(this.state.showFormAdd, "TAMBAH"); 
  }

  handleChangeStartDate(start) {
    this.setState({
      startDate: start,
    })
  }

  handleChangeUnitIndukSingle(v) {
    if(v == null){
      v=null
    }
    this.setState({
      selectSingleUnitInduk : v,
    });
  }

  handleShowFilter() {    
    this.setState(state => ({      
        showFilter: !state.showFilter
    }));
  }

  handlerFilter =  (e) =>  {
    e.preventDefault();
    this.setState({
      loadingBtn:true
    });

    let startDateModif = moment(this.state.startDate);
    let dataFilter={
      startDate:startDateModif.format('YYYY-MM'),
      selectSingleUnitInduk:this.state.selectSingleUnitInduk.id,
    };
    console.log(dataFilter);
    this.props.dataFilter(dataFilter);
  };

  removeArray(arr) {
  var what, a = arguments, L = a.length, ax;
  while (L > 1 && arr.length) {
      what = a[--L];
      while ((ax= arr.indexOf(what)) !== -1) {
          arr.splice(ax, 1);
      }
  }
  return arr;
  }



}

const mapStateToProps = (state) => {
    return{
        listPengUnitInduk: state.listPengUnitInduk,
    } 
}
export default connect(mapStateToProps, {getListUnitInduk})(CreateFilterSingleMonthSingleUnitInduk);