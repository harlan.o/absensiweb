import React, { Component } from 'react';
import { Button, Card, Col, Form, Spinner } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import "./Filter.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';
// import {faFilter} from '@fortawesome/free-solid-svg-icons';
import { faFilter, faPrint } from '@fortawesome/free-solid-svg-icons';

import ReactToPrint from 'react-to-print';

import {getListAbsenInduk, getListAbsenKerja} from "../../actions/absensiAction";
import { getListUnitKerja } from "../../actions/unitKerjaAction";
import { getListUnitInduk } from "../../actions/unitIndukAction";
import {connect} from 'react-redux';
import { cekHakAksesClient } from '../../actions/hakAksesActions';


const moment = require('moment');
// const MyContainer = ({ className, children }) => {
//     return (
//       <div style={{ padding: "16px", background: "#216ba5", color: "#fff" }}>
//         <CalendarContainer className={className}>
//           <div style={{ background: "#f0f0f0" }}>
//             What is your favorite day?
//           </div>
//           <div style={{ position: "relative" }}>{children}</div>
//         </CalendarContainer>
//       </div>
//     );
//   };

const ListUnitKerja = () => {
  return(
    <option>asd</option>
  )
}
let today =new Date();

class FilterMonthYearShowData extends Component {
    constructor (props) {
        super(props)
        this.state = {
          listUnitInduk:null,
          listUnitKerja:null,
          kodeUnitInduk: null,
          kodeUnitKerja: null,
          startDate: today,
          showFilter: false,
          loading:true,
          loadingBtn:false,
        };
        this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        this.handleShowFilter = this.handleShowFilter.bind(this);
        // this.handlerFilter = this.handlerFilter.bind(this);
        this.dataSelecteUnitInduk = this.dataSelecteUnitInduk.bind(this);
        this.dataSelecteUnitKerja = this.dataSelecteUnitKerja.bind(this);
        this.handleChangeUnitInduk = this.handleChangeUnitInduk.bind(this);
        this.handleChangeUnitKerja = this.handleChangeUnitKerja.bind(this);
      }

      handleChangeStartDate(start) {
        // this.preventDefault();
        this.setState({
          startDate: start,
        })
      }
      handleChangeUnitInduk(v) {
        // console.log(v.target.value);
        this.setState({
          kodeUnitInduk: v.target.value,
          kodeUnitKerja: "all",
        },
        () =>{
          let unitKerjaKecil= this.props.listPengUnitKerja?.data.map( (data, key) => {
            if(data !== undefined){
              if( data.unitInduk?.id == v.target.value && (v.target.value!==null || v.target.value!=="all") ){
                return data;
              }
            }
          });
          this.setState({
            listUnitKerja: unitKerjaKecil,
          }
          );
        } 
          // this.state.listUnitKerja
        )
      }
      handleChangeUnitKerja(v) {
        // console.log(v.target.value);
        this.setState({
          kodeUnitKerja: v.target.value,
        })
      }
      handleShowFilter() {    
        this.setState(state => ({      
            showFilter: !state.showFilter
        }));
      }
      handlerFilter =  (e) =>  {
        e.preventDefault();
        this.setState({
          loadingBtn:true
         });
        let startDateModif = moment(this.state.startDate);
        let dataFilter={
          kodeUnitInduk:this.state.kodeUnitInduk,
          kodeUnitKerja:this.state.kodeUnitKerja,
          startDate:startDateModif.format('YYYY-MM'),
        };
        // console.log(this.state.startDate);
        this.props.dataFilter(dataFilter);

    };

    // tinggal di test kemudia update dashboard
    dataSelecteUnitKerja(list, unitInduk){
      if(list !== undefined && this.state.loading !== true){
        return  list.map( (data, key) => {
          if(data !== undefined){
            if(data.unitInduk?.id === unitInduk && (unitInduk!==null || unitInduk!=="all")){
              return <option key={key} value={data.id}>{data.nama}</option>;
              }else{
                return <option key={key} value={data.id} >{data.nama}</option>;
              }
          }
        });
      }
     }

     dataSelecteUnitInduk(list){
      //  console.log(this.state.loading)
      if(list !== undefined && this.state.loading !== true){
        return  list.map( (data, key) => {
          return <option key={key} value={data.id} >{data.nama}</option>;
        });
      }
     }

     async callApi(){
     await this.props.getListUnitInduk().then(()=>{
       this.props.getListUnitKerja().then(()=>{
            this.setState(() => ({
              loading:false,
              listUnitInduk:this.props.listPengUnitInduk?.data,
              listUnitKerja:this.props.listPengUnitKerja?.data,
            }));
            
          });  
      });
      
     }

     componentDidMount(){
      this.callApi();
      
    }
    componentDidUpdate(prevProps, prevState) {
      if(prevProps.listUnitKerja !== this.props.listUnitKerja){
        this.callApi();
        
      }
      const {loading} = this.props.listHistoryTpp;
      if(this.props.listHistoryTpp !== prevProps.listHistoryTpp){
        if (loading===false){
          this.setState({
            loadingBtn:false
          });
        }
      }
    }
     
render()
{
    return (
        <div className="custom-filter">
        <Button variant="secondary" className="btn-width" onClick={this.handleShowFilter} > <FontAwesomeIcon icon={faFilter} /> </Button>{' '}
        
        {cekHakAksesClient(this.props.kodeCetak) ?
        <ReactToPrint
                    trigger={() => {
                       
                        return <Button variant="secondary" className="btn-width"> <FontAwesomeIcon icon={faPrint} /></Button>;
                    }}
                    content={this.props.print}
                />
        :""}
        <div className={this.state.showFilter ? "hidden unhidden" : "hidden"}>
                <Card className="custom-card-filter">
                    <Card.Title>Filter</Card.Title>
                        <Form >
                        <Form.Row className="align-items-center">

                            <Col xs="auto" className="col-md-6 my-1" >
                              <Form.Control
                                  as="select"
                                  className="mr-sm-2"
                                  id="dinasFilter"
                                  custom
                                  onChange={this.handleChangeUnitInduk}

                              >
                                  <option value="all">Semua Unit Induk...</option>
                                  {this.dataSelecteUnitInduk(this.state.listUnitInduk)}
                              </Form.Control>
                            </Col>
                                
                          <Col xs="auto" className="col-md-6 my-1" >
                            <Form.Control
                              as="select"
                              className="mr-sm-2"
                              id="groupFilter"
                              custom
                              onChange={this.handleChangeUnitKerja}
                            >
                              <option value="all">Semua Unit Kerja...</option>
                              {this.dataSelecteUnitKerja(this.state.listUnitKerja, this.state.kodeUnitInduk)}
                            </Form.Control>
                          </Col>
                    

                <Col xs="auto" className=" my-1" >
                <Form.Label htmlFor="inlineFormInputName" srOnly>
                    Name
                </Form.Label>
                <DatePicker
                                className="form-control"
                                selected={this.state.startDate }
                                onChange={this.handleChangeStartDate}
                                placeholderText="Pilih Bulan dan Tahun"
                                dateFormat="yyyy-MM"
                                showMonthYearPicker
                                showTwoColumnMonthYearPicker
                            />
                </Col>
                


                <Col xs="auto" className="my-1">

                <Button type="submit" onClick={this.handlerFilter}>
                {this.state.loadingBtn ? (
                            <Spinner animation="border" className="spin" />
                        ) : ( "Filter"
                        )}
                        
                </Button>
                </Col>
            </Form.Row>
            </Form>
            </Card>
        </div>
        </div>
    );
}


}

const mapStateToProps = (state) => {
  return{
      listPengUnitInduk: state.listPengUnitInduk,
      listPengUnitKerja: state.listPengUnitKerja,
      listHistoryTpp: state.listHistoryTpp,
  } 
}
export default connect(mapStateToProps, {getListUnitInduk, getListUnitKerja, getListAbsenInduk, getListAbsenKerja})(FilterMonthYearShowData);