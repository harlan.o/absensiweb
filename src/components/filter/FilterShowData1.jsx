import React, { Component } from 'react';
import { Button, Card, Col, Form, Spinner } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import "./Filter.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';
// import {faFilter} from '@fortawesome/free-solid-svg-icons';
import { faFilter, faPrint } from '@fortawesome/free-solid-svg-icons';

import ReactToPrint from 'react-to-print';

import {getListAbsenInduk, getListAbsenKerja} from "../../actions/absensiAction";
import { getListUnitKerja } from "../../actions/unitKerjaAction";
import { getListUnitInduk } from "../../actions/unitIndukAction";
import {connect} from 'react-redux';


const moment = require('moment');
let today =new Date();

class FilterShowData1 extends Component {
    constructor (props) {
        super(props)
        this.state = {
          listUnitInduk:null,
          listUnitKerja:null,
          kodeUnitInduk: null,
          kodeUnitKerja: null,
          startDate: today,
          endDate: today,
          showFilter: false,
          loading:true,
          loadingBtn:false,
        };
        this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        this.handleChangeEndDate = this.handleChangeEndDate.bind(this);
        this.handleShowFilter = this.handleShowFilter.bind(this);
        this.dataSelecteUnitInduk = this.dataSelecteUnitInduk.bind(this);
        this.dataSelecteUnitKerja = this.dataSelecteUnitKerja.bind(this);
        this.handleChangeUnitInduk = this.handleChangeUnitInduk.bind(this);
        this.handleChangeUnitKerja = this.handleChangeUnitKerja.bind(this);
      }


     async callApi(){
     await this.props.getListUnitInduk().then(()=>{
       this.props.getListUnitKerja().then(()=>{
            this.setState(() => ({
              loading:false,
              listUnitInduk:this.props.listPengUnitInduk?.data,
              listUnitKerja:this.props.listPengUnitKerja?.data,
            }));
            
          });  
      });
      
     }

     componentDidMount(){
      this.callApi();
      
    }
    componentDidUpdate(prevProps, prevState) {
      if(prevProps.listUnitKerja !== this.props.listUnitKerja){
        this.callApi();  
      }
      if(this.props.loadingBtn !== prevProps.loadingBtn){
        console.log(this.props.loadingBtn)
        if (this.props.loadingBtn===false){
          this.setState({
            loadingBtn:false
          });
        }
      }
    }
     
  render()
  {
      return (
          <div className="custom-filter">
          <Button variant="secondary" className="btn-width" onClick={this.handleShowFilter} > <FontAwesomeIcon icon={faFilter} /> </Button>{' '}
          <ReactToPrint
                      trigger={() => {
                          return <Button variant="secondary" className="btn-width"> <FontAwesomeIcon icon={faPrint} /></Button>;
                      }}
                      content={this.props.print}
          />
          <div className={this.state.showFilter ? "hidden unhidden" : "hidden"}>
                  <Card className="custom-card-filter">
                      <Card.Title>Filter</Card.Title>
                          <Form >
                          <Form.Row className="align-items-center">

                              <Col xs="auto" className="col-md-6 my-1" >
                                <Form.Control
                                    as="select"
                                    className="mr-sm-2"
                                    id="dinasFilter"
                                    custom
                                    onChange={this.handleChangeUnitInduk}
                                >
                                  <option value="all">Semua Unit Induk...</option>
                                  {this.dataSelecteUnitInduk(this.state.listUnitInduk)}
                                </Form.Control>
                              </Col>
                                  
                            <Col xs="auto" className="col-md-6 my-1" >
                              <Form.Control
                                as="select"
                                className="mr-sm-2"
                                id="groupFilter"
                                custom
                                onChange={this.handleChangeUnitKerja}
                              >
                                <option value="all">Semua Unit Kerja...</option>
                                {this.dataSelecteUnitKerja(this.state.listUnitKerja, this.state.kodeUnitInduk)}
                              </Form.Control>
                            </Col>
                      
                  <Col xs="auto" className=" my-1" >
                  <Form.Label htmlFor="inlineFormInputName" srOnly>
                      Name
                  </Form.Label>
                  <DatePicker
                                  className="form-control"
                                  selected={this.state.startDate }
                                  onChange={this.handleChangeStartDate}
                                  placeholderText="Pilih Tanggal Awal"
                              />
                  </Col>
                  <Col xs="auto" className="my-1" >
                  <Form.Label htmlFor="inlineFormInputName" srOnly>
                      Name
                  </Form.Label>
                  <DatePicker     
                                  className="form-control"
                                  selected={this.state.endDate }
                                  onChange={this.handleChangeEndDate}
                                  placeholderText="Pilih Tanggal Akhir"
                              />
                  </Col>
                  
                  <Col xs="auto" className="my-1">

                  <Button type="submit" onClick={this.handlerFilter}>
                  {this.state.loadingBtn ? (
                              <Spinner animation="border" className="spin" />
                          ) : ( "Filter"
                          )}
                          
                  </Button>
                  </Col>
              </Form.Row>
              </Form>
              </Card>
          </div>
          </div>
      );
  }

  handleChangeStartDate(start) {
    this.setState({
      startDate: start,
    })
  }
  handleChangeEndDate(end) {
    this.setState({
      endDate: end,
    })
  }
  handleChangeUnitInduk(v) {
    this.setState({
      kodeUnitInduk: v.target.value,
      kodeUnitKerja: "all",
    }, () =>{
      let unitKerjaKecil= this.props.listPengUnitKerja?.data.map( (data, key) => {
        if(data !== undefined){
          if( data.unitInduk?.id == v.target.value && (v.target.value!==null || v.target.value!=="all") ){
            return data;
          }
        }
      });
      this.setState({
        listUnitKerja: unitKerjaKecil,
      }
      );
    })
  }
  handleChangeUnitKerja(v) {
    this.setState({
      kodeUnitKerja: v.target.value,
    })
  }
  handleShowFilter() {    
    this.setState(state => ({      
        showFilter: !state.showFilter
    }));
  }
  handlerFilter =  (e) =>  {
    e.preventDefault();
    this.setState({
      loadingBtn:true
    });
    let startDateModif = moment(this.state.startDate);
    let endDateModif = moment(this.state.endDate);
    let dataFilter={
      kodeUnitInduk:this.state.kodeUnitInduk,
      kodeUnitKerja:this.state.kodeUnitKerja,
      startDate:startDateModif.format('YYYY-MM-DD'),
      endDate:endDateModif.format('YYYY-MM-DD'),
    };
    this.props.dataFilter(dataFilter);

  };

  dataSelecteUnitKerja(list, unitInduk){
    if(list !== undefined && this.state.loading !== true){
      return  list.map( (data, key) => {
        if(data !== undefined){
          if(data.unitInduk?.id === unitInduk && (unitInduk!==null || unitInduk!=="all")){
            return <option key={key} value={data.id}>{data.nama}</option>;
            }else{
              return <option key={key} value={data.id} >{data.nama}</option>;
            }
        }
      });
    }
  }

  dataSelecteUnitInduk(list){
    if(list !== undefined && this.state.loading !== true){
      return  list.map( (data, key) => {
        return <option key={key} value={data.id} >{data.nama}</option>;
      });
    }
  }
  
}

const mapStateToProps = (state) => {
  return{
      listPengUnitInduk: state.listPengUnitInduk,
      listPengUnitKerja: state.listPengUnitKerja,
      listAbsensiInduk: state.listAbsensiInduk,
  } 
}
export default connect(mapStateToProps, {getListUnitInduk, getListUnitKerja, getListAbsenInduk, getListAbsenKerja})(FilterShowData1);