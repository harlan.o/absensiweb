import React, { Component } from 'react';
import { Button, Card, Col, Form, FormCheck, Spinner } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import "./Filter.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { faFilter, faPrint } from '@fortawesome/free-solid-svg-icons';

import ReactToPrint from 'react-to-print';

import {getListAbsenInduk, getListAbsenKerja} from "../../actions/absensiAction";
import { getListUnitKerja } from "../../actions/unitKerjaAction";
import { getListUnitInduk } from "../../actions/unitIndukAction";
import {connect} from 'react-redux';

import SelectMultiplyUnitInduk from '../unit-induk/SelectMultiplyUnitInduk';
import SelectMultiplyUnitKerja from '../unit-kerja/SelectMultiplyUnitKerja';


const moment = require('moment');

let today =new Date();

class FilterShowDataDashboardtanpaUnitInduk extends Component {
    constructor (props) {
        super(props)
        this.state = {
          listUnitInduk:null,
          listUnitKerja:null,
          kodeUnitInduk: null,
          kodeUnitKerja: null,
          groupingUnitKerja:[],
          startDate: today,
          showFilter: false,
          loading:true,
          loadingBtn:false,

          selectMultyUnitInduk:[],
          selectMultyUnitKerja:[]
        };
        this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        this.handleShowFilter = this.handleShowFilter.bind(this);
        this.dataSelecteUnitInduk = this.dataSelecteUnitInduk.bind(this);
        this.dataSelecteUnitKerja = this.dataSelecteUnitKerja.bind(this);
        this.handleChangeUnitInduk = this.handleChangeUnitInduk.bind(this);
        this.handleChangeUnitKerja = this.handleChangeUnitKerja.bind(this);
      }
      handleChangeStartDate(start) {
        this.setState({
          startDate: start,
        })
      }

      handleChangeUnitInduk(v) {
        this.setState({
          kodeUnitInduk: v.target.value,
          kodeUnitKerja: "all",
          groupingUnitKerja:[]
        },
        () =>{
          let unitKerjaKecil= this.props.listPengUnitKerja?.data.map( (data, key) => {
            if(data !== undefined){
              if( data.unitInduk?.id == v.target.value && (v.target.value!==null || v.target.value!=="all") ){
                return data;
              }
            }
          });
          this.setState({
            listUnitKerja: unitKerjaKecil,
          }
          );
        })

      }
      handleChangeUnitKerja(v) {
        this.setState({
          kodeUnitKerja: v.target.value,
          groupingUnitKerja:[]
        })
      }
      handleShowFilter() {    
        this.setState(state => ({      
            showFilter: !state.showFilter
        }));
      }
      handlerFilter =  (e) =>  {
        e.preventDefault();
        this.setState({
          loadingBtn:true
         });
        let startDateModif = moment(this.state.startDate);
        let endDateModif = moment(this.state.endDate);
        let dataFilter={
          kodeUnitInduk:this.state.kodeUnitInduk,
          kodeUnitKerja:this.state.kodeUnitKerja,
          groupingUnitKerja:this.state.groupingUnitKerja,
          startDate:startDateModif.format('YYYY-MM-DD'),
          endDate:endDateModif.format('YYYY-MM-DD'),

          selectMultyUnitInduk:this.state.selectMultyUnitInduk,
          selectMultyUnitKerja:this.state.selectMultyUnitKerja
        };
        this.props.dataFilter(dataFilter);

    };

    dataCheckBox(list, unitInduk){    
      let datagroup = [];
      if(list !== undefined && this.state.loading !== true){
        list.map( (data) => {
          if(data !== undefined){
            if(data.unitInduk?.id == unitInduk && (unitInduk!==null || unitInduk!=="all")){
              let [a]=data.nama.split(" ");
              if(a == 'RUMAH'){ a='RUMAH SAKIT'}              
              const found = datagroup.some(el => el.namaGroup === a);
              if(found === false){
                let _data={"namaGroup":a}
                datagroup.push(_data);
              }
            }
          }
        });
      }
      // console.log(datagroup)
      // console.log(this.state.groupingUnitKerja)
      return datagroup.map ((data, key) => {
        let groups=[...this.state.groupingUnitKerja]
        if(data !== undefined){
          let namaGroupFix=data.namaGroup
          return <FormCheck inline type='checkbox' checked={groups.includes(namaGroupFix)} id={namaGroupFix} label={namaGroupFix} onChange={v => {
            // console.log(v.target.checked)
              if(v.target.checked){
                groups.push(namaGroupFix)
                this.setState({
                  groupingUnitKerja:groups,
                  kodeUnitKerja: "all"
                })
              }else{
                this.removeArray(groups, namaGroupFix);
                this.setState({
                  groupingUnitKerja:groups,
                  kodeUnitKerja: "all"
                })
              }
            }
          }/>
        }
      })         
    }

    removeArray(arr) {
      var what, a = arguments, L = a.length, ax;
      while (L > 1 && arr.length) {
          what = a[--L];
          while ((ax= arr.indexOf(what)) !== -1) {
              arr.splice(ax, 1);
          }
      }
      return arr;
    }
    // tinggal di test kemudia update dashboard
    dataSelecteUnitKerja(list, unitInduk){    
      let datagroup = [];
      if(list !== undefined && this.state.loading !== true){
        return  list.map( (data, key) => {
          if(data !== undefined){
            if(data.unitInduk?.id == unitInduk && (unitInduk!==null || unitInduk!=="all")){
              return <option key={key} value={data.id}>{data.nama}</option>;
            }else{
              return "";
            }
          }
        });
      }
     }

     dataSelecteUnitInduk(list){

      if(list !== undefined && this.state.loading !== true){
        return  list.map( (data, key) => {
          return <option key={key} value={data.id} >{data.nama}</option>;
        });
      }
     }

     async callApi(){
     await this.props.getListUnitInduk().then(()=>{
       this.props.getListUnitKerja().then(()=>{
            this.setState(() => ({
              loading:false,
              listUnitInduk:this.props.listPengUnitInduk?.data,
              listUnitKerja:this.props.listPengUnitKerja?.data,
            }));
            
          });  
      });
      
     }

     componentDidMount(){
      this.callApi();
      
    }
    componentDidUpdate(prevProps, prevState) {
      if(prevProps.listUnitKerja !== this.props.listUnitKerja){
        this.callApi();
        
      }
      const {loading} = this.props.listAbsensiDashboard;
      if(this.props.listAbsensiDashboard !== prevProps.listAbsensiDashboard){
        if (loading===false){
          this.setState({
            loadingBtn:false
          });
        }
      }
    }
     
render()
{

    return (
        <div className="custom-filter">
        <Button variant="secondary" className="btn-width" onClick={this.handleShowFilter} > <FontAwesomeIcon icon={faFilter} /> </Button>{' '}
        
        
        <ReactToPrint
                    trigger={() => {
                       
                        return <Button variant="secondary" className="btn-width"> <FontAwesomeIcon icon={faPrint} /></Button>;
                    }}
                    content={this.props.print}
                />
        <div className={this.state.showFilter ? "hidden unhidden" : "hidden"}>
                <Card className="custom-card-filter">
                    <Card.Title>Filter</Card.Title>
                        <Form >
                        <Form.Row className="align-items-center">

                        <Col xs="auto" className="col-md-6 my-1" >
                          { this.state.listUnitInduk ? <SelectMultiplyUnitInduk dataoption={this.state.listUnitInduk} valueGetMultiply={ v =>{this.setState({ selectMultyUnitInduk : v })} } /> : ""}
                        </Col>
                        <Col xs="auto" className="col-md-6 my-1" >
                          { this.state.listUnitKerja ? <SelectMultiplyUnitKerja dataoption={this.state.listUnitKerja} valueGetMultiply={ v =>{this.setState({ selectMultyUnitKerja : v })} } /> : ""}
                        </Col>



                            <Col xs="auto" className="col-md-6 my-1" >
                              <Form.Control
                                  as="select"
                                  className="mr-sm-2"
                                  id="dinasFilter"
                                  custom
                                  onChange={this.handleChangeUnitInduk}

                              >
                                  <option value="all">Semua Unit Induk...</option>
                                  {this.dataSelecteUnitInduk(this.state.listUnitInduk)}
                              </Form.Control>
                            </Col>
                                
                          <Col xs="auto" className="col-md-6 my-1" >
                            <Form.Control
                              as="select"
                              className="mr-sm-2"
                              id="groupFilter"
                              custom
                              value={this.state.kodeUnitKerja}
                              onChange={this.handleChangeUnitKerja}
                            >
                              <option value="all">Semua Unit Kerja...</option>
                              {this.dataSelecteUnitKerja(this.state.listUnitKerja, this.state.kodeUnitInduk)}
                            </Form.Control>
                          </Col>
                          { this.state.kodeUnitInduk ? (<Col xs="auto" className="col-md-12 my-1" >
                            <Card border="secondary" style={{ width: '100%' }}>
                              <Card.Body>
                                <Card.Title>Group Filter Berdasarkan Unit Induk</Card.Title>
                                <Card.Text>
                                  {this.dataCheckBox(this.state.listUnitKerja, this.state.kodeUnitInduk)}
                                </Card.Text>
                              </Card.Body>
                            </Card>  
                          </Col>):'' }
                          
                    

                <Col xs="auto" className=" my-1" >
                <Form.Label htmlFor="inlineFormInputName" srOnly>
                    Name
                </Form.Label>
                <DatePicker
                                className="form-control"
                                selected={this.state.startDate }
                                onChange={this.handleChangeStartDate}
                                placeholderText="Pilih Tanggal Awal"
                            />
                </Col>

         

        
                <Col xs="auto" className="my-1">
                <Button type="submit" onClick={this.handlerFilter}>
                {this.state.loadingBtn ? (
                            <Spinner animation="border" className="spin" />
                        ) : ( "Filter"
                        )}
                        
                </Button>
                </Col>
            </Form.Row>
            </Form>
            </Card>
        </div>
        </div>
    );
}


}

// get all kode unit induk 
// get all kode unit kerja

// action getListAbsenInduk
// action getListAbsenKerja
const mapStateToProps = (state) => {
  return{
      listPengUnitInduk: state.listPengUnitInduk,
      listPengUnitKerja: state.listPengUnitKerja,
      listAbsensiDashboard: state.listAbsensiDashboard,
      // getListUnitKerja: state.getListUnitKerja,
  } 
}
export default connect(mapStateToProps, {getListUnitInduk, getListUnitKerja, getListAbsenInduk, getListAbsenKerja})(FilterShowDataDashboardtanpaUnitInduk);