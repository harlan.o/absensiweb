import React, { Component } from 'react';
import { Button, Card, Col, Form, FormCheck, Spinner } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import "./Filter.css";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

import "react-datepicker/dist/react-datepicker.css";
import 'bootstrap/dist/css/bootstrap.min.css';
import { faPrint } from '@fortawesome/free-solid-svg-icons';

import ReactToPrint from 'react-to-print';

import { getListUnitKerja } from "../../actions/unitKerjaAction";
import { getListUnitInduk } from "../../actions/unitIndukAction";
import {connect} from 'react-redux';

import SelectMultiplyUnitInduk from '../unit-induk/SelectMultiplyUnitInduk';
import SelectMultiplyUnitKerja from '../unit-kerja/SelectMultiplyUnitKerja';
import { uIIDP } from '../../actions/userActions';
import { cekHakAksesClient } from '../../actions/hakAksesActions';


const moment = require('moment');

let today = new Date();
class TrueFilterSingleMonthMultipleKerja extends Component {
  // Dipakai Khusus Schedule Pegawai
    constructor (props) {
        super(props)
        this.state = {
          filterType:"SUPER_ADMIN",
          listUnitInduk:null,
          listUnitKerja:null,
          groupingUnitKerja:[],
          startDate:today,
          loading:true,
          loadingBtn:false,
          selectMultyUnitInduk:[],
          selectMultyUnitKerja:[],

          showFormAdd: true,
        };
        this.handleChangeStartDate = this.handleChangeStartDate.bind(this);
        this.handleChangeUnitIndukMultiply = this.handleChangeUnitIndukMultiply.bind(this);
      }


     async callApi(){
      // let listUI=[];
      let listUK=[];
      // if(this.props.listPengUnitInduk?.data != null && this.props.listPengUnitInduk?.data != undefined){
      //     listUI=this.props.listPengUnitInduk?.data;
      // }
      if(this.props.listPengUnitKerja?.data != null && this.props.listPengUnitKerja?.data != undefined){
          listUK=this.props.listPengUnitKerja?.data;
      }

      this.setState(() => ({
          loading:false,
          // listUnitInduk:listUI,
          listUnitKerja:listUK
        })); 
      // console.log("cek here");
      // console.log(this.props.listPengUnitInduk?.data)
      // console.log(this.props.listPengUnitKerja?.data)
     await this.props.getListUnitKerja().then(()=>{
        this.setState(() => ({
          loading:false,
          listUnitKerja:this.props.listPengUnitKerja?.data,
        }), () => {
          let _v = [{ id:uIIDP()}]
          this.handleChangeUnitIndukMultiply(_v)
        });
        
      });  
      
      
     }

     componentDidMount(){
      this.callApi();   
    }
    componentDidUpdate(prevProps, prevState) {
      if(prevProps.listUnitInduk !== this.props.listUnitInduk){
        this.callApi();
      }
      // const {loading} = this.props.listAbsensiDashboard;
      const loading = this.props.setloading;
      if(this.props.setloading !== prevProps.setloading){
        if (this.props.setloading===false){
          this.setState({
            loadingBtn:false
          });
        }
      }
    }
     
render()
{
    return (
        <div className="custom-filter">
          <div className="mb-2">
                <Card className="custom-card-filter">
                    <Card.Title>Permintaan Jadwal Pegawai</Card.Title>
                        <Form >
                        <Form.Row className="align-items-center">

                        {/* <Col xs="auto" className="col-md-6 my-1" >
                          { this.state.listUnitInduk ? <SelectMultiplyUnitInduk dataoption={this.state.listUnitInduk} valueGetMultiply={this.handleChangeUnitIndukMultiply} valueOption={this.state.selectMultyUnitInduk}/> : ""}
                        </Col> */}
                        <Col xs="auto" className="col-md-6 my-1" >
                          { this.state.listUnitKerja ? <SelectMultiplyUnitKerja dataoption={ this.state.selectMultyUnitInduk.length != 0 ? this.state.listUnitKerja : []} valueGetMultiply={ v =>{this.setState({ selectMultyUnitKerja : v, groupingUnitKerja:[] })} } valueOption={this.state.selectMultyUnitKerja}/> : ""}
                        </Col>

                                
{/* 
                          { this.state.selectMultyUnitInduk.length ? (<Col xs="auto" className="col-md-12 my-1" >
                            <Card border="secondary" style={{ width: '100%' }}>
                              <Card.Body>
                                <Card.Title>Group Filter Berdasarkan Unit Induk</Card.Title>
                                <Card.Text>
                                  {this.dataCheckBox(this.state.listUnitKerja)}
                                </Card.Text>
                              </Card.Body>
                            </Card>  
                          </Col>):'' }
                           */}
                    

                <Col xs="auto" className=" my-1" >
                <Form.Label htmlFor="inlineFormInputName" srOnly>
                    Name
                </Form.Label>
                <DatePicker
                                className="form-control"
                                selected={this.state.startDate }
                                onChange={this.handleChangeStartDate}
                                placeholderText="Pilih Bulan dan Tahun"
                                dateFormat="yyyy-MM"
                                showMonthYearPicker
                                showTwoColumnMonthYearPicker
                  />
                </Col>

         

        
                <Col xs="auto" className="my-1">
                <Button type="submit" onClick={this.handlerFilter}>
                {this.state.loadingBtn ? (
                            <Spinner animation="border" className="spin" />
                        ) : ( "Lihat"
                        )}
                        
                </Button>
                </Col>
            </Form.Row>
            </Form>
            </Card>
        </div>

        {!this.props.setloading ? (
          <div>          
            {cekHakAksesClient(this.props.kodeCetak) ? 
          <ReactToPrint
                      trigger={() => {
                        
                          return <Button variant="secondary" className="btn-width"> <FontAwesomeIcon icon={faPrint} /></Button>;
                      }}
                      content={this.props.print}
                  />
                  :""}
                  </div>
        ) : " "}

      </div>
    );
}



  handleChangeStartDate(start) {
    this.setState({
      startDate: start,
    })
  }

  handleChangeUnitIndukMultiply(v) {
    let selectMultyUnitKerja= this.state.selectMultyUnitKerja
    if(v == null){
      v=[]
    }
    this.setState({
      selectMultyUnitInduk : v,
      groupingUnitKerja:[],
      selectMultyUnitKerja:selectMultyUnitKerja
    },
    () =>{
      let idIndukArr = [];
      for(var i = 0; i < v?.length; i++)
      {
          idIndukArr = idIndukArr.concat(v[i].id);
      }
      
      let unitKerjaKecil= this.props.listPengUnitKerja.data
      if(v.length != 0 && v != null){
        
        unitKerjaKecil= unitKerjaKecil?.filter(
          (kerja) => {
            return idIndukArr.indexOf(kerja.unitInduk.id) != -1
          });
        selectMultyUnitKerja= selectMultyUnitKerja.filter(
          (kerja) => {
            return idIndukArr.indexOf(kerja.unitInduk.id) != -1
          });
      }else{
        unitKerjaKecil=[]
        selectMultyUnitKerja=[]
      }
      this.setState({
        listUnitKerja: unitKerjaKecil,
        selectMultyUnitKerja: selectMultyUnitKerja,
      }
      );
    })

  }



  handlerFilter =  (e) =>  {
    e.preventDefault();
    this.setState({
      loadingBtn:true
    });
    let startDateModif = moment(this.state.startDate);
    let endDateModif = moment(this.state.endDate);
    let dataFilter={
      groupingUnitKerja:this.state.groupingUnitKerja,
      startDate:startDateModif.format('YYYY-MM'),
      selectMultyUnitInduk:this.state.selectMultyUnitInduk,
      selectMultyUnitKerja:this.state.selectMultyUnitKerja
    };
    this.props.dataFilter(dataFilter);

  };

  dataCheckBox(list){    
  let datagroup = [];
  if(list !== undefined && this.state.loading !== true){
    list.map( (data) => {

      let [a]=data.nama.split(" ");
      if(a == 'RUMAH'){ a='RUMAH SAKIT'}              
      const found = datagroup.some(el => el.namaGroup === a);
      if(found === false){
      let _data={"namaGroup":a}
            datagroup.push(_data);
      }
    });
  }
  return datagroup.map ((data, key) => {
    let groups=[...this.state.groupingUnitKerja]
    if(data !== undefined){
      let namaGroupFix=data.namaGroup
      return <FormCheck inline type='checkbox' checked={groups.includes(namaGroupFix)} id={namaGroupFix} label={namaGroupFix} onChange={v => {
          if(v.target.checked){
            groups.push(namaGroupFix)
            this.setState({
              groupingUnitKerja:groups,
              selectMultyUnitKerja: []
            })
          }else{
            this.removeArray(groups, namaGroupFix);
            this.setState({
              groupingUnitKerja:groups,
              selectMultyUnitKerja: []
            })
          }
        }
      }/>
    }
  })         
  }

  removeArray(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
  }



}

const mapStateToProps = (state) => {
  return{
      listPengUnitInduk: state.listPengUnitInduk,
      listPengUnitKerja: state.listPengUnitKerja,
  } 
}
export default connect(mapStateToProps, {getListUnitInduk, getListUnitKerja})(TrueFilterSingleMonthMultipleKerja);