import React, { Component } from 'react';
import AsyncSelect from 'react-select/async';
import { GolonganPegawai } from '../../constants/dataHardcode';

export class SelectSingleGolongan_NameSend extends Component {
  constructor(props) {
    super(props);
    this.state = {
        optionUnitInduk:GolonganPegawai,
        inputValue: '',
        valueOption:null,
        isDisabled:false,
    };
    
  }

  async update(dataValue) {    
    await this.setState(({       
      valueOption: dataValue,
    }));
  }
  componentDidMount(){
    this.update(this.props.valueOption)
  }
  componentDidUpdate(prevProps, prevState) {
    if(this.props.valueOption !== prevProps.valueOption){
      if(this.props.valueOption !== undefined){
        console.log("asd")
        console.log(this.props.valueOption)
        this.setState((state) => ({
            valueOption: this.props.valueOption,
        }));
      } 
    }
    if(this.props.isDisabled !== prevProps.isDisabled){
      if(this.props.valueOption !== undefined){
        this.setState((state) => ({
          isDisabled: this.props.isDisabled,
        }));
      } 
    }
  }

   filterColors = (inputValue) => {
     let bab;
       bab= this.state.optionUnitInduk?.filter(i =>
       i.toLowerCase().includes(inputValue.toLowerCase())
       );
     return bab
  };
  
   promiseOptions = inputValue =>
    new Promise(resolve => {
      setTimeout(() => {
        resolve(this.filterColors(inputValue));
      }, 1000);
    });
  
    valueGetSend = (v)=>
    {
      this.props.valueGetSingle(v);
    }

  
  render() {
    return (      
      <AsyncSelect cacheOptions onChange={this.valueGetSend} 
      value={this.state.optionUnitInduk.filter(option => {
        return option === this.state.valueOption;
      })}
      defaultOptions={this.state.optionUnitInduk}
      isDisabled={this.state.isDisabled}
      getOptionValue={option => option}
      getOptionLabel={option => option}
      isClearable
      loadOptions={this.promiseOptions} 
      placeholder='Pilih Golongan'
      />
    );
  }
}


export default SelectSingleGolongan_NameSend;

