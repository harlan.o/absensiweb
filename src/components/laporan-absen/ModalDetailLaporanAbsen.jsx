import React, { Component } from 'react';
import { Badge, Button, Card, Col, Form, ListGroup, Modal, Row } from 'react-bootstrap';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ResponsiveCalender from '../ResponsiveCalender';

const moment = require('moment');
const dataLokasi=[{id:1,nama:"Manado"},{id:2,nama:"Siau"},{id:3,nama:"Tagulandang"},{id:4,nama:"Biaro"}]
class ModalDetailLaporanAbsen extends Component {
    constructor(props) {
        super(props);
        this.state = {
           judul:"Harap Tunggu...",
            data:this.props.dataonerow,
           listTanggal:null,
           listTanggalJadwalConfirm:[],
           listJadwal:null,
        };
    }
    componentDidMount(){
        this.callAPI()
     }
    async callAPI(){
        await this.setState(({       
            data:this.props.dataonerow,
        }));
    }


    render (){
        let totHadir=this.state.data.listAbsen.filter(r =>{
          let status= r.status.split(" - ")
          if(status[0] === "HADIR"){
            return true
          }
        }).length;
        let totTerlambat=this.state.data.listAbsen.filter(r =>{
          let status= r.status.split(" - ")
          if(status[0] === "HADIR"){
            if(status[1] === "TERLAMBAT" || status[1] === "TIDAK ABSEN MASUK"){
              return true
            }
          }
        }).length;
        let totIjin=this.state.data.listAbsen.filter(r =>{
          let status= r.status.split(" - ")
          if(status[0] === "IJIN"){
              return true
          }
        }).length;
        let totSakit=0;
        let totTanpaKeterangan=this.state.data.listAbsen.filter(r =>{
          let status= r.status.split(" - ")
          if(status[0] === "TIDAK HADIR"){
            if(status[1] === "TANPA KETERANGAN"){
              return true
            }
          }
        }).length;


    return (
      <>
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>

                <Row>
                    <Col xs={12} md={6} lg={4}>
                        <Card className="mb-2">
                            <Card.Body>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_nama">
                                    <Form.Label className="textblack">Nama</Form.Label>
                                    <h4>{this.state.data.namaPegawai}</h4>
                                    <hr/>
                                    </Form.Group>
                                </Form.Row>   
                                
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_nip">
                                    <Form.Label className="textblack">Nip</Form.Label>
                                    <h4>{this.state.data.nip}</h4>
                                    <hr/>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_nip">
                                    <Form.Label className="textblack">Unit</Form.Label>
                                    <h4>{this.state.data.unitInduk}<br/><small>{this.state.data.unitKerja}</small></h4>
                                    <hr/>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_nip">
                                    <Form.Label className="textblack">Total Hari Kerja</Form.Label>
                                    <h4>{this.state.data.totalHariKerja}</h4>
                                    <hr/>
                                    </Form.Group>
                                </Form.Row>                   
                            </Card.Body>
                        </Card>

                    </Col>
                    <Col xs={12} md={6} lg={8}>
                        <Card className="mb-2">
                            <Card.Body className="mx-auto">
                                <ListGroup horizontal >
                                    <ListGroup.Item variant="">Hadir | <Badge variant="secondary">{totHadir}</Badge></ListGroup.Item>
                                    <ListGroup.Item variant="">Terlambat | <Badge variant="secondary">{totTerlambat}</Badge></ListGroup.Item>
                                    <ListGroup.Item variant="">Ijin | <Badge variant="secondary">{totIjin}</Badge></ListGroup.Item>
                                    <ListGroup.Item variant="">Sakit | <Badge variant="secondary">{totSakit}</Badge></ListGroup.Item>
                                    <ListGroup.Item variant="">Tanpa Keterangan | <Badge variant="secondary">{totTanpaKeterangan}</Badge></ListGroup.Item>
                                </ListGroup>
                            </Card.Body>
                        </Card>
                        <Card className="mb-2">
                            <Card.Body className="mx-auto">

                                <ResponsiveCalender listAbsen={this.state.data?.listAbsen} tahunBulan={this.props.filter.startDate}/>                   
                                </Card.Body>
                        </Card>
                    </Col>
                </Row>

              </Modal.Body>
          <Modal.Footer>
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
          </Modal.Footer>
        </>
      );
}


}

export default ModalDetailLaporanAbsen;