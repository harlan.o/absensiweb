import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import ModalDetailLaporanAbsen from './ModalDetailLaporanAbsen';
import ModalFormUpdateJadwalPegawai from './ModalDetailLaporanAbsen';

class ModalLaporanAbsen extends Component{
    constructor(props) {
        super(props);        
        this.state = {
           jenisModal:"JENIS",
           data:null,
        };
    }

    async callAPI(modal, data){
        this.setState(() => ({
            jenisModal:modal,
            data:data
        }));
    }

    componentDidMount(){
        console.log("hadir")
        this.callAPI(this.props.jenisModal, this.props.dataonerow);
    }

    render (){
        return (
            <Modal 
              {...this.props}
              aria-labelledby="contained-modal-title-vcenter"
              dialogClassName="modal-90w">
                  {this.randerModal(this.state.jenisModal)}
            </Modal>
          );
    }
    randerModal(){
        console.log(this.state.jenisModal)
        switch (this.state.jenisModal) {
            case "DETAIL":
                return(
                <ModalDetailLaporanAbsen filter={this.props.filter} onHide={()=> {this.props.onHide()}} dataonerow={this.props.dataonerow} reloadTabel={()=>{this.props.reloadTabel()}}/>
                )
            default:
                break;
        }
    }

    // handleChangeModeDetailToEdit(){
    //     this.setState({
    //         jenisModal: "UBAH",
    //       })
    // }
    // handleChangeModeEditToDetail(){
    //     this.setState({
    //         jenisModal: "DETAIL",
    //       })
    // }

}

export default  ModalLaporanAbsen;