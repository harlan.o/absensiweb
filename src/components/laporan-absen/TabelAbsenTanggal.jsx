import React, { Component } from "react";
import { Table, Card, Col, Form, Row, Button, Spinner } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import "./TabelDefault.css";
import { faBriefcase, faBuilding, faEdit, faHouseUser, faLaptopHouse, faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { clockConversi } from "../../actions/genneralAction";
const moment = require('moment');
class TabelJadwalPegawai extends Component {
    constructor(props) {
        super(props); 
        this.state = {
          totalData:0,
            search:'',
            rowData:null,
            loading:true
        };
        this.renderTableData=this.renderTableData.bind(this);    
      }
    
  async callApi(){
    if(this.props.dataRow !== null && this.props.dataRow !== undefined){
      let dataCombin=this.props.dataRow;
      await this.setState(() => ({
       rowData:dataCombin,  
       loading:false    
      })) 
    }
  }

  componentDidMount(){
    if(this.state.loading !== false){
      this.callApi();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.getFilter !== this.props.getFilter || prevProps.dataRow !== this.props.dataRow){
      this.callApi();
    }
  }

  thHeader(no){
    let indents = [];
    for (let i = 1; i <= no; i++) {
      indents.push(<th key={i}>{i}</th>);
    }
    return indents;
  }
  tdJadwal(arr, totalTd){
    let indents = [];
    
    for (let i = 1; i <= totalTd; i++) {
        let d = arr.find((x) =>  moment(x.tanggalKerja).format('DD') == i)
        // console.log(d)
        let _icon=faBuilding;
        if(d?.wfh){
          _icon=faHouseUser;
        }

        if(d != null && d != undefined){
            indents.push(<td key={i}>
              
              <small>
              {clockConversi(d.jamMasuk)}<br/>{clockConversi(d.jamPulang)} 
              <br/>
              <FontAwesomeIcon icon={_icon} />
              
              {/* <FontAwesomeIcon icon={faBuilding} /> */}
              </small>
              {d?.melaksanakanTugas ? (
               <>{' '}<FontAwesomeIcon icon={faBriefcase} /></>
              ):"" }
              {d?.tugasLuar ? (
               <>{' '}<FontAwesomeIcon icon={faBriefcase} /></>
              ):"" }

              </td>);
        }else{
            indents.push(<td key={i}>- </td>);
        }
    }
    return indents;
  }
  renderTableData(totalTd){
    if(this.state.loading !== true && this.state.rowData !== undefined && this.state.rowData !== null){
      let datashow = this.state.rowData.filter( rowData =>{
        if(rowData.namaPegawai.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1 || rowData.namaPegawai.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1){
          return true
        }
        return false
      });

      return datashow.map( (data, key) => {
        
        let no= key+1;
        this.count=no;

        return(
          <tr key={key}>
              <td>{no}</td>
              <td>{data.namaPegawai}
              </td>
              <td>  <p style={{margin:'0px'}} className="text-right">Masuk<br/>Pulang<br/>Info</p></td>
                {this.tdJadwal(data.jadwalList, totalTd)}
                <td>
                {/* <Button variant="secondary" className="btn-width"
                onClick={ 
                  (e)=>{
                      e.preventDefault();
                      this.props.setdataonerow(data, "UBAH_WFH")
                  }}
                size="sm"><FontAwesomeIcon icon={faLaptopHouse} /></Button>{' '} */}
                  <Button variant="secondary" className="btn-width"
                onClick={ 
                  (e)=>{
                      e.preventDefault();
                      this.props.setdataonerow(data, "UBAH")
                  }}
                size="sm"><FontAwesomeIcon icon={faPencilAlt} /></Button>
                </td>
          </tr>
        )
      }     
      );
    }else{
      return(
        <tr>
          <td colSpan={totalTd+3} className="loadingPage3">
            <Spinner animation="border" className="spin"  variant="dark" />
          </td>
        </tr>
      )
    }
  }
  

  render() {
    // console.log("render table");
    return (

    <Card className="mb-4" border="light" text="dark">  
        
        <Card.Body>
        <Row className="mb-2">
        <Col xs={6} md={4}>
                 <h2>{this.props.title}</h2>
                 {/* <p> ({this.props.jumlahTanggal} hari)</p> */}
            </Col>
            <Col xs={6} md={{ span: 4, offset: 4 }}>
              <Form.Row className="align-items-center">
                <Form.Label  column xs="auto">
                  Cari
                </Form.Label>
                <Col>
                  <Form.Control type="text" placeholder="Cari " onChange={v => this.setState(({search: v.target.value }))}/>
                </Col>
              </Form.Row>
            </Col> 
            </Row>
            <Table responsive bordered className="table table-striped table-hover custom-table4 text-nowrap">
            <tbody>
                <tr>
                  <th>No.</th>
                  <th className="colomInti">______</th>
                  {this.thHeader(this.props.jumlahTanggal)}
                  <th>---</th>
                </tr>
                    {/* {this.renderTableData(this.props.jumlahTanggal)} */}
                </tbody>
            </Table>
            <Card className="mt-2" border="secondary">
              <Card.Header>
                Info:
              </Card.Header>
              <Card.Body>
      
      <Row>
        <Col lg="2">
            <FontAwesomeIcon icon={faBuilding} /> Lokasi Kantor 
        </Col>
        <Col lg="2">
            <FontAwesomeIcon icon={faHouseUser} /> Kerja Dari Rumah
        </Col>
        <Col lg="2">
            <FontAwesomeIcon icon={faBriefcase} /> Tugas / Melaksanakan Tugas
        </Col>
      </Row>
              </Card.Body>
            </Card>
        </Card.Body>
    </Card>
    );
  }
}
export default TabelJadwalPegawai;