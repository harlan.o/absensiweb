import React, { Component } from "react";
import { Table, Card, Col, Form, Row, Button, Spinner } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import "./TabelDefault.css";
import { faBuilding, faEdit, faEye, faHouseUser, faLaptopHouse, faPencilAlt } from "@fortawesome/free-solid-svg-icons";
import { clockConversi } from "../../actions/genneralAction";
const moment = require('moment');

class TabelLaporanAbsen extends Component {
  constructor(props) {
        super(props); 
        this.state = {
          totalData:0,
            search:'',
            rowData:null,
            loading:true
        };
        this.renderTableData=this.renderTableData.bind(this);    
  }
    
  async callApi(){
    if(this.props.dataRow !== null && this.props.dataRow !== undefined){
      let dataCombin=this.props.dataRow;
      await this.setState(() => ({
       rowData:dataCombin,  
       loading:false    
      })) 
    }
  }

  componentDidMount(){
    if(this.state.loading !== false){
      this.callApi();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.getFilter !== this.props.getFilter || prevProps.dataRow !== this.props.dataRow){
      this.callApi();
    }
  }


  renderTableData(totalTd){
    if(this.state.loading !== true && this.state.rowData !== undefined && this.state.rowData !== null){
      let datashow = this.state.rowData.filter( rowData =>{
        if(rowData.namaPegawai.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1 || rowData.namaPegawai.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1){
          return true
        }
        return false
      });

      return datashow.map( (data, key) => {
        
        let no= key+1;
        this.count=no;
        console.log(data.listAbsen)
        let totHadir=data.listAbsen.filter(r =>{
          let status= r.status.split(" - ")
          if(status[0] === "HADIR"){
            return true
          }
        }).length;
        let totTerlambat=data.listAbsen.filter(r =>{
          let status= r.status.split(" - ")
          if(status[0] === "HADIR"){
            if(status[1] === "TERLAMBAT" || status[1] === "TIDAK ABSEN MASUK"){
              return true
            }
          }
        }).length;
        let totIjin=data.listAbsen.filter(r =>{
          let status= r.status.split(" - ")
          if(status[0] === "IJIN"){
              return true
          }
        }).length;
        let totSakit=0;
        let totTanpaKeterangan=data.listAbsen.filter(r =>{
          let status= r.status.split(" - ")
          if(status[0] === "TIDAK HADIR"){
            if(status[1] === "TANPA KETERANGAN"){
              return true
            }
          }
        }).length;
        
        return(
          <tr key={key}>
              <td>{no}</td>
              <td>{data.namaPegawai} <br/><small> {data.nip}</small><br/>
              {data.unitInduk}<br/> <small>{data.unitKerja}</small> </td>
              <td>{data.totalHariKerja}</td>
              <td>{totHadir}</td>
              <td>{totTerlambat}</td>
              <td>{totIjin}</td>
              <td>{totSakit}</td>
              <td>{totTanpaKeterangan}</td>
                <td>
                  <Button variant="secondary" className="btn-width"
                onClick={ 
                  (e)=>{
                      e.preventDefault();
                      this.props.setdataonerow(data, "DETAIL")
                  }}
                size="sm"><FontAwesomeIcon icon={faEye} /></Button>
                </td>
          </tr>
        )
      }     
      );
    }else{
      return(
        <tr>
          <td colSpan={9} className="loadingPage3">
            <Spinner animation="border" className="spin"  variant="dark" />
          </td>
        </tr>
      )
    }
  }
  

  render() {
    // console.log("render table");
    return (

    <Card className="mb-4" border="light" text="dark">  
        
        <Card.Body>
        <Row className="mb-2">
        <Col xs={6} md={4}>
                 <h2>{this.props.title}</h2>
                 {/* <p> ({this.props.jumlahTanggal} hari)</p> */}
            </Col>
            <Col xs={6} md={{ span: 4, offset: 4 }}>
              <Form.Row className="align-items-center">
                <Form.Label  column xs="auto">
                  Cari
                </Form.Label>
                <Col>
                  <Form.Control type="text" placeholder="Cari " onChange={v => this.setState(({search: v.target.value }))}/>
                </Col>
              </Form.Row>
            </Col> 
            </Row>
            <Table responsive bordered className="table table-striped table-hover custom-table4 text-nowrap">
            <tbody>
                <tr>
                  <th>No.</th>
                  <th className="colomInti">Pegawai</th>
                  <th className="colomInti">Hari Kerja</th>
                  <th className="colomInti">Hadir</th>
                  <th className="colomInti">Terlambat</th>
                  <th className="colomInti">Ijin</th>
                  <th className="colomInti">Sakit</th>
                  <th className="colomInti">Tidak Hadir</th>
                  <th>---</th>
                </tr>
                    {this.renderTableData()}
                </tbody>
            </Table>
            {/* <Card className="mt-2" border="secondary">
              <Card.Header>
                Info:
              </Card.Header>
              <Card.Body>
      
      <Row>
        <Col lg="2">
            <FontAwesomeIcon icon={faBuilding} /> Lokasi Kantor 
        </Col>
        <Col lg="2">
            <FontAwesomeIcon icon={faHouseUser} /> Kerja Dari Rumah
        </Col>
      </Row>
              </Card.Body>
            </Card> */}
        </Card.Body>
    </Card>
    );
  }
}
export default TabelLaporanAbsen;