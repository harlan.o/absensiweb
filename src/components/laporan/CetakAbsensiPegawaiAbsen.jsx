import React, { Component, Fragment } from 'react';
import { Table } from 'react-bootstrap';
import './laporan.css';
const moment = require('moment');
class CetakAbsensiPegawaiAbsen extends Component {
    
    render(){
        let datarow
        if(this.props.datatable != null){
            datarow= this.props.datatable;
        }
        return (
            <div>
                <h1>Daftar Absen <br/><small>{this.props.title}</small></h1>
                <Table>
                    <thead>
                    <th>No.</th>
                    <th>Pegawai</th>
                    <th>Keterangan</th>
                    </thead>
                    <tbody>
                    {this.renderTableData(datarow)}
                    </tbody>
                </Table>
            </div>
        );
    }
    renderTableData(datarow){
       if(datarow){
           return datarow.map( (data, key) => {
             let no= key+1;
             this.count=no;
             return(
               <tr key={key}>
                   <td>{no}</td>
                   <td>{data?.namaPegawai}<br/>{data?.nip} <br/>{data?.unitInduk}<br/>{data?.unitKerja} </td>
                   <td>{data.keterangan.toUpperCase()}</td>
               </tr>
             )
           }     
           );
       } 
      }
}


export default CetakAbsensiPegawaiAbsen;