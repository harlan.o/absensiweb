

import React, { Component, Fragment } from 'react';
import { Table } from 'react-bootstrap';
import './laporan.css';
const moment = require('moment');
class CetakAbsensiPegawaiHadir extends Component {
    
    render(){
        let datarow
        if(this.props.datatable != null){
            datarow= this.props.datatable;
        }
        return (
            <div>
                <h1>Daftar Kehadiran <br/><small>{this.props.title}</small></h1>
    
                <Table>
                    <thead>
                    <th>No.</th>
                    <th>Pegawai</th>
                    <th>Waktu</th>
                    </thead>
                    <tbody>
                    {this.renderTableData(datarow)}
                    </tbody>
                </Table>
            </div>
        );
    }
    renderTableData(datarow){
       if(datarow){
           return datarow.map( (data, key) => {
             let no= key+1;
             this.count=no;
             return(
               <tr key={key}>
                   <td>{no}</td>
                   <td>{data?.nama}<br/>{data?.nip} <br/>{data?.unitInduk}<br/>{data?.unitKerja} </td>
                   <td>
                        {data.absen ? this.listAbsen(data.absen):"-"}
                    </td>
               </tr>
             )
           }     
           );
       } 
      }

      listAbsen(absen){
        return absen.map((data, key) => {
          let dateAbsen = moment(data.waktu);
          let tgl= dateAbsen.format('YYYY-MM-DD');
            return(<Fragment key={key}>
                <p>{data.type} : {dateAbsen.format(' HH:mm')}{' '}{data.keterangan == "MASUK_TERLAMBAT" ? (<b>Terlambat</b>):""} </p>
              </Fragment>)
        });
      }

}


export default CetakAbsensiPegawaiHadir;