

import React, { Component, Fragment } from 'react';
import { Table } from 'react-bootstrap';
import './laporan.css';
const moment = require('moment');
class CetakDashboard extends Component {
    
    render(){
        let datarow=[]
        let datarow2=[]
        if(this.props.dataCetak.daftarHadir != null){
            datarow= this.props.dataCetak.daftarHadir;
        }
        if(this.props.dataCetak.daftarHadir != null){
            datarow2= this.props.dataCetak.daftarTerlambat;
        }
        return (
            <div>
                <h1>Daftar Kehadiran Tepat Waktu</h1>
                <Table>
                    <thead>
                    <th>No.</th>
                    <th>Waktu</th>
                    <th>Pegawai</th>
                    </thead>
                    <tbody>
                    {this.renderTableData(datarow)}
                    </tbody>
                </Table>
                <div className="page-break"/>
                <h1>Daftar Kehadiran Terlambat </h1>
                <Table>
                    <thead>
                    <th>No.</th>
                    <th>Waktu</th>
                    <th>Pegawai</th>
                    </thead>
                    <tbody>
                    {this.renderTableData(datarow2)}
                    </tbody>
                </Table>

                <h2>Rangkuman</h2>
                <Table>
                    <tbody>
                        <tr>
                            <td>Total Pegawai</td>
                            <td>{this.props.dataCetak.totalPegawai}</td>
                        </tr>
                    </tbody>
                </Table>
                <Table>
                    <tbody>
                        <tr>
                            <td>WFH | {this.props.dataCetak.totalWfh}</td>
                            <td>Tugas Luar | {this.props.dataCetak.totalTugasLuar}</td>
                            <td>Melaksanakan Tugas | {this.props.dataCetak.totalMelaksanakanTugas}</td>
                        </tr>
                        </tbody>
                </Table>
                <Table>
                    <tbody>
                        <tr>
                            <td>Hadir</td>
                            <td>{this.props.dataCetak.totalHadir}</td>
                        </tr>
                        <tr>
                            <td>Terlambat</td>
                            <td>{this.props.dataCetak.totalTerlambat}</td>
                        </tr>
                        <tr>
                            <td>Ijin</td>
                            <td>{this.props.dataCetak.totalIjin}</td>
                        </tr>
                        <tr>
                            <td>Tanpa Keterangan</td>
                            <td>{this.props.dataCetak.totalTanpaKeterangan}</td>
                        </tr>
                    </tbody>
                </Table>

            </div>
        );
    }
    renderTableData(datarow){
       if(datarow){
           return datarow.map( (data, key) => {
             let no= key+1;
             this.count=no;
             let dateAbsen = moment(data.jamAbsen);
             return(
               <tr key={key}>
                   <td>{no}</td>
                   <td>{dateAbsen.format(' h:mm:ss a')}</td>
                   <td>{data?.nama}<br/>{data?.nip} <br/>{data?.unitInduk}<br/>{data?.unitKerja} </td>
               </tr>
             )
           }     
           );
       } 
      }

      listAbsen(absen){
        return absen.map((data, key) => {
          let dateAbsen = moment(data.waktu);
          let tgl= dateAbsen.format('YYYY-MM-DD');
            return(<Fragment key={key}>
                <p>{data.type} : {dateAbsen.format(' HH:mm')}{' '}{data.keterangan == "MASUK_TERLAMBAT" ? (<b>Terlambat</b>):""} </p>
              </Fragment>)
        });
      }

}


export default CetakDashboard;