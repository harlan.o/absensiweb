import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import './laporan.css';

class CetakHariLibur extends Component {
    
    render(){
        let datarow
        if(this.props.datatable != null){
            datarow=  this.props.datatable.sort((a,b) => { return new Date(a.tanggal) - new Date(b.tanggal)});
        }
        return (
            <div>
                <h1>Daftar Hari Libur</h1>
    
                <Table>
                    <thead>
                    <th>No.</th>
                    <th>Tanggal</th>
                    <th>Keterangan</th>                    
                    </thead>
                    <tbody>
                    {this.renderTableData(datarow)}
                    </tbody>
                </Table>
            </div>
        );
    }
    renderTableData(datarow){
       if(datarow){

           return datarow.map( (data, key) => {
             let no= key+1;
             this.count=no;
             return(
               <tr key={key}>
                   <td>{no}</td>
                   <td>{data?.tanggal}</td>
                   <td>{data?.keterangan}</td>
               </tr>
             )
           }     
           );
       }
        
      }

}


export default CetakHariLibur;