import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import './laporan.css';
const moment = require('moment');
class DaftarPegawai extends Component {
    
    render(){
        let datarow
        if(this.props.datatable != null){
            datarow= this.props.datatable;
        }
        return (
            <div>
                <h1>Daftar Pegawai</h1>
    
                <Table>
                    <thead>
                    <th>No.</th>
                    <th>NIP/Nama/KTP</th>
                    <th>JK</th>
                    <th>Unit/Jabatan</th>
                    <th>Tempat/Tgl Lahir</th>
                    
                    </thead>
                    <tbody>
                    {this.renderTableData(datarow)}
                    </tbody>
                </Table>
            </div>
        );
    }
    renderTableData(datarow){
       if(datarow){

           return datarow.map( (data, key) => {
             let dateAbsen = moment(data.jamAbsen);
             let no= key+1;
             this.count=no;
             return(
               <tr key={key}>
                   <td>{no}</td>
                   <td>{data?.nip}<br/>{data?.nama} <br/>{data?.noKtp} </td>
                   <td>{data?.jenisKelamin}</td>
                   <td>{data?.unitInduk}<br/>{data?.unitKerja}<br/>{data?.golongan}<br/>{data?.jenisJabatan}<br/>{data?.jabatan}</td>
                   <td>{data?.tempatLahir}, {data?.tanggalLahir}</td>
                   
               </tr>
             )
           }     
           );
       }
        
      }

}


export default DaftarPegawai;