import React, { Component } from 'react';
import './laporan.css';
import './laporanNormal.css';
const moment = require('moment');
class DaftarPegawaiTpp extends Component {
    
    render(){
        let datarow
        if(this.props.datatable != null){
            datarow= this.props.datatable;
        }
        return (
            <div>
                <h1>Daftar Pegawai TPP</h1>
    
                <table className="table-daftar">
                    <thead>
                    <th className="center ">No.</th>
                    <th className="center ">Nama</th>
                    <th className="center ">NIP</th>
                    <th className="center ">Nominal</th>
                    </thead>
                    <tbody>
                    {this.renderTableData(datarow)}
                    </tbody>
                </table>
            </div>
        );
    }
    renderTableData(datarow){
       if(datarow){
           return datarow.map( (data, key) => {
             let no= key+1;
             return(
               <tr key={key}>
                   <td className="center ">{no}</td>
                   <td className="left ">{data?.namaPegawai}</td>
                   <td className="left ">{data?.nip}</td>
                   <td className="right ">{data?.nominalTpp.toLocaleString()}</td>
               </tr>
             )
           }     
           );
       }
        
      }

}


export default DaftarPegawaiTpp;