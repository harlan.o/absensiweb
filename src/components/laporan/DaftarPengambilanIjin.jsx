import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import './laporan.css';
const moment = require('moment');
class DaftarPengambilanIjin extends Component {
    
    render(){
        let datarow
        if(this.props.datatable != null){
            datarow= this.props.datatable;
        }
        return (
            <div>
                <h1>Daftar Pegawai Ijin</h1>
    
                <Table>
                    <thead>
                    <th>No.</th>
                    <th>Nama/NIP</th>
                    <th>Unit</th>
                    <th>Mulai</th>
                    <th>Selesai</th>
                    <th>Alasan</th>
                    
                    </thead>
                    <tbody>
                    {this.renderTableData(datarow)}
                    </tbody>
                </Table>
            </div>
        );
    }
    renderTableData(datarow){
       if(datarow){

           return datarow.map( (data, key) => {
             let dateAbsen = moment(data.jamAbsen);
             let no= key+1;
             this.count=no;
             return(
               <tr key={key}>
                   <td>{no}</td>
                   <td>{data?.namaPegawai}<br/>{data?.nip} </td>
                   <td>{data?.unitInduk}<br/>{data?.unitKerja}</td>
                   <td>{data?.startDate}</td>
                   <td>{data?.endDate}</td>
                   <td>{data?.alasan}</td>
                   
               </tr>
             )
           }     
           );
       }
        
      }

}


export default DaftarPengambilanIjin;