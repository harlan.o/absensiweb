import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import './laporan.css';
const moment = require('moment');
class DaftarUnitInduk extends Component {
    render(){
        let datarow
        if(this.props.datatable != null){
            datarow= this.props.datatable;
        }
        return (
            <div>
                <h1>Daftar Unit Induk</h1>
                <Table>
                    <thead>
                    <th>No.</th>
                    <th>Kode</th>
                    <th>Nama</th>
                    {/* <th>Dibuat</th> */}
                    {/* <th>Diubah</th> */}
                    
                    </thead>
                    <tbody>
                    {this.renderTableData(datarow)}
                    </tbody>
                </Table>
            </div>
        );
    }
    renderTableData(datarow){
       if(datarow){

           return datarow.map( (data, key) => {
             let dateAbsen = moment(data.jamAbsen);
             let no= key+1;
             this.count=no;
             return(
               <tr key={key}>
                   <td>{no}</td>
                   <td>{data?.id}</td>
                   <td>{data?.nama}</td>
               </tr>
             )
           }     
           );
       }    
    }

}


export default DaftarUnitInduk;