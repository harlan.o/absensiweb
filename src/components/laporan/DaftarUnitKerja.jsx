import React, { Component } from 'react';
import { Fragment } from 'react';
import { Table } from 'react-bootstrap';
import './laporan.css';
const moment = require('moment');
class DaftarUnitKerja extends Component {
    render(){
        let datarow
        if(this.props.datatable != null){
            datarow= this.props.datatable;
        }
        return (
            <div>
                <h1>Daftar Unit Induk</h1>
                <Table>
                    <thead>
                    <th>No.</th>
                    <th>Kode</th>
                    <th>Nama</th>
                    {/* <th>Dibuat</th> */}
                    {/* <th>Diubah</th> */}
                    
                    </thead>
                    <tbody>
                    {this.renderTableData(datarow)}
                    </tbody>
                </Table>
            </div>
        );
    }
    renderTableData(datarow){
        const groupBy = key => array =>
        array.reduce((objectsByKeyValue, obj) => {
            const value = obj[key]['nama'];
            objectsByKeyValue[value] = (objectsByKeyValue[value] || []).concat(obj);
            return objectsByKeyValue;
        }, {});
        var groubedByTeam = groupBy('unitInduk')
        let b = groubedByTeam(datarow)
        const res = Object.keys(b).map(el => ({
            unitInduk: el,
            daftar: b[el]
        })).sort((a, b) => {return a.unitInduk.localeCompare(b.unitInduk)});
        console.log(res)

        if(datarow){
           return res.map( (data, key) => {
                let no= key+1;
                this.count=no;
               return(
                <Fragment>  
                   <tr key={key}>
                        <td colSpan='3'><b>{data?.unitInduk}</b></td>
                   </tr>
                   {data.daftar.sort((a, b) => {return a.nama.localeCompare(b.nama)}).map( (dataChild, key) => {
                       let dateAbsen = moment(dataChild.jamAbsen);
                       no= key+1;
                       this.count=no;
                       return(
                        <tr key={key}>
                            <td>{no}</td>
                            <td>{dataChild?.id}</td>
                            <td>{dataChild?.nama}</td>
                        </tr>
                           )
                        })}
                </Fragment>
             )
           }     
           );
       }    
    }

}


export default DaftarUnitKerja;