import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import './laporan.css';
const moment = require('moment');
class LaporanAbsensiBulanan extends Component {
    // componentDidMount(){
    //     this.props.breadcurmbChild(this.state.listBreadcrumbUser);        
    // }
    render(){
        let datarow
        if(this.props.dataolahan != null && this.props.dataolahan!= undefined){
            datarow=this.props.dataolahan
        }
        return (
            <div>
                <h1>Laporan Summary</h1>
                <p>{this.props.filterData.startDate} sampai {this.props.filterData.endDate}</p>         
                <Table>
                    <thead>
                    <th>No.</th>
                    <th>Pegawai</th>
                    <th>Jumlah</th>
                    </thead>
                    <tbody>
                    {this.renderTableData(datarow)}
                    </tbody>
                </Table>
            </div>
        );
    }
    renderTableData(datarow){
       if(datarow){

           return datarow.map( (data, key) => {
             let dateAbsen = moment(data.jamAbsen);
             let no= key+1;
             this.count=no;
             return(
               <tr key={key}>
                   <td>{no}</td>
                   <td>{data.nama} <br/><small> {data.nip}</small><br/>
              {data.unitInduk}<br/> <small>{data.unitKerja}</small> </td>

                   <td style={{width:'150px'}}>{data.totalKehadiran ? data.totalKehadiran: 0}x Hadir <br/> { data.totalTerlambat ? data.totalTerlambat: 0}x Terlambat</td>
               </tr>
             )
           }     
           );
       }
        
      }

}


export default LaporanAbsensiBulanan;