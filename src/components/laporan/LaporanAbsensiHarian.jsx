import React, { Component, Fragment } from 'react';
import { Table } from 'react-bootstrap';
import './laporan.css';
const moment = require('moment');
class LaporanAbsensiHarian extends Component {
    // componentDidMount(){
    //     this.props.breadcurmbChild(this.state.listBreadcrumbUser);        
    // }
    render(){
        let datarow
        if(this.props.dataolahan != null && this.props.dataolahan!= undefined){
            datarow=this.props.dataolahan
        }
        return (
            <div>
                <h1>Laporan Harian</h1>
                <p>{this.props.filterData.startDate} sampai {this.props.filterData.endDate}</p>         
                <Table>
                    <thead>
                    <th>No.</th>
                    <th>Pegawai</th>
                    <th width='350px'>Waktu</th>
                    </thead>
                    <tbody>
                    {this.renderTableData(datarow)}
                    </tbody>
                </Table>
            </div>
        );
    }
    listAbsen(absen){
      let tglSebelum;
      return absen.map((data, key) => {
        let dateAbsen = moment(data.waktu);
        let tgl= dateAbsen.format('YYYY-MM-DD');
        if(tglSebelum != tgl){
          tglSebelum = tgl 
          return(<Fragment key={key}><br/> {tgl} | {dateAbsen.format(' HH:mm')} (<small>{data.type}</small>)</Fragment>)
        }else{
          return(<Fragment key={key}>{' '}- {dateAbsen.format(' H:mm')} (<small>{data.type}</small>) </Fragment>)
        }
  
      });
    }
    renderTableData(datarow){
       if(datarow){

           return datarow.map( (data, key) => {
             let dateAbsen = moment(data.jamAbsen);
             let no= key+1;
             this.count=no;
             return(
               <tr key={key}>
                   <td>{no}</td>
                   <td>{data.nama} <br/><small> {data.nip}</small><br/>
              {data.unitInduk}<br/> <small>{data.unitKerja}</small> </td>
                   <td><p>
                  {data.absen ? this.listAbsen(data.absen):"-"}
                  </p></td>
               </tr>
             )
           }     
           );
       }
        
      }

}


export default LaporanAbsensiHarian;