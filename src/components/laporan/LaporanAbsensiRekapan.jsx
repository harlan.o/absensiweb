import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import { clockConversi, monthName } from '../../actions/genneralAction';
import './laporanLs.css';
import './laporanAbsensiRekapan.css';
const moment = require('moment');
class LaporanAbsensiRekapan extends Component {
    // componentDidMount(){
    //     this.props.breadcurmbChild(this.state.listBreadcrumbUser);        
    // }
    render(){
        let datarow
        if(this.props.dataolahan != null && this.props.dataolahan!= undefined){
            datarow=this.props.dataolahan
        }
        return (
            <div className="print-container" style={{ margin: "0", padding: "0" }}>
            <>
            <div>
                <h1>Laporan Absensi Rekapan</h1>
                <p>{"Jadwal Bulan "+monthName(moment(this.props.filterData.startDate).format('MM'))+' '+moment(this.props.filterData.startDate).format('YYYY')}</p>         
                <table className="table-daftar">
                    <thead>
                        <tr>
                            <th rowSpan='2'>No.</th>
                            <th className="colomInti30" rowSpan='2'>Nama</th>
                            <th className="colomInti70" rowSpan='2'>Unit</th>
                            <th className="" colSpan='6'>Total</th>
                        </tr>
                        <tr>
                            <th  className="center min-with100">Hari Kerja</th>
                            <th className="center min-with100">Hadir</th>
                            <th className="center min-with100">Terlambat (T)</th>
                            <th className="center min-with100">Ijin (i)</th>
                            <th className="center min-with100">Sakit (s)</th>
                            <th className="center min-with100">Tidak Hadir (X)</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.renderTableData1()}
                    </tbody>
                </table>
                </div>
                </>
                <div className="page-break"/>
                <>
                <div>
                <table className="table-daftar">
                    <thead>
                        <tr>
                            <th rowSpan='2'>No.</th>
                            <th className="colomInti" rowSpan='2'>Pegawai</th>
                            <th colSpan={moment(this.props.filterData.startDate).daysInMonth()}>{monthName(moment(this.props.filterData.startDate).format('MM'))} </th>
                        </tr>
                        
                        <tr>
                            {this.thHeader(moment(this.props.filterData.startDate).daysInMonth())}
                        </tr>
                    </thead>
                    <tbody>
                    {this.renderTableData(moment(this.props.filterData.startDate).daysInMonth())}
                    </tbody>
                </table>
            </div>
            </>
            <br/>
            <h5>Keterangan</h5>
            
            <table>
                <tr>
                    <td width="35px">00:00</td>
                    <td>:</td>
                    <td> Informasi waktu pengambilan absen</td>
                </tr>
                <tr>
                    <td>T</td>
                    <td>:</td>
                    <td>Terlambat</td>
                </tr>
                <tr>
                    <td>i</td>
                    <td>:</td>
                    <td>Ijin</td>
                </tr>
                <tr>
                    <td>s</td>
                    <td>:</td>
                    <td>Sakit</td>
                </tr>
                <tr>
                    <td>X</td>
                    <td>:</td>
                    <td>Tidak Hadir</td>
                </tr>
                <tr>
                    <td>-</td>
                    <td>:</td>
                    <td>Belum diketahui</td>
                </tr>
            </table>
            <hr/>
            </div>
        );
    }

    renderTableData1(){
        if(this.props.datatable){
            return this.props.datatable.map( (data, key) => {
                let no= key+1;
                let totHadir=data.listAbsen.filter(r =>{
                    let status= r.status.split(" - ")
                    if(status[0] === "HADIR"){
                      return true
                    }
                  }).length;
                  let totTerlambat=data.listAbsen.filter(r =>{
                    let status= r.status.split(" - ")
                    if(status[0] === "HADIR"){
                      if(status[1] === "TERLAMBAT" || status[1] === "TIDAK ABSEN MASUK"){
                        return true
                      }
                    }
                  }).length;
                  let totIjin=data.listAbsen.filter(r =>{
                    let status= r.status.split(" - ")
                    if(status[0] === "IJIN"){
                        return true
                    }
                  }).length;
                  let totSakit=0;
                  let totTanpaKeterangan=data.listAbsen.filter(r =>{
                    let status= r.status.split(" - ")
                    if(status[0] === "TIDAK HADIR"){
                      if(status[1] === "TANPA KETERANGAN"){
                        return true
                      }
                    }
                  }).length;

                  return(
                    <>
                    <tr key={key}>
                        <td rowSpan='2' className="center">{no}</td>
                        <td className="left">{data.namaPegawai}</td>
                        <td className="left">{data.unitInduk}</td>
                        <td rowSpan='2'  className="center">{data.totalHariKerja}</td>
                        <td rowSpan='2'  className="center">{totHadir}</td>
                        <td rowSpan='2'  className="center">{totTerlambat}</td>
                        <td rowSpan='2'  className="center">{totIjin}</td>
                        <td rowSpan='2'  className="center">{totSakit}</td>
                        <td rowSpan='2'  className="center">{totTanpaKeterangan}</td>
                    </tr>
                    <tr>
                        <td className="left">{data.nip}</td>    
                        <td className="left">{data.unitKerja}</td>    
                    </tr>
                    </>
                  )
            })
        }
    }


    thHeader(no){
        let indents = [];
        for (let i = 1; i <= no; i++) {
          indents.push(<th key={i}>{i}</th>);
        }
        return indents;
    }

    renderTableData(totalRow){
        if(this.props.datatable){
           return this.props.datatable.map( (data, key) => {
            let no= key+1;
             return(
            <>
            <tr key={key}>
                <td rowSpan='2' className="center">{no}</td>
                <td className="left">{data.namaPegawai}</td>
                {this.tdJadwal(data.listAbsen, totalRow)}
            </tr>
            <tr>
                <td className="left">{data.nip}</td>
                
            </tr>
            </>
             )
           }     
           );
       }    
    }

    tdJadwal(arr, totalTd){
        let indents = []; 
        for (let i = 1; i <= totalTd; i++) {
            let d = arr?.find((x) =>  moment(x.tanggal).format('DD') == i)
            let keterangan="";
            if(d != undefined && d != null){
                let status= d.status.split(" - ")
                switch (status[0]) {
                    case "HADIR":
                        if(status[1] === "TERLAMBAT"){ keterangan="T" }
                        else{ keterangan=""}
                        break;
                    case "IJIN":
                        keterangan="i"
                        break;
                    case "TIDAK HADIR":
                        keterangan="X"
                        break;
                }

            }


            if(d != null && d != undefined){
                indents.push(
                    <td key={i} rowSpan='2' className="center">
                        <small>
                            {d.jamMasuk != null || d.jamPulang != null ?
                            <span className="time">
                                {d.jamMasuk != null && d.jamMasuk != undefined ? clockConversi(d.jamMasuk):" - "}<br/>{d.jamPulang != null && d.jamPulang != undefined ?clockConversi(d.jamPulang):" - "} 
                                <br/>
                            </span>
                            :""}
                            {keterangan}
                        </small>    
                    </td>
                );
            }else{
                indents.push(<td key={i} rowSpan='2'  className="center"> - </td>);
            }
        }
        return indents;
      }

}


export default LaporanAbsensiRekapan;