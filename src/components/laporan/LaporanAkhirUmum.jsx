import React, { Component } from 'react';
import { monthName } from '../../actions/genneralAction';
import './laporanLs.css';
import './laporanAkhirUmum.css';
import { Fragment } from 'react';
import LogoKab from '../..//img/logo-kabupaten.png';
const moment = require('moment');
class LaporanAkhirUmum extends Component {

    render(){
        let datarow
        if(this.props.dataolahan != null){
            datarow= this.props.dataolahan.sort(function (a, b) {
                return a.namaPegawai.localeCompare(b.namaPegawai);
              });
        }
        let [year, month] = this.props.filterData.startDate.split("-")
        let bulan = monthName(month);
        return (
            <div>
                <img src={LogoKab} width={74} height={74} className="logo" alt="Logo"/>
                <div className="cop-surat center">
                        <p>PEMERINTAH KABUPATEN</p>
                        <p>KEPULAUAN SIAU TAGULANDANG BIARO</p>
                        <p className="cop-1"><b>BADAN KEPEGAWAIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA</b></p>
                        <p className="cop-3">Ondong Kecamatan Siau Barat, Kode Pos : 95862</p>
                </div>

                <p className="sub-cop">OPD <br/>Rekapan Bulan {bulan} {year}</p>       
                <table className="table-daftar">
                <thead>
                    <tr>
                        <th className="rospan" rowSpan='2'><br/>No.</th>
                        <th className="rospan" rowSpan='2'><br/>NAMA/NIP</th>
                        <th className="rospan" rowSpan='2'><br/>JABATAN</th>
                        <th rowSpan='2'><br/>JUMLAH<br/>HARI KERJA</th>
                        <th colSpan='6'>KETERANGAN</th>
                        <th className="rospan" rowSpan='2'><br/>NILAI TPP</th>
                    </tr>
                    <tr>
                        <th>HADIR</th>
                        <th>IZIN</th>
                        <th>SAKIT</th>
                        <th>TANPA <br/> KETERANGAN</th>
                        <th>CUTI</th>
                        <th>TUGAS LUAR</th>
                    </tr>
                </thead>
                    <tbody>
                    {this.renderTableData(datarow)}
                    </tbody>
                </table>

                <table className="tandatangan">
                    <tr>
                        <td></td>
                        <td>MENGETAHUI,<br/>KEPALA BADAN KEPEGAWAIAN DAN PENGEMBANGAN SUMBER DAYA MANUSIA<br/>KAB. KEPL. SIAU TAGULANDANG BIARO BIARO
                        <br/><br/><br/><br/><br/>
                        NAMA_PEGAWAI<br/>JABATAN_PEGAWAI<br/>NIP. NIP_PEGAWAI</td>
                    </tr>
                </table>
            </div>
        );
    }

    tppFinal = (nominalTpp, potongan) => {
        let nilaiPotongan = nominalTpp*potongan/100;
        return nominalTpp-nilaiPotongan;
      }

    renderTableData(datarow){
       if(datarow){

           return datarow.map( (data, key) => {
             let no= key+1;
             this.count=no;
             return(
                 <Fragment>
               <tr key={key}>
                   <td className="center" rowSpan='2'>{no}</td>
                    <td className="left">{data.namaPegawai}</td>
                    <td rowSpan='2' className="left">{data.jabatan}</td>
                    <td rowSpan='2' className="center">{data.totalHariKerja}</td>
                    <td rowSpan='2' className="center">{data.totalHadir}</td>
                    <td rowSpan='2' className="center">{data.totalIzin}</td>
                    <td rowSpan='2' className="center">{data.totalSakit}</td>
                    <td rowSpan='2' className="center">{data.totalTanpaKeterangan}</td>
                    <td rowSpan='2' className="center">{data.totalCuti}</td>
                    <td rowSpan='2' className="center">{data.totalTugasLuar}</td>
                    <td rowSpan='2' className="right">{data.nilaiTpp?.toLocaleString()}</td>
               </tr>
               <tr>
                   <td className="left">
                   {data.nip}
                   </td>
               </tr>
               </Fragment>
             )
           }     
           );
       }
        
      }

}


export default LaporanAkhirUmum;