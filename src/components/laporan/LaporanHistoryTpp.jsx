import React, { Component } from 'react';
import { Table } from 'react-bootstrap';
import { monthName } from '../../actions/genneralAction';
import './laporan.css';
const moment = require('moment');
class LaporanHistoryTpp extends Component {

    render(){
        let datarow
        if(this.props.datarow != null){
            datarow= this.props.datarow.sort(function (a, b) {
                return a.namaPegawai.localeCompare(b.namaPegawai);
              });
        }
        let [year, month] = this.props.filterData.startDate.split("-")
        let bulan = monthName(month);
        return (
            <div>
                <h1>Laporan TPP Bulan {bulan} {year}</h1>       
                <Table>
                    <thead>
                    <th>No.</th>
                    <th>NIP/Nama</th>
                    <th>Nominal Tpp</th>
                    <th>Potongan</th>
                    <th>TPP Terima</th>
                    </thead>
                    <tbody>
                    {this.renderTableData(datarow)}
                    </tbody>
                </Table>
            </div>
        );
    }

    tppFinal = (nominalTpp, potongan) => {
        let nilaiPotongan = nominalTpp*potongan/100;
        return nominalTpp-nilaiPotongan;
      }

    renderTableData(datarow){
       if(datarow){

           return datarow.map( (data, key) => {
             let no= key+1;
             this.count=no;
             return(
               <tr key={key}>
                   <td>{no}</td>
                   <td>{data.nip}<br/>{data.namaPegawai} </td>
                   <td>{data.nominalTpp?.toLocaleString()}</td>
                   <td>{data.potongan} %</td>
                   <td>{this.tppFinal(data.nominalTpp, data.potongan).toLocaleString()}</td>
               </tr>
             )
           }     
           );
       }
        
      }

}


export default LaporanHistoryTpp;