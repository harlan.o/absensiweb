import React, {Component} from 'react';


import {markerStyle} from './Marker_style.js';

export default class Marker extends Component {


  render() {
    return (
       <div >
           
           <div style={markerStyle}></div>
          {this.props.text}
       </div>
    );
  }
}