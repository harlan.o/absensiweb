import '../css/dataTables.bootstrap4.min.css';
import '../DataTable.css';
import React, {Component} from 'react';
import { Table, Card, Button } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faKey, faMobile, faMobileAlt, faPencilAlt, faTablet } from '@fortawesome/free-solid-svg-icons';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU060, REGISTER_FU061, REGISTER_FU062 } from '../../constants/hakAksesConstants';
const $ =require('jquery')
$.DataTable = require('datatables.net-bs4')


export class DataTablePengaturanPegawai extends Component {
    constructor(props){
        super(props);
        this.state={
            header: null,
            dataHTML: null,
            dataRow:null,
        };
        this.dataRowImport = this.dataRowImport.bind(this);
        this.handleShowFormModalEdit = this.handleShowFormModalEdit.bind(this);
        this.handleShowFormModalDetail = this.handleShowFormModalDetail.bind(this);
        this.handleShowFormModalResetMobile = this.handleShowFormModalResetMobile.bind(this);
        this.handleShowFormModalResetPass = this.handleShowFormModalResetPass.bind(this);
    }
    componentDidMount(){
        // console.log(this.el)
        this.$el = $(this.el)
        this.setState(state => ({      
            dataRow:this.props.dataRow
        }));
        // this.$el.style="asd"
        this.$el.DataTable(
            {
                language: {
                    emptyTable:     "Data tidak ditemukan",
                    info:           "Tampil _START_ dari _END_ pada _TOTAL_ Data",
                    infoEmpty:      "Tampil 0 data",
                    lengthMenu:     "Tampil _MENU_ Data",
                    search:         "Cari:",
                    paginate: {
                        first:    '«',
                        previous: '‹',
                        next:     '›',
                        last:     '»'
                    },
                    aria: {
                        paginate: {
                            first:    'First',
                            previous: 'Previous',
                            next:     'Next',
                            last:     'Last'
                        }
                    }
                },
            }
        )
        // console.log(this.el);
    }
    dataRowImport(dataRowI){
        return (
            <tbody>
                {dataRowI.map((data, key) => {
                    return (
                        <tr key={key}>
                            <td> {key+1} </td>
                            <td> 
                            {(data.gelarDepan !== "" && data.gelarDepan !== null) ? data.gelarDepan+". " : "" }
                            {data.nama}
                            {(data.gelarBelakang !== "" && data.gelarBelakang !== null) ? ", " +data.gelarBelakang: "" }
                            <br/>{data.nip} </td>
                            <td> {data.jenisKelamin} </td>
                            <td> {data.unitInduk}<br/>{data.unitKerja} </td>
                            <td> {data.jabatan}</td>
                            <td> 
                                <Button variant="secondary"  size="sm" onClick={() => this.handleShowFormModalDetail(data)}><FontAwesomeIcon icon={faEye} /> </Button>{' '}
                                {cekHakAksesClient(REGISTER_FU060) ?
                                <Button variant="secondary" onClick={() => this.handleShowFormModalEdit(data)} size="sm"><FontAwesomeIcon icon={faPencilAlt} /> </Button>
                                :""}
                            {/* Reset Pass dan reset Mac id */}
                            {' '}
                            {cekHakAksesClient(REGISTER_FU061) ?
                            <Button variant="secondary" onClick={() => this.handleShowFormModalResetMobile(data)} size="sm"><FontAwesomeIcon icon={faMobileAlt} /> </Button>
                            :""}
                            {' '}
                            {cekHakAksesClient(REGISTER_FU062) ?
                            <Button variant="secondary" onClick={() => this.handleShowFormModalResetPass(data)} size="sm"><FontAwesomeIcon icon={faKey} /> </Button>
                            :""} </td>
                        </tr>
                    );
                    })}
            </tbody>
          );
    }
    handleShowFormModalEdit(dataOneRow){
        this.props.dataOneRow(dataOneRow); 
    }
    handleShowFormModalDetail(dataOneRow){
        this.props.detail(dataOneRow, "DETAIL"); 
    }
    handleShowFormModalResetMobile(dataOneRow){
        this.props.detail(dataOneRow, "RESET_MOBILE"); 
    }
    handleShowFormModalResetPass(dataOneRow){
        this.props.detail(dataOneRow, "RESET_PASS"); 
    }
 
    render(){
        return (
            <Card className="customCard">
                 <Table ref={el => this.el = el} className="table table-striped table-hover custom-table text-nowrap  table-responsive">
                     <thead>
                         <tr> 
                             <th> No.</th>
                             <th className="custom-fit"> Nama/NIP </th>
                             <th> JK</th>
                             <th> Unit Induk/Kerja </th>
                             <th> Jabatan </th>
                             <th> ---- </th>
                         </tr>
                     </thead>
                     {this.dataRowImport(this.props.dataRow)}
                 </Table>
            </Card>
        );
    }
}


export default DataTablePengaturanPegawai;