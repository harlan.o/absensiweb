import React, { Component } from 'react';
import { Button, Form, FormCheck, ListGroup } from 'react-bootstrap';
import {connect} from 'react-redux';
import { getSimpleListPegawai } from "../../actions/pegawaiActions";
import './ListCheckBoxPegawai.css';

class ListCheckBoxPegawai extends Component{
    constructor(props) {
        super(props);
        this.state = {
            dataPegawai:null,
            pegawaiCheck:[],
            // nama, nip
            idUnitInduk:this.props.idUnitInduk,
            idUnitKerja:this.props.idUnitKerja,
            search:'',
            loading:true,
            centangSemua:false,
        };
    }

    async callAPI(){
        let id=null;
        let idKerja=null;
        if(this.state.idUnitInduk != -1){
            id=this.state.idUnitInduk 
        }
        if(this.state.idUnitKerja != -1){
            idKerja=this.state.idUnitKerja 
        }
        console.log("list simple", id, idKerja)
        // console.log(id)
        // console.log(idKerja)
        this.props.getSimpleListPegawai(id, idKerja).then(()=>{
            this.setState({
                dataPegawai: this.props.simpleListPengPegawai.data,
                loading:false,
                pegawaiCheck:[],
            })
            console.log(this.props.simpleListPengPegawai)
        })
    }

    componentDidMount(){
        this.callAPI();
    }

    componentDidUpdate(prevProps, prevState) {
        console.log(this.props.idUnitInduk)
        if(prevProps.idUnitInduk !== this.props.idUnitInduk || prevProps.idUnitInduk !== this.props.idUnitInduk){
            this.setState({
                idUnitInduk: this.props.idUnitInduk,
            }, ()=>{this.callAPI()} )
        }
        if(prevProps.idUnitKerja !== this.props.idUnitKerja || prevProps.idUnitKerja !== this.props.idUnitKerja){
            this.setState({
                idUnitKerja: this.props.idUnitKerja,
            }, ()=>{this.callAPI()} )
        }
        console.log("call Awa;")
        if(this.state.dataPegawai != null){
            console.log("call Awa2")

            if(this.props.valueList != null && this.props.valueList !== this.state.pegawaiCheck){
                this.setState({
                    pegawaiCheck: this.props.valueList,
                })
                console.log("call")
                console.log(this.props.valueList)
            }
        } 
      }
    
    renderPilihPegawai(){
        if(this.state.loading !== true && this.state.dataPegawai !=null && this.state.dataPegawai !=undefined){
            let datashow = this.state.dataPegawai.filter( dataPegawai =>{
                if(dataPegawai.nama.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1 || dataPegawai.nama.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1){
                  return true
                }
                return false
              });
            
            if(this.props?.fixDisable){
                datashow= datashow.filter(d => { if (d.fixJadwal == false){ return true}else{ return false} })
                console.log("filter fix", datashow)
            }
        
            return datashow.map( (data, key) => {
                let groups=[...this.state.pegawaiCheck]
                return(
                    <ListGroup.Item key={key}>
                        <FormCheck key={key} checked={groups.includes(data.id)} type='checkbox' label={data.nama+" / "+data.nip} id={data.id} 
                        
                        onChange={v => {
                            if(v.target.checked){
                              groups.push(data.id)
                              this.setState({
                                pegawaiCheck:groups,
                              })
                            }else{
                              this.removeArray(groups, data.id);
                              this.setState({
                                pegawaiCheck:groups,
                              })
                            }
                            this.props.listSelected(groups);
                          }
                        }
                        
                        />
                    </ListGroup.Item>
                )
            
            });
        }
    }
    
    renderPegawaiTerpilih(){
        let pegawaiCheck=this.state.pegawaiCheck;
        if(pegawaiCheck != null && pegawaiCheck != undefined && pegawaiCheck.length > 0 ){

            let dataTerpilih = this.state.dataPegawai.filter((pegawai)=>{
                let nilai=0;
                for(let i = 0; i < pegawaiCheck.length; i++){
                    if (pegawai.id == pegawaiCheck[i]){
                        nilai=1
                    }
                }
                if(nilai == 1){
                    return true
                }
            });
            return dataTerpilih.map( (data, key) => {
                
                return(
                    <ListGroup.Item key={key}>
                        {data.nama+" / "+data.nip}
                    </ListGroup.Item>
                )
            
            });
        }
    }

    render(){
        return(
            <div>
                <h4>Pilih Pegawai</h4>
                <Form.Control placeholder="Cari Nama" className="mb-2" onChange={v => this.setState(({search: v.target.value }))} />
                 <FormCheck checked={this.state.centangSemua}  type='checkbox' label="Centang Semua"
                        
                        onChange={v => {
                            if(v.target.checked){
                                this.setState({
                                    pegawaiCheck:this.checkSemua(),
                                    centangSemua:true
                                  })
                                  this.props.listSelected(this.checkSemua());
                            }else{
                              this.setState({
                                pegawaiCheck:[],
                                centangSemua:false
                              })
                              this.props.listSelected([]);
                            }
                            
                          }
                        }
                        
                        />
                <ListGroup className="list-scrol">
                {this.renderPilihPegawai()}
                </ListGroup>

                <h4>Daftar Terpilih</h4>
                <ListGroup className="list-scrol">
                    {this.state.pegawaiCheck.length > 0 ? this.renderPegawaiTerpilih(): " "}
                </ListGroup>
            </div>
        )
    }

    removeArray(arr) {
        var what, a = arguments, L = a.length, ax;
        while (L > 1 && arr.length) {
            what = a[--L];
            while ((ax= arr.indexOf(what)) !== -1) {
                arr.splice(ax, 1);
            }
        }
        return arr;
      }

    checkSemua(){
        if(this.state.loading !== true && this.state.dataPegawai !=null && this.state.dataPegawai !=undefined){
            let pegawai=this.state.dataPegawai
            let checked=[];
            for(var i = 0; i < pegawai.length; i++) {
                let obj = pegawai[i];
                checked.push(obj.id)
            }
            console.log(checked);
            return checked;
        }
        return [];

    }

}


const mapStateToProps = (state) => {
    return{
        simpleListPengPegawai: state.simpleListPengPegawai,

    } 
 }

export default connect(mapStateToProps, {getSimpleListPegawai}) (ListCheckBoxPegawai);