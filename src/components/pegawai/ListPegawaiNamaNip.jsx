import React, { Component } from 'react';
import { Button, Form, FormCheck, ListGroup } from 'react-bootstrap';
import './ListCheckBoxPegawai.css';

class ListPegawaiNamaNip extends Component{

    renderList(datas){
        if(datas!=null && datas !=undefined){

            return datas.map( (data, key) => {
                
                return(
                    <ListGroup.Item key={key}>
                        {data.nama+" / "+data.nip}
                    </ListGroup.Item>
                )
                
            });
        }
    }

    render(){
        return(
            <div>
                <h4>Daftar Pegawai</h4>
                <ListGroup className="list-scrol">
                    {this.renderList(this.props.datas)}
                </ListGroup>
            </div>
        )
    }



}


export default ListPegawaiNamaNip;