import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import { Alert, Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import {DataGolongan} from "../../stockdata";
import SelectUnitKerja from '../unit-kerja/SelectUnitKerja';

import SelectSingleUnitInduk_IdSend from '../unit-induk/SelectSingleUnitInduk_IdSend';
import SelectSingleUnitKerja_IdSend from '../unit-kerja/SelectSingleUnitKerja_IdSend';

// Redux
import {connect} from 'react-redux';
import { createPegawai, updatePegawai } from "../../actions/pegawaiActions";
import { getListUnitKerja } from "../../actions/unitKerjaAction";
import { getListUnitInduk } from "../../actions/unitIndukAction";

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import SelectTpp from '../tpp/SelectTpp';
import SelectSingleGolongan_NameSend from '../golongan/SelectSingleGolongan_NameSend';

class ModalPengaturanPegawai extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"Tambah Pegawai",
           data:{ 
            id:null,
            nip: "",
            noKtp: "",
            nama: "",
            gelarDepan: "",
            gelarBelakang: "",
            jenisKelamin: "",
            tempatLahir: "",
            tanggalLahir: new Date(),
            unitKerjaId:"",
            jabatan: "",
            golongan: "",
            pendidikan_terakhir: "",
            // tppId:"",
            // tpp:0,
            golongan:"",

        },
        valueOptionUnitKerja:{
            id:"",
            nama:""
        },
        valueOptionTpp:{
            id:"",
            nama:""
        },

        listUnitKerja:[],
        listUnitInduk:[],
        edit:false,
        dataoptionUnitKerja:this.props.dataoptionunitkerja,
        // dataoptionTpp:this.props.dataoptiontpp,
        
        };
        this.headleChangeUnitInduk = this.headleChangeUnitInduk.bind(this);
        this.MySwal = withReactContent(Swal);
    }
    async update(dataOne) {    
        await this.setState(({       
            data:dataOne,
            valueOptionUnitKerja:{
                id:dataOne.unitKerjaId,
                nama:dataOne.unitKerja
            },
            // valueOptionTpp:{
            //     id:dataOne.tppId
            // },
            judul:"Ubah Data Pegawai",
            edit:true,
        }));
    }
    componentDidMount(){
        this.callApi()
        if(this.props.dataedit != null){
            this.update(this.props.dataedit)
            // console.log(this.state.data)
            // console.log("did")
        }
    }

    async callApi(){
        let listUI=[]
        let listUK=[]
        if(this.props.listPengUnitInduk?.data != null){
            listUI=this.props.listPengUnitInduk?.data;
        }
        if(this.props.listPengUnitKerja?.data != null){
            listUK=this.props.listPengUnitKerja?.data;
        }
        this.setState(() => ({
            loading:false,
            listUnitInduk:listUI,
            listUnitKerja:listUK
        }));
        if(this.props.dataedit != null){
            console.log("here 1b")
            let unitKerjaKecil= listUK;
            if(this.props.dataedit?.unitIndukId != ""){
                unitKerjaKecil= unitKerjaKecil.filter(
                (kerja) => {
                    return kerja.unitInduk?.id== this.state.data?.unitIndukId
                });
            }else{
                unitKerjaKecil=[]
            }
            this.setState({
                listUnitInduk: listUI,
                listUnitKerja: unitKerjaKecil,
            })
        }else{
            this.setState(() => ({
              loading:false,
              listUnitInduk:listUI,
              listUnitKerja:listUK,
            }));               
        }

        await this.props.getListUnitInduk().then(()=>{
          this.props.getListUnitKerja().then(()=>{
            if(this.props.dataedit != null){
                console.log("here 1b")
                let unitKerjaKecil= this.props.listPengUnitKerja?.data;
                if(this.props.dataedit?.unitIndukId != ""){
                    unitKerjaKecil= unitKerjaKecil.filter(
                    (kerja) => {
                        return kerja.unitInduk?.id== this.state.data?.unitIndukId
                    });
                }else{
                    unitKerjaKecil=[]
                }
                this.setState({
                    listUnitInduk:this.props.listPengUnitInduk?.data,
                    listUnitKerja: unitKerjaKecil,
                })
            }else{
                this.setState(() => ({
                  loading:false,
                  listUnitInduk:this.props.listPengUnitInduk?.data,
                  listUnitKerja:this.props.listPengUnitKerja?.data,
                }));               
            }
             });  
        });
    }

    render (){
    // console.log(this.props.dataOptionUnitKerja);
        return (
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Body>
                      <Form.Row>
                          <Form.Group as={Col} controlId="form_nip">
                          <Form.Label className="textblack">NIP*</Form.Label>
                          <Form.Control type="text" placeholder="Masukan Nomor Induk" value={this.state.data.nip} onChange={v => this.setState(prevState => ({data:{...prevState.data, nip : v.target.value}}))} disabled={this.state.edit}/>
                          </Form.Group>
  
                          <Form.Group as={Col} controlId="form_ktp">
                          <Form.Label className="textblack">No KTP</Form.Label>
                          <Form.Control type="text" placeholder="Masukan No KTP" value={this.state.data.noKtp} onChange={v => this.setState(prevState => ({data:{...prevState.data, noKtp : v.target.value}}))} />
                          </Form.Group>
                      </Form.Row>
                      <Form.Row>
                          <Form.Group as={Col} controlId="form_nama">
                          <Form.Label className="textblack">Nama Pegawai*</Form.Label>
                          <Form.Control type="text" placeholder="Masukan Nama Pegawai"  value={this.state.data.nama} onChange={v => this.setState(prevState =>({data:{...prevState.data, nama : v.target.value.toUpperCase()}}))} />
                          </Form.Group>
  
                          <Form.Group as={Col} sm="3" controlId="form_gelarDepan">
                          <Form.Label className="textblack">Gelar Depan</Form.Label>
                          <Form.Control type="text" placeholder="Gelar Depan"  value={this.state.data.gelarDepan} onChange={v => this.setState(prevState =>({data:{...prevState.data, gelarDepan : v.target.value}}))}/>
                          
                          </Form.Group>
                          <Form.Group as={Col} sm="3"  controlId="form_gelarBelakang">
                          <Form.Label className="textblack">Gelar Belakang</Form.Label>
                          <Form.Control type="text" placeholder="Gelar Belakang"  value={this.state.data.gelarBelakang} onChange={v => this.setState(prevState =>({data:{...prevState.data, gelarBelakang : v.target.value}}))}/>

                          </Form.Group>
                      </Form.Row>
                      <hr/>
                      <Form.Group>
                          <Form.Label className="textblack">Jenis Kelamin*</Form.Label>
                          <div>
                              <Form.Check
                                  inline
                                  checked={this.state.data.jenisKelamin == "L"}  
                                  name="form_jenis_kelamin"
                                  label="Laki-Laki"
                                  type="radio"
                                  onChange={v => this.setState(prevState =>({data:{...prevState.data, jenisKelamin : "L"}}))}
                              />
                              <Form.Check
                                  inline
                                  checked={this.state.data.jenisKelamin == "P"} 
                                  name="form_jenis_kelamin"
                                  label="Perempuan"
                                  type="radio"
                                  onChange={v => this.setState(prevState =>({data:{...prevState.data, jenisKelamin : "P"}}))}
                              />
                          </div>
                      </Form.Group>
                      <Form.Row>
                          <Form.Group as={Col} controlId="form_tempatLahir">
                          <Form.Label className="textblack">Tempat Lahir</Form.Label>
                          <Form.Control type="text" placeholder="Masukan Tempat Lahir" value={this.state.data.tempatLahir} onChange={v => this.setState(prevState =>({data:{...prevState.data, tempatLahir : v.target.value.toUpperCase()}}))}/>
                          </Form.Group>
  
                          <Form.Group as={Col} controlId="form_tanggalLahir">
                          <Form.Label className="textblack">Tanggal Lahir</Form.Label>
                          <div >
                          <DatePicker         
                                    className="form-control"
                                    selected={new Date(this.state.data.tanggalLahir)}
                                    dateFormat="yyyy-MM-dd"
                                    onChange={v => this.setState(prevState => ({data:{...prevState.data, tanggalLahir : v}}))}
                                    placeholderText="Pilih Tanggal"
                                />
                                              </div>
                          </Form.Group>
                      </Form.Row>
                      <hr/>
                      <Form.Row>
                          <Form.Group as={Col} controlId="form_jabatan">
                          <Form.Label className="textblack">Jabatan*</Form.Label>
                          <Form.Control type="text" placeholder="Masukan Jabatan Pegawai"  value={this.state.data.jabatan} onChange={v => this.setState(prevState =>({data:{...prevState.data, jabatan : v.target.value.toUpperCase()}}))} />
                          </Form.Group>
  
                          <Form.Group as={Col} sm="3"  controlId="form_golongan">
                          <Form.Label className="textblack">Golongan*</Form.Label>

                          <SelectSingleGolongan_NameSend valueOption={this.state.data.golongan} valueGetSingle={v => this.setState(prevState =>({data:{...prevState.data, golongan : v}}))}/>
                          {/* <Form.Control as="select" value={this.state.data.gelarBelakang} onChange={v => this.setState(prevState =>({data:{...prevState.data, gelarBelakang : v.target.value}}))}>
                              <option value="">Pilih...</option>
                              <option value={this.state.data.gelarBelakang}>{this.state.data.gelarBelakang}</option>
                          </Form.Control> */}
                          </Form.Group>
                      </Form.Row>
                      <Form.Row>
                          <Form.Group as={Col} controlId="form_unitInduk">
                          <Form.Label className="textblack">Unit Induk*</Form.Label>
                          
                          <SelectSingleUnitInduk_IdSend dataoption={ this.state.listUnitInduk.length != 0 ? this.state.listUnitInduk : []} valueOption={this.state.data?.unitIndukId} valueGetSingle={this.headleChangeUnitInduk} placeholder="Pilih Unit Induk"/> 
                          
                          </Form.Group>
                      </Form.Row>
                      <Form.Row>
                          <Form.Group as={Col} controlId="form_unitKerja">
                          <Form.Label className="textblack">Unit Kerja*</Form.Label>
                          
                          <SelectSingleUnitKerja_IdSend dataoption={ this.state.listUnitKerja.length != 0 && this.state.data.unitIndukId != null ? this.state.listUnitKerja : []}  valueOption={this.state.data?.unitKerjaId} valueGetSingle={v => {
                                            let c=""; if(v!=null){c=v.id}else{c=null} this.setState(prevState => ({data:{...prevState.data, unitKerjaId : c }}))          
                                            }} placeholder="Pilih Unit Kerja"/>
                          </Form.Group>
                      </Form.Row>
                
                  </Card.Body>
              </Card>


                  </Form>
                  <Alert variant='dark'>
                    Symbol *) menandakan Harus diisi
                   </Alert>
              </Modal.Body>
          <Modal.Footer>
              
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleCreatePegawai} >
                  Simpan
              </Button>
          </Modal.Footer>
        </Modal>
      );
    }

    headleChangeUnitInduk = (v) => {
        let c=""; 
        if(v!=null){c=v.id}else{
            c=null;
        } 
        this.setState(prevState => ({data:{...prevState.data, unitIndukId : c, unitKerjaId:null, idPegawai:null }}),
            () => {
                let unitKerjaKecil= this.props.listPengUnitKerja.data;
                if(c != ""){
                    unitKerjaKecil= unitKerjaKecil.filter(
                      (kerja) => {
                        return kerja.unitInduk?.id== c
                      });
                  }else{
                    unitKerjaKecil=[]
                  }
                  this.setState({
                    listUnitKerja: unitKerjaKecil,
                  })
            })
    }


    handleCreatePegawai =  (e) =>  {
        e.preventDefault();
        let info="";
        let nVerif=0;
        if(this.state.data.nip === "" || this.state.data.nip === null){
            info+="Nip belum terisi <br/>";
            nVerif=1;
        }
        if(this.state.data.nama === "" || this.state.data.nama === null){
            info+="Nama belum terisi <br/>";
            nVerif=1;
        }
        
        if(this.state.data.unitKerjaId === "" || this.state.data.unitKerjaId === null){
            info+="Unit Kerja belum dipilih <br/>";
            nVerif=1;
        }
        if(nVerif===1){
            this.MySwal.fire({
                icon: "error",
                title: info,
              });
        }else if(this.state.data.id === null && nVerif!==1){
            // console.log(this.state.data);
            this.props.createPegawai(this.state.data).then(()=>{
                const {success} = this.props.infoCreatePegawai;
                // console.log(success);
                if(success){
                    this.MySwal.fire({
                        icon: "success",
                        title: this.state.data.nama+" berhasil ditambahkan",
                      });
                    }
                    this.props.actionLoading();
                    this.props.onHide();
                });
        }else if(this.state.data.id !== null && nVerif!==1){
            // console.log(this.state.data);
            this.props.updatePegawai(this.state.data).then(()=>{
                const {success} = this.props.infoUpdatePegawai;
                // console.log(success);
                if(success){
                    this.MySwal.fire({
                        icon: "success",
                        title: this.state.data.nama+" berhasil diperbaharui",
                      });
                    }
                    this.props.actionLoading();
                    this.props.onHide();
                });
        }
    }
        
}

const mapStateToProps = (state) => {
    return{
        infoCreatePegawai: state.infoCreatePegawai,
        infoUpdatePegawai: state.infoUpdatePegawai,
        listPengUnitInduk: state.listPengUnitInduk,
        listPengUnitKerja: state.listPengUnitKerja,

    } 
 }
export default connect(mapStateToProps, {getListUnitInduk, getListUnitKerja, createPegawai, updatePegawai})(ModalPengaturanPegawai);