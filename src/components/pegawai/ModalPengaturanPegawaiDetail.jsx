import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import { Alert, Button, Card, Col, Form, ListGroup, Modal, Row } from 'react-bootstrap';
import {DataGolongan} from "../../stockdata";


class ModalPengaturanPegawaiDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"Tidak Ada Pegawai",
           data:{ 
            id:null,
            nip: "",
            noKtp: "",
            nama: "",
            gelarDepan: "",
            gelarBelakang: "",
            jenisKelamin: "",
            tempatLahir: "",
            tanggalLahir: "",
            unitKerjaId:"",
            jabatan: "",
            golongan: "",
            pendidikan_terakhir: "",
            tppId:"",
        },
        valueOptionUnitKerja:{
            id:"",
            nama:""
        },
        valueOptionTpp:{
            id:"",
            nama:""
        },
        edit:false,
        dataoptionUnitKerja:this.props.dataoptionunitkerja,
        dataoptionTpp:this.props.dataoptiontpp,
        
        };
    }

    tppText = (option) =>{
        let a=option?.daerahKerja;
        if(option?.eselon !== null){
          a+=" Eselon: "+option?.eselon;
        }
        if(option?.golongan !== null){
          a+=" Gol: "+option?.golongan;
        }
        if(option?.nominal !== null){
          a+=" Rp: "+option?.nominal.toLocaleString();
        }
        if(option?.keterangan !== null){
          a+=" (Ket: "+option?.keterangan+")";
        }
        if(option?.tenagaMedis){
          a+=" Tenaga Medis";
        }else{
          a+=" Non Tenaga Medis";
        }
        // "Rp."+option.nominal+ {option.eselon ? (" Eln: "+option.eselon) : ""} " Ket "+option.keterangan+"("+option.daerahKerja+" "+option.daerahKerja+")"
  
        return a;
      }

    async update(dataOne) { 
       let tmp_tpp= this.props.dataoptiontpp.filter(tpp => tpp.id===dataOne.tppId);
       console.log(tmp_tpp);
       let tppName=this.tppText(tmp_tpp[0]);   
        await this.setState(({       
            data:dataOne,
            valueOptionUnitKerja:{
                id:dataOne.unitKerjaId,
                nama:dataOne.unitKerja
            },
            valueOptionTpp:{
                id:dataOne.tppId,
                nama:tppName,
            },
            judul:"Detail Pegawai",
            edit:true,
        }));
    }
    componentDidMount(){
        if(this.props.datadetail != null){
            this.update(this.props.datadetail)
            console.log(this.state.data)
            console.log("did")
        }
    }


    render (){
    // console.log(this.props.dataOptionUnitKerja);
        return (
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Body>
                    <h1>{this.state.data.gelarDepan ? this.state.data.gelarDepan+".":"" } {this.state.data.nama}{this.state.data.gelarBelakang ? ", "+this.state.data.gelarBelakang:"" }<br/><small>NIP. {this.state.data.nip}</small></h1> 
                    <p>Gol. {this.state.data.golongan}, Jabatan {this.state.data.jenisJabatan} - {this.state.data.jabatan}</p>
                    <h4><small> Induk : </small>{this.state.data.unitInduk}<small>, Kerja : </small>{this.state.data.unitKerja}</h4> 
                    <br/> TPP : {this.state.valueOptionTpp.nama}
                  </Card.Body>
              </Card>
              <Card>
                  <Card.Body>
                  <ListGroup>
                        <ListGroup.Item>KTP<h5>{this.state.data.noKtp}</h5></ListGroup.Item>
                        <ListGroup.Item>Jenis Kelamin<h5>{this.state.data.jenisKelamin}</h5></ListGroup.Item>
                        <ListGroup.Item>Tanggal Lahir <h5>{this.state.data.tanggalLahir}</h5></ListGroup.Item>
                        <ListGroup.Item>Tempat Lahir <h5>{this.state.data.tempatLahir}</h5></ListGroup.Item>
                    </ListGroup>
                  </Card.Body>
              </Card>

                  </Form>
              </Modal.Body>
          <Modal.Footer>
              
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
          </Modal.Footer>
        </Modal>
      );
    }


  
        
}

export default ModalPengaturanPegawaiDetail;