import React, { Component } from 'react';
import { Alert, Button, Card, Col, Form, ListGroup, Modal, Row } from 'react-bootstrap';

// Redux
import { connect } from 'react-redux';
import { resetPasswordPegawai } from "../../actions/pegawaiActions";

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";


class ModalPengaturanPegawaiResetPass extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"Tidak Ada Pegawai",
           data:{ 
            id:null,
            nip: "",
            noKtp: "",
            nama: "",
            gelarDepan: "",
            gelarBelakang: "",
            jenisKelamin: "",
            tempatLahir: "",
            tanggalLahir: "",
            unitKerjaId:"",
            jabatan: "",
            golongan: "",
            pendidikan_terakhir: "",
            tppId:"",
        },        
        };
        this.MySwal = withReactContent(Swal);
    }


    async update(dataOne) { 
      await this.setState(({       
            data:dataOne,
            judul:"Reset Password Pegawai",
            edit:true,
        }));
    }
    componentDidMount(){
        if(this.props.datadetail != null){
            this.update(this.props.datadetail)
        }
    }


    render (){
        return (
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Body>
                    <h1>{this.state.data.gelarDepan ? this.state.data.gelarDepan+".":"" } {this.state.data.nama}{this.state.data.gelarBelakang ? ", "+this.state.data.gelarBelakang:"" }<br/><small>NIP. {this.state.data.nip}</small></h1> 
                    <p>Anda yakin ingin mereset password menjadi NIP?</p>
                    
                  </Card.Body>
              </Card>
             
                  </Form>
              </Modal.Body>
          <Modal.Footer>
              
              <Button variant="secondary" onClick={this.props.onHide}>
                  Batal
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleReset} >
                  Ya
              </Button>
          </Modal.Footer>
        </Modal>
      );
    }

  handleReset =  (e) =>  {
    e.preventDefault();
    console.log("here")
    this.props.resetPasswordPegawai(this.state.data).then(()=> {
        const {success} = this.props.infoResetPasswordPegawai;
        const {loading} = this.props.infoResetPasswordPegawai;
        console.log(success)
        if(success){
            this.MySwal.fire({
                icon: "success",
                title: "Berhasil!",
                text: "Reset Password a/n "+this.state.data.nama+" berhasil dilakukan",
                // customClass: 'swal-wide',
            });
            this.props.onHide();
        }
        if(loading == false && success == false){
            this.MySwal.fire({
                icon: "error",
                title: "Coba Lagi!",
                // html: info,
                // customClass: 'swal-wide',
            });
        }
    })
  }
    
  
        
}

const mapStateToProps = (state) => {
  return{
    infoResetPasswordPegawai: state.infoResetPasswordPegawai,
  } 
}

export default connect(mapStateToProps, {resetPasswordPegawai})(ModalPengaturanPegawaiResetPass);