
import React, { Component } from 'react';
import { LineChart, Line, CartesianGrid, XAxis, YAxis, Tooltip, Legend } from 'recharts';



class StatisticAbsensiPegawai extends Component {

    constructor (props){
        super(props)
        this.state = {
            chartData :[{name: 'Page A', uv: 400, pv: 2400, amt: 2400},
                        {name: 'Page B', uv: 400, pv: 2400, amt: 2400},
                        {name: 'Page B', uv: 40, pv: 2400, amt: 2400}],
        }
    }

    componentDidMount(){

    }

    render(){
        // console.log(this.props.dataabsen);
        this.createChartObject();
        return(
            <LineChart width={600} height={300} data={this.createChartObject()} margin={{ top: 5, right: 20, bottom: 5, left: 0 }}>
                 <XAxis dataKey="tanggalKerja"/>
                 <YAxis/>
                <Line type="monotone" dataKey="kehadiran" name="Total Kehadiran" stroke="#82ca9d" />
                <Line type="monotone" dataKey="terlambat" name="Total Terlambat" stroke="#8884d8" />
                <CartesianGrid stroke="#ccc" strokeDasharray="5 5" />
                <Legend />
                <Tooltip />
            </LineChart>
        )
    }


    createChartObject(){
        let datarow;
        if(this.props.dataabsen != null && this.props.dataabsen != undefined){
            // console.log(datarow);
            var kehadiran = this.props.dataabsen.reduce( (r, row) => {
                r[row.tanggalKerja] = ++r[row.tanggalKerja] || 1;
                return r;
            }, {});
            
            var terlambat = this.props.dataabsen.reduce( (r, row) => {
                if(row.keterangan=="MASUK_TERLAMBAT"){
                    r[row.tanggalKerja] = ++r[row.tanggalKerja] || 1;
                }
                return r;
            }, {});
            
            var kehadiranSum = Object.keys(kehadiran).map(function (key) {
                return { tanggalKerja: key, kehadiran: kehadiran[key] };
            });
            var terlambatSum = Object.keys(terlambat).map(function (key) {
                return { tanggalKerja: key, terlambat: terlambat[key] };
            });
            datarow = kehadiranSum.map((item, i) => Object.assign({}, item, terlambatSum[i])).sort((a, b) => new Date(a.tanggalKerja) - new Date(b.tanggalKerja));
        }
        console.log(datarow);
        return datarow
    }

}
export default StatisticAbsensiPegawai;