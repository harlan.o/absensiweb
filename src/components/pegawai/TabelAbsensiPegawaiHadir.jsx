import React, { Component } from "react";
import { Table, Card } from "react-bootstrap";
import "../TabelAbsensiPegawai.css";
import {connect} from 'react-redux';
import {getListAbsenInduk, getListAbsenKerja} from "../../actions/absensiAction";
const moment = require('moment');


class TableAbsensiPegawaiHadir extends Component {
  constructor(props) {
    super(props); //since we are extending class Table so we have to use super in order to overrnipe Component class constructor
    this.state = {
      totalData:0,
        search:'',
        kodeUnitInduk: this.props.getFilter?.kodeUnitInduk,
        kodeUnitKerja: this.props.getFilter?.kodeUnitKerja,
        startDate: this.props.getFilter?.startDate,
        endDate: this.props.getFilter?.endDate,
        rowData:null,
        loading:true
    };
    this.renderTableData=this.renderTableData.bind(this);
    // this.props.getListAbsenInduk(this.props.getFilter?.kodeUnitInduk, this.props.getFilter?.startDate, this.props.getFilter?.endDate, "masuk");  
    
  }
  
  async callApi(){
    if(this.props.dataRow !== null){
      await this.setState(() => ({
       rowData:this.props.dataRow,  
       loading:false    
      })) 
      // console.log("Tabel Hadir")
    }
  }

  componentDidMount(){
    if(this.state.loading !== false){
      this.callApi();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.getFilter !== this.props.getFilter || prevProps.dataRow !== this.props.dataRow){
      // console.log("call");
      this.callApi();
    }
  }

    
  renderTableData(){
    if(this.state.loading !== true && this.state.rowData !== undefined && this.state.rowData !== null){
      this.state.rowData.filter( rowData =>{
        // console.log(rowData.nama);
        if(rowData.nama.toLowerCase().indexOf( this.props.search ) !== -1 || rowData.nama.toLowerCase().indexOf( this.props.search ) !== -1){
          return true
        }
        return false
      });
      return this.state.rowData.map( (data, key) => {
        let dateAbsen = moment(data.jamAbsen);
        let no= key+1;
        this.count=no;
        return(
          <tr key={key}>
              <td>{no}</td>
              <td>{data.nama} <br/> {data.nip} </td>
              <td>{data.unitInduk}<br/> {data.unitKerja}</td>
              <td>{dateAbsen.format('YYYY-MM-D, | h:mm:ss a')}</td>
          </tr>
        )
      }     
      );
    }
  }


  render() {
    // console.log("render table");
    return (

    <Card className="mb-4" bg="dark" text="white">  
        <Card.Header as="h5">Hadir</Card.Header>
        <Card.Body>
            <Table responsive className="table table-striped table-hover custom-table2 text-nowrap">
                <tbody>
                <tr>
                  <th>No.</th>
                  <th>Nama/NIP</th>
                  <th>Unit</th>
                  <th>Waktu</th>
                </tr>
                    {this.renderTableData()}
                </tbody>
            </Table>
                
        </Card.Body>
    </Card>
    );
  }
}




const mapStateToProps = (state) => {
  return{
    listAbsensiInduk: state.listAbsensiInduk
  } 
}
export default connect(mapStateToProps, {getListAbsenInduk, getListAbsenKerja})(TableAbsensiPegawaiHadir);