import React, { Component, Fragment } from "react";
import { Table, Card, Col, Form, Row, Spinner } from "react-bootstrap";
import "./TabelLaporanAbsensi.css";
import {connect} from 'react-redux';
import {getListAbsenInduk, getListAbsenKerja} from "../../actions/absensiAction";
const moment = require('moment');


class TabelLaporanHarianPegawai extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      totalData:0,
        search:'',
        kodeUnitInduk: this.props.getFilter?.kodeUnitInduk,
        kodeUnitKerja: this.props.getFilter?.kodeUnitKerja,
        startDate: this.props.getFilter?.startDate,
        endDate: this.props.getFilter?.endDate,
        rowData:null,
        loading:true
    };
    this.renderTableData=this.renderTableData.bind(this);    
  }
  
  async callApi(){
    if(this.props.dataRow !== null && this.props.dataRow !== undefined){
      let dataCombin=this.props.dataRow;
      await this.setState(() => ({
       rowData:dataCombin,  
       loading:false    
      })) 
    }
  }

  componentDidMount(){
    if(this.state.loading !== false){
      this.callApi();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.getFilter !== this.props.getFilter || prevProps.dataRow !== this.props.dataRow){
      this.callApi();
    }
  }
    
  renderTableData(){
    if(this.state.loading !== true && this.state.rowData !== undefined && this.state.rowData !== null){
      let datashow = this.state.rowData.filter( rowData =>{
        if(rowData.nama.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1 || rowData.nama.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1){
          return true
        }
        return false
      });

      return datashow.map( (data, key) => {
        
        let no= key+1;
        this.count=no;
        return(
          <tr key={key}>
              <td>{no}</td>
              <td>{data.nama} <br/><small> {data.nip}</small><br/>
              {data.unitInduk}<br/> <small>{data.unitKerja}</small> </td>
              <td><p>
                  {data.absen ? this.listAbsen(data.absen):"-"}
              </p>
                 
              </td>
          </tr>
        )
      }     
      );
    }else{
      return(
        <tr>
          <td colSpan="4" className="loadingPage2">
            <Spinner animation="border" className="spin"  variant="light" />
          </td>
        </tr>
      )
    }
  }

  listAbsen(absen){
    let tglSebelum;
    return absen.map((data, key) => {
      let dateAbsen = moment(data.waktu);
      let tgl= dateAbsen.format('YYYY-MM-DD');
      if(tglSebelum != tgl){
        tglSebelum = tgl 
        return(<Fragment key={key}><br/> {tgl} | {dateAbsen.format(' HH:mm')} (<small>{data.type}</small>)</Fragment>)
      }else{
        return(<Fragment key={key}>{' '}- {dateAbsen.format(' H:mm')} (<small>{data.type}</small>) </Fragment>)
      }

    });
  }

  render() {
    // console.log("render table");
    return (

    <Card className="mb-4" bg="dark" text="white">  
        <Card.Header>
        <Row>
        <Col xs={6} md={4}>
              Laporan Harian
            </Col>
            <Col xs={6} md={{ span: 4, offset: 4 }}>
              <Form.Row className="align-items-center">
                <Form.Label  column xs="auto">
                  Cari
                </Form.Label>
                <Col>
                  <Form.Control type="text" placeholder="Cari Nama Pegawai" onChange={v => this.setState(({search: v.target.value }))}/>
                </Col>
              </Form.Row>
            </Col> 
            </Row>
        </Card.Header>
        <Card.Body>
            <Table responsive className="table table-striped table-hover custom-table3 text-nowrap">
            <tbody>
                <tr>
                  <th>No.</th>
                  <th className="colomInti">Pegawai</th>
                  <th>Waktu</th>
                </tr>
                    {this.renderTableData()}
                </tbody>
            </Table>
            
        </Card.Body>
    </Card>
    );
  }
}




const mapStateToProps = (state) => {
  return{
    listAbsensiInduk: state.listAbsensiInduk
  } 
}
export default connect(mapStateToProps, {getListAbsenInduk, getListAbsenKerja})(TabelLaporanHarianPegawai);