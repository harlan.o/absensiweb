import React, { Component } from "react";
import { Table, Card, Col, Form, Row, Spinner } from "react-bootstrap";
// import "./TabelLaporanAbsensi.css";
import {connect} from 'react-redux';
import {getListAbsenInduk, getListAbsenKerja} from "../../actions/absensiAction";
const moment = require('moment');


class TabelLaporanSummaryPegawai extends Component {
  constructor(props) {
    super(props); //since we are extending class Table so we have to use super in order to overrnipe Component class constructor
    this.state = {
      totalData:0,
        search:'',
        kodeUnitInduk: this.props.getFilter?.kodeUnitInduk,
        kodeUnitKerja: this.props.getFilter?.kodeUnitKerja,
        startDate: this.props.getFilter?.startDate,
        endDate: this.props.getFilter?.endDate,
        rowData:null,
        loading:true
    };
    this.renderTableData=this.renderTableData.bind(this);
    // this.props.getListAbsenInduk(this.props.getFilter?.kodeUnitInduk, this.props.getFilter?.startDate, this.props.getFilter?.endDate, "masuk");  
    
  }
  
  async callApi(){
    if(this.props.dataRow !== null){
      await this.setState(() => ({
       rowData:this.props.dataRow,  
       loading:false    
      })) 
      // console.log("Tabel Hadir")
    }
  }

  componentDidMount(){
    if(this.state.loading !== false){
      this.callApi();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.getFilter !== this.props.getFilter || prevProps.dataRow !== this.props.dataRow){
      // console.log("call");
      this.callApi();
    }
  }

    
  renderTableData(){
    if(this.state.loading !== true && this.state.rowData !== undefined && this.state.rowData !== null){
      let datashow = this.state.rowData.filter( rowData =>{
        // console.log(rowData.nama);
        if(rowData.nama.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1 || rowData.nama.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1){
          return true
        }
        return false
      });
      return datashow.map( (data, key) => {
        // let dateAbsen = moment(data.jamAbsen);
        let no= key+1;
        this.count=no;
        return(
          <tr key={key}>
              <td className="no">{no}</td>
              <td>{data.nama} <br/><small> {data.nip}</small><br/>
              {data.unitInduk}<br/> <small>{data.unitKerja}</small> </td>
              <td>{data.totalKehadiran ? data.totalKehadiran: 0}x Hadir </td>
              <td> { data.totalTerlambat ? data.totalTerlambat: 0}x Terlambat</td>
          </tr>
        )
      }     
      );
    }else{
      return(
        <tr>
          <td colSpan="4" className="loadingPage2">
            <Spinner animation="border" className="spin"  variant="light" />
          </td>
        </tr>
      )
    }
  }


  render() {
    // console.log(this.state.rowData);
    return (

    <Card className="mb-4" bg="dark" text="white">  
        <Card.Header >
        <Row>
        <Col xs={6} md={4}>
            Laporan Summary
            </Col>
            <Col xs={6} md={{ span: 4, offset: 4 }}>
              <Form.Row className="align-items-center">
                <Form.Label  column xs="auto">
                  Cari
                </Form.Label>
                <Col>
                  <Form.Control type="text" placeholder="Cari Nama Pegawai" onChange={v => this.setState(({search: v.target.value }))}/>
                </Col>
              </Form.Row>
            </Col> 
            </Row>
          
          </Card.Header>
        <Card.Body>
            <Table responsive className="table table-striped table-hover custom-table3 text-nowrap">
                <tbody>
                <tr>
                  <th>No.</th>
                  <th className="colomInti">Pegawai</th>
                  <th>Total Hadir</th>
                  <th>Total Terlambat</th>
                </tr>
             
                    {this.renderTableData()}
                </tbody>
            </Table>
                
        </Card.Body>
    </Card>
    );
  }
}




const mapStateToProps = (state) => {
  return{
    listAbsensiInduk: state.listAbsensiInduk
  } 
}
export default connect(mapStateToProps, {getListAbsenInduk, getListAbsenKerja})(TabelLaporanSummaryPegawai);