import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import ListCheckBoxPegawai from '../pegawai/ListCheckBoxPegawai';
import SelectSingleUnitInduk_IdSend from '../unit-induk/SelectSingleUnitInduk_IdSend';
import SelectSingleUnitKerja_IdSend from '../unit-kerja/SelectSingleUnitKerja_IdSend';

// Reducer
import { getListUnitKerja } from "../../actions/unitKerjaAction";
import { getListUnitInduk } from "../../actions/unitIndukAction";
import { getSimpleListPegawai } from "../../actions/pegawaiActions";
import { getListJenisIjin, createPengambilanIjin } from "../../actions/pengambilanIjinAction";
import {connect} from 'react-redux';

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import '../css/sweetaler2-style.css';
import SelectSingleJenisIjin_IdSend from './SelectSingleJenisIjin_IdSend';
import SelectSinglePegawai_IdSend from '../pegawai/SelectSinglePegawai_IdSend';
import { data } from 'jquery';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { uIIDP } from '../../actions/userActions';
import { REGISTER_FU001 } from '../../constants/hakAksesConstants';

const dataLokasi=[{id:1,nama:"Manado"},{id:2,nama:"Siau"},{id:3,nama:"Tagulandang"},{id:4,nama:"Biaro"}]

class ModalFormAddPengambilanIjin extends Component{
    constructor(props) {
        super(props);        
        this.state = {
           judul:"Judul",
           jenisModal:"JENIS",
           data:{
               id:null,
               idPegawai:null,
               idIjin:null,
               tanggalAwal:null,
               tanggalAkhir:null,               
               alasan:"",
               unitIndukId:null,
               unitKerjaId:null,
           },
           listJenisIjin:[],
           listUnitInduk:[],
           listUnitKerja:[],
           listPegawai:[],
           readonly:false,
        };
        this.headleChangeUnitInduk = this.headleChangeUnitInduk.bind(this);
        this.MySwal = withReactContent(Swal);
    }

    getListSelectedPegawai = (v)=>{
        this.setState( prevState => ({       
            data:{...prevState.data, pegawaiIdList : v }
        }));
    }

    // async callAPI(){
    // //  if needed  
    // }

    async callApi(){
        await this.props.getListUnitInduk().then(()=>{
          this.props.getListUnitKerja().then(()=>{
               this.setState(() => ({
                 loading:false,
                 listUnitInduk:this.props.listPengUnitInduk?.data,
                 listUnitKerja:this.props.listPengUnitKerja?.data,
               }), () => {
                    if (cekHakAksesClient(REGISTER_FU001) != true) {
                        console.log("here",cekHakAksesClient(REGISTER_FU001) )
                        let _v = { id:uIIDP()}
                        this.headleChangeUnitInduk(_v)
                    } 
              });               
             });  
        });
    }
    callAPIPegawai(unitInduk, unitKerja){
        this.props.getSimpleListPegawai(unitInduk, unitKerja).then(()=>{
            this.setState({
                listPegawai: this.props.simpleListPengPegawai.data,
            })
        })
        this.props.getListJenisIjin().then(()=>{
            this.setState({
                listJenisIjin: this.props.listJenisIjin.data,
            })
        })
    }

    componentDidMount(){
        this.callApi()
        // this.callApiUnit()
    }

    render (){
        return (
            <>
              <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Tambah Ijin Pegawai
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>

                <Row>
                    <Col xs={12} md={12} lg={12}>
                        <Card className="mb-2">
                            <Card.Body>
                               <Form.Row>
                                    <Form.Group as={Col} md={12}>
                                        <Form.Label className="textblack">Unit Induk*</Form.Label>
                                        <SelectSingleUnitInduk_IdSend dataoption={ this.state.listUnitInduk.length != 0 ? this.state.listUnitInduk : []} valueOption={this.state.data?.unitIndukId} valueGetSingle={this.headleChangeUnitInduk} isDisabled={cekHakAksesClient(REGISTER_FU001) ?  false : true} placeholder="Pilih Unit Induk"/> 
                                    </Form.Group>
                                </Form.Row>
                                { this.state.data.unitIndukId != null ? 
                                <Form.Row>
                                    <Form.Group as={Col} md={12}>
                                        <Form.Label className="textblack">Unit Kerja*</Form.Label>
                                        <SelectSingleUnitKerja_IdSend dataoption={ this.state.listUnitKerja.length != 0 ? this.state.listUnitKerja : []}  valueOption={this.state.data?.unitKerjaId} valueGetSingle={v => {
                                            let c=""; if(v!=null){c=v.id}else{c=null} this.setState(prevState => ({data:{...prevState.data, unitKerjaId : c, idPegawai:null }}))
                                            this.callAPIPegawai(this.state.data.unitIndukId,c)
                                            }} placeholder="Pilih Unit Kerja"/>
                                    </Form.Group>
                                </Form.Row>                                
                                :""}
                                { this.state.data.unitKerjaId != null ?
                                <Form.Row>
                                    <Form.Group as={Col} md={12}>
                                        <Form.Label className="textblack">Pegawai*</Form.Label>
                                        <SelectSinglePegawai_IdSend dataoption={this.state.listPegawai.length != 0 ? this.state.listPegawai : []} valueOption={this.state.data.idPegawai} 
                                        valueGetSingle={v => {let c=""; if(v!=null){c=v.id}else{c=null} this.setState(prevState => ({data:{...prevState.data, idPegawai : c, tanggalAwal: null, tanggalAkhir:null, alasan:"", idIjin:null } }))}}
                                        />
{/*                                         
                                        <SelectSinglePegawai_IdSend dataoption={dataLokasi} valueOption={this.state.data.idPegawai} 
                                        valueGetSingle={v => {let c=""; if(v!=null){c=v.id} this.setState(prevState => ({data:{...prevState.data, idPegawai : c }}))}}/> */}
                                    </Form.Group>
                                </Form.Row>
                                :""}
                            </Card.Body>
                        </Card>
                        { this.state.data.idPegawai != null ?
                        <Card className="mb-2">
                            <Card.Body>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_tanggal_mulai">
                                        <Form.Label className="textblack">Tanggal Mulai*</Form.Label>
                                        <br/>
                                        <DatePicker   
                                            className="form-control"
                                            selected={this.state.data.tanggalAwal }
                                            dateFormat="yyyy-MM-dd"
                                            minDate={new Date()}
                                            onChange={v => this.setState(prevState => ({data:{...prevState.data, tanggalAwal : v }}))}
                                            placeholderText="Pilih Tanggal Mulai"
                                        />
                                    </Form.Group>
                                    <Form.Group as={Col} controlId="form_tanggal_selesai">
                                        <Form.Label className="textblack">Tanggal Selesai*</Form.Label>
                                        <br/>
                                        <DatePicker   
                                            className="form-control"
                                            selected={this.state.data.tanggalAkhir }
                                            dateFormat="yyyy-MM-dd"
                                            minDate={this.state.data.tanggalAwal != null ? this.state.data.tanggalAwal : new Date()}
                                            // onChange={this.handleChangeEndDate}
                                            onChange={v => this.setState(prevState => ({data:{...prevState.data, tanggalAkhir : v }}))}
                                            placeholderText="Pilih Tanggal Selesai"
                                        />
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} md={12}>
                                        <Form.Label className="textblack">Jenis Ijin*</Form.Label>
                                        <SelectSingleJenisIjin_IdSend  dataoption={this.state.listJenisIjin.length != 0 ? this.state.listJenisIjin : []} valueOption={this.state.data?.idIjin} valueGetSingle={v => {let c=""; if(v!=null){c=v.id} this.setState(prevState => ({data:{...prevState.data, idIjin : c }}))}}/>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Group controlId="alasan">
                                    <Form.Label className="textblack">Alasan</Form.Label>
                                    <Form.Control as="textarea" rows={3} value={this.state.data.alasan} onChange={v => this.setState(prevState =>({data:{...prevState.data, alasan : v.target.value}})) } />
                                </Form.Group>
                            </Card.Body>
                        </Card>
                        :""}
                    </Col>

                </Row>
                  </Modal.Body>
              <Modal.Footer>
                  <Button variant="secondary" onClick={this.props.onHide}>
                      Tutup
                  </Button>
                  <Button variant="primary" className="btn-them-red" onClick={this.handleCreateEven}>
                      Simpan
                  </Button>
                 
              </Modal.Footer>
              </>
          );
    }
    headleChangeUnitInduk = (v) => {
        let c=""; 
        console.log("call", v)
        if(v!=null){c=v.id}else{
            c=null;
        } 
        this.setState(prevState => ({data:{...prevState.data, unitIndukId : c, unitKerjaId:null, idPegawai:null }}),
            () => {
                let unitKerjaKecil= this.props.listPengUnitKerja.data;
                if(c != ""){
                    unitKerjaKecil= unitKerjaKecil.filter(
                      (kerja) => {
                        return kerja.unitInduk?.id== c
                      });
                  }else{
                    unitKerjaKecil=[]
                  }
                  this.setState({
                    listUnitKerja: unitKerjaKecil,
                  })
                  this.callAPIPegawai(c, null);
            })
    }

    handleCreateEven =  (e) =>  {
        e.preventDefault();
        let info="";
        let nVerif=0;
        if(this.state.data.unitIndukId === "" || this.state.data.unitIndukId === null){
            info+="Belum memilih Unit Induk <br/>";
            nVerif=1;
        }else{
            if(this.state.data.unitKerjaId === "" || this.state.data.unitKerjaId === null){
                info+="Belum memilih Unit Kerja <br/>";
                nVerif=1;
            }else{
                if(this.state.data.idPegawai === "" || this.state.data.idPegawai === null){
                    info+="Belum memilih Unit Pegawai <br/>";
                    nVerif=1;
                }else{
                    if(this.state.data.tanggalAkhir === "" || this.state.data.tanggalAkhir === null){
                        info+="Tanggal Akhir Belum ditentukan <br/>";
                        nVerif=1;
                    }
                    if(this.state.data.tanggalAwal === "" || this.state.data.tanggalAwal === null){
                        info+="Tanggal Awal Belum ditentukan <br/>";
                        nVerif=1;
                    }
                    if(Date.parse(this.state.data.tanggalAwal) > Date.parse(this.state.data.tanggalAkhir)){
                        info+="Tanggal Awal dan tanggal akhir tak sesuai <br/>";
                        nVerif=1;
                    }
                    if(this.state.data.idIjin === "" || this.state.data.idIjin === null){
                        info+="Jenis Ijin belum di pilih <br/>";
                        nVerif=1;
                    }
                }
            }

        }
        console.log(this.state.data)
        if(nVerif===1){
            this.MySwal.fire({
                icon: "error",
                title: "Perhatian!",
                html: info,
                customClass: 'swal-wide',
            });
        }else{
            this.props.createPengambilanIjin(this.state.data).then(()=> {
                const {success} = this.props.infoCreatePengambilanIjin;
                const {loading} = this.props.infoCreatePengambilanIjin;
                if(success){
                    this.MySwal.fire({
                        icon: "success",
                        title: "Berhasil!",
                        text: "Pelaksana Tugas telah dibuat",
                        // customClass: 'swal-wide',
                    });
                    this.props.actionLoading();
                    this.props.onHide();
                }
                if(loading == false && success == false){
                    this.MySwal.fire({
                        icon: "error",
                        title: "Coba Lagi!",
                        // html: info,
                        // customClass: 'swal-wide',
                    });
                }
            })
        }

    }

}

const mapStateToProps = (state) => {
    return{
        infoCreatePengambilanIjin: state.infoCreatePengambilanIjin,
        listPengUnitInduk: state.listPengUnitInduk,
        listPengUnitKerja: state.listPengUnitKerja,
        simpleListPengPegawai: state.simpleListPengPegawai,
        listJenisIjin: state.listJenisIjin,
    } 
  }
export default connect(mapStateToProps, {createPengambilanIjin, getListUnitInduk, getListUnitKerja, getSimpleListPegawai, getListJenisIjin}) (ModalFormAddPengambilanIjin)