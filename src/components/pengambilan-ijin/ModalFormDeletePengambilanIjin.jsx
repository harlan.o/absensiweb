import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import ListCheckBoxPegawai from '../pegawai/ListCheckBoxPegawai';
import SelectSingleUnitInduk_IdSend from '../unit-induk/SelectSingleUnitInduk_IdSend';

// Reducer
import { deletePengambilanIjin } from "../../actions/pengambilanIjinAction";
import {connect} from 'react-redux';
import { clockConversi, padLeadingZero } from '../../actions/genneralAction';

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import '../css/sweetaler2-style.css';

class ModalFormDeletePengambilanIjin extends Component{
    constructor(props) {
        super(props);        
        this.state = {
           judul:"Judul",
           jenisModal:"JENIS",
           data:{
            id:null,
            idPegawai:null,
            idIjin:null,
            tanggalAwal:null,
            tanggalAkhir:null,               
            alasan:"",
            unitIndukId:null,
            unitKerjaId:null,
        },
           readonly:false,
        };
        this.MySwal = withReactContent(Swal);
    }

    getListSelectedPegawai = (v)=>{
        this.setState( prevState => ({       
            data:{...prevState.data, pegawaiIdList : v }
        }));
    }

    async callAPI(){
        console.log(this.props.dataonerow)
        let _data={
            id:this.props.dataonerow.id,
            idPegawai:null,
            namaPegawai:this.props.dataonerow.namaPegawai,
            idIjin:null,
            tanggalAwal:this.props.dataonerow.tanggalAwal,
            tanggalAkhir:this.props.dataonerow.tanggalAkhir,
        }
        this.setState({      
            data: _data,
            loading:false,
        });

    }

    componentDidMount(){
        this.callAPI()
    }

    render (){
        return (
            <>
              <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Hapus Ijin Pegawai
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
<Card>
              <Card.Body>
                      <h1>Perhatian!</h1>
                      <h4>
                      <small>Apakah yakin ingin menghapus ijin pegawai </small> {this.state.data.namaPegawai} <small> untuk tugas luar pada {this.state.data.tanggalAwal } sampai {this.state.data.tanggalAkhir } ?</small></h4>
                  </Card.Body>
</Card>

                  </Modal.Body>
              <Modal.Footer>
                  <Button variant="secondary" onClick={this.props.onHide}>
                      Batal
                  </Button>
                  <Button variant="primary" className="btn-them-red" onClick={this.handleCreateEven}>
                      Hapus
                  </Button>
              </Modal.Footer>
              </>
          );
    }


    handleCreateEven =  (e) =>  {
        e.preventDefault();
        this.props.deletePengambilanIjin(this.state.data).then(()=> {
            const {success} = this.props.infoDeletePengambilanIjin;
            const {loading} = this.props.infoDeletePengambilanIjin;
            console.log("here")
            console.log(success)
            if(success){
                this.MySwal.fire({
                    icon: "success",
                    title: "Berhasil!",
                    text: "Ijin pegawai a/n "+this.state.data.nama+" berhasil dihapus",
                    // customClass: 'swal-wide',
                });
                this.props.actionLoading();
                this.props.onHide();
            }
            if(loading == false && success == false){
                this.MySwal.fire({
                    icon: "error",
                    title: "Coba Lagi!",
                    // html: info,
                    // customClass: 'swal-wide',
                });
            }
        })
    }

}

const mapStateToProps = (state) => {
    return{
        infoDeletePengambilanIjin: state.infoDeletePengambilanIjin,
    } 
  }
export default connect(mapStateToProps, {deletePengambilanIjin}) (ModalFormDeletePengambilanIjin)