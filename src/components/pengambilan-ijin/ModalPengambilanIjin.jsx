import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import ModalFormAddPengambilanIjin from './ModalFormAddPengambilanIjin';
import ModalFormDeletePengambilanIjin from './ModalFormDeletePengambilanIjin';

class ModalPengambilanIjin extends Component{
    constructor(props) {
        
        super(props);        
        this.state = {
           jenisModal:"JENIS",
           data:null,
        };
        // this.handleChangeModeDetailToEdit = this.handleChangeModeDetailToEdit.bind(this);
        // this.handleChangeModeEditToDetail = this.handleChangeModeEditToDetail.bind(this);
    }

    async callAPI(modal, data){
        this.setState(() => ({
            jenisModal:modal,
            data:data
        }));
    }

    componentDidMount(){
        console.log("hadir")
        this.callAPI(this.props.jenisModal, this.props.dataonerow);
    }

    // componentDidUpdate(prevProps, prevState){
    //     if(this.state.jenisModal !== prevState.jenisModal){
    //         this.callApi(this.props.jenisModal);
    //     }

    // }

    render (){
        return (
            <Modal 
              {...this.props}
              aria-labelledby="contained-modal-title-vcenter"
            //   dialogClassName="modal-90w"
              >
                  {this.randerModal(this.state.jenisModal)}
            </Modal>
          );
    }
    randerModal(){
        console.log(this.state.jenisModal)
        console.log(this.props.dataonerow)
        switch (this.state.jenisModal) {
            case "TAMBAH":
                return(<ModalFormAddPengambilanIjin onHide={()=> {this.props.onHide()}} actionLoading={()=> {this.props.actionLoading()}} />)
            case "HAPUS":
                return(<ModalFormDeletePengambilanIjin onHide={()=> {this.props.onHide()}} actionLoading={()=> {this.props.actionLoading()}} dataonerow={this.props.dataonerow} />)
            default:
                break;
        }
    }

}

export default  ModalPengambilanIjin