import React, { Component } from 'react';
import AsyncSelect from 'react-select/async';

export class SelectSingleJenisIjin_IdSend extends Component {
  constructor(props) {
    super(props);
    this.state = {
        optionUnitInduk:this.props.dataoption,
        inputValue: '',
        valueOption:this.props?.valueOption,
    };
    
  }

  async update(dataList, dataValue) {    
    console.log(dataList)
    await this.setState(({       
      dataRow: dataList,
      valueOption: dataValue,
    }));
  }
  componentDidMount(){
    this.update( this.props.dataoption, this.props.valueOption)
  }
  componentDidUpdate(prevProps, prevState) {
    if(this.props.dataoption !== prevProps.dataoption){
        // default= 
        // this.props.dataoption.push({id:0, nama:"Semua Unit Induk"})
        console.log(this.props.dataoption)
        if(this.props.dataoption !== undefined){
            this.setState((state) => ({
                dataRow: this.props.dataoption,
                valueOption: this.props.valueOption,
                renderTable:true,
                loadingPage:false
            }));
        } 
    }
    if(this.props.valueOption !== prevProps.valueOption){
      if(this.props.valueOption !== undefined){
        this.setState((state) => ({
            valueOption: this.props.valueOption,
        }));
      } 
    }
  }

   filterColors = (inputValue) => {
     let bab;
       bab= this.state.optionUnitInduk?.filter(i =>
       i.nama?.toLowerCase().includes(inputValue.toLowerCase())
       );
     return bab
  };
  
   promiseOptions = inputValue =>
    new Promise(resolve => {
      setTimeout(() => {
        resolve(this.filterColors(inputValue));
      }, 1000);
    });
  
    valueGetSend = (v)=>
    {
      this.props.valueGetSingle(v);
    }

  
  render() {
    let valueId={id: this.state.valueOption};
    console.log(valueId);
    console.log(this.state.optionUnitInduk);

    return (
      <>
      {(this.state.optionUnitInduk.length > 0) ? (<AsyncSelect cacheOptions onChange={this.valueGetSend} 
      value={this.state.optionUnitInduk.filter(option => {
        return option.id === this.state.valueOption;
      })}
      isDisabled={this.props.canedit}
      getOptionValue={option => option.id} 
      getOptionLabel={option => option.nama}
      defaultOptions
      isClearable
      loadOptions={this.promiseOptions} 
      placeholder='Pilih Jenis Cuti'
      />) : "" }
      </>
    //   <AsyncSelect cacheOptions onChange={this.valueGetSend} 
    //   inputProps ={valueId}
    //   getOptionValue={option => option.id} 
    //   getOptionLabel={option => option.nama+" ("+option.checkIn+"-"+option.checkOut+")"}
    //   defaultOptions
    //   isClearable
    //   loadOptions={this.promiseOptions} 
    //   placeholder='Pilih Jadwal'
    // //   isMulti
    // //   className="basic-multi-select"
    // //   classNamePrefix="select"
    //   />
    );
  }
}


export default SelectSingleJenisIjin_IdSend;

