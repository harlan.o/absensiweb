import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import DatePicker from 'react-datepicker';
import ListCheckBoxPegawai from '../pegawai/ListCheckBoxPegawai';
import SelectSingleUnitInduk_IdSend from '../unit-induk/SelectSingleUnitInduk_IdSend';

// Reducer
import { getListUnitInduk } from "../../actions/unitIndukAction";
import { updateMelaksanakanTugas } from "../../actions/penugasanAction";
import {connect} from 'react-redux';
import { clockConversi, padLeadingZero } from '../../actions/genneralAction';

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import '../css/sweetaler2-style.css';

class ModalFormDeleteTugasLuar extends Component{
    constructor(props) {
        super(props);        
        this.state = {
           judul:"Judul",
           jenisModal:"JENIS",
           data:{
               id:null,
               tanggalAwal:null,
               tanggalAkhir:null,               
               pegawaiIdList:[],
           },
           readonly:false,
        };
        this.MySwal = withReactContent(Swal);
    }

    getListSelectedPegawai = (v)=>{
        this.setState( prevState => ({       
            data:{...prevState.data, pegawaiIdList : v }
        }));
    }

    async callAPI(){
        console.log(this.props.dataonerow)
        let _data={
            nama:this.props.dataonerow.namaPegawai,
            nip:this.props.dataonerow.nip,
            id:this.props.dataonerow.id,
            tanggalAwal:this.props.dataonerow.tanggalAwal,
            tanggalAkhir:this.props.dataonerow.tanggalAkhir,
        }
        this.setState({      
            data: _data,
            loading:false,
        });

    }

    componentDidMount(){
        this.callAPI()
    }

    render (){
        return (
            <>
              <Modal.Header closeButton>
                <Modal.Title id="contained-modal-title-vcenter">
                    Hapus Pegawai Tugas Luar
                </Modal.Title>
              </Modal.Header>
              <Modal.Body>
<Card>
              <Card.Body>
                      <h1>Perhatian!</h1>
                      <h4>
                      <small>Apakah Yakin ingin menghapus pegawai </small> {this.state.data.nama} <small> untuk tugas luar pada {this.state.data.tanggalAwal } sampai {this.state.data.tanggalAkhir } ?</small></h4>
                  </Card.Body>
</Card>

                {/* <Row>
                    <Col xs={12} md={12} lg={12}>
                        <Card className="mb-2">
                            <Card.Body>
                                <Form.Row>
                                <Form.Group as={Col} controlId="form_tanggal">
                                        <h5>{this.state.data.nama}<br/> <small>{this.state.data.nip}</small></h5>
                                    </Form.Group>
                                </Form.Row>
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_tanggal">
                                        <Form.Label className="textblack">Tanggal Mulai*</Form.Label>
                                        <br/>
                                        <DatePicker   
                                            className="form-control"
                                            selected={this.state.data.tanggalAwal }
                                            dateFormat="yyyy-MM-dd"
                                            // onChange={this.handleChangeEndDate}
                                            onChange={v => this.setState(prevState => ({data:{...prevState.data, tanggalAwal : v }}))}
                                            placeholderText="Pilih Tanggal Kegiatan"
                                        />
                                    </Form.Group>
                                    <Form.Group as={Col} controlId="form_tanggal">
                                        <Form.Label className="textblack">Tanggal Selesai*</Form.Label>
                                        <br/>
                                        <DatePicker   
                                            className="form-control"
                                            selected={this.state.data.tanggalAkhir }
                                            dateFormat="yyyy-MM-dd"
                                            // onChange={this.handleChangeEndDate}
                                            onChange={v => this.setState(prevState => ({data:{...prevState.data, tanggalAkhir : v }}))}
                                            placeholderText="Pilih Tanggal Kegiatan"
                                        />
                                    </Form.Group>
                                </Form.Row>
                            </Card.Body>
                        </Card>

                    </Col>
                </Row> */}
                  </Modal.Body>
              <Modal.Footer>
                  <Button variant="secondary" onClick={this.props.onHide}>
                      Batal
                  </Button>
                  <Button variant="primary" className="btn-them-red" onClick={this.handleCreateEven}>
                      Hapus
                  </Button>
              </Modal.Footer>
              </>
          );
    }


    handleCreateEven =  (e) =>  {
        e.preventDefault();
        console.log(this.state.data)
        let info="";
        let nVerif=0;
        if(this.state.data.tanggalAkhir === "" || this.state.data.tanggalAkhir === null){
            info+="Tanggal Akhir Belum ditentukan <br/>";
            nVerif=1;
        }
        if(this.state.data.tanggalAwal === "" || this.state.data.tanggalAwal === null){
            info+="Tanggal Awal Belum ditentukan <br/>";
            nVerif=1;
        }
        if(Date.parse(this.state.data.tanggalAwal) > Date.parse(this.state.data.tanggalAkhir)){
            info+="Tanggal Awal dan tanggal akhir tak sesuai <br/>";
            nVerif=1;
        }
 
        console.log(nVerif)
        if(nVerif===1){
            this.MySwal.fire({
                icon: "error",
                title: "Perhatian!",
                html: info,
                customClass: 'swal-wide',
            });
        }else{
            this.props.updateMelaksanakanTugas(this.state.data).then(()=> {
                const {success} = this.props.infoUpdateMelaksanakanTugas;
                const {loading} = this.props.infoUpdateMelaksanakanTugas;
                console.log("here")
                console.log(success)
                if(success){
                    this.MySwal.fire({
                        icon: "success",
                        title: "Berhasil!",
                        text: "Tanggal melakukan tugas a/n "+this.state.data.nama+" berhasil diubah",
                        // customClass: 'swal-wide',
                    });
                    this.props.onHide();
                }
                if(loading == false && success == false){
                    this.MySwal.fire({
                        icon: "error",
                        title: "Coba Lagi!",
                        // html: info,
                        // customClass: 'swal-wide',
                    });
                }
            })
        }        
    }

}

const mapStateToProps = (state) => {
    return{
        infoUpdateMelaksanakanTugas: state.infoUpdateMelaksanakanTugas,
    } 
  }
export default connect(mapStateToProps, {updateMelaksanakanTugas}) (ModalFormDeleteTugasLuar)