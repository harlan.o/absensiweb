import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import ModalFormAddMelaksanakanTugas from './ModalFormAddMelaksanakanTugas';
import ModalFormUpdateMelaksanakanTugas from './ModalFormUpdateMelaksanakanTugas';
import ModalFormDeleteMelaksanakanTugas from './ModalFormDeleteMelaksanakanTugas';

class ModalMelaksanakanTugas extends Component{
    constructor(props) {
        
        super(props);        
        this.state = {
           jenisModal:"JENIS",
           data:null,
        };
        // this.handleChangeModeDetailToEdit = this.handleChangeModeDetailToEdit.bind(this);
        // this.handleChangeModeEditToDetail = this.handleChangeModeEditToDetail.bind(this);
    }

    async callAPI(modal, data){
        this.setState(() => ({
            jenisModal:modal,
            data:data
        }));
    }

    componentDidMount(){
        console.log("hadir")
        this.callAPI(this.props.jenisModal, this.props.dataonerow);
    }

    // componentDidUpdate(prevProps, prevState){
    //     if(this.state.jenisModal !== prevState.jenisModal){
    //         this.callApi(this.props.jenisModal);
    //     }

    // }

    render (){
        return (
            <Modal 
              {...this.props}
              aria-labelledby="contained-modal-title-vcenter"
            //   dialogClassName="modal-90w"
              >
                  {this.randerModal(this.state.jenisModal)}
            </Modal>
          );
    }
    randerModal(){
        console.log(this.state.jenisModal)
        console.log(this.props.dataonerow)
        switch (this.state.jenisModal) {
            case "TAMBAH":
                return(<ModalFormAddMelaksanakanTugas onHide={()=> {this.props.onHide()}}/>)
            case "HAPUS":
                return(<ModalFormDeleteMelaksanakanTugas onHide={()=> {this.props.onHide()}} dataonerow={this.props.dataonerow} />)
                
            case "UBAH":
                return(<ModalFormUpdateMelaksanakanTugas onHide={()=> {this.props.onHide()}} dataonerow={this.props.dataonerow} />)
            default:
                break;
        }
    }

    // handleChangeModeDetailToEdit(){
    //     this.setState({
    //         jenisModal: "UBAH",
    //       })
    // }
    // handleChangeModeEditToDetail(){
    //     this.setState({
    //         jenisModal: "HAPUS",
    //       })
    // }

}

export default  ModalMelaksanakanTugas