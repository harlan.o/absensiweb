import React, { Component } from "react";
import { Card, Col, Row, Spinner, Button, Form, Table } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrash } from "@fortawesome/free-solid-svg-icons";
import { cekHakAksesClient } from "../../actions/hakAksesActions";
import { REGISTER_FU023, REGISTER_FU024 } from "../../constants/hakAksesConstants";

const moment = require('moment');

class TabelTugasLuar extends Component{
    constructor(props){
        super(props);
        this.state = {
            totalData:0,
            search:'',
            rowData:null,
            loading:true,
        }
    }

    async callApi(){
        if(this.props.datarow !== null && this.props.datarow !== undefined){
            await this.setState({
                rowData:this.props.datarow,
                loading:false
            })
        }
    }

    componentDidMount(){
        if(this.state.loading != false){
            this.callApi();
        }
    }

    componentDidUpdate(prevProps, prevState){
        console.log("render Berikut")
        console.log(this.props.getFIlter !== prevProps.getFIlter)

        if(this.props.getFIlter !== prevProps.getFIlter || prevProps.datarow !== this.props.datarow){
            this.callApi();
        }
    }

    renderTrTd(){
        if(this.state.loading !== true && this.state.rowData !== undefined && this.state.rowData !== null){
            let datashow = this.state.rowData.filter( rowData =>{
                            if(rowData.namaPegawai.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1 || rowData.namaPegawai.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1){
                                return true
                            }
                            return false
                            });

            return datashow.map( (data, key) => {
                let no= key+1;
                this.count=no;
                return(
                    <tr key={key}>
                        <td>{no}</td>
                        <td>{data.namaPegawai}<br/> <small>{data.nip}</small></td>
                        <td>{data.unitInduk}<br/> <small>{data.unitKerja}</small></td>
                        <td>{data.tanggalAwal}</td>
                        <td>{data.tanggalAkhir}</td>
                        <td>
                            {cekHakAksesClient(REGISTER_FU023) ?
                            <Button variant="secondary" className="btn-width" onClick={
                                (e) => {
                                    e.preventDefault();
                                    this.props.setdataonerow(data, "UBAH")
                                }
                            } > <FontAwesomeIcon icon={faPencilAlt} />
                            </Button>  
                            :""}{' '}
                            {cekHakAksesClient(REGISTER_FU024) ?
                            <Button variant="secondary" className="btn-width" onClick={
                                (e) => {
                                    e.preventDefault();
                                    this.props.setdataonerow(data, "HAPUS")
                                }
                            } > <FontAwesomeIcon icon={faTrash} />
                            </Button>
                            :""}
                        </td>
                    </tr>
                )
            })
        }else{
            return(
                <tr>
                  <td colSpan={6} className="loadingPage3">
                    <Spinner animation="border" className="spin"  variant="dark" />
                  </td>
                </tr>
              )
        }
    }

    render(){

        return(
            <Card className="mb-4" border="light" text="dark">
                <Card.Body>
                    <Row className="mb-2">
                        <Col xs={6} md={4}>
                            <h2>Tugas Luar</h2>
                        </Col>
                        <Col xs={6} md={{ span: 4, offset: 4 }}>
                            <Form.Row className="align-items-center">
                                <Form.Label  column xs="auto">
                                    Cari
                                </Form.Label>
                                <Col>
                                    <Form.Control type="text" placeholder="Cari " onChange={v => this.setState(({search: v.target.value }))}/>
                                </Col>
                            </Form.Row>
                        </Col>
                    </Row>
                    <Table responsive bordered className="table table-striped table-hover custom-table4 text-nowrap">
                        <tbody>
                            <tr>
                                <th>No.</th>
                                <th className="colomInti">Nama/NIP</th>
                                <th>Unit</th>
                                <th>Mulai</th>
                                <th>Selesai</th>
                                <th>---</th>
                            </tr>
                                {this.renderTrTd()}
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
        )
    }
}
export default TabelTugasLuar;