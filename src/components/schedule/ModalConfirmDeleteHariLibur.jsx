import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import TimeKeeper from 'react-timekeeper';
import '../TimeKeeper.css';
// Redux
import {connect} from 'react-redux';
import { deleteHariLibur } from "../../actions/hariLiburAction";

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
const moment = require('moment');

class ModalConfirmDeleteHariLibur extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"",
           data:{
            keterangan: "",
            tanggal: "",
           },
           readonly:false
        
        };

        this.MySwal = withReactContent(Swal);
    }
    async update(dataOne) {    
        await this.setState(({       
            data:dataOne,
            judul:"Konfirmasi Penghapusan",
            readonly:true,
        }));
    }
    componentDidMount(){
        if(this.props.dataedit != null){
            console.log("data awal",this.props.dataedit)
            this.update(this.props.dataedit)
            console.log("data",this.state.data)

        }
    }


render (){
    return (
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Body>

                      <h1>Perhatian !</h1>
                      <h2>
                      <small>Apakah Yakin ingin menghapus </small>
                            "{this.state.data.keterangan}" ?</h2>
                            <p>tanggal : {this.state.data.tanggal}</p>
                  </Card.Body>
              </Card>

                  </Form>
              </Modal.Body>
          <Modal.Footer>
              <Button variant="secondary" onClick={this.props.onHide}>
                  Batal
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleAction}>
                  Ya
              </Button>
          </Modal.Footer>
        </Modal>
      );
    }

    handleAction= (e) => {
            this.props.deleteHariLibur(this.state.data.tanggal).then(()=>{
                const {success} = this.props.infoDeleteHariLibur;
                if(success){
                    this.MySwal.fire({
                        icon: "success",
                        title: this.state.data.nama+" berhasil dihapus",
                    });
                    }
                    this.props.onHide();
                });
    }

}

const mapStateToProps = (state) => {
    return{
        infoDeleteHariLibur: state.infoDeleteHariLibur,
    } 
 }
export default connect(mapStateToProps, {deleteHariLibur})(ModalConfirmDeleteHariLibur);