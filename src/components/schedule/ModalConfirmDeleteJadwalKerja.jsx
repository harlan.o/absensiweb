import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import TimeKeeper from 'react-timekeeper';
import '../TimeKeeper.css';
// Redux
import {connect} from 'react-redux';
import { deleteJadwalKerja } from "../../actions/jadwalKerjaAction";

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
const moment = require('moment');

class ModalConfirmDeleteJadwalKerja extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"",
           data:{
            id: null,
            nama: "",
            checkIn: "00:00:00",
            checkOut: "00:00:00",
            sebelumMasuk: 0,
            sesudahMasuk: 0,
            sesudahPulang: 0,
            createdBy:null,
            createdDate:null,
            modifiedBy:null,
            modifiedDate:null,
           },
           readonly:false
        
        };

        this.MySwal = withReactContent(Swal);
    }
    async update(dataOne) {    
        await this.setState(({       
            data:dataOne,
            judul:"Konfirmasi Penghapusan",
            readonly:true,
        }));
    }
    componentDidMount(){
        if(this.props.dataedit != null){
            this.update(this.props.dataedit)
            console.log(this.state.data)

        }
    }

    clockConversi = (timeState) => {
        let [hour, minute]=timeState.split(":");
        let clock = hour+":"+minute;
        return clock;
    }

    padLeadingZero(timeState){
       let [hour, minute] = timeState.split(":");
       while (hour.length < 2) hour = "0" + hour;
       let clock = hour+":"+minute;
        return clock;
    }

render (){
    let clockIn = this.clockConversi(this.state.data.checkIn);
    let clockOut = this.clockConversi(this.state.data.checkOut);

    console.log(this.state.data);
    let createdDate="";
    let modifiedDate="";
    if(this.state.createdDate !== null && this.state.modifiedDate !== null){
        createdDate = moment(this.state.createdDate).format('DD-MM-YYYY HH:mm');
        modifiedDate = moment(this.state.modifiedDate).format('DD-MM-YYYY HH:mm');
    }
    
    // console.log(this.padLeadingZero(this.state.data.checkIn));
    return (
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Body>

                      <h1>Perhatian !</h1>
                      <h2>
                      <small>Apakah Yakin ingin menghapus </small>
                            "{this.state.data.nama}" ?</h2>
                            <p>masuk : {clockIn}, keluar : {clockOut}</p>
                      
                      
                      
                  </Card.Body>
              </Card>

                  </Form>
              </Modal.Body>
          <Modal.Footer>
              <Button variant="secondary" onClick={this.props.onHide}>
                  Batal
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleCreateJadwalKerja}>
                  Ya
              </Button>
          </Modal.Footer>
        </Modal>
      );
}

handleCreateJadwalKerja= (e) => {

        this.props.deleteJadwalKerja(this.state.data).then(()=>{
            const {success} = this.props.infoDeleteJadwalKerja;
            if(success){
                this.MySwal.fire({
                    icon: "success",
                    title: this.state.data.nama+" berhasil dihapus",
                  });
                }
                this.props.onHide();
            });
}

}

const mapStateToProps = (state) => {
    return{
        infoDeleteJadwalKerja: state.infoDeleteJadwalKerja,
    } 
 }
export default connect(mapStateToProps, {deleteJadwalKerja})(ModalConfirmDeleteJadwalKerja);