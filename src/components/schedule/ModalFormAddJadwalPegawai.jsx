import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import DatePicker, { Calendar } from "react-multi-date-picker";
import { DatePanel } from "react-multi-date-picker/plugins";
import '../css/multi-date-picker-style.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import {connect} from 'react-redux';
import { getListUnitInduk } from "../../actions/unitIndukAction";
import { getListUnitKerja } from "../../actions/unitKerjaAction";
import {getListJadwalKerja} from "../../actions/jadwalKerjaAction";
import {createJadwalPegawai} from "../../actions/jadwalPegawaiAction";
import SelectSingleSchedule_IdSend from './SelectSingleSchedule_IdSend';
import ListCheckBoxPegawai from '../pegawai/ListCheckBoxPegawai';
import SelectSingleUnitInduk_IdSend from '../unit-induk/SelectSingleUnitInduk_IdSend';

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import '../css/sweetaler2-style.css';
import { dayName, stringArrayId } from '../../actions/genneralAction';
import SelectSingleLokasi_NamaSend from './SelectSingleLokasi_NamaSend';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU001 } from '../../constants/hakAksesConstants';
import { uIIDP } from '../../actions/userActions';

const moment = require('moment');

const dataLokasi=[{id:1,nama:"MANADO"},{id:2,nama:"SIAU"},{id:3,nama:"TAGULANDANG"},{id:4,nama:"BIARO"}]

class ModalFormAddJadwalPegawai extends Component {
    constructor(props) {
        super(props);
        this.state = {
           judul:"Jadwal Pegawai",
           readonly:false,
           
           kunciTahap1: false,

           unitInduk:null,
           listUnitInduk:[],
           unitKerja:null,
           listUnitKerja:[],
           

           listTanggal:null,
           listTanggalJadwalConfirm:[],
           listJadwal:null,

           pegawaiIdList:[],
           lokasiWFH:null,
        };
        this.MySwal = withReactContent(Swal);
    }
    async update(dataOne) {    
        await this.setState(({       
            data:dataOne,
            judul:"Ubah Data Unit Induk",
            readonly:true,
        }));
    }
    componentDidMount(){
        this.props.getListJadwalKerja().then(()=> {
            this.setState(({       
                listJadwal:this.props.listJadwalKerja.data,
            }));
        })
        if(this.props.dataedit != null){
            this.update(this.props.dataedit)
            console.log(this.state.data)
            console.log("did")
        }
        this.props.getListUnitInduk().then(()=>{
            this.setState(() => ({
              listUnitInduk:this.props.listPengUnitInduk?.data,
            }), ()=> {
                if(!cekHakAksesClient(REGISTER_FU001)){
                    this.setState({unitInduk: uIIDP()})
                }   
            });
          }); 
        // this.props.getListUnitKerja().then(()=>{
        //     this.setState(() => ({
        //       listUnitKerja:this.props.listPengUnitKerja?.data,
        //     }));
        //   }); 
    }

    componentDidUpdate(prevProps, prevState){
        if(prevState.unitInduk !== this.state.unitInduk){
            let unitKerja_temp = this.props.listPengUnitKerja?.data.filter(v => v.unitInduk.id == this.state.unitInduk)
            this.setState(() => ({
                listUnitKerja:unitKerja_temp,
              }), () => console.log(this.state.listUnitKerja) );

          }
    }

    removeArrayCalender(arr, attr, value) {
        var i = arr.length;
        while(i--){
            console.log(attr)
            console.log(arr[i][attr])
            if( arr[i] 
                && (arguments.length > 2 && arr[i][attr] === value ) ){ 
               arr.splice(i,1);
           }
        }
        return arr;
    }

    removeArray(arr, attr, value) {
        var i = arr.length;
        while(i--){
            if( arr[i] 
                && arr[i].hasOwnProperty(attr) 
                && (arguments.length > 2 && arr[i][attr] === value ) ){ 
               arr.splice(i,1);
           }
        }
        return arr;
    }

    getObjArray(arr, attr, value) {
        var i = arr.length;
        let obj={};
        while(i--){
           if( arr[i] 
               && arr[i].hasOwnProperty(attr) 
               && (arguments.length > 2 && arr[i][attr] === value ) ){ 
               obj=arr[i]
           }
        }
        return obj;
    }

    getListSelectedPegawai = (v)=>{

        this.setState( { pegawaiIdList : v });
    }

    renderListTanggal(){
        console.log("a")
        if( this.state.listTanggal !== undefined && this.state.listTanggal !== null && this.state.listTanggal.length > 0 ){
            
            return this.state.listTanggal.map( (data, key) => {
                let obj=this.getObjArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD"))
            //  console.log()
                let _wfh=false;
                if(obj?.wfh){
                    _wfh=obj.wfh
                }

             return(
             <Col key={data.format("YYYY-MM-DD")} xs={12} md={12} lg={6} className="mb-2">
                    <Card>
                        <Card.Body>
                            <Card.Text>
                            <Form inline>
                            <div className="my-1 mr-2" as="h5" htmlFor="inlineFormCustomSelectPref">
                            {dayName(data.weekDay.index) },  {data.format("YYYY-MM-DD")}
                            </div>
                            { this.state.listJadwal ?
                            <div style={{width:'55%'}}>
        
        <SelectSingleSchedule_IdSend dataoption={this.state.listJadwal} valueOption={obj.jadwalId} valueGetSingle={v => {
            let id=0;
            let listTglConf=this.removeArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD"));
            if(v!= null && v!=undefined){
                id=v.id
            }
            let jadwalConf={
                tanggalKerja: data.format("YYYY-MM-DD"),
                jadwalId: id,
                wfh:_wfh,
            }
            if(id!=0){
                listTglConf.push(jadwalConf);
            }
            this.setState(prevState => ({listTanggalJadwalConfirm: listTglConf }), () => console.log(this.state.listTanggalJadwalConfirm));  }}/>

                        </div> : ""}
                        <Button className="my-1" variant="secondary" onClick={(e)=>{
                            e.preventDefault();
                            let listTglConf =this.removeArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD"));

                            let listTgl =this.removeArrayCalender([...this.state.listTanggal], 'dayOfBeginning', data.dayOfBeginning);
                            console.log(listTglConf);
                            this.setState(prevState => ({listTanggalJadwalConfirm: listTglConf, listTanggal: listTgl }), () => console.log(this.state.listTanggalJadwalConfirm));
                            }}>
                            <FontAwesomeIcon icon={faTimes} />
                        </Button>
                        </Form>

                        <Form inline>
                        <div className="my-1 mr-2" as="h5" htmlFor="inlineFormCustomSelectPref">
                            <Form.Group controlId={data.format("YYYY-MM-DD")}>
                                <Form.Check type="checkbox" label="Kerja Dari Lokasi Rumah" checked={_wfh} onChange={
                                    v=>{
                                        console.log(v.target.checked)
                                        let _wfh=v.target.checked
                                        let id=obj.jadwalId;
                                        let listTglConf=this.removeArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD"));
                                        
                                        let jadwalConf={
                                            tanggalKerja: data.format("YYYY-MM-DD"),
                                            jadwalId: id,
                                            wfh:_wfh,
                                        }
                                        listTglConf.push(jadwalConf);
                                        
                                        this.setState(prevState => ({listTanggalJadwalConfirm: listTglConf }), () => console.log(this.state.listTanggalJadwalConfirm));
                                    }
                                }/>
                            </Form.Group>
                            </div>
                            
                            {/* { (this.state.listJadwal && obj.wfh === true) ?
                                <>
                            <div style={{width:'100%'}}>

                            <SelectSingleLokasi_IdSend dataoption={this.state.listJadwal} valueOption={this.getObjArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD")).jadwalId} valueGetSingle={v => {
                                let id=0;
                                let listTglConf=this.removeArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD"));
                                if(v!= null && v!=undefined){
                                    id=v.id
                                }
                                let jadwalConf={
                                    tanggalKerja: data.format("YYYY-MM-DD"),
                                    jadwalId: id
                                }
                                if(id!=0){
                                    listTglConf.push(jadwalConf);
                                }
                                this.setState(prevState => ({listTanggalJadwalConfirm: listTglConf }), () => console.log(this.state.listTanggalJadwalConfirm));  }}/>

                                            </div> </>: ""} */}
                        </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

             )
            });
        }

    }
render (){

    return (
      <>
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >

                <Row>
                    <Col xs={12} md={6} lg={4}>
                        <Card className="mb-2">
                            <Card.Body>
                                <Form.Row >
                                    
                                    <Form.Group  as={Col} controlId="form_nama" >
                                    <Form.Label className="textblack">Unit Induk</Form.Label>
                                    { this.state.listUnitInduk.length > 0 ?
                                    <SelectSingleUnitInduk_IdSend  dataoption={this.state.listUnitInduk} isDisabled={cekHakAksesClient(REGISTER_FU001) ? this.state.kunciTahap1 : true } valueOption={this.state.unitInduk} valueGetSingle={v => {

                                        let id=-1;
                                        if(v!= null && v!=undefined){
                                            id=v.id
                                        }
                                        this.setState(prevState => ({ unitInduk : id })); console.log(id) }}/> : ""}
                                    </Form.Group>
                                </Form.Row>
                                
                                   { this.state.listUnitKerja.length > 0 ?
                                   (
                                    <Form.Row>
                                    <Form.Group as={Col} controlId="form_nama">
                                    <Form.Label className="textblack">Unit Kerja</Form.Label>
                                   <SelectSingleUnitInduk_IdSend dataoption={this.state.listUnitKerja} isDisabled={this.state.kunciTahap1} valueOption={this.state.unitKerja} valueGetSingle={v => {
                                    let id=-1;
                                    if(v!= null && v!=undefined){
                                        id=v.id
                                    }
                                    this.setState(prevState => ({ unitKerja : id })); console.log(id) }}/>
                                                                                                        </Form.Group>
                                </Form.Row>)
                                     : ""}                               
                            </Card.Body>
                            <Card.Footer>
                            <Button variant="primary" className="btn-them-red" onClick={(e)=>{
                            e.preventDefault();
                            this.setState({kunciTahap1: !this.state.kunciTahap1 });
                            }}>
                                Lanjut
                            </Button>
                            </Card.Footer>
                        </Card>

                        {    this.state.kunciTahap1 ? 
                        (
                        <Card className="mb-2">
                            <Card.Body>
                           <ListCheckBoxPegawai fixDisable idUnitInduk={this.state.unitInduk} idUnitKerja={this.state.unitKerja} listSelected={this.getListSelectedPegawai} />
                               
                               
                            </Card.Body>
                        </Card>
                        ) : " "}
                    </Col>
                    
                    {    this.state.kunciTahap1 ? 
                    (
                        <Col xs={12} md={6} lg={8}>
                        <Row>
                            <Col xs={12} md={12} lg={12} className="mb-2">
                                <div className="customDatePickerWidth">
                                        <Calendar className="customDatePickerWidth red" value={this.state.listTanggal} format="YYYY-MM-DD" onChange={v=> {this.setState({listTanggal: v}); console.log(moment(v[0]).format("YYYY-MM-DD")) }}multiple  /> 
                                </div>
                            </Col>
                            <Col  xs={12} md={12} lg={12}>
                                <Card>
                                    <Card.Body>
                                       <h5>List Tanggal</h5>
                                       <Row>
                                            <Col xs={12} md={12} lg={12} className="mb-2">
                                                <div style={{width:'100%'}}>
                                                <Form.Label className="textblack">Lokasi Kerja Jika WFH</Form.Label>
                                                        <SelectSingleLokasi_NamaSend dataoption={dataLokasi} valueOption={this.state.lokasiWFH} valueGetSingle={v => { let c=""; if(v!=null){c=v.nama} this.setState({lokasiWFH: c });  }}/>
                                                    </div>
                                            </Col>
                                            {this.renderListTanggal()}
                                        </Row> 
                               

                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Col>
                    ) :" "
                    }

                </Row>             
                  </Form>

              </Modal.Body>
          <Modal.Footer>
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleCreateJadwalPegawai}>
                  Simpan
              </Button>
          </Modal.Footer>
        </>
      );
}

handleCreateJadwalPegawai =  (e) =>  {
    e.preventDefault();
    let idArrPegawai=this.state.pegawaiIdList;
    let jadwalKerja= this.state.listTanggalJadwalConfirm.filter( v =>{
        if(v.jadwalId != null && v.jadwalId != undefined){
            return this.state.listTanggal.find(c => c.format("YYYY-MM-DD") == v.tanggalKerja)
        }
    })
    let formatJadwal = []
    let wfhJadwal = []
    jadwalKerja.forEach((v, k) => {
        let _reform={
            jadwalId:v.jadwalId, 
            tanggal:v.tanggalKerja,
        }
        formatJadwal.push(_reform)
        
        idArrPegawai.forEach((v1, k1)=>{
            let _wfhJadwal = {
                pegawaiId: v1,
                wfh:v.wfh,
                tanggal:v.tanggalKerja,
                tempatAbsen:this.state.lokasiWFH
            }
            wfhJadwal.push(_wfhJadwal)

        })

    })


    let arrCreateSchedule = {
        idPegawai:idArrPegawai,
        jadwalPegawai:jadwalKerja,
    }

    let sendFormat = {
        idPegawai : idArrPegawai,  
        jadwalPegawai : formatJadwal,
        wfh: wfhJadwal
    }
    console.log(arrCreateSchedule)

    let info="";
    let nVerif=0;
    if(this.state.kunciTahap1 ==  false ){
        info+="Unit Belum dipilih dan klik 'Lanjut'<br/>";
        nVerif=1;
    }
    if(this.state.pegawaiIdList == null || this.state.pegawaiIdList == undefined || this.state.pegawaiIdList.length == 0 ){
        info+="Pegawai Belum Dipilih<br/>";
        nVerif=1;
    }
    if(jadwalKerja == null || jadwalKerja == undefined || jadwalKerja.length == 0 ){
        info+="Jadwal per tanggal belum ditentukan<br/>";
        nVerif=1;
    }

    
    let wfhChek= jadwalKerja.filter(v =>{
        return v.wfh == true;
    })
    if(wfhChek.length > 0 && (this.state.lokasiWFH == null || this.state.lokasiWFH == undefined || this.state.lokasiWFH == "")){
        info+="Lokasi WFH belum diisi<br/>";
        nVerif=1;
    }
    console.log(wfhChek.length);

    if(nVerif===1){
        this.MySwal.fire({
            icon: "error",
            title: "Perhatian!",
            html: info,
            customClass: 'swal-wide',
        });
    }else{
        console.log("send",sendFormat)
        this.props.createJadwalPegawai(sendFormat).then(()=>{
            const {success} = this.props.infoCreateJadwalPegawai;
            const {error} = this.props.infoCreateJadwalPegawai;
            const {loading} = this.props.infoCreateJadwalPegawai;
            if(success){
                this.MySwal.fire({
                    icon: "success",
                    title: "Berhasil!",
                    text: "Jadwal Pegawai telah berhasil dibuat",
                    // customClass: 'swal-wide',
                });
                this.props.reloadTabel();
                this.props.onHide();
            }else{
                this.MySwal.fire({
                    icon: "error",
                    title: "Coba Lagi!",
                    // html: info,
                    // customClass: 'swal-wide',
                });
            }

        })
        console.log("berhasil aja")
    }

}

}


const mapStateToProps = (state) => {
    return{
        listPengUnitInduk: state.listPengUnitInduk,
        listPengUnitKerja: state.listPengUnitKerja,
        listJadwalKerja: state.listJadwalKerja,
        infoCreateJadwalPegawai: state.infoCreateJadwalPegawai,
    } 
  }

export default connect(mapStateToProps, {getListUnitInduk, getListUnitKerja, getListJadwalKerja, createJadwalPegawai})(ModalFormAddJadwalPegawai);