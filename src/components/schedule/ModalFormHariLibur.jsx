import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import TimeKeeper from 'react-timekeeper';
import '../TimeKeeper.css';
// Redux
import {connect} from 'react-redux';
import {createHariLibur} from "../../actions/hariLiburAction";

import DatePicker from 'react-datepicker';

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
const moment = require('moment');

class ModalFormHariLibur extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"Tambah Hari Libur",
            tanggal: "",
            keterangan:"",
           readonly:false        
        };

        this.MySwal = withReactContent(Swal);
    }

    // componentDidMount(){

    // }


render (){
    let dateN = new Date()
    return (
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Body>
                      <Form.Row>
                      <Form.Group as={Col} controlId="keterangan">
                 <Form.Label className="textblack">Nama Hari Libru / Keterangan</Form.Label>
                 <Form.Control type="text" placeholder="Hari Libur...." value={this.state.keterangan} onChange={v => this.setState({keterangan: v.target.value})}/>
                 </Form.Group>

                          <Form.Group as={Col} controlId="keterangan">
                          <Form.Label className="textblack">Tanggal</Form.Label>
                          <br/>
                          <DatePicker   
                                            className="form-control"
                                            selected={this.state.tanggal }
                                            dateFormat="yyyy-MM-dd"
                                            
                                            minDate={dateN.setDate(dateN.getDate() + 1)}

                                            onChange={v => this.setState({tanggal : v })}
                                            placeholderText="Pilih Tanggal Kegiatan"
                                        />
                          </Form.Group>
              
                      </Form.Row>    
                        
                  </Card.Body>
              </Card>

                  </Form>
              </Modal.Body>
          <Modal.Footer>
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleCreateHariLibur}>
                  Simpan
              </Button>
          </Modal.Footer>
        </Modal>
      );
}

handleCreateHariLibur= (e) => {
    e.preventDefault();
    let info="";
    let nVerif=0;
    if(this.state.tanggal === "" || this.state.tanggal === null){
        info+="Tanggal belum terisi <br/>";
        nVerif=1;
    }
    if(this.state.keterangan === "" || this.state.keterangan === null){
        info+="Keterangan belum terisi <br/>";
        nVerif=1;
    }
  
    if(nVerif===1){
        this.MySwal.fire({
            icon: "error",
            title: info,
          });
    }
    else if(nVerif!==1){
        // console.log(this.state.data);
        let dataSend={
            tanggal:this.state.tanggal,
            keterangan:this.state.keterangan,
        }
        this.props.createHariLibur(dataSend).then(()=>{
            const {success, loading} = this.props.infoCreateHariLibur;
            // console.log(success);
            if(success){
                this.MySwal.fire({
                    icon: "success",
                    title: this.state.keterangan+" berhasil ditambahkan",
                  });
                  this.props.onHide();
            }
            if(loading == false && success == false){
                this.MySwal.fire({
                    icon: "error",
                    title: "Coba Lagi!",
                    // html: info,
                    // customClass: 'swal-wide',
                });
            }
                
            });
    
        }
}

}

const mapStateToProps = (state) => {
    return{
        infoCreateHariLibur: state.infoCreateHariLibur,
    } 
 }
export default connect(mapStateToProps, {createHariLibur})(ModalFormHariLibur);