import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import TimeKeeper from 'react-timekeeper';
import '../TimeKeeper.css';
// Redux
import {connect} from 'react-redux';
import { createJadwalKerja, updateJadwalKerja } from "../../actions/jadwalKerjaAction";

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
const moment = require('moment');

class ModalFormJadwalKerja extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"Tambah Jadwal Keja",
           data:{
            id: null,
            nama: "",
            checkIn: "00:00:00",
            checkOut: "00:00:00",
            sebelumMasuk: 0,
            sesudahMasuk: 0,
            sesudahPulang: 0,
            createdBy:null,
            createdDate:null,
            modifiedBy:null,
            modifiedDate:null,
           },
           readonly:false
        
        };

        this.MySwal = withReactContent(Swal);
    }
    async update(dataOne) {    
        await this.setState(({       
            data:dataOne,
            judul:"Ubah Jadwal Kerja",
            readonly:true,
        }));
    }
    componentDidMount(){
        if(this.props.dataedit != null){
            this.update(this.props.dataedit)
            console.log(this.state.data)
            console.log("did")
        }
    }

    clockConversi = (timeState) => {
        let [hour, minute]=timeState.split(":");
        let clock = hour+":"+minute;
        return clock;
    }

    padLeadingZero(timeState){
       let [hour, minute] = timeState.split(":");
       while (hour.length < 2) hour = "0" + hour;
       let clock = hour+":"+minute;
        return clock;
    }

render (){
    let clockIn = this.clockConversi(this.state.data.checkIn);
    let clockOut = this.clockConversi(this.state.data.checkOut);

    console.log(this.state.data);
    let createdDate="";
    let modifiedDate="";
    if(this.state.createdDate !== null && this.state.modifiedDate !== null){
        createdDate = moment(this.state.createdDate).format('DD-MM-YYYY HH:mm');
        modifiedDate = moment(this.state.modifiedDate).format('DD-MM-YYYY HH:mm');
    }
    
    // console.log(this.padLeadingZero(this.state.data.checkIn));
    return (
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Body>
                      <Form.Row>
                          {/* <Form.Group as={Col} controlId="form_id">
                          <Form.Label className="textblack">Kode Jadwal Kerja</Form.Label>
                          <Form.Control type="text" readOnly={this.state.readonly} placeholder="Masukan Kode Unit Induk" value={this.state.data.id}onChange={v => this.setState(prevState => ({data:{...prevState.data, id : v.target.value}}))} />
                          </Form.Group> */}
  
                          <Form.Group as={Col} controlId="form_ktp">
                          <Form.Label className="textblack">Nama Jadwal Kerja</Form.Label>
                          <Form.Control type="text" placeholder="Masukan Nama Induk" value={this.state.data.nama} onChange={v => this.setState(prevState => ({data:{...prevState.data, nama : v.target.value}}))}/>
                          </Form.Group>
               
                      </Form.Row>
                      <Form.Row>
                          <Col>
                          <h5>Waktu Masuk</h5>
                         <TimeKeeper
                         onChange={v => this.setState(prevState => ({data:{...prevState.data, checkIn : this.padLeadingZero(v.formatted24)}}))}
                         time={clockIn}
                         hour24Mode={true}
                         switchToMinuteOnHourSelect
                         />
                         <br/>.
                          </Col>
                          <Col>
                          <h5>Waktu Keluar</h5>
                         <TimeKeeper
                         onChange={v => this.setState(prevState => ({data:{...prevState.data, checkOut : this.padLeadingZero(v.formatted24) }}))}
                         time={clockOut}
                         hour24Mode={true}
                         switchToMinuteOnHourSelect
                         />
                         <br/>.
                          </Col>
                      </Form.Row>
                      <Form.Row>
                        <Form.Group as={Col} controlId="form_sebelumMasuk">
                            <Form.Label className="textblack">Sebelum Masuk</Form.Label>
                            <Form.Control type="number" min='0' placeholder="Masukan Nama Induk" value={this.state.data.sebelumMasuk} onChange={v => this.setState(prevState => ({data:{...prevState.data, sebelumMasuk : v.target.value}}))}/>
                        </Form.Group>
                        <Form.Group as={Col} controlId="form_sesudahMasuk">
                          <Form.Label className="textblack">Sesudah Masuk</Form.Label>
                          <Form.Control type="number" min='0' placeholder="Masukan Nama Induk" value={this.state.data.sesudahMasuk} onChange={v => this.setState(prevState => ({data:{...prevState.data, sesudahMasuk : v.target.value}}))}/>
                        </Form.Group>
                        <Form.Group as={Col} controlId="form_sesudahPulang">
                          <Form.Label className="textblack">Sesudah Pulang</Form.Label>
                          <Form.Control type="number" min='0' placeholder="Masukan Nama Induk" value={this.state.data.sesudahPulang} onChange={v => this.setState(prevState => ({data:{...prevState.data, sesudahPulang : v.target.value}}))}/>
                        </Form.Group>
                            
                      </Form.Row>
                      
                      
                  </Card.Body>
                  <Card.Footer>
                        <small className="text-muted">Dibuat: {this.state.data.createdBy }- {createdDate}</small><br/>
                        <small className="text-muted">Diubah: {this.state.data.modifiedBy }-{modifiedDate}</small>
                  </Card.Footer>
              </Card>

                  </Form>
              </Modal.Body>
          <Modal.Footer>
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleCreateJadwalKerja}>
                  Simpan
              </Button>
          </Modal.Footer>
        </Modal>
      );
}

handleCreateJadwalKerja= (e) => {
    e.preventDefault();
    let info="";
    let nVerif=0;
    if(this.state.data.nama === "" || this.state.data.nama === null){
        info+="Nama belum terisi <br/>";
        nVerif=1;
    }
    if(this.state.data.sebelumMasuk <= 0){ 
        info+="Waktu Sebelum Masuk tak boleh 0 atau di bawah 0 <br/>";
        nVerif=1;
    }
    if(this.state.data.sesudahMasuk <= 0){ 
        info+="Waktu Sesudah Masuk tak boleh 0 atau di bawah 0 <br/>";
        nVerif=1;
    }
    if(this.state.data.sesudahPulang <= 0){ 
        info+="Waktu Sesudah Pulang tak boleh 0 atau di bawah 0 <br/>";
        nVerif=1;
    }
    if(nVerif===1){
        this.MySwal.fire({
            icon: "error",
            title: info,
          });
    }
    else if(this.state.data.id === null && nVerif!==1){
        // console.log(this.state.data);
        this.props.createJadwalKerja(this.state.data).then(()=>{
            const {success} = this.props.infoCreateJadwalKerja;
            // console.log(success);
            if(success){
                this.MySwal.fire({
                    icon: "success",
                    title: this.state.data.nama+" berhasil ditambahkan",
                  });
                }
                this.props.onHide();
            });
    }else if(this.state.data.id !== null && nVerif!==1){
        // console.log(this.state.data);
        this.props.updateJadwalKerja(this.state.data).then(()=>{
            const {success} = this.props.infoUpdateJadwalKerja;
            // console.log(success);
            if(success){
                this.MySwal.fire({
                    icon: "success",
                    title: this.state.data.nama+" berhasil diperbaharui",
                  });
                }
                this.props.onHide();
            });
    }
}

}

const mapStateToProps = (state) => {
    return{
        infoCreateJadwalKerja: state.infoCreateJadwalKerja,
        infoUpdateJadwalKerja: state.infoUpdateJadwalKerja
    } 
 }
export default connect(mapStateToProps, {createJadwalKerja, updateJadwalKerja})(ModalFormJadwalKerja);