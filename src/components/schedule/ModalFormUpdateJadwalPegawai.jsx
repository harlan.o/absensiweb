import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import DatePicker, { Calendar } from "react-multi-date-picker";
import { DatePanel } from "react-multi-date-picker/plugins";
import '../css/multi-date-picker-style.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import {connect} from 'react-redux';
import {getListJadwalKerja} from "../../actions/jadwalKerjaAction";
import {updateJadwalPegawai} from "../../actions/jadwalPegawaiAction";
import {getListLokasiKota} from "../../actions/_lokasiAction";

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import '../css/sweetaler2-style.css';
import { dayName, stringArrayId } from '../../actions/genneralAction';
import SelectSingleSchedule_IdSend from './SelectSingleSchedule_IdSend';


import DateObject from "react-date-object";
import SelectSingleLokasi_NamaSend from './SelectSingleLokasi_NamaSend';

const moment = require('moment');
const dataLokasi=[{id:1,nama:"MANADO"},{id:2,nama:"SIAU"},{id:3,nama:"TAGULANDANG"},{id:4,nama:"BIARO"}]
class ModalFormUpdateJadwalPegawai extends Component {
    constructor(props) {
        super(props);
        this.state = {
           judul:"Perbaharui Jadwal Pegawai",
           readonly:false,
            data:null,
            lokasiWFH:null,
           listTanggal:null,
           listTanggalJadwalConfirm:[],
           listJadwal:null,
           listTanggalRemove:[]
        };
        this.MySwal = withReactContent(Swal);
    }
    // listTanggalJadwalConfirm {jadwalId, tanggalKerja, wfh, lokasi}
    // async update(dataOne) {    
    //     console.log(dataOne)
    //     await this.setState(({       
    //         data:dataOne,
    //         listTanggalJadwalConfirm:
    //     }));
    // }
    componentDidMount(){
        this.props.getListJadwalKerja().then(()=> {
            this.setState(({       
                listJadwal:this.props.listJadwalKerja.data,
            }));
        })
        if(this.props.dataonerow != null){
            // this.update(this.props.dataonerow)
            let d=this.props.dataonerow;
            // console.log(this.props.dataonerow)
            let _listTanggalJadwalConfirm=[];
            let _listTanggal=[];
            let lokasi=""

            // console.log("jadwal value", d.jadwalList)
            for(let i = 0; i < d.jadwalList.length; i++){
                // console.log(d.jadwalList[i].tanggalKerja)
                let _wfh=false;
                if(d.jadwalList[i].wfh){
                    _wfh=d.jadwalList[i].wfh
                }
                let isi_ljs={
                            jadwalId:d.jadwalList[i].jadwalId, 
                            tanggalKerja:d.jadwalList[i].tanggalKerja,
                            wfh:_wfh,
                        }
                let tglKerjaDate = moment(d.jadwalList[i].tanggalKerja).format('YYYY-MM-DD');
                let tglKerjaObj = new DateObject(tglKerjaDate);
                //lokasi wfh
                console.log("lokasi", d.jadwalList[i].tempatAbsen)
                if(d.jadwalList[i].tempatAbsen != null && d.jadwalList[i].tempatAbsen != ""){
                    lokasi = d.jadwalList[i].tempatAbsen
                    // console.log("tempat Absen", lokasi)
                }
                if(d.jadwalList[i].melaksanakanTugas != true && d.jadwalList[i].tugasLuar != true){
                    _listTanggalJadwalConfirm.push(isi_ljs);
                    _listTanggal.push(tglKerjaObj);
                }
                

            }
            // console.log(_listTanggal)
            this.setState({       
                data:this.props.dataonerow,
                listTanggalJadwalConfirm:_listTanggalJadwalConfirm,
                listTanggal:_listTanggal,
                lokasiWFH: lokasi
            });
        }

     }

    // componentDidUpdate(prevProps, prevState){

    // }

    removeArrayCalender(arr, attr, value) {
        var i = arr.length;
        while(i--){
            // console.log(attr)
            // console.log(arr[i][attr])
            if( arr[i] 
                && (arguments.length > 2 && arr[i][attr] === value ) ){ 
               arr.splice(i,1);
           }
        }
        return arr;
    }

    removeArray(arr, attr, value) {
        var i = arr.length;
        while(i--){
            if( arr[i] 
                && arr[i].hasOwnProperty(attr) 
                && (arguments.length > 2 && arr[i][attr] === value ) ){ 
               arr.splice(i,1);
           }
        }
        return arr;
    }

    getObjArray(arr, attr, value) {
        var i = arr.length;
        let obj={};
        while(i--){
           if( arr[i] 
               && arr[i].hasOwnProperty(attr) 
               && (arguments.length > 2 && arr[i][attr] === value ) ){ 
               obj=arr[i]
           }
        }
        // console.log(obj)
        return obj;
    }

    getListSelectedPegawai = (v)=>{

        this.setState( { pegawaiIdList : v });
    }

    renderListTanggal(){
        // console.log("a")
        if( this.state.listTanggal !== undefined && this.state.listTanggal !== null && this.state.listTanggal.length > 0 ){
            let notEdit=false;
            if (this.state.data.statusJadwal == "FIX"){
                notEdit=true;
            }
            // console.log("edittable")
            // console.log(notEdit)
            return this.state.listTanggal.map( (data, key) => {
                let obj=this.getObjArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD"))

             return(
             <Col key={data.format("YYYY-MM-DD")} xs={12} md={12} lg={6} className="mb-2">
                    <Card>
                        <Card.Body>
                            <Card.Text>
                            <Form inline>
                            <div className="my-1 mr-2" as="h5" htmlFor="inlineFormCustomSelectPref">
                            {dayName(data.weekDay.index) }, {data.format("YYYY-MM-DD")}
                            
                            </div>
                            { this.state.listJadwal ?
                            <div style={{width:'55%'}}>
                            <SelectSingleSchedule_IdSend canedit={notEdit} dataoption={this.state.listJadwal} valueOption={obj.jadwalId} valueGetSingle={v => {
                                
                                let _wfh=obj.wfh
                                let id=0;
                                let listTglConf=this.removeArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD"));
                                if(v!= null && v!=undefined){
                                    id=v.id
                                }
                                let jadwalConf={
                                    tanggalKerja: data.format("YYYY-MM-DD"),
                                    jadwalId: id,
                                    wfh:_wfh,
                                }
                                if(id!=0){
                                    listTglConf.push(jadwalConf);
                                }
                                this.setState(prevState => ({listTanggalJadwalConfirm: listTglConf }));  }}/>

                                            </div> : ""}
                            <Button className="my-1" variant="secondary" disabled={notEdit} onClick={(e)=>{
                                e.preventDefault();
                                let listTglConf =this.removeArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD"));
                                
                                let arrDateDel = this.state.listTanggalRemove
                                arrDateDel.push(data.format("YYYY-MM-DD"))
                                // console.log("arrDateDel", arrDateDel)
                                let listTgl =this.removeArrayCalender([...this.state.listTanggal], 'dayOfBeginning', data.dayOfBeginning);
                                // console.log(listTglConf);

                                this.setState(prevState => ({listTanggalJadwalConfirm: listTglConf, listTanggal: listTgl, listTanggalRemove: arrDateDel }));
                                }}>
                                <FontAwesomeIcon icon={faTimes} />
                            </Button>
                        </Form>
                        <Form inline>
                        <div className="my-1 mr-2" as="h5" htmlFor="inlineFormCustomSelectPref">
                            <Form.Group controlId={data.format("YYYY-MM-DD")}>
                                <Form.Check type="checkbox" label="Kerja Dari Lokasi Rumah" checked={obj.wfh} onChange={
                                    v=>{
                                        // console.log(v.target.checked)
                                        let _wfh=v.target.checked
                              
                                        let id=obj.jadwalId;
                                        let listTglConf=this.removeArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD"));
                                        
                                        let jadwalConf={
                                            tanggalKerja: data.format("YYYY-MM-DD"),
                                            jadwalId: id,
                                            wfh:_wfh,
                                          
                                        }
                                        listTglConf.push(jadwalConf);
                                        
                                        this.setState(prevState => ({listTanggalJadwalConfirm: listTglConf }));
                                    }
                                }/>
                            </Form.Group>
                            </div>
                            
                            {/* { (this.state.listJadwal && obj.wfh === true) ?
                                <>
                            <div style={{width:'100%'}}>
                           
                            <SelectSingleLokasi_NamaSend canedit={notEdit} dataoption={this.state.listJadwal} valueOption={this.getObjArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD")).jadwalId} valueGetSingle={v => {
                                let id=0;
                                let listTglConf=this.removeArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD"));
                                if(v!= null && v!=undefined){
                                    id=v.id
                                }
                                let jadwalConf={
                                    tanggalKerja: data.format("YYYY-MM-DD"),
                                    jadwalId: id
                                }
                                if(id!=0){
                                    listTglConf.push(jadwalConf);
                                }
                                this.setState(prevState => ({listTanggalJadwalConfirm: listTglConf }), () => console.log(this.state.listTanggalJadwalConfirm));  }}/>

                                            </div> </>: ""} */}
                        </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

             )
            });
        }

    }
render (){
        console.log("lokasi render",this.state.lokasiWFH)
    return (
      <>
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >

                <Row>
                    <Col xs={12} md={6} lg={4}>
                        <Card className="mb-2">
                            <Card.Body>
                                <Form.Row >
                                    <Form.Group as={Col} controlId="form_nama" >
                                    <Form.Label className="textblack">Nama Pegawai</Form.Label>
                                    <Form.Control value={this.state.data?.namaPegawai} disabled readonly />
                                    </Form.Group>
                                </Form.Row>
                                
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_nama">
                                    <Form.Label className="textblack">NIP</Form.Label>
                                    <Form.Control value={this.state.data?.nip} disabled readonly />
                                    </Form.Group>
                                </Form.Row>                             
                            </Card.Body>
                        </Card>

                        <Card>
                            <Card.Body>
                                <div className="customDatePickerWidth">
                                            <Calendar className="customDatePickerWidth red" value={this.state.listTanggal} format="YYYY-MM-DD" onChange={v=> {this.setState({listTanggal: v}); }}multiple  /> 
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                    

                        <Col xs={12} md={6} lg={8}>
                        <Row>
                            <Col  xs={12} md={12} lg={12}>
                                <Card>
                                    <Card.Body>
                                       <h5>List Tanggal</h5>
                                       <Row>
                                            <Col xs={12} md={12} lg={12} className="mb-2">
                                                <div style={{width:'100%'}}>
                                                <Form.Label className="textblack">Lokasi Kerja Jika WFH</Form.Label>
                                                        <SelectSingleLokasi_NamaSend dataoption={dataLokasi} valueOption={this.state.lokasiWFH} valueGetSingle={v => { let c=""; if(v!=null){c=v.nama} this.setState(({ lokasiWFH: c }));  }}/>
                                                    </div>
                                            </Col>
                                            {this.renderListTanggal()}
                                        </Row> 
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Col>

                </Row>             
                  </Form>

              </Modal.Body>
          <Modal.Footer>
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleCreateJadwalPegawai}>
                  Simpan
              </Button>
          </Modal.Footer>
        </>
      );
}

handleCreateJadwalPegawai =  (e) =>  {
    e.preventDefault();
    let idArrPegawai=this.state.data?.pegawaiId;
    let jadwalKerja= this.state.listTanggalJadwalConfirm.filter( v =>{
        return this.state.listTanggal.find(c => c.format("YYYY-MM-DD") == v.tanggalKerja)
    })

    
    let tanggalRemoveObj= this.state.listTanggalJadwalConfirm.filter( v =>{
        if (!this.state.listTanggal.find(c => c.format("YYYY-MM-DD") == v.tanggalKerja)){
            return true
        }
    })

    let tanggalRemove = []
    tanggalRemoveObj.forEach((v, k) => {
        let tanggal=v.tanggalKerja
        tanggalRemove.push(tanggal)
    })

    // console.log("tanggal Remove",tanggalRemove)
    // console.log("tanggal Kerja",jadwalKerja)
    
    let dataUpdateSchedule = {
        idPegawai:idArrPegawai,
        jadwalPegawai:jadwalKerja,

    }

    let info="";
    let nVerif=0;
    
    if(jadwalKerja == null || jadwalKerja == undefined || jadwalKerja.length == 0 ){
        info+="Jadwal per tanggal belum ditentukan<br/>";
        nVerif=1;
    }

    if(nVerif===1){
        this.MySwal.fire({
            icon: "error",
            title: "Perhatian!",
            html: info,
            customClass: 'swal-wide',
        });
    }else{
        // console.log(dataUpdateSchedule)
        // console.log(this.state.data)
        let formatJadwal = []
        let wfhJadwal = []
        dataUpdateSchedule.jadwalPegawai.forEach((v, k) => {
            let _reform={
                jadwalId:v.jadwalId, 
                tanggal:v.tanggalKerja,
            }
            formatJadwal.push(_reform)

            let _wfhJadwal = {
                pegawaiId: this.state.data?.pegawaiId,
                wfh:v.wfh,
                tanggal:v.tanggalKerja,
                tempatAbsen:this.state.lokasiWFH
            }
            wfhJadwal.push(_wfhJadwal)
        })

            // console.log("new format",formatJadwal)
            tanggalRemove.concat(this.state.listTanggalRemove)
        let sendFormat = {
            idPegawai : dataUpdateSchedule.idPegawai,  
            jadwalPegawai : formatJadwal,
            tanggalHapus : tanggalRemove,
            wfh: wfhJadwal
        }
// console.log("send", sendFormat)
        this.props.updateJadwalPegawai(sendFormat).then(()=>{
            const {success} = this.props.infoUpdateJadwalPegawai;
            const {error} = this.props.infoUpdateJadwalPegawai;
            const {loading} = this.props.infoUpdateJadwalPegawai;
            if(success){
                this.MySwal.fire({
                    icon: "success",
                    title: "Berhasil!",
                    text: "Jadwal Pegawai "+this.state.data.namaPegawai+" telah berhasil diubah",
                    // customClass: 'swal-wide',
                });
                this.props.reloadTabel();
                this.props.onHide();
            }else{
                this.MySwal.fire({
                    icon: "error",
                    title: "Coba Lagi!",
                    // html: info,
                    // customClass: 'swal-wide',
                });
            }

        })

    }

}

}


const mapStateToProps = (state) => {
    return{
        listJadwalKerja: state.listJadwalKerja,
        infoUpdateJadwalPegawai: state.infoUpdateJadwalPegawai,
        listLokasiKota: state.listLokasiKota,
    } 
  }

export default connect(mapStateToProps, {getListJadwalKerja, updateJadwalPegawai, getListLokasiKota})(ModalFormUpdateJadwalPegawai);