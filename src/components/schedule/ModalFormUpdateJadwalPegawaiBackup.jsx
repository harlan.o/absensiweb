import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import DatePicker, { Calendar } from "react-multi-date-picker";
import { DatePanel } from "react-multi-date-picker/plugins";
import '../css/multi-date-picker-style.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimes } from '@fortawesome/free-solid-svg-icons';

import {connect} from 'react-redux';
import {getListJadwalKerja} from "../../actions/jadwalKerjaAction";
import {updateJadwalPegawai} from "../../actions/jadwalPegawaiAction";

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import '../css/sweetaler2-style.css';
import { stringArrayId } from '../../actions/genneralAction';
import SelectSingleSchedule_IdSend from './SelectSingleSchedule_IdSend';


import DateObject from "react-date-object";

const moment = require('moment');
class ModalFormUpdateJadwalPegawai extends Component {
    constructor(props) {
        super(props);
        this.state = {
           judul:"Jadwal Pegawai",
           readonly:false,
            data:null,
           listTanggal:null,
           listTanggalJadwalConfirm:[],
           listJadwal:null,

        };
        this.MySwal = withReactContent(Swal);
    }
    // async update(dataOne) {    
    //     console.log(dataOne)
    //     await this.setState(({       
    //         data:dataOne,
    //         listTanggalJadwalConfirm:
    //     }));
    // }
    componentDidMount(){
        this.props.getListJadwalKerja().then(()=> {
            this.setState(({       
                listJadwal:this.props.listJadwalKerja.data,
            }));
        })
        if(this.props.dataonerow != null){
            // this.update(this.props.dataonerow)
            let d=this.props.dataonerow;
            console.log(this.props.dataonerow)
            let _listTanggalJadwalConfirm=[];
            let _listTanggal=[];
            

            console.log(d.jadwalList)
            for(let i = 0; i < d.jadwalList.length; i++){
                console.log(d.jadwalList[i].tanggalKerja)
                
                let isi_ljs={jadwalId:d.jadwalList[i].jadwalId, tanggalKerja:d.jadwalList[i].tanggalKerja}
                _listTanggalJadwalConfirm.push(isi_ljs);
                

                let tglKerjaDate = moment(d.jadwalList[i].tanggalKerja).format('YYYY-MM-DD');
                let tglKerjaObj = new DateObject(tglKerjaDate);
                _listTanggal.push(tglKerjaObj);
            }
            console.log(_listTanggal)
            this.setState({       
                data:this.props.dataonerow,
                listTanggalJadwalConfirm:_listTanggalJadwalConfirm,
                listTanggal:_listTanggal
            });
        }

     }

    // componentDidUpdate(prevProps, prevState){

    // }

    removeArrayCalender(arr, attr, value) {
        var i = arr.length;
        while(i--){
            console.log(attr)
            console.log(arr[i][attr])
            if( arr[i] 
                && (arguments.length > 2 && arr[i][attr] === value ) ){ 
               arr.splice(i,1);
           }
        }
        return arr;
    }

    removeArray(arr, attr, value) {
        var i = arr.length;
        while(i--){
            if( arr[i] 
                && arr[i].hasOwnProperty(attr) 
                && (arguments.length > 2 && arr[i][attr] === value ) ){ 
               arr.splice(i,1);
           }
        }
        return arr;
    }

    getObjArray(arr, attr, value) {
        var i = arr.length;
        let obj={};
        while(i--){
           if( arr[i] 
               && arr[i].hasOwnProperty(attr) 
               && (arguments.length > 2 && arr[i][attr] === value ) ){ 
               obj=arr[i]
           }
        }
        console.log(obj)
        return obj;
    }

    getListSelectedPegawai = (v)=>{

        this.setState( { pegawaiIdList : v });
    }

    renderListTanggal(){
        console.log("a")
        if( this.state.listTanggal !== undefined && this.state.listTanggal !== null && this.state.listTanggal.length > 0 ){
            
            return this.state.listTanggal.map( (data, key) => {

                // console.log(data.format("YYYY-MM-DD"))
                // console.log(data.dayOfBeginning)
                // {console.log(moment(data).format("YYYY-MM-DD"))}
             
             return(
             <Col key={data.format("YYYY-MM-DD")} xs={12} md={12} lg={6} className="mb-2">
                    <Card>
                        <Card.Body>
                            <Card.Text>
                            <Form inline>
                            <div className="my-1 mr-2" as="h5" htmlFor="inlineFormCustomSelectPref">
                                {data.format("YYYY-MM-DD")}
                            </div>
                            { this.state.listJadwal ?
                            <div style={{width:'60%'}}>
        {console.log(this.getObjArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD")).jadwalId)}
        <SelectSingleSchedule_IdSend dataoption={this.state.listJadwal} valueOption={this.getObjArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD")).jadwalId} valueGetSingle={v => {
            let id=0;
            let listTglConf=this.removeArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD"));
            if(v!= null && v!=undefined){
                id=v.id
            }
            let jadwalConf={
                tanggalKerja: data.format("YYYY-MM-DD"),
                jadwalId: id
            }
            if(id!=0){
                listTglConf.push(jadwalConf);
            }
            this.setState(prevState => ({listTanggalJadwalConfirm: listTglConf }), () => console.log(this.state.listTanggalJadwalConfirm));  }}/>

                        </div> : ""}
                        <Button className="my-1" variant="secondary" onClick={(e)=>{
                            e.preventDefault();
                            let listTglConf =this.removeArray([...this.state.listTanggalJadwalConfirm], 'tanggalKerja', data.format("YYYY-MM-DD"));

                            let listTgl =this.removeArrayCalender([...this.state.listTanggal], 'dayOfBeginning', data.dayOfBeginning);
                            console.log(listTglConf);
                            this.setState(prevState => ({listTanggalJadwalConfirm: listTglConf, listTanggal: listTgl }), () => console.log(this.state.listTanggalJadwalConfirm));
                            }}>
                            <FontAwesomeIcon icon={faTimes} />
                        </Button>
                        </Form>
                            </Card.Text>
                        </Card.Body>
                    </Card>
                </Col>

             )
            });
        }

    }
render (){
    
    return (
      <>
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >

                <Row>
                    <Col xs={12} md={6} lg={4}>
                        <Card className="mb-2">
                            <Card.Body>
                                <Form.Row >
                                    <Form.Group as={Col} controlId="form_nama" >
                                    <Form.Label className="textblack">Nama Pegawai</Form.Label>
                                    <Form.Control value={this.state.data?.namaPegawai} disabled readonly />
                                    </Form.Group>
                                </Form.Row>
                                
                                <Form.Row>
                                    <Form.Group as={Col} controlId="form_nama">
                                    <Form.Label className="textblack">NIP</Form.Label>
                                    <Form.Control value={this.state.data?.nip} disabled readonly />
                                    </Form.Group>
                                </Form.Row>                             
                            </Card.Body>
                        </Card>

                        <Card>
                            <Card.Body>
                                <div className="customDatePickerWidth">
                                            <Calendar className="customDatePickerWidth red" value={this.state.listTanggal} format="YYYY-MM-DD" onChange={v=> {this.setState({listTanggal: v}); console.log(moment(v[0]).format("YYYY-MM-DD")) }}multiple  /> 
                                </div>
                            </Card.Body>
                        </Card>
                    </Col>
                    

                        <Col xs={12} md={6} lg={8}>
                        <Row>
                            <Col  xs={12} md={12} lg={12}>
                                <Card>
                                    <Card.Body>
                                       <h5>List Tanggal</h5>
                                       <Row>
                                            {this.renderListTanggal()}
                                        </Row> 
                                    </Card.Body>
                                </Card>
                            </Col>
                        </Row>
                    </Col>

                </Row>             
                  </Form>

              </Modal.Body>
          <Modal.Footer>
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleCreateJadwalPegawai}>
                  Simpan
              </Button>
          </Modal.Footer>
        </>
      );
}

handleCreateJadwalPegawai =  (e) =>  {
    e.preventDefault();
    let idArrPegawai=this.state.data?.pegawaiId;
    let jadwalKerja= this.state.listTanggalJadwalConfirm.filter( v =>{
        return this.state.listTanggal.find(c => c.format("YYYY-MM-DD") == v.tanggalKerja)
    })

    let dataUpdateSchedule = {
        idPegawai:idArrPegawai,
        jadwalPegawai:jadwalKerja,

    }

    let info="";
    let nVerif=0;
    
    if(jadwalKerja == null || jadwalKerja == undefined || jadwalKerja.length == 0 ){
        info+="Jadwal per tanggal belum ditentukan<br/>";
        nVerif=1;
    }

    if(nVerif===1){
        this.MySwal.fire({
            icon: "error",
            title: "Perhatian!",
            html: info,
            customClass: 'swal-wide',
        });
    }else{
        console.log(dataUpdateSchedule)
        this.props.updateJadwalPegawai(dataUpdateSchedule).then(()=>{
            const {success} = this.props.infoUpdateJadwalPegawai;
            const {error} = this.props.infoUpdateJadwalPegawai;
            const {loading} = this.props.infoUpdateJadwalPegawai;
            if(success){
                this.MySwal.fire({
                    icon: "success",
                    title: "Berhasil!",
                    text: "Jadwal Pegawai "+this.state.data.namaPegawai+" telah berhasil diubah",
                    // customClass: 'swal-wide',
                });
                this.props.reloadTabel();
                this.props.onHide();
            }else{
                this.MySwal.fire({
                    icon: "error",
                    title: "Coba Lagi!",
                    // html: info,
                    // customClass: 'swal-wide',
                });
            }

        })
    }

}

}


const mapStateToProps = (state) => {
    return{
        listJadwalKerja: state.listJadwalKerja,
        infoUpdateJadwalPegawai: state.infoUpdateJadwalPegawai,
    } 
  }

export default connect(mapStateToProps, {getListJadwalKerja, updateJadwalPegawai})(ModalFormUpdateJadwalPegawai);