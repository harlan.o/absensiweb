import React, { Component } from "react";
import { Table, Card, Col, Form, Row, Button, Spinner } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrash } from '@fortawesome/free-solid-svg-icons';
import { cekHakAksesClient } from "../../actions/hakAksesActions";
import { REGISTER_FU056 } from "../../constants/hakAksesConstants";
// import "./TabelLaporanAbsensi.css";
const moment = require('moment');

class TabelHariLibur extends Component {
    constructor(props) {
        super(props); //since we are extending class Table so we have to use super in order to overrnipe Component class constructor
        this.state = {
          totalData:0,
            search:'',
            rowData:null,
            loading:true
        };
        this.renderTableData=this.renderTableData.bind(this);        
    }
      
    async callApi(){
        if(this.props.dataRow !== null){
            await this.setState(() => ({
            rowData:this.props.dataRow,  
            loading:false    
            })) 
        }
    }

    componentDidMount(){
        if(this.state.loading !== false){
          this.callApi();
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.dataRow !== prevProps.dataRow){
            this.callApi();
            console.log(this.state.rowData)
        }
    }

  renderTableData(){
    if(this.state.loading !== true && this.state.rowData !== undefined && this.state.rowData !== null && this.state.rowData.length !== undefined){
        console.log(this.state.rowData.length)

       let _datashow = this.state.rowData.sort((a,b) => { return new Date(a.tanggal) - new Date(b.tanggal)})
       let datashow = _datashow.filter( rowData =>{
          if(rowData.keterangan.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1 || rowData.keterangan.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1){
              return true
            }
            if(rowData.tanggal.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1 || rowData.tanggal.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1){
              return true
            }
            return false
        });
        console.log(datashow.nama);
      return datashow.map( (data, key) => {
        // let dateAbsen = moment(data.jamAbsen);
        let no= key+1;
        this.count=no;
        
        let hapus = false
        if(moment(data.tanggal) > moment()){
          hapus = true
        }
        return(
          <tr key={key}>
              <td className="no">{no}</td>
              <td className="colomInti">{data.tanggal} </td>
              <td>{data.keterangan}</td>
              <td>
                {cekHakAksesClient(REGISTER_FU056) ?
                ( 
                  <>
                  {hapus ? 
                <Button variant="secondary"  onClick={() => this.handleShowFormModalDelete(data)} size="sm"><FontAwesomeIcon icon={faTrash} /> </Button>
                :<Button variant="dark" size="sm" disabled><FontAwesomeIcon icon={faTrash} /> </Button>}
                  </>
                )
                :""}{' '}
              
              </td>
          </tr>
        )
      }     
      );
    } else{
      return(
          <tr>
            <td colSpan={4} className="loadingPage2">
              <Spinner animation="border" className="spin"  variant="dark" />
            </td>
          </tr>
        )
    }
  }


  render() {
    console.log(this.state.rowData);
    return (

    <Card className="mb-4" bg="dark" text="white">  
        <Card.Header >
        <Row>
        <Col xs={6} md={4}>
            Hari Libur
            </Col>
            <Col xs={6} md={{ span: 4, offset: 4 }}>
              <Form.Row className="align-items-center">
                <Form.Label  column xs="auto">
                  Cari
                </Form.Label>
                <Col>
                  <Form.Control type="text" placeholder="Cari.... " onChange={v => this.setState(({search: v.target.value }))}/>
                </Col>
              </Form.Row>
            </Col> 
            </Row>
          
          </Card.Header>
        <Card.Body>
            <Table responsive className="table table-striped table-hover custom-table3 text-nowrap">
                <tbody>
                <tr>
                  <th>No.</th>
                  <th className="colomInti">Tanggal</th>
                  <th>Keterangan</th>
                  <th>---</th>
                </tr>
             
                    {this.renderTableData()}
                </tbody>
            </Table>
                
        </Card.Body>
    </Card>
    );
  }

  // handleShowFormModalEdit(dataOneRow){
  //   this.props.dataOneRow(dataOneRow, "edit"); 
  // }
  handleShowFormModalDelete(dataOneRow){
    this.props.dataOneRow(dataOneRow, "delete"); 
  }
}

export default TabelHariLibur;