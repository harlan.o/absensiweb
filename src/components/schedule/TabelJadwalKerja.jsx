import React, { Component } from "react";
import { Table, Card, Col, Form, Row, Button } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilAlt, faTrash } from '@fortawesome/free-solid-svg-icons';
import { cekHakAksesClient } from "../../actions/hakAksesActions";
import { REGISTER_FU052, REGISTER_FU051 } from "../../constants/hakAksesConstants";
// import "./TabelLaporanAbsensi.css";


class TabelJadwalKerja extends Component {
    constructor(props) {
        super(props); //since we are extending class Table so we have to use super in order to overrnipe Component class constructor
        this.state = {
          totalData:0,
            search:'',
            rowData:null,
            loading:true
        };
        this.renderTableData=this.renderTableData.bind(this);        
    }
      
    async callApi(){
        if(this.props.dataRow !== null){
            await this.setState(() => ({
            rowData:this.props.dataRow,  
            loading:false    
            })) 
        }
    }

    componentDidMount(){
        if(this.state.loading !== false){
          this.callApi();
        }
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.dataRow !== prevProps.dataRow){
            this.callApi();
            console.log(this.state.rowData)
        }
    }

  renderTableData(){
    if(this.state.loading !== true && this.state.rowData !== undefined && this.state.rowData !== null && this.state.rowData.length !== undefined){
        console.log(this.state.rowData.length)
      let datashow = this.state.rowData.filter( rowData =>{
          if(rowData.nama.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1 || rowData.nama.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1){
              return true
            }
            return false
        });
        console.log(datashow.nama);
      return datashow.map( (data, key) => {
        // let dateAbsen = moment(data.jamAbsen);
        let no= key+1;
        this.count=no;
        return(
          <tr key={key}>
              <td className="no">{no}</td>
              <td className="colomInti">{data.nama} </td>
              <td>{data.checkIn}</td>
              <td>{data.checkOut}</td>
              <td>{data.sebelumMasuk}</td>
              <td>{data.sesudahMasuk}</td>
              <td>{data.sesudahPulang}</td>
              <td>
                {cekHakAksesClient(REGISTER_FU052) ?
              <Button variant="secondary" onClick={() => this.handleShowFormModalDelete(data)} size="sm"><FontAwesomeIcon icon={faTrash} /> </Button>
                :""}{' '}
                {cekHakAksesClient(REGISTER_FU051) ?
                <Button variant="secondary" onClick={() => this.handleShowFormModalEdit(data)} size="sm"><FontAwesomeIcon icon={faPencilAlt} /> </Button>
                :""}
              
              </td>
          </tr>
        )
      }     
      );
    }
  }


  render() {
    console.log(this.state.rowData);
    return (

    <Card className="mb-4" bg="dark" text="white">  
        <Card.Header >
        <Row>
        <Col xs={6} md={4}>
            Jadwal Kerja
            </Col>
            <Col xs={6} md={{ span: 4, offset: 4 }}>
              <Form.Row className="align-items-center">
                <Form.Label  column xs="auto">
                  Cari
                </Form.Label>
                <Col>
                  <Form.Control type="text" placeholder="Cari Nama Jadwal" onChange={v => this.setState(({search: v.target.value }))}/>
                </Col>
              </Form.Row>
            </Col> 
            </Row>
          
          </Card.Header>
        <Card.Body>
            <Table responsive className="table table-striped table-hover custom-table3 text-nowrap">
                <tbody>
                <tr>
                  <th>No.</th>
                  <th className="colomInti">Nama</th>
                  <th>Jam Masuk</th>
                  <th>Jam Pulang</th>

                  <th>Sebelum Masuk</th>
                  <th>Sesudah Masuk</th>
                  <th>Sesudah Pulang</th>
                  <th>---</th>
                </tr>
             
                    {this.renderTableData()}
                </tbody>
            </Table>
                
        </Card.Body>
    </Card>
    );
  }

  handleShowFormModalEdit(dataOneRow){
    this.props.dataOneRow(dataOneRow, "edit"); 
  }
  handleShowFormModalDelete(dataOneRow){
    this.props.dataOneRow(dataOneRow, "delete"); 
  }
}

export default TabelJadwalKerja;