import React, { Component, Fragment } from "react";
import { Button, Table, Card, Col, Form, Row, Spinner } from "react-bootstrap";

import "./TabelDefault.css";
const moment = require('moment');

class ListDataStatistikBar extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      totalData:0,
        search:'',
        rowData:null,
        loading:true
    };
    this.renderTableData=this.renderTableData.bind(this);    
  }
  
  async callApi(){
    if(this.props.dataRow !== null && this.props.dataRow !== undefined){
      let dataCombin=this.props.dataRow;
      await this.setState(() => ({
       rowData:dataCombin,  
       loading:false    
      })) 
    }
  }

  componentDidMount(){
    if(this.state.loading !== false){
      this.callApi();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.getFilter !== this.props.getFilter || prevProps.dataRow !== this.props.dataRow){
      this.callApi();
    }
  }
    
  renderTableData(){
    if(this.state.loading !== true && this.state.rowData !== undefined && this.state.rowData !== null){
      let datashow = this.state.rowData.filter( rowData =>{
        if(rowData.nama.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1 || rowData.nama.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1){
          return true
        }
        return false
      });

      return datashow.map( (data, key) => {
        
        let no= key+1;
        this.count=no;
        return(
          <tr key={key}>
              <td>{data.kode}</td>
              <td>{data.nama}</td>
              <td>{data.totalPegawai}</td>
              <td>{data.hadir}</td>
              <td>{data.terlambat}</td>
              <td>{data.ijin}</td>
              {/* {data.unitInduk}<br/> <small>{data.unitKerja}</small> </td>
              <td>
              </td> */}
          </tr>
        )
      }     
      );
    }else{
      return(
        <tr>
          <td colSpan="4" className="loadingPage3">
            <Spinner animation="border" className="spin"  variant="dark" />
          </td>
        </tr>
      )
    }
  }





  render() {
    // console.log("render table");
    return (

    <Card className="mb-4" border="light" text="dark">  
        
        <Card.Body>
        <Row className="mb-2">
        <Col xs={6} md={4}>
          
            </Col>
            <Col xs={6} md={{ span: 4, offset: 4 }}>
              <Form.Row className="align-items-center">
                <Form.Label  column xs="auto">
                  Cari
                </Form.Label>
                <Col>
                  <Form.Control type="text" placeholder="Cari " onChange={v => this.setState(({search: v.target.value }))}/>
                </Col>
              </Form.Row>
            </Col> 
            </Row>
            <Table responsive className="table table-striped table-hover custom-table4 text-nowrap">
            <tbody>
                <tr>
                  <th>Label.</th>
                  <th className="colomInti">Unit</th>
                  <th>Pegawai</th>
                  <th>Hadir</th>
                  <th>Terlambat</th>
                  <th>Ijin</th>
                  {/* <th>Waktu</th> */}
                </tr>
                    {this.renderTableData()}
                </tbody>
            </Table>
            
        </Card.Body>
    </Card>
    );
  }
}




export default ListDataStatistikBar;