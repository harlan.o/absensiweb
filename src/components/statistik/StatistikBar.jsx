
import React, { Component } from 'react';
import './custom-tooltip.css';
import {
    BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, Brush, ResponsiveContainer
  } from 'recharts';

  
class StatisticBar extends Component {
    constructor(props) {
        super(props);
        this.state = {          
            data:[],
        };
    }

    componentDidMount(){
        this.setState({
            data:this.props.data
        })
    }

    render() {
    return (
    <ResponsiveContainer width='100%' height={500}>
      <BarChart
        data={this.state.data}
        margin={{
          top: 5, right: 30, left: 20, bottom: 5,
        }}>
        <CartesianGrid  strokeDasharray="1 1"/>
        <XAxis dataKey="kode" />
        <YAxis />
        <Tooltip content={this.CustomTooltip}  />
        <Legend />

        <Bar dataKey="hadir" barSize={20} fill="#ec8892" />
        <Bar dataKey="terlambat" barSize={20} fill="#f10219" />
        <Bar dataKey="ijin"  barSize={20} fill="#BD2130"/>
        <Bar dataKey="tanpaKeterangan" barSize={20} fill="#6c757d" />

{/*         
        <Bar dataKey="terlambat"  barSize={20} fill="#BD2130"/>
        <Bar dataKey="hadir" barSize={20} fill="#6c757d" />
        <Bar dataKey="ijin" barSize={20} fill="#f10219" />
        <Bar dataKey="tanpaKeterangan" barSize={20} fill="#ec8892" /> */}
        <Brush dataKey='name' height={30} stroke="#8884d8"/>
      </BarChart>
    </ResponsiveContainer>
    );
  }

// getIntroOfPage = (label) => {
//     let onedata = this.state.data.filter(obj => {
//         return obj.kode === label
//       })

      

//     if (label === 1) {
//       return "Page A is about men's clothing";
//     } if (label === 'Page B') {
//       return "Page B is about women's dress";
//     } if (label === 'Page C') {
//       return "Page C is about women's bag";
//     } if (label === 'Page D') {
//       return 'Page D is about household goods';
//     } if (label === 'Page E') {
//       return 'Page E is about food';
//     } if (label === 'Page F') {
//       return 'Page F is about baby food';
//     }
    


//   };
  
CustomTooltip = ({ active, payload, label }) => {
    if (active) {
        let pegawai=payload[0].payload.totalPegawai;
        let hadir=payload[0].payload.hadir;
        let terlambat=payload[0].payload.terlambat;
        let ijin=payload[0].payload.ijin;
        let tanpaKeterangan=payload[0].payload.tanpaKeterangan;
      return (
        <div className="custom-tooltip">
          <p className="label">{`${label} : ${payload[0].payload.nama}`}</p>
          <p className="desc">
          {pegawai} Pegawai <br/>
          {hadir*100/pegawai}% | {hadir} Hadir <br/>
          {terlambat*100/pegawai}% | {terlambat}  Terlambat <br/>
          {ijin*100/pegawai}% | {ijin} Ijin <br/>
          {tanpaKeterangan*100/pegawai}% | {tanpaKeterangan} Tanpa Keterangan <br/>
              </p>
          {/* <p className="intro">{this.getIntroOfPage(label)}</p> */}
          {/* <p className="desc">Anything you want can be displayed here.</p> */}
        </div>
      );
    }
    return null;
  };
    
}
export default StatisticBar;