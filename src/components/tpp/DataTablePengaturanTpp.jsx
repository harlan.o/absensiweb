import '../css/dataTables.bootstrap4.min.css';
import '../DataTable.css';
import React, {Component} from 'react';
import { Table, Card, Button } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faPencilAlt } from '@fortawesome/free-solid-svg-icons';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU073 } from '../../constants/hakAksesConstants';
const $ =require('jquery')
$.DataTable = require('datatables.net-bs4')


export class DataTablePengaturanTpp extends Component {
    constructor(props){
        super(props);
        this.state={
            header: null,
            dataHTML: null,
            dataRow:null,
        };
        this.dataRowImport = this.dataRowImport.bind(this);
        this.handleShowFormModalEdit = this.handleShowFormModalEdit.bind(this);
    }
    componentDidMount(){
        // console.log(this.props.dataRow);
        this.$el = $(this.el)
        this.setState(state => ({      
            dataRow:this.props.dataRow
        }));
        // this.$el.style="asd"
        this.$el.DataTable(
            {
                language: {
                    emptyTable:     "Data tidak ditemukan",
                    info:           "Tampil _START_ dari _END_ pada _TOTAL_ Data",
                    infoEmpty:      "Tampil 0 data",
                    lengthMenu:     "Tampil _MENU_ Data",
                    search:         "Cari:",
                    paginate: {
                        first:    '«',
                        previous: '‹',
                        next:     '›',
                        last:     '»'
                    },
                    aria: {
                        paginate: {
                            first:    'First',
                            previous: 'Previous',
                            next:     'Next',
                            last:     'Last'
                        }
                    }
                },
            }
        )
        // console.log(this.el);
    }
    dataRowImport(dataRowI){
        return (
            <tbody>
                {dataRowI.map((data, key) => {
                    return (
                        <tr key={key}>
                            <td> {key+1} </td>
                            <td> {data.nip} </td>
                            <td> {data.namaPegawai} </td>
                            <td> {data.nominalTpp ? data.nominalTpp.toLocaleString(): ""} </td>
                            <td> 
                                {cekHakAksesClient(REGISTER_FU073) ?
                                <Button variant="secondary" onClick={() => this.handleShowFormModalEdit(data)} size="sm"><FontAwesomeIcon icon={faPencilAlt} /> </Button>
                                :""}
                                 </td>
                        </tr>
                    );
                    })}
            </tbody>
          );
    }
    handleShowFormModalEdit(dataOneRow){
        this.props.dataOneRow(dataOneRow); 
    }
 
    render(){
        return (
            <Card className="customCard">
                 <Table ref={el => this.el = el} className="table table-striped table-hover custom-table text-nowrap  table-responsive">
                     <thead>
                         <tr> 
                             <th> No.</th>
                             <th> NIP </th>
                             <th className="custom-fit"> Nama Pegawai</th>
                             <th> Nominal</th>
                             <th> ---- </th>
                         </tr>
                     </thead>
                     {this.dataRowImport(this.props.dataRow)}
                 </Table>
            </Card>
        );
    }
}


export default DataTablePengaturanTpp;