import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import { Alert, Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';

// Redux
import {connect} from 'react-redux';
import { createTpp, updateTpp } from "../../actions/tppActions";

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

class ModalPengaturanTpp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"Tambah Tpp",
           data:{ 
            pegawaiId:null,
            namaPegawai: "",
            nip: "",
            nominalTpp: 0,
        },
        edit:false,        
        };
        this.MySwal = withReactContent(Swal);
    }
    async update(dataOne) {    
        await this.setState(({       
            data:dataOne,
            judul:"Ubah Tpp Pegawai",
            edit:true,
        }));
    }
    componentDidMount(){
        if(this.props.dataedit != null){
            this.update(this.props.dataedit)
            // console.log(this.state.data)
            // console.log("did")
        }
    }


    render (){
    // console.log(this.props.dataOptionUnitKerja);
        return (
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Body>
                      <Form.Row>
                          <Form.Group as={Col} controlId="form_nama">
                          <Form.Label className="textblack">Nama Pegawai</Form.Label>
                          <Form.Control type="text"  value={this.state.data.namaPegawai} disabled/>
                          </Form.Group>
                      </Form.Row>
                      <Form.Row>
                          <Form.Group as={Col} controlId="form_nip">
                          <Form.Label className="textblack">NIP</Form.Label>
                          <Form.Control type="text"  value={this.state.data.nip} disabled/>
                          </Form.Group>
                      </Form.Row>
                      <Form.Row>
                          <Form.Group as={Col} controlId="form_nominalTpp">
                          <Form.Label className="textblack">Nominal</Form.Label>
                          <Form.Control type="number" min={0} placeholder="Masukan Nominal"  value={this.state.data.nominalTpp} onChange={v => this.setState(prevState =>({data:{...prevState.data, nominalTpp : v.target.value}}))} />
                          </Form.Group>
                      </Form.Row>
                      {/* <Form.Group>
                          <Form.Label className="textblack">Tenaga Medis</Form.Label>
                          <div>
                              <Form.Check
                                  inline
                                  checked={this.state.data.jenisKelamin == "L"}  
                                  name="form_jenis_kelamin"
                                  label="Laki-Laki"
                                  type="radio"
                                  onChange={v => this.setState(prevState =>({data:{...prevState.data, jenisKelamin : "L"}}))}
                              />
                              <Form.Check
                                  inline
                                  checked={this.state.data.jenisKelamin == "P"} 
                                  name="form_jenis_kelamin"
                                  label="Perempuan"
                                  type="radio"
                                  onChange={v => this.setState(prevState =>({data:{...prevState.data, jenisKelamin : "P"}}))}
                              />
                          </div>
                      </Form.Group> */}
                  </Card.Body>
              </Card>
                  </Form>
                  <Alert variant='dark'>
                    Symbol *) menandakan Harus diisi
                   </Alert>
              </Modal.Body>
          <Modal.Footer>
              
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleCreateTpp} >
                  Simpan
              </Button>
          </Modal.Footer>
        </Modal>
      );
    }


    handleCreateTpp =  (e) =>  {
        e.preventDefault();
        let info="";
        let nVerif=0;
        if(this.state.data.nominalTpp === "" || this.state.data.nominalTpp === null){
            info+="Nama belum terisi <br/>";
            nVerif=1;
        }
        if(nVerif===1){
            this.MySwal.fire({
                icon: "error",
                title: info,
              });
        }
        // else if(this.state.data.id === null && nVerif!==1){
        //     // console.log(this.state.data);
        //     this.props.createTpp(this.state.data).then(()=>{
        //         const {success} = this.props.infoCreateTpp;
        //         // console.log(success);
        //         if(success){
        //             this.MySwal.fire({
        //                 icon: "success",
        //                 title: this.state.data.nominalTpp+" berhasil ditambahkan",
        //               });
        //             }
        //             this.props.onHide();
        //         });
        // }
        else if(this.state.data.pegawaiId !== null && nVerif!==1){
            // console.log(this.state.data);
            this.props.updateTpp(this.state.data).then(()=>{
                const {success} = this.props.infoUpdateTpp;
                // console.log(success);
                if(success == true){
                    this.MySwal.fire({
                        icon: "success",
                        title: this.state.data.namaPegawai+" berhasi diubah nominal TPP",
                      });
                      this.props.onHide();
                }else if(success == false){
                    this.MySwal.fire({
                        icon: "error",
                        title: "Coba Lagi",
                      });
                }
                });
        }
    }
        
}

const mapStateToProps = (state) => {
    return{
        infoCreateTpp: state.infoCreateTpp,
        infoUpdateTpp: state.infoUpdateTpp
    } 
 }
export default connect(mapStateToProps, {createTpp, updateTpp})(ModalPengaturanTpp);