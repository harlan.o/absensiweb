import React, { Component } from 'react';
import DatePicker from 'react-datepicker';
import { Alert, Button, Card, Col, Form, Modal} from 'react-bootstrap';

// Redux
import {connect} from 'react-redux';
import { revisiTppHistory } from "../../actions/tppActions";

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

class ModalRevisiTpp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"Harap Tunggu.....",
            potonganDummy:100,
           data:{ 
            id:null,
            nip: "",
            namaPegawai: "",
            nominalTpp: "",
            potongan: "",
            tahunBulan: "",
            
        },
        edit:false,
        };
        this.MySwal = withReactContent(Swal);
    }
    async update(dataOne) {    
        await this.setState(({       
            data:dataOne,
            judul:"Revisi TPP Pegawai",
            edit:true,
            potonganDummy:dataOne.potongan
        }));
    }
    componentDidMount(){
        if(this.props.data != null){
            this.update(this.props.data)
        }
    }

    tppFinal = (nominalTpp, potongan) => {
        let nilaiPotongan = nominalTpp*potongan/100;
        return nominalTpp-nilaiPotongan;
      }

    render (){
        return (
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Body>
                      <Form.Row>
                          <Form.Group as={Col} controlId="form_nip">
                          <Form.Label className="textblack">NIP</Form.Label>
                          <Form.Control type="text" readOnly value={this.state.data.nip}/>
                          </Form.Group>
  
                          <Form.Group as={Col} controlId="form_nama">
                          <Form.Label className="textblack">Nama Pegawai</Form.Label>
                          <Form.Control type="text" readOnly value={this.state.data.namaPegawai} />
                          </Form.Group>
                      </Form.Row>
                      <Form.Row>
                      <Form.Group as={Col} controlId="form_nominalTpp">
                          <Form.Label className="textblack">Nominal TPP</Form.Label>
                          <Form.Control type="text" readOnly  value={this.state.data.nominalTpp.toLocaleString()} />
                          </Form.Group>
                          <Form.Group as={Col} controlId="form_potongan">
                          <Form.Label className="textblack">Potongan (%)</Form.Label>
                          <Form.Control type="number"  placeholder={"Maximal "+this.state.potonganDummy+" %"} min={0} max={100}  value={this.state.data.potongan} onChange={v => this.setState(prevState =>({data:{...prevState.data, potongan : v.target.value}}))} />
                          </Form.Group>
                          <Form.Group as={Col} controlId="form_tppTerima">
                          <Form.Label className="textblack">TPP Terima</Form.Label>
                          <Form.Control type="text" readOnly placeholder="Masukan Nama Pegawai"  value={this.tppFinal(this.state.data.nominalTpp, this.state.data.potongan).toLocaleString()}  />
                          </Form.Group>
                      </Form.Row>
                      
                  </Card.Body>
              </Card>
    

                  </Form>
              </Modal.Body>
          <Modal.Footer>
              
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleRevisiTpp} >
                  Simpan
              </Button>
          </Modal.Footer>
        </Modal>
      );
    }


    handleRevisiTpp =  (e) =>  {
        e.preventDefault();
        let info="";
        let nVerif=0;
        if(this.state.data.potongan <= -1 || this.state.data.potongan >= 101){
            info+="Pastikan potongan antara 0% dan 100% <br/>";
            nVerif=1;
        }
        if(this.state.data.potongan > this.state.potonganDummy ){
            info+="Jumlah potongan melebihi potongan awal ("+this.state.potonganDummy+"%) <br/>";
            nVerif=1;
        }
        if(nVerif===1){
            this.MySwal.fire({
                icon: "error",
                title: info,
              });
        }else if(this.state.data.id !== null && nVerif!==1){
            // console.log(this.state.data);
            this.props.revisiTppHistory(this.state.data.id, this.state.data.potongan, this.props.filterData).then(()=>{
                const {success} = this.props.infoRevisiHistoryTpp;
                // console.log(success);
                if(success){
                    this.MySwal.fire({
                        icon: "success",
                        title: this.state.data.namaPegawai+" berhasil diperbaharui",
                      });
                    }
                    this.props.onHide();
                }
                );
        }
    }
        
}

const mapStateToProps = (state) => {
    return{
        infoRevisiHistoryTpp: state.infoRevisiHistoryTpp,
    } 
 }
export default connect(mapStateToProps, {revisiTppHistory})(ModalRevisiTpp);