import React, { Component } from 'react';
import {connect} from 'react-redux';
import { getListUnitInduk } from "../../actions/unitIndukAction";
import AsyncSelect from 'react-select/async';

export class SelectTpp extends Component {
  constructor(props) {
    super(props);
    this.state = {
        optionData:this.props.dataoption,
        inputValue: '',
        valueOption:null,
    };
    // this.valueSend = this.valueSend.bind(this);

  }
  // componentDidMount(){
  //   console.log(this.props.listPengUnitInduk?.data)
  // }
  componentDidUpdate(prevProps, prevState) {
    if(this.props.listPengUnitInduk !== prevProps.listPengUnitInduk){
        
        if(this.props.listPengUnitInduk?.data !== undefined){
            // console.log(this.props.listPengUnitInduk?.data);
            // console.log(this.props.valueOption);
            this.setState((state) => ({
                dataRow: this.props.listPengUnitInduk?.data,
                valueOption: this.props.valueOption,
                renderTable:true,
                loadingPage:false
            }));
        } 
    }
    if(this.props.valueOption !== prevProps.valueOption){
      if(this.props.valueOption !== undefined){
        // console.log(this.props.valueOption);
        this.setState((state) => ({
            valueOption: this.props.valueOption,
        }));
    } 
    }


  }

   filterColors = (inputValue) => {
    //  console.log("HAIW");
     let bab= this.state.optionData.filter(i =>
      this.optionText(i).toLowerCase().includes(inputValue.toLowerCase())
      );
    return bab
  };
  valueShow = (id) => {
    //  console.log("HAIW");
     let sel= this.state.optionData.filter(i => i.id===id);
    return sel
  };
  
   promiseOptions = inputValue =>
    new Promise(resolve => {
      setTimeout(() => {
        resolve(this.filterColors(inputValue));
      }, 1000);
    });
  
    valueGetSend = (v)=>
    {

      this.props.valueGet({id:v.id, nama:this.optionText(v) });
    }

    optionText = (option) =>{
      let a=option.daerahKerja;
      if(option.eselon !== null){
        a+=" Eselon: "+option.eselon;
      }
      if(option.golongan !== null){
        a+=" Gol: "+option.golongan;
      }
      if(option.nominal !== null){
        a+=" Rp: "+option.nominal.toLocaleString();
      }
      if(option.keterangan !== null){
        a+=" (Ket: "+option.keterangan+")";
      }
      if(option.tenagaMedis){
        a+=" Tenaga Medis";
      }else{
        a+=" Non Tenaga Medis";
      }
      // "Rp."+option.nominal+ {option.eselon ? (" Eln: "+option.eselon) : ""} " Ket "+option.keterangan+"("+option.daerahKerja+" "+option.daerahKerja+")"

      return a;
    }
   
  render() {
    // console.log(this.state.valueOption)
    // console.log("HAI")
    return (
      <AsyncSelect cacheOptions onChange={this.valueGetSend} 
      value={this.valueShow(this.state.valueOption?.id) }
      // {}
        getOptionValue={option => option.id} 
      getOptionLabel={option => this.optionText(option)}
      defaultOptions
      loadOptions={this.promiseOptions} />
    );
  }
}


export default SelectTpp;

