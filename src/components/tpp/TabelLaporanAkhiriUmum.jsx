import React, { Component, Fragment } from "react";
import { Table, Card, Col, Form, Row, Spinner } from "react-bootstrap";
import "./TabelLaporanAkhir.css";
import {connect} from 'react-redux';
import {getListAbsenInduk, getListAbsenKerja} from "../../actions/absensiAction";
const moment = require('moment');


class TabelLaporanAkhirUmum extends Component {
  constructor(props) {
    super(props); 
    this.state = {
      totalData:0,
        search:'',
        kodeUnitInduk: this.props.getFilter?.kodeUnitInduk,
        kodeUnitKerja: this.props.getFilter?.kodeUnitKerja,
        startDate: this.props.getFilter?.startDate,
        endDate: this.props.getFilter?.endDate,
        rowData:null,
        loading:true
    };
    this.renderTableData=this.renderTableData.bind(this);    
  }
  
  async callApi(){
    if(this.props.dataRow !== null && this.props.dataRow !== undefined){
      let dataCombin=this.props.dataRow;
      await this.setState(() => ({
       rowData:dataCombin,  
       loading:false    
      })) 
    }
  }

  componentDidMount(){
    if(this.state.loading !== false){
      this.callApi();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.getFilter !== this.props.getFilter || prevProps.dataRow !== this.props.dataRow){
      this.callApi();
    }
  }
    
  renderTableData(){
    if(this.state.loading !== true && this.state.rowData !== undefined && this.state.rowData !== null){
      let datashow = this.state.rowData.filter( rowData =>{
        if(rowData.namaPegawai.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1 || rowData.namaPegawai.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1){
          return true
        }
        return false
      });

      return datashow.map( (data, key) => {
        
        let no= key+1;
        this.count=no;
        return(
          <tr key={key}>
              <td>{no}</td>
              <td>{data.namaPegawai}<br/><small> {data.nip}</small><br/><small> {data.jabatan}</small></td>
              <td>{data.totalHariKerja}</td>
              <td>{data.totalHadir}</td>
              <td>{data.totalIzin}</td>
              <td>{data.totalSakit}</td>
              <td>{data.totalTanpaKeterangan}</td>
              <td>{data.totalCuti}</td>
              <td>{data.totalTugasLuar}</td>
              <td>{data.nilaiTpp?.toLocaleString()}</td>
          </tr>
        )
      }     
      );
    }else{
      return(
        <tr>
          <td colSpan="4" className="loadingPage2">
            <Spinner animation="border" className="spin"  variant="light" />
          </td>
        </tr>
      )
    }
  }

  render() {
    // console.log("render table");
    return (

    <Card className="mb-4" bg="dark" text="white">  
        <Card.Header>
        <Row>
        <Col xs={6} md={4}>
              Laporan Akhir Umum
            </Col>
            <Col xs={6} md={{ span: 4, offset: 4 }}>
              <Form.Row className="align-items-center">
                <Form.Label  column xs="auto">
                  Cari
                </Form.Label>
                <Col>
                  <Form.Control type="text" placeholder="Cari Nama Pegawai" onChange={v => this.setState(({search: v.target.value }))}/>
                </Col>
              </Form.Row>
            </Col> 
            </Row>
        </Card.Header>
        <Card.Body>
            <Table responsive className="table table-striped table-hover custom-table3 text-nowrap">
            <tbody>
                <tr>
                  <th>No.</th>
                  <th className="colomInti">Pegawai</th>
                  <th>Jumlah Hari Kerja</th>
                  <th>Hadir</th>
                  <th>Izin</th>
                  <th>Sakit</th>
                  <th>Tanpa Keterangan</th>
                  <th>Cuti</th>
                  <th>Tugas Luar</th>
                  <th>Nilai Tpp</th>
                </tr>
                    {this.renderTableData()}
                </tbody>
            </Table>
            
        </Card.Body>
    </Card>
    );
  }
}




const mapStateToProps = (state) => {
  return{
    listAbsensiInduk: state.listAbsensiInduk
  } 
}
export default connect(mapStateToProps, {getListAbsenInduk, getListAbsenKerja})(TabelLaporanAkhirUmum);