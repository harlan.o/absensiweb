import { faPencilAlt, faInfo, faInfoCircle } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import React, { Component } from "react";
import { Table, Card, Col, Form, Row, Accordion, Button, Spinner } from "react-bootstrap";
// import "./TabelLaporanAbsensi.css";
import {connect} from 'react-redux';
import { monthName } from "../../actions/genneralAction";
import { cekHakAksesClient } from "../../actions/hakAksesActions";
import { REGISTER_FU029 } from "../../constants/hakAksesConstants";
const moment = require('moment');


class TabelLaporanHistoryTpp extends Component {
  constructor(props) {
    super(props); //since we are extending class Table so we have to use super in order to overrnipe Component class constructor
    this.state = {
      totalData:0,
        search:'',
        rowData:null,
        loading:true
    };
    this.renderTableData=this.renderTableData.bind(this);
    this.handleShowFormRevisi = this.handleShowFormRevisi.bind(this);
  }
  
  async callApi(){
    if(this.props.dataRow !== null){
      await this.setState(() => ({
       rowData:this.props.dataRow,  
       loading:false    
      })) 
      // console.log("Tabel Hadir")
    }else{
      await this.setState(() => ({
        rowData:null,  
        loading:true    
       }))
    }
  }

  componentDidMount(){
    if(this.state.loading !== false){
      this.callApi();
    }
  }

  componentDidUpdate(prevProps, prevState) {
    if(prevProps.getFilter !== this.props.getFilter || prevProps.dataRow !== this.props.dataRow){
      // console.log("call");
      this.callApi();
    }
  }

  tppFinal = (nominalTpp, potongan) => {
    let nilaiPotongan = nominalTpp*potongan/100;
    return nominalTpp-nilaiPotongan;
  }
    
  renderTableData(){
    if(this.state.loading !== true && this.state.rowData !== undefined && this.state.rowData !== null){
      let datashow = this.state.rowData.sort(function (a, b) {
        return a.namaPegawai.localeCompare(b.namaPegawai); //using String.prototype.localCompare()
      }).filter( rowData =>{
        // console.log(rowData.nama);
        if(rowData.namaPegawai.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1 || rowData.namaPegawai.toLowerCase().indexOf( this.state.search.toLowerCase() ) !== -1){
          return true
        }
        return false
      });


      return datashow.map( (data, key) => {
        // let dateAbsen = moment(data.jamAbsen);
        let no= key+1;
        this.count=no;

        return(
          <>
          <tr key={key}>
              <td className="no">{no}</td>
              <td className="colomInti">{data.namaPegawai} <br/> {data.nip}<br/>
              
              </td>
              <td>{data.nominalTpp?.toLocaleString()}</td>
              <td>{data.potongan} %</td>
              <td>{this.tppFinal(data.nominalTpp, data.potongan).toLocaleString()}</td>
              <td><Accordion.Toggle as={Button} variant="secondary" eventKey={data.nip} size="sm">{' '}<FontAwesomeIcon icon={faInfoCircle} /></Accordion.Toggle>{' '}
              {' '}
              {cekHakAksesClient(REGISTER_FU029) ?
              <Button variant="secondary" size="sm"  onClick={() => this.handleShowFormRevisi(data)} ><FontAwesomeIcon icon={faPencilAlt} /></Button>
              :""}
              </td>
          </tr>
      <tr  className="colomInti"><td colSpan="6">
          <Accordion.Collapse eventKey={data.nip}>
                      <ul>
                {data.ketPotongan ? this.renderKeterangan(data.ketPotongan):"-"}
                </ul>
        </Accordion.Collapse> 
          </td>
        </tr>
          </>
        )
      }     
      );
    }else{
      return(
        <tr>
          <td colSpan="6" className="loadingPage2">
        <Spinner animation="border" className="spin"  variant="light" />
          </td>
        </tr>
      )
    }
  }
  renderKeterangan(keterangan){
    return keterangan.map((data, key) => <li key={key}>{data}</li>);
  }

  handleShowFormRevisi(dataOneRow){
    this.props.dataOneRow(dataOneRow); 
  } 
  render() {
    // console.log(this.state.rowData);
    let [year, month] = this.props.getFilter.startDate.split("-")
    let bulan = monthName(month);
    // {this.state.loadingPage ? (
    //   <div className="loadingPage">
    //   </div>
    //   ) : ("")}
    return (
        <Card className="mb-4" bg="dark" text="white">  
        
        <Card.Header >
        <Row>
        <Col xs={6} md={4}>
            Laporan TPP Bulan {bulan} {year}
            </Col>
            <Col xs={6} md={{ span: 4, offset: 4 }}>
              <Form.Row className="align-items-center">
                <Form.Label  column xs="auto">
                  Cari
                </Form.Label>
                <Col>
                  <Form.Control type="text" placeholder="Cari Nama Pegawai" onChange={v => this.setState(({search: v.target.value }))}/>
                </Col>
              </Form.Row>
            </Col> 
            </Row>
          
          </Card.Header>
        <Card.Body>
          <Accordion>
            <Table responsive className="table table-striped table-hover custom-table3 text-nowrap">
                <tbody>
                <tr>
                  <th>No.</th>
                  <th className="colomInti">Nama/NIP</th>
                  <th>Nominal Tpp</th>
                  <th>Potongan</th>
                  <th>TPP Terima</th>
                  <th>---</th>
                </tr>
                    {this.renderTableData()}
                </tbody>
            </Table>
            </Accordion>
        </Card.Body>
    </Card>
    );
  }
}



export default TabelLaporanHistoryTpp;