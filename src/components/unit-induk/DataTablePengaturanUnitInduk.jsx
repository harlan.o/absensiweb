import '../css/dataTables.bootstrap4.min.css';
import '../DataTable.css';
import React, {Component} from 'react';
import { Table, Card, Button } from "react-bootstrap";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEye, faPencilAlt } from '@fortawesome/free-solid-svg-icons';
// import {moment} from 'moment';
// import {Moment} from 'react-moment';
import {connect} from 'react-redux';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU066 } from '../../constants/hakAksesConstants';
// import { getListUnitInduk } from "../actions/unitIndukAction";

const $ =require('jquery')
$.DataTable = require('datatables.net-bs4')
const moment = require('moment');

export class DataTablePengaturanUnitInduk extends Component {
    constructor(props){
        super(props);
        this.state={
            dataRowTable: this.props.listPengUnitInduk?.data,
            header: null,
            dataHTML: null,
        };
        this.handleShowFormModalEdit = this.handleShowFormModalEdit.bind(this);
    }
    componentDidMount(){
        // console.log(this.el);
        // this.props.getListUnitInduk();
        this.$el = $(this.el)
        this.$el.DataTable(
            {
                language: {
                    emptyTable:     "Data tidak ditemukan",
                    info:           "Tampil _START_ dari _END_ pada _TOTAL_ Data",
                    infoEmpty:      "Tampil 0 data",
                    lengthMenu:     "Tampil _MENU_ Data",
                    search:         "Cari:",
                    paginate: {
                        first:    '«',
                        previous: '‹',
                        next:     '›',
                        last:     '»'
                    },
                    aria: {
                        paginate: {
                            first:    'First',
                            previous: 'Previous',
                            next:     'Next',
                            last:     'Last'
                        }
                    }
                },
            }
        )
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.listPengUnitInduk !== prevProps.listPengUnitInduk){
            // if(this.props.listPengUnitInduk !== undefined) 
            // console.log(this.props.listPengUnitInduk?.data);
        }
    }
    dataRowImport(dataRowI){
        // console.log(dataRowI)
        if(dataRowI === undefined){
            return(
            <tbody>
                <tr>
                    <td> a</td>
                    <td> a</td>
                    <td>a- </td>
                    <td> -a</td>
                    <td>a-</td>
                    <td> <Button variant="secondary" size="sm"><FontAwesomeIcon icon={faEye} /> </Button>{' '}<Button variant="secondary" size="sm"><FontAwesomeIcon icon={faPencilAlt} /> </Button> </td>
                </tr>
                
            </tbody> );
        }
        return (
            <tbody>
                {dataRowI.map((data, key) => {
                    let dateCreate = moment(data.createdDate);
                    let dateModif = moment(data.modifiedDate);
                    return (
                        <tr key={key}>
                            <td> {key+1} </td>
                            <td> {data.id} </td>
                            <td> {data.nama} </td>
                            <td> {data.createdBy} : {dateCreate.format('YYYY-mm-D, h:mm:ss a')} </td>
                            <td> {data.modifiedBy} : {dateModif.format('YYYY-mm-D, h:mm:ss a')} </td>
                            <td> 
                                {cekHakAksesClient(REGISTER_FU066) ?
                                <Button variant="secondary" onClick={() => this.handleShowFormModalEdit(data)} size="sm"><FontAwesomeIcon icon={faPencilAlt} /> </Button> 
                                :""}
                                </td>
                        </tr>
                    );
                    })}
            </tbody>
          );
    }
    handleShowFormModalEdit(dataOneRow){
        this.props.dataOneRow(dataOneRow); 
    }
    render(){
        return (
            <Card className="customCard">
                 <Table ref={el => this.el = el} className="table table-striped table-hover custom-table text-nowrap  table-responsive">
                     <thead>
                         <tr> 
                             <th> No </th>
                             <th> Kode </th>
                             <th className="custom-fit" > Nama </th>
                             <th> Dibuat </th>
                             <th> Diubah </th>
                             <th> ---- </th>
                         </tr>
                     </thead>
                        {this.dataRowImport(this.state.dataRowTable)}
                 </Table>
                 <Table>
                 </Table>
            </Card>
        );
    }
}
const mapStateToProps = (state) => {
    return{
        listPengUnitInduk: state.listPengUnitInduk
    } 
 }
export default connect(mapStateToProps, null)(DataTablePengaturanUnitInduk);