import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';

// Redux
import {connect} from 'react-redux';
import {updateUnitInduk, createUnitInduk} from "../../actions/unitIndukAction";

import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

class ModalPengaturanUnitInduk extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"Tambah Unit Induk",
           data:{
            id: null,
            nama: "",
           },
           readonly:false
        
        };
        this.MySwal = withReactContent(Swal);
    }
    async update(dataOne) {    
        await this.setState(({       
            data:dataOne,
            judul:"Ubah Data Unit Induk",
            readonly:true,
        }));
    }
    componentDidMount(){
        if(this.props.dataedit != null){
            this.update(this.props.dataedit)
            console.log(this.state.data)
            console.log("did")
        }
    }

render (){
    
    return (
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Body>
                      <Form.Row>
                      {this.state.readonly ?
                          <Form.Group as={Col} controlId="form_id">
                          <Form.Label className="textblack">Kode Unit Induk</Form.Label>
                          <Form.Control type="text" readOnly={this.state.readonly} placeholder="Masukan Kode Unit Induk" value={this.state.data.id}onChange={v => this.setState(prevState => ({data:{...prevState.data, id : v.target.value}}))} />
                          </Form.Group>
                    :""}
                          <Form.Group as={Col} controlId="form_ktp">
                          <Form.Label className="textblack">Nama Induk</Form.Label>
                          <Form.Control type="text" placeholder="Masukan Nama Induk" value={this.state.data.nama} onChange={v => this.setState(prevState => ({data:{...prevState.data, nama : v.target.value}}))}/>
                          </Form.Group>
                      </Form.Row>
                      
                  </Card.Body>
              </Card>

                  </Form>
              </Modal.Body>
          <Modal.Footer>
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleCreateUnitInduk}>
                  Simpan
              </Button>
          </Modal.Footer>
        </Modal>
      );
}



handleCreateUnitInduk =  (e) =>  {
    e.preventDefault();
    let info="";
    let nVerif=0;
  
    
    if(this.state.data.nama === "" || this.state.data.nama === null){
        info+="Nama belum terisi <br/>";
        nVerif=1;
    }
    
    if(nVerif===1){
        this.MySwal.fire({
            icon: "error",
            title: info,
          });
    }else if(this.state.data.id === null && nVerif!==1){
        console.log(this.state.data);
        this.props.createUnitInduk(this.state.data).then(()=>{
            const {success} = this.props.infoCreateUnitInduk;
            // console.log(success);
            if(success){
                this.MySwal.fire({
                    icon: "success",
                    title: this.state.data.nama+" berhasil ditambahkan",
                  });
                }
                this.props.actionLoading();
                this.props.onHide();
            });
    }else if(this.state.data.id !== null && nVerif!==1){
        console.log(this.state.data);
        this.props.updateUnitInduk(this.state.data).then(()=>{
            const {success} = this.props.infoUpdateUnitInduk;
            if(success){
                this.MySwal.fire({
                    icon: "success",
                    title: this.state.data.nama+" berhasil diperbaharui",
                  });
                }
                this.props.actionLoading();
                this.props.onHide();
            });
    }
}


}

const mapStateToProps = (state) => {
    return{
        infoCreateUnitInduk: state.infoCreateUnitInduk,
        infoUpdateUnitInduk: state.infoUpdateUnitInduk,
    } 
 }
 export default connect(mapStateToProps, {createUnitInduk, updateUnitInduk})(ModalPengaturanUnitInduk);