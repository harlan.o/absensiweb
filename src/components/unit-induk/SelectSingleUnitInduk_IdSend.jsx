import React, { Component } from 'react';
import AsyncSelect from 'react-select/async';

export class SelectSingleUnitInduk_IdSend extends Component {
  constructor(props) {
    super(props);
    this.state = {
        optionUnitInduk:this.props.dataoption,
        inputValue: '',
        valueOption:null,
        isDisabled:false,
    };
    
  }

  async update(dataList, dataValue) {    
    // console.log(dataList)
    // console.log(dataValue)
    await this.setState(({       
      optionUnitInduk: dataList,
      valueOption: dataValue,
    }));
  }
  componentDidMount(){
    this.update( this.props.dataoption, this.props.valueOption)
  }
  componentDidUpdate(prevProps, prevState) {
    if(this.props.dataoption !== prevProps.dataoption){
        // default= 
        if(this.props.placeholder == null){
          this.props.dataoption.push({id:0, nama:"Semua Unit Induk"})
        }
        // console.log(this.props.dataoption)
        if(this.props.dataoption !== undefined){
            this.setState((state) => ({
              optionUnitInduk: this.props.dataoption,
                valueOption: this.props.valueOption,
                renderTable:true,
                loadingPage:false
            }));
        } 
    }
    if(this.props.valueOption !== prevProps.valueOption){
      if(this.props.valueOption !== undefined){
        this.setState((state) => ({
            valueOption: this.props.valueOption,
        }));
      } 
    }
    console.log("isdisable 1", this.props.isDisabled)
    if(this.props.isDisabled !== prevProps.isDisabled){
      console.log("isdisable", this.props.isDisabled)
      if(this.props.isDisabled !== undefined){
        this.setState((state) => ({
          isDisabled: this.props.isDisabled,
        }));
      } 
    }
  }

   filterColors = (inputValue) => {
     let bab;
       bab= this.state.optionUnitInduk?.filter(i =>
       i.nama?.toLowerCase().includes(inputValue.toLowerCase())
       );
     return bab
  };
  
   promiseOptions = inputValue =>
    new Promise(resolve => {
      setTimeout(() => {
        resolve(this.filterColors(inputValue));
      }, 1000);
    });
  
    valueGetSend = (v)=>
    {
      this.props.valueGetSingle(v);
    }

  
  render() {
    // console.log("Here");
    // console.log(this.state.optionUnitInduk);
    // console.log(this.state.valueOption);
    // console.log("isdisable end", this.state.isDisabled)

    return (
      
      <AsyncSelect cacheOptions onChange={this.valueGetSend} 
      value={this.state.optionUnitInduk.filter(option => {
        return option.id === this.state.valueOption;
      })}
      getOptionValue={option => option.id} 
      getOptionLabel={option => option.nama}
      defaultOptions={this.state.optionUnitInduk}
      isDisabled={this.props?.isDisabled}
      isClearable
      loadOptions={this.promiseOptions} 
      placeholder={this.props.placeholder ? this.props.placeholder :"Semua Unit Induk"}
    //   isMulti
    //   className="basic-multi-select"
    //   classNamePrefix="select"
      />
    );
  }
}


export default SelectSingleUnitInduk_IdSend;

