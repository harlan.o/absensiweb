import React, { Component } from 'react';
import AsyncSelect from 'react-select/async';

export class SelectSingleUnitInduk_NameSend extends Component {
  constructor(props) {
    super(props);
    this.state = {
        optionUnitInduk:this.props.dataoption,
        inputValue: '',
        valueOption:[],
        isDisabled:false,
    };
    
  }

  async update(dataList, dataValue) {    
    // console.log(dataList)
    await this.setState(({       
      dataRow: dataList,
      valueOption: dataValue,
    }));
  }
  componentDidMount(){
    this.update( this.props.dataoption, this.props.valueOption)
  }
  componentDidUpdate(prevProps, prevState) {
    if(this.props.dataoption !== prevProps.dataoption){
        // default= 
        this.props.dataoption.push({id:0, nama:"Semua Unit Induk"})
        // console.log(this.props.dataoption)
        if(this.props.dataoption !== undefined){
            this.setState((state) => ({
                dataRow: this.props.dataoption,
                valueOption: this.props.valueOption,
                renderTable:true,
                loadingPage:false
            }));
        } 
    }
    if(this.props.valueOption !== prevProps.valueOption){
      if(this.props.valueOption !== undefined){
        this.setState((state) => ({
            valueOption: this.props.valueOption,
        }));
      } 
    }
    if(this.props.isDisabled !== prevProps.isDisabled){
      if(this.props.valueOption !== undefined){
        this.setState((state) => ({
          isDisabled: this.props.isDisabled,
        }));
      } 
    }
  }

   filterColors = (inputValue) => {
     let bab;
       bab= this.state.optionUnitInduk?.filter(i =>
       i.nama?.toLowerCase().includes(inputValue.toLowerCase())
       );
     return bab
  };
  
   promiseOptions = inputValue =>
    new Promise(resolve => {
      setTimeout(() => {
        resolve(this.filterColors(inputValue));
      }, 1000);
    });
  
    valueGetSend = (v)=>
    {
      this.props.valueGetSingle(v);
    }

  
  render() {
    // console.log("Here2");
    // console.log(this.state.optionUnitInduk);
    // console.log(this.state.valueOption);
    return (
      <AsyncSelect cacheOptions onChange={this.valueGetSend} 
      value={this.state.optionUnitInduk.filter(option => {
        return option.nama === this.state.valueOption;
      })}
      getOptionValue={option => option.id} 
      getOptionLabel={option => option.nama}
      defaultOptions
      isDisabled={this.state.isDisabled}
      isClearable
      loadOptions={this.promiseOptions} 
      placeholder='Semua Unit'
    //   isMulti
    //   className="basic-multi-select"
    //   classNamePrefix="select"
      />
    );
  }
}


export default SelectSingleUnitInduk_NameSend;

