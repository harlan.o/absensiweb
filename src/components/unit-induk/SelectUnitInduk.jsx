import React, { Component } from 'react';
import {connect} from 'react-redux';
import { getListUnitInduk } from "../../actions/unitIndukAction";
import AsyncSelect from 'react-select/async';

export class SelectUnitInduk extends Component {
  constructor(props) {
    super(props);
    this.state = {
        optionUnitInduk:this.props.dataoption,
        inputValue: '',
        valueOption:null,
    };

  }

  async update(dataList, dataValue) {    
    await this.setState(({       
      optionUnitInduk: dataList,
      valueOption: dataValue,
    }));
  }
  componentDidMount(){
    this.update( this.props.dataoption, this.props.valueOption)
  }
  componentDidUpdate(prevProps, prevState) {
    if(this.props.dataoption !== prevProps.dataoption){
        
        if(this.props.dataoption !== undefined){
            // console.log(this.props.listPengUnitInduk?.data);
            // console.log(this.props.valueOption);
            this.setState((state) => ({
              optionUnitInduk: this.props.dataoption,
                valueOption: this.props.valueOption,
                renderTable:true,
                loadingPage:false
            }));
        } 
    }
    if(this.props.valueOption !== prevProps.valueOption){
      if(this.props.valueOption !== undefined){
        // console.log(this.props.valueOption);
        this.setState((state) => ({
            valueOption: this.props.valueOption,
        }));
      } 
    }
  }

   filterColors = (inputValue) => {
     let bab;
    //  console.log(this.state.optionUnitInduk);
    //  if(inputValue){
       bab= this.state.optionUnitInduk.filter(i =>
       i.nama?.toLowerCase().includes(inputValue.toLowerCase())
       );
     return bab
    // }
  };
  
   promiseOptions = inputValue =>
    new Promise(resolve => {
      setTimeout(() => {
        resolve(this.filterColors(inputValue));
      }, 1000);
    });
  
    valueGetSend = (v)=>
    {

      this.props.valueGet({id:v.id, nama:v.nama });
    }

   
  render() {

    return (
      <AsyncSelect cacheOptions onChange={this.valueGetSend} 
      value={this.state.valueOption}
      // {}
      getOptionValue={option => option.id} 
      getOptionLabel={option => option.nama}
      defaultOptions
      loadOptions={this.promiseOptions} />
    );
  }
}


export default SelectUnitInduk;

