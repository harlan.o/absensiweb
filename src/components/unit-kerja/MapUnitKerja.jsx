import React, { Component } from 'react';
import GoogleMapReact from 'google-map-react';
import MarkerMapUnitKerja from './MarkerMapUnitKerja';
import Marker from '../map/Marker';
 
// const AnyReactComponent = ({ text }) => <div>{text}</div>;
// const icon = {
        
//   path: "M-20,0a20,20 0 1,0 40,0a20,20 0 1,0 -40,0",
//   fillColor: '#FF0000',
//   fillOpacity: .6,
//   strokeWeight: 0,
//   scale: 0.5,
//   // anchor: new window.google.maps.Point(10, 10)
// }
let circle;
class MapUnitKerja extends Component {
  static defaultProps = {
    center: {
      lat: 2.74437333401,
      lng: 125.362329483
    },
    zoom: 11
  };
  constructor(props) {
    super(props);
    this.state = {
      radiusUnit: 0,
      positionUnit:{
        lat:0,
        lng:0,
      },
      klikMouse:{
        lat:"",
        lng:"",
      },
    };   
}

  setPosition = (key) => {
    // console.log(key)
    this.setState(() => ({
      klikMouse:{
        lat:key.lat,
        lng:key.lng,
      },
    }), ()=>{
      this.props.getPosisiTagBaru(this.state.klikMouse);
    });
    
    // this.props.onCenterChange([childProps.lat, childProps.lng]);
  }
  componentDidMount(){
    if(this.props.positionUnit.lat!=="" && this.props.positionUnit.lat!==null){
      this.setState(() => ({
        positionUnit:{
          lat:parseFloat(this.props.positionUnit.lat),
          lng:parseFloat(this.props.positionUnit.lng),
        },
        radiusUnit: this.props.rediusUnit,
      }));
    }
  }
  componentDidUpdate(prevProps, prevState) {
    if(this.props.positionUnit !== prevProps.positionUnit){
      if(this.props.positionUnit.lat!=="" && this.props.positionUnit.lat!==null){
        this.setState(() => ({
          positionUnit:{
            lat:parseFloat(this.props.positionUnit.lat),
            lng:parseFloat(this.props.positionUnit.lng),
          },
        }),() =>{
          console.log("cirlce");
          console.log(circle);
          if(circle !== undefined){
            circle.setCenter(this.state.positionUnit);
          }
        });
      }
    }

    if(this.props.rediusUnit !== prevProps.rediusUnit){
        this.setState(() => ({
          radiusUnit: this.props.rediusUnit,
        }),() =>{
          if(circle !== undefined){
            circle.setRadius(Number(this.state.radiusUnit));
          }
        });
    }
  }

  render() {
    console.log(this.state.radiusUnit);
    const apiIsLoaded = (map, maps) => {
      circle = new maps.Circle({
        strokeColor: "#FF0000",
        strokeOpacity: 0.8,
        strokeWeight: 2,
        fillColor: "#FF0000",
        fillOpacity: 0.3,
        map,
        center: this.state.positionUnit,
        radius: this.state.radiusUnit,
      });
    };

    return (
      // Important! Always set the container height explicitly
      <div style={{ height: '60vh', width: '100%' }}>
        <GoogleMapReact
          bootstrapURLKeys={{ key: "AIzaSyB8L7AFUamts-nbU2tuFK4LkfDykESmCoM" }}
          yesIWantToUseGoogleMapApiInternals
          defaultCenter={this.props.center}
          defaultZoom={this.props.zoom}
            onGoogleApiLoaded={({ map, maps }) => apiIsLoaded(map, maps)}
            onClick={this.setPosition}
        >
            {(this.state.positionUnit.lat!=="" && this.state.positionUnit.lng!=="") ? <MarkerMapUnitKerja lat={this.state.positionUnit.lat} lng={this.state.positionUnit.lng}/> : "" }
            {(this.state.klikMouse.lat!=="" && this.state.klikMouse.lng!=="" &&this.state.klikMouse.lng!=this.state.positionUnit.lng) ? <Marker lat={this.state.klikMouse.lat} lng={this.state.klikMouse.lng}/> : "" }
            
        </GoogleMapReact>
      </div>
    );
  }
}
 
export default MapUnitKerja;