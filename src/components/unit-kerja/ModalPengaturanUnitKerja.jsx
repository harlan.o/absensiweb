import React, { Component } from 'react';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import { SelectUnitInduk } from '../unit-induk/SelectUnitInduk';
import MapUnitKerja from './MapUnitKerja';

// Redux
import {connect} from 'react-redux';
import {updateUnitKerja, createUnitKerja} from "../../actions/unitKerjaAction";

import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

class ModalPengaturanUnitKerja extends Component {
    constructor(props) {
        super(props);
        this.state = {
            judul:"Tambah Unit Kerja",
            data:{
                id: null,
                nama: "",
                geolocation: ",",
                radius: 0,
                unitInduk:{
                    id:"",
                },
                fixJadwal: false,
            },
            geolocationTagBaru:",",
            dataoption:this.props.dataoption,

            readonly:false
        
        };
        this.handleGetPosisiTag=this.handleGetPosisiTag.bind(this);
        this.MySwal = withReactContent(Swal);
        // console.log("dataOption ",this.props.dataoption)
        // this.handleChangeGeolocation = this.handleChangeGeolocation.bind(this);
    }

    handleGetPosisiTag = (e) => {
        // console.log("modal new location");
        // console.log(e);
        this.setState(({       
            geolocationTagBaru:e.lat +", "+e.lng
        }));
    }
    handleChangeGeolocation = () =>{
        this.setState( prevState => ({       
            data:{...prevState.data, geolocation : this.state.geolocationTagBaru }
        }));
    }
    async update(dataOne) {    
        await this.setState(({       
            data:dataOne,
            judul:"Ubah Data Unit Kerja",
            readonly:true,
        }));
    }
    componentDidMount(){
        if(this.props.dataedit != null){
            this.update(this.props.dataedit)
            // console.log(this.state.data)
            // console.log("did")
        }
    }

render (){
    // console.log(this.state.data);
    let geolocation = this.state.data.geolocation.split(",");
    let center= {
        lat: geolocation[0],
        lng: geolocation[1],
      };
    let geolocationNew= this.state.geolocationTagBaru;
    return (
        <Modal 
          {...this.props}
          size="lg"
          aria-labelledby="contained-modal-title-vcenter"
          centered
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-vcenter">
            {this.state.judul}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
              <Form >
              <Card>
                  <Card.Body>
                  
                      <Form.Row>
                        {this.state.readonly ?
                         (<Form.Group as={Col} controlId="form_id">
                         <Form.Label className="textblack">Kode Unit Kerja</Form.Label>
                         <Form.Control type="text" readOnly={this.state.readonly} placeholder="Masukan Kode Unit Kerja" value={this.state.data.id}onChange={v => this.setState(prevState => ({data:{...prevState.data, id : v.target.value}}))} />
                         </Form.Group>) :"" 
                        }
  
                          <Form.Group as={Col} controlId="form_ktp">
                          <Form.Label className="textblack">Nama Unit Kerja</Form.Label>
                          <Form.Control type="text" placeholder="Masukan Nama Kerja" value={this.state.data.nama} onChange={v => this.setState(prevState => ({data:{...prevState.data, nama : v.target.value}}))}/>
                          </Form.Group>
                      </Form.Row>
                      <Form.Row>
                          <Form.Group as={Col} controlId="form_id">
                          <Form.Label className="textblack">Unit Induk</Form.Label>
                          <SelectUnitInduk dataoption={this.state.dataoption} valueOption={this.state.data.unitInduk} valueGet={v => this.setState(prevState => ({data:{...prevState.data, unitInduk : {id:v.id, nama:v.nama} }}))}/>
                          </Form.Group>
                      </Form.Row>
                      <Form.Row>
                      <div className="my-1 mr-2" as="h5" htmlFor="inlineFormCustomSelectPref">
                        <Form.Group as={Col} controlId="tipe_jadwal_kerja">
                            <Form.Check type="checkbox" label="Tipe Fix Jadwal Kerja" checked={this.state.data.fixJadwal} onChange={
                                    v=>{
                                        this.setState(prevState => ({data:{...prevState.data, fixJadwal : v.target.checked }}))
                                    }
                                }/>
                        </Form.Group>
                        </div>
                      </Form.Row>
                  </Card.Body>
              </Card>
              <Card>
                  <Card.Body>
                        <Form.Row>
                            <Form.Group as={Col} xs={6}  controlId="form_geolocation">
                                <Form.Label className="textblack">Lokasi Sekarang</Form.Label>
                                <Form.Control type="text" readOnly placeholder="Masukan Kode Unit Kerja" value={geolocation} onChange={v => this.setState(prevState => ({data:{...prevState.data, geolocation : v.target.value}}))} />
                            </Form.Group>
                            <Form.Group as={Col} xs={6}  controlId="form_geolocation_baru">
                                <Form.Label className="textblack">Lokasi Baru</Form.Label>
                                <Form.Control type="text" readOnly placeholder="Masukan Kode Unit Kerja" value={geolocationNew} />
                                
                            </Form.Group>
                            <Form.Group as={Col} xs={12} controlId="form_geolocation_baru">
                                <Button variant="secondary" onClick={this.handleChangeGeolocation} >
                                Kunci Lokasi Baru
                            </Button>
                            </Form.Group>
                            <Form.Group as={Col} xs={6}  controlId="form_geolocation_baru">
                                <Form.Label className="textblack">Radius</Form.Label>
                                <Form.Control type="number" placeholder="Masukan Radius" value={this.state.data.radius} min={0} onChange={v => this.setState(prevState => ({data:{...prevState.data, radius : v.target.value}}) )} />
                            </Form.Group>
                            
                      </Form.Row>
                      
                      <Form.Row>
                            <MapUnitKerja positionUnit={center} rediusUnit={this.state.data.radius} getPosisiTagBaru={this.handleGetPosisiTag}/>
                      </Form.Row>
                  </Card.Body>
              </Card>

                  </Form>
              </Modal.Body>
          <Modal.Footer>
              <Button variant="secondary" onClick={this.props.onHide}>
                  Tutup
              </Button>
              <Button variant="primary" className="btn-them-red" onClick={this.handleCreateUnitKerja}>
                  Simpan
              </Button>
          </Modal.Footer>
        </Modal>
      );
}

handleCreateUnitKerja =  (e) =>  {
    e.preventDefault();
    let info="";
    let nVerif=0;
  
    
    if(this.state.data.nama === "" || this.state.data.nama === null){
        info+="Nama belum terisi <br/>";
        nVerif=1;
    }
    
    if(this.state.data.unitInduk.id === "" || this.state.data.unitInduk.id === null){
        info+="Unit Induk belum dipilih <br/>";
        nVerif=1;
    }

    if(this.state.data.geolocation === "," || this.state.data.geolocation === null){
        info+="Geolocation Belum dikunci <br/>";
        nVerif=1;
    }
    
    if(this.state.data.radius === 0 || this.state.data.radius === null  || this.state.data.radius === ""){
        info+="Radius belum ditentukan <br/>";
        nVerif=1;
    }
    let intrad = parseInt(this.state.data.radius)
    if(intrad == NaN ){
        info+="Radius Bukan Number <br/>";
        nVerif=1;
    }

    if(nVerif===1){
        this.MySwal.fire({
            icon: "error",
            title: info,
          });
    }else if(this.state.data.id === null && nVerif!==1){
        console.log(this.state.data);
        this.props.createUnitKerja(this.state.data).then(()=>{

            const {success} = this.props.infoCreateUnitKerja;
            // console.log(success);
            if(success){
                this.MySwal.fire({
                    icon: "success",
                    title: this.state.data.nama+" berhasil ditambahkan",
                  });
                }
                this.props.actionLoading();
                this.props.onHide();
            });
    }else if(this.state.data.id !== null && nVerif!==1){
        console.log(this.state.data);
        this.props.updateUnitKerja(this.state.data).then(()=>{
            const {success} = this.props.infoUpdateUnitKerja;
            // console.log(success);
            if(success){
                this.MySwal.fire({
                    icon: "success",
                    title: this.state.data.nama+" berhasil diperbaharui",
                  });
                }
                this.props.actionLoading();
                this.props.onHide();
            });
    }
}

}

const mapStateToProps = (state) => {
    return{
        infoCreateUnitKerja: state.infoCreateUnitKerja,
        infoUpdateUnitKerja: state.infoUpdateUnitKerja,
    } 
 }
export default connect(mapStateToProps, {createUnitKerja, updateUnitKerja})(ModalPengaturanUnitKerja);