import React, { Component } from 'react';
import AsyncSelect from 'react-select/async';

export class SelectMultiplyUnitKerja extends Component {
  constructor(props) {
    super(props);
    this.state = {
        dataRow:null,
        inputValue: '',
        valueOption:null,
    };

  }

  async update(dataList, dataValue) {    
    await this.setState(({       
      dataRow: dataList,
      valueOption: dataValue,
    }));
  }
  componentDidMount(){
    this.update( this.props.dataoption, this.props.valueOption)
  }
  componentDidUpdate(prevProps, prevState) {
    if(this.props.dataoption !== prevProps.dataoption){
      console.log(this.props.dataoption)
        if(this.props.dataoption !== undefined){
            this.setState((state) => ({
                dataRow: this.props.dataoption,
                valueOption: this.props.valueOption,
                renderTable:true,
                loadingPage:false
            }));
        } 
    }
    if(this.props.valueOption !== prevProps.valueOption){
      if(this.props.valueOption !== undefined){
        this.setState((state) => ({
            valueOption: this.props.valueOption,
        }));
      } 
    }
  }

   filterColors = (inputValue) => {
     let bab;
       bab= this.state.dataRow?.filter(i =>{
         if(i !== undefined && i.nama != null){
           return i.nama?.toLowerCase().includes(inputValue.toLowerCase())
           console.log(i.nama)
         }
       }
       );
       console.log(bab)
     return bab
  };
  
  promiesStartOption= () =>{
    let bab;
       bab= this.state.dataRow?.filter(i =>{
         if(i !== undefined && i.nama != null){
           return i;
         }
       }
       );
    console.log(bab)
     return bab
  }

   promiseOptions = inputValue =>
    new Promise(resolve => {
      setTimeout(() => {
        resolve(this.filterColors(inputValue));
      }, 1000);
    });
  
    valueGetSend = (v)=>
    {
      this.props.valueGetMultiply(v);
    }


    // Grouping
    groupStyles = {
      display: 'flex',
      alignItems: 'center',
      justifyContent: 'space-between',
    };
    groupBadgeStyles = {
      backgroundColor: '#EBECF0',
      borderRadius: '2em',
      color: '#172B4D',
      display: 'inline-block',
      fontSize: 12,
      fontWeight: 'normal',
      lineHeight: '1',
      minWidth: 1,
      padding: '0.16666666666667em 0.5em',
      textAlign: 'center',
    };
    formatGroupLabel = data => (
      <div style={this.groupStyles}>
        <span>{data.unitInduk.nama}</span>
        <span style={this.groupBadgeStyles}>{data.options.length}</span>
      </div>
    );
    // END Grouping
  render() {

    return (
      <AsyncSelect onChange={this.valueGetSend} 
      value={this.state.valueOption}
      getOptionValue={option => option.id} 
      getOptionLabel={option => option.nama}
      placeholder='Pilih Unit Kerja'
      defaultOptions={this.state.dataRow?.filter(i =>{
        if(i !== undefined && i.nama != null){
          return i
        }
        })
      }
      loadOptions={this.promiseOptions} 
      formatGroupLabel={this.formatGroupLabel}
      isMulti
      className="basic-multi-select"
      classNamePrefix="select"
      />
    );
  }
}

export const colourOptions=[{id: 704, nama: "SD NEGERI TATAHADENG"}, {id: 706, nama: "SMP NEGERI 1 SIAU BARAT SELATAN"}
, {id: 710, nama: "SD NEGERI INPRES DAME"}]

export default SelectMultiplyUnitKerja;

