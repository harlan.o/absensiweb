import React, { Component } from 'react';
import {connect} from 'react-redux';
import { getListUnitInduk } from "../../actions/unitIndukAction";
import AsyncSelect from 'react-select/async';

export class SelectUnitKerja extends Component {
  constructor(props) {
    super(props);
    this.state = {
        optionUnitInduk:this.props.dataoption,
        inputValue: '',
        valueOption:null,
    };
    // this.valueSend = this.valueSend.bind(this);

  }
  // componentDidMount(){
  //   console.log(this.props.listPengUnitInduk?.data)
  // }
  async update(dataList, dataValue) {    
    await this.setState(({       
      dataRow: dataList,
      valueOption: dataValue,
    }));
  }
  componentDidMount(){
    this.update( this.props.listPengUnitInduk?.data, this.props.valueOption)
  }
  componentDidUpdate(prevProps, prevState) {
    if(this.props.listPengUnitInduk !== prevProps.listPengUnitInduk){
        if(this.props.listPengUnitInduk?.data !== undefined){
            // console.log(this.props.listPengUnitInduk?.data);
            console.log(this.props.valueOption);
            this.setState((state) => ({
                dataRow: this.props.listPengUnitInduk?.data,
                valueOption: this.props.valueOption,
                renderTable:true,
                loadingPage:false
            }));
        } 
    }

    if(this.props.valueOption !== prevProps.valueOption){
      if(this.props.valueOption !== undefined || this.props.valueOption !== null){
        // console.log(this.props.valueOption);
        this.setState((state) => ({
            valueOption: this.props.valueOption,
        }));
      } 
    }

  }

   filterColors = (inputValue) => {
    //  console.log("HAIW");
     let bab= this.state.optionUnitInduk?.filter(i =>
      i.nama.toLowerCase().includes(inputValue.toLowerCase())
      );
    return bab
  };
  
   promiseOptions = inputValue =>
    new Promise(resolve => {
      setTimeout(() => {
        resolve(this.filterColors(inputValue));
      }, 1000);
    });
  
    valueGetSend = (v)=>
    {
      this.props.valueGet({id:v.id, nama:v.nama });
    }

   
  render() {
    // console.log(this.props.valueOption)
    // console.log(this.state.valueOption)
    // console.log("HAI")
    return (
      <AsyncSelect cacheOptions onChange={this.valueGetSend} 
      value={this.state.valueOption}
      // {}
        getOptionValue={option => option.id} 
      getOptionLabel={option => option.nama}
      defaultOptions
      loadOptions={this.promiseOptions} />
    );
  }
}


export default SelectUnitKerja;

