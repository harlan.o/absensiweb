// export const URL_API = 'http://34.101.95.238:8080/resource';
// export const URL_API_LOGIN = 'http://34.101.95.238:8080/authserver';

export const URL_API = 'http://35.219.19.91:8080/resource';
export const URL_API_LOGIN = 'http://35.219.19.91:8080/authserver';

// cadangan sementara
// export const URL_API = 'https://hr-resourceserver-sitaro.herokuapp.com/';
// export const URL_API_LOGIN = 'https://hr-resourceserver-sitaro.herokuapp.com/';

// USER LOGIN
export const API_LOGIN = '/api/v1/users/login';
export const API_PROFILE_USER = '/api/v1/pegawai/profil';

// HAK AKSES
export const API_GET_HAK_AKSES = '/api/v1/hakAkses'

// UNIT INDUK
export const API_LIST_UNIT_INDUK = '/api/v1/unitInduk';
export const API_ADD_UNIT_INDUK = '/api/v1/unitInduk';
export const API_UPDATE_UNIT_INDUK = '/api/v1/unitInduk';

// UNIT KERJA
export const API_LIST_UNIT_KERJA = '/api/v1/unitKerja';
export const API_ADD_UNIT_KERJA = '/api/v1/unitKerja';
export const API_UPDATE_UNIT_KERJA = '/api/v1/unitKerja';

// ABSENSI KERJA 
export const API_LIST_ABSENSI_KERJA = '/api/v1/absensi/byUnitKerja';

// ABSENSI PEGAWAI
export const API_LIST_ABSENSI_PEGAWAI = '/api/v2/absensi/menuAbsensiPegawai';

// ABSENSI INDUK
export const API_LIST_ABSENSI_INDUK = '/api/v1/absensi/byUnitInduk';
export const API_LIST_ABSENSI_INDUK_ALL = '/api/v1/absensi/all';

// Dashboard
export const API_LIST_ABSENSI_DASHBOARD='/api/v1/absensi/adminDashboard';
export const API_LIST_ABSENSI_DASHBOARD_V2='/api/v2/absensi/adminDashboard';

// Laporan Absensi
export const API_LAPORAN_ABSENSI='/api/v2/absensi/rekapanAbsensi';

// Pegawai
export const API_LIST_PEGAWAI_BY_UNIT_KERJA='/api/v1/pegawai/byUnitKerja';
export const API_LIST_PEGAWAI='/api/v1/pegawai/all';
export const API_SIMPLE_LIST_PEGAWAI='/api/v1/pegawai/simpleList';
export const API_ADD_PEGAWAI='/api/v1/pegawai';
export const API_UPDATE_PEGAWAI_PROFIL='/api/v1/pegawai/profil';
export const API_UPDATE_PEGAWAI='/api/v1/pegawai/profil/updateByAdmin';

export const API_RESET_DEVICE_ID='/api/v1/users/resetDeviceId';
export const API_RESET_PASSWORD='/api/v1/users/resetPassword';


// TPP
export const API_LIST_TPP='/api/v1/tpp';
export const API_ADD_TPP='/api/v1/tpp';
export const API_UPDATE_TPP='/api/v1/tpp';
// export const API_UPDATE_TPP='/api/v1/tpp';
export const API_LIST_TPP_HISTORY='/api/v1/tpp/history';
export const API_LIST_PEGAWAI_TPP_HISTORY='/api/v1/tpp/history/pegawai';
export const API_REVISI_TPP_HISTORY='/api/v1/tpp/history';

export const API_LIST_TPP_PEGAWAI='/api/v1/pegawai/getDataTppWithFilter';
export const API_UPDATE_TPP_PEGAWAI='/api/v1/pegawai/modifyTpp';


// Artikel
export const API_LIST_ARTIKEL='/api/v1/news';
export const API_LIST_ARTIKEL_FILTER_UNIT_INDUK='/api/v1/news/byUnitInduk';
export const API_LIST_ARTIKEL_FILTER_UNIT_KERJA='/api/v1/news/byUnitKerja';
export const API_ADD_ARTIKEL='/api/v1/news';
export const API_UPDATE_ARTIKEL='/api/v1/news';
export const API_DELETE_ARTIKEL='/api/v1/news';
export const API_PUSH_NOTIFICATION='https://fcm.googleapis.com/fcm/send';

//get FCM phone
export const API_LIST_FCM_PHONE='/api/v1/pegawai/fcm';

// Jadwal Kerja
export const API_LIST_JADWAL_KERJA='/api/v1/jadwal';
export const API_ADD_JADWAL_KERJA='/api/v1/jadwal';
export const API_UPDATE_JADWAL_KERJA='/api/v1/jadwal';
export const API_DELETE_JADWAL_KERJA='/api/v1/jadwal';

// Jadwal Pegawai
export const API_LIST_JADWAL_PEGAWAI='/api/v1/jadwal/admin';
export const API_ADD_JADWAL_PEGAWAI='/api/v1/jadwal/assignJadwal';
export const API_UPDATE_JADWAL_PEGAWAI='/api/v1/jadwal/assignJadwal';
export const API_DELETE_JADWAL_PEGAWAI='/api/v1/jadwal/deletePegawaiJadwal';
export const API_WFH_JADWAL_PEGAWAI='/api/v1/jadwal/modWfh';

// Hari Libur
export const API_LIST_HARI_LIBUR='/api/v1/jadwal/liburKalender';
export const API_ADD_HARI_LIBUR='/api/v1/jadwal/liburKalender';
export const API_DELETE_HARI_LIBUR='/api/v1/jadwal/liburKalender';

//even
export const API_LIST_EVEN='/api/v1/events/unitPenyelenggara/';
export const API_ADD_EVEN='/api/v1/events';

// Penugasan
export const API_LIST_MELAKSANAKAN_TUGAS='';
export const API_ADD_MELAKSANAKAN_TUGAS='/api/v1/jadwal/penugasan';
export const API_UPDATE_MELAKSANAKAN_TUGAS='';
export const API_DELETE_MELAKSANAKAN_TUGAS='/api/v1/jadwal/penugasan';
export const API_LIST_TUGAS_LUAR='';
export const API_ADD_TUGAS_LUAR='/api/v1/jadwal/penugasan';
export const API_UPDATE_TUGAS_LUAR='';
export const API_DELETE_TUGAS_LUAR='/api/v1/jadwal/penugasan';

// Lokasi
export const API_LIST_LOKASI_KOTA='';

// Pengambilan Ijin
export const API_LIST_PENGAMBILAN_IJIN='/api/v1/ijin/appliedIjinData';
export const API_ADD_PENGAMBILAN_IJIN='/api/v1/ijin/pegawai/approveByAdmin';
export const API_DELETE_PENGAMBILAN_IJIN='/api/v1/ijin/pegawai/deleteApprovedIjin';

// Jenis Ijin
export const API_LIST_JENIS_IJIN='/api/v1/ijin';

// Laporan Akhir
export const API_LIST_LAPORAN_AKHIR_UMUM='/api/v2/absensi/laporanAkhir';
export const API_LIST_LAPORAN_AKHIR_KHUSUS='';
