// Test fcm
import firebase from "firebase/app";
import { FIREBASE_API_KEY, FIREBASE_MESSAGING_SENDER_ID, FIREBASE_AUTH_DOMAIN, FIREBASE_DATABASE_URL, FIREBASE_PROJECT_ID, FIREBASE_STORAGE_BUCKET, FIREBASE_APP_ID, FIREBASE_MEASUREMENT_ID } from "../constants/firebaseConstants";

firebase.initializeApp({
  messagingSenderId: FIREBASE_MESSAGING_SENDER_ID,
  apiKey: FIREBASE_API_KEY,
  authDomain: FIREBASE_AUTH_DOMAIN,
  databaseURL: FIREBASE_DATABASE_URL,
  projectId: FIREBASE_PROJECT_ID,
  storageBucket: FIREBASE_STORAGE_BUCKET,
  appId: FIREBASE_APP_ID,
  measurementId: FIREBASE_MEASUREMENT_ID

});

export default firebase;
// console.log(firebase.database())