import React, { Component } from 'react';
import { withRouter } from "react-router";
import {connect} from 'react-redux';
import "./Layout.css";
import NavbarUser from '../components/NavbarUser';
import SidebarUser from '../components/SidebarUser';
import Dashboard from "../views/dashboard/Dashboard";
import Pengaturan from "../views/pengaturan/Pengaturan";
import LaporanAbsen from '../views/laporan-absen/LaporanAbsen';
import LaporanTpp from '../views/laporan-tpp/LaporanTpp';
import Statistik from '../views/statistik/Statistik';
import AbsensiPegawai from '../views/absensi-pegawai/AbsensiPegawai';
import DokumenSurat from '../views/dokumen-surat/DokumenSurat';

import { dataProfile } from "../actions/userActions";
import Artikel from '../views/artikel/Artikel';
import Schedule from '../views/schedule/Schedule';
import LaporanAkhir from '../views/laporan-akhir/LaporanAkhir';
import Even from '../views/even/Even';
import Penugasan from '../views/penugasan/Penugasan';
import PengambilanIjin from '../views/pengambilan-ijin/PengambilanIjin';

class LayoutWithRouter extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isToggleOnSidebar: true,
            page: "",
            sub_page: "",
        };
       this.handleClickShowSidebar = this.handleClickShowSidebar.bind(this);  
       
    }
    
    handleClickShowSidebar(e) {    
        this.setState(() => ({      
              isToggleOnSidebar: !e
        }));  
    }
    
    renderPage(param, param2) {
        switch(param) {
            case 'dashboard':
                return <Dashboard/>;
            case 'statistik':
                return <Statistik/>;
            case 'absensi-pegawai':
                return <AbsensiPegawai sub_page={param2}/>;
            // case 'dokumen-surat':
            //     return <DokumenSurat/>;
            case 'pengambilan-ijin':
                return <PengambilanIjin/>;
            case 'penugasan':
                return <Penugasan sub_page={param2}/>;
            case 'laporan-absen':
                return <LaporanAbsen sub_page={param2}/>;
            case 'laporan-tpp':
                    return <LaporanTpp/>;
            case 'laporan-akhir':
                    return <LaporanAkhir/>;
            case 'artikel':
                    return <Artikel/>;
            case 'even':
                    return <Even/>;
            case 'schedule':
                    return <Schedule sub_page={param2}/>;
            case 'pengaturan':
                return <Pengaturan sub_page={param2}/>
            case 'profile':
                return <Dashboard/>;
            default:
                return 'Layout tidak tersedia';
        }
    }
    
    componentDidMount(prevProps, prevState){
        this.props.dataProfile();
    }
    componentDidUpdate(prevProps, prevState) {
        // if(this.props.userDataProfile !== prevProps.userDataProfile){
        //     if(this.props.userDataProfile?.profileUser !== undefined) 
        //     console.log(this.props.userDataProfile?.profileUser);
        // }
    }


    render(){
        
        return(
        <div className={this.state.isToggleOnSidebar ? 'sb-nav-fixed' : 'sb-nav-fixed sb-sidenav-toggled'}>
           <NavbarUser onShowSidebar={this.handleClickShowSidebar}/>

        <div id="layoutSidenav">
            <SidebarUser/>
            <div id="layoutSidenav_content">
                <main>
                    {this.withRouter} 
                    {this.renderPage(this.props.page, this.props.sub_page)}
                </main>
                <footer className="py-4 bg-light mt-auto">
                    <div className="container-fluid">
                        <div className="d-flex align-items-center justify-content-between small">
                            <div className="text-muted">Copyright &copy; Psikku 2020</div>
                            <div>
                                <a href="/">Privacy Policy</a>
                                &middot;
                                <a href="/">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>

        );
    }

}

const mapStateToProps = (state) => {
    return{
        userDataProfile: state.userDataProfile
    } 
 }
const Layout = withRouter(LayoutWithRouter);

export default connect(mapStateToProps, {dataProfile})(Layout);