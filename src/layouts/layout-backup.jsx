import React, { Component } from 'react';
// import NavbarUser from "../components/NavbarUser"
import { Col, Container, Navbar, Nav, NavDropdown, Row } from 'react-bootstrap';
import { withRouter } from "react-router";
import { Switch, Route, Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faUser, faTachometerAlt, faBars, faUserClock, faFile, faChartArea, faBookOpen, faCog, faBell } from '@fortawesome/free-solid-svg-icons'
import "./Layout.css";

class Layout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isToggleOnSidebar: true,
            isToggleOnUserBar: false,
        };
       this.handleClickShowSidebar = this.handleClickShowSidebar.bind(this);  
       this.handleClickShowUserBar = this.handleClickShowUserBar.bind(this);  
    }
    
    handleClickShowSidebar() {    
          this.setState(state => ({      
              isToggleOnSidebar: !state.isToggleOnSidebar
    }));  
    }
    handleClickShowUserBar() {    
        this.setState(state => ({      
            isToggleOnUserBar: !state.isToggleOnUserBar
    }));  
    }

    render(){
        return(

        <div className={this.state.isToggleOnSidebar ? 'sb-nav-fixed' : 'sb-nav-fixed sb-sidenav-toggled'}>
            <Navbar variant="dark" className="sb-topnav navbar navbar-expand navbar-darkred bg-darkred">
                <Navbar.Brand href="#home">Absensi</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="btn-sm order-1 order-lg-0">
                    <Nav.Link href="#features" className="" onClick={this.handleClickShowSidebar} >
                        <FontAwesomeIcon icon={faBars} />
                    </Nav.Link>
                    </Nav>
                    <div className="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0"></div>
                    <Nav className="navbar-nav ml-auto ml-md-0">
                            <Nav.Item>
                                <a className="nav-link" role="button"><FontAwesomeIcon icon={faBell} /></a>
                            </Nav.Item>
                            <Nav.Item className={this.state.isToggleOnUserBar ? 'dropdown show' : ' dropdown'}>
                                <a className="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded={this.state.isToggleOnUserBar ? 'true' : 'false'} onClick={this.handleClickShowUserBar}><FontAwesomeIcon icon={faUser} /></a>
                                <div className={this.state.isToggleOnUserBar ? 'dropdown-menu dropdown-menu-right show' : 'dropdown-menu dropdown-menu-right'} aria-labelledby="userDropdown">
                                    <a className="dropdown-item" href="#">Profile</a>
                                    <a className="dropdown-item" href="#">Activity Log</a>
                                    <div className="dropdown-divider"></div>
                                    <a className="dropdown-item" href="login.html">Logout</a>
                                </div>
                            </Nav.Item>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>

        {/* <nav className="sb-topnav navbar navbar-expand navbar-darkred bg-darkred">
            <a className="navbar-brand" href="index.html">Absensi</a>
            <button className="btn btn-link btn-sm order-1 order-lg-0" id="sidebarToggle" onClick={this.handleClickShowSidebar} href="#"><FontAwesomeIcon icon={faBars} /></button>
            
            <div className="d-none d-md-inline-block form-inline ml-auto mr-0 mr-md-3 my-2 my-md-0"></div>
         
            <ul className="navbar-nav ml-auto ml-md-0 ">
                <li className="nav-item">
                    <a className="nav-link " role="button"><FontAwesomeIcon icon={faBell} /></a>
                </li>
                
                <li className={this.state.isToggleOnUserBar ? 'nav-item dropdown show' : 'nav-item dropdown'}>
                    <a className="nav-link dropdown-toggle" id="userDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded={this.state.isToggleOnUserBar ? 'true' : 'false'} onClick={this.handleClickShowUserBar}><FontAwesomeIcon icon={faUser} /></a>
                    <div className={this.state.isToggleOnUserBar ? 'dropdown-menu dropdown-menu-right show' : 'dropdown-menu dropdown-menu-right'} aria-labelledby="userDropdown">
                        <a className="dropdown-item" href="#">Settings</a>
                        <a className="dropdown-item" href="#">Activity Log</a>
                        <div className="dropdown-divider">asdf</div>
                        <a className="dropdown-item" href="login.html">Logout</a>
                    </div>
                </li>
            </ul>
        </nav> */}

        <div id="layoutSidenav">
            <div id="layoutSidenav_nav">
                <nav className="sb-sidenav accordion sb-sidenav-darkred" id="sidenavAccordion">
                <a className="navbar-brand-sidebar" href="index.html">Absensi</a>
                    <div className="sb-sidenav-menu">
                        <Nav> 
                            <Nav.Link as={Link} to="/" href="/"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faTachometerAlt} />
                                </div>
                                Dashboard
                            </Nav.Link>
                            <Nav.Link as={Link} to="/statistik" href="/statistik"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faChartArea} />
                                </div>
                                Statistik
                            </Nav.Link>
                            <Nav.Link as={Link} to="/absensi-pegawai" href="/absensi-pegawai"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faUserClock} />
                                </div>
                                Absensi Pegawai
                            </Nav.Link>
                            <Nav.Link as={Link} to="/surat" href="/dokume-surat"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faFile} />
                                </div>
                                Dokumen Surat
                            </Nav.Link>
                            <Nav.Link as={Link} to="/laporan" href="/laporan"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faBookOpen} />
                                </div>
                                Laporan
                            </Nav.Link>
                            <Nav.Link as={Link} to="/pengaturan" href="/pengaturan"> 
                                <div className="sb-nav-link-icon">
                                    <FontAwesomeIcon icon={faCog} />
                                </div>
                                Pengaturan
                            </Nav.Link>
                        </Nav>
                        {/* <div className="nav">
                            
                            <div className="sb-sidenav-menu-heading">Core</div>
                            <a className="nav-link active" href="index.html">
                                <div className="sb-nav-link-icon"><FontAwesomeIcon icon={faTachometerAlt} />
                                </div>
                                Dashboard
                            </a>
                            <a className="nav-link" href="index.html">
                                <div className="sb-nav-link-icon"><FontAwesomeIcon icon={faChartArea} /></div>
                                Statistik
                            </a><a className="nav-link" href="index.html">
                                <div className="sb-nav-link-icon"><FontAwesomeIcon icon={faUserClock} /></div>
                                Absensi Pegawai
                            </a>
                            <a className="nav-link" href="index.html">
                                <div className="sb-nav-link-icon"><FontAwesomeIcon icon={faFile} /></div>
                                Dokumen Surat
                            </a><a className="nav-link" href="index.html">
                                <div className="sb-nav-link-icon"><FontAwesomeIcon icon={faBookOpen} /></div>
                                Laporan
                            </a><a className="nav-link" href="index.html">
                                <div className="sb-nav-link-icon"><FontAwesomeIcon icon={faCog} /></div>
                                Pengaturan
                            </a>
                           
                        </div> */}
                    </div>
                    
                    <div className="sb-sidenav-footer">
                        <hr/>
                        <div className="small">Logged in as:</div>
                        Start Bootstrap
                    </div>
                </nav>
            </div>


            
            <div id="layoutSidenav_content">
                
                
                <main>
                    <div className="container-fluid">
                        <h1 className="mt-4">Dashboard</h1>
                        <ol className="breadcrumb mb-4">
                            <li className="breadcrumb-item active">Dashboard</li>
                        </ol>
                        <div className="row">
                            <div className="col-xl-3 col-md-6">
                                <div className="card bg-primary text-white mb-4">
                                    <div className="card-body">Primary Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div><div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div><div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div><div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div><div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div><div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div><div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div><div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div><div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div><div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div><div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div><div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div><div className="col-xl-3 col-md-6">
                                <div className="card bg-warning text-white mb-4">
                                    <div className="card-body">Warning Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-md-6">
                                <div className="card bg-success text-white mb-4">
                                    <div className="card-body">Success Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-xl-3 col-md-6">
                                <div className="card bg-danger text-white mb-4">
                                    <div className="card-body">Danger Card</div>
                                    <div className="card-footer d-flex align-items-center justify-content-between">
                                        <a className="small text-white stretched-link" href="#">View Details</a>
                                        <div className="small text-white"><i className="fas fa-angle-right"></i></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    
                    </div>
                </main>





                <footer className="py-4 bg-light mt-auto">
                    <div className="container-fluid">
                        <div className="d-flex align-items-center justify-content-between small">
                            <div className="text-muted">Copyright &copy; Your Website 2020</div>
                            <div>
                                <a href="#">Privacy Policy</a>
                                &middot;
                                <a href="#">Terms &amp; Conditions</a>
                            </div>
                        </div>
                    </div>
                </footer>
            </div>
        </div>
    </div>

        );
    }

}


export default withRouter(Layout);