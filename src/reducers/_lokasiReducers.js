import {
    LIST_LOKASI_KOTA_REQUEST,
    LIST_LOKASI_KOTA_SUCCESS,
    LIST_LOKASI_KOTA_FAIL,
    LIST_LOKASI_KOTA_RESET,
  } from "../constants/_lokasiConstants";

  export const listLokasiKotaReducer = (state = {daftarLokasi:{}}, action) => {
    switch (action.type) {
      case LIST_LOKASI_KOTA_REQUEST:
        return { loading: true };
      case LIST_LOKASI_KOTA_SUCCESS:
        return { loading: false, data: action.payload };
      case LIST_LOKASI_KOTA_FAIL:
        return { loading: false, error: action.payload };
      case LIST_LOKASI_KOTA_RESET:
        return {};
      default:
        return state;
    }
  };