import {
    LIST_ABSENSI_PEGAWAI_REQUEST,
    LIST_ABSENSI_PEGAWAI_SUCCESS,
    LIST_ABSENSI_PEGAWAI_FAIL,
    LIST_ABSENSI_PEGAWAI_RESET,
  } from "../constants/absensiConstants";


  export const listAbsensiPegawaiReducer = (state = {data:{}}, action) => {
    switch (action.type) {
      case LIST_ABSENSI_PEGAWAI_REQUEST:
        return { loading: true };
      case LIST_ABSENSI_PEGAWAI_SUCCESS:
        return { loading: false, data: action.payload };
      case LIST_ABSENSI_PEGAWAI_FAIL:
        return { loading: false, error: action.payload };
      case LIST_ABSENSI_PEGAWAI_RESET:
        return {};
      default:
        return state;
    }
  };