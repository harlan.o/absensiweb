import {
    LIST_ABSENSI_INDUK_REQUEST,
    LIST_ABSENSI_INDUK_SUCCESS,
    LIST_ABSENSI_INDUK_FAIL,
    LIST_ABSENSI_INDUK_RESET,
    LIST_ABSENSI_KERJA_REQUEST,
    LIST_ABSENSI_KERJA_SUCCESS,
    LIST_ABSENSI_KERJA_FAIL,
    LIST_ABSENSI_KERJA_RESET,
    TOTAL_HADIR,
    TOTAL_HADIR_REQUEST,
    LIST_ABSENSI_DASHBOARD_REQUEST,
    LIST_ABSENSI_DASHBOARD_SUCCESS,
    LIST_ABSENSI_DASHBOARD_FAIL,
    LIST_ABSENSI_DASHBOARD_RESET,
    LAPORAN_ABSENSI_REQUEST,
    LAPORAN_ABSENSI_SUCCESS,
    LAPORAN_ABSENSI_FAIL,
    LAPORAN_ABSENSI_RESET,
  } from "../constants/absensiConstants";

  export const listAbsensiIndukReducer = (state = {daftarAbsen:{}}, action) => {
    switch (action.type) {
      case LIST_ABSENSI_INDUK_REQUEST:
        return { loading: true };
      case LIST_ABSENSI_INDUK_SUCCESS:
        return { loading: false, data: action.payload };
      case LIST_ABSENSI_INDUK_FAIL:
        return { loading: false, error: action.payload };
      case LIST_ABSENSI_INDUK_RESET:
        return {};
      default:
        return state;
    }
  };


  export const listAbsensiKerjaReducer = (state = {daftarAbsen:{}}, action) => {
    // console.log(state);
    switch (action.type) {
      case LIST_ABSENSI_KERJA_REQUEST:
        return { loading: true };
      case LIST_ABSENSI_KERJA_SUCCESS:
        return { loading: false, data: action.payload };
      case LIST_ABSENSI_KERJA_FAIL:
        return { loading: false, error: action.payload };
      case LIST_ABSENSI_KERJA_RESET:
        return {};
      default:
        return state;
    }
  };

  export const listAbsensiDashboardReducer = (state = {daftarAbsen:{}}, action) => {
    switch (action.type) {
      case LIST_ABSENSI_DASHBOARD_REQUEST:
        return { loading: true };
      case LIST_ABSENSI_DASHBOARD_SUCCESS:
        return { loading: false, data: action.payload };
      case LIST_ABSENSI_DASHBOARD_FAIL:
        return { loading: false, error: action.payload };
      case LIST_ABSENSI_DASHBOARD_RESET:
        return {};
      default:
        return state;
    }
  };
  // export const totalHadirReducer = (state=null, action) => {
  //   // console.log(state);
  //   switch (action.type) {
  //     case TOTAL_HADIR_REQUEST:
  //       return { loading: true };
  //     case TOTAL_HADIR:
  //       return { loading: false, data: action.payload };
  //     default:
  //       return state;
  //   }
  // };

  export const laporanAbsensiReducer = (state = {rekapAbsen:{}}, action) => {
    switch (action.type) {
      case LAPORAN_ABSENSI_REQUEST:
        return { loading: true };
      case LAPORAN_ABSENSI_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case LAPORAN_ABSENSI_FAIL:
        return { loading: false, success:false, error: action.payload };
      case LAPORAN_ABSENSI_RESET:
        return {};
      default:
        return state;
    }
  };
  