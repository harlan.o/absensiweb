import {
    PENG_LIST_ARTIKEL_REQUEST,
    PENG_LIST_ARTIKEL_SUCCESS,
    PENG_LIST_ARTIKEL_FAIL,
    PENG_LIST_ARTIKEL_RESET,
    PENG_CREATE_ARTIKEL_REQUEST,
    PENG_CREATE_ARTIKEL_SUCCESS,
    PENG_CREATE_ARTIKEL_FAIL,
    PENG_CREATE_ARTIKEL_RESET,
    PENG_UPDATE_ARTIKEL_REQUEST,
    PENG_UPDATE_ARTIKEL_SUCCESS,
    PENG_UPDATE_ARTIKEL_FAIL,
    PENG_UPDATE_ARTIKEL_RESET,
    PENG_DELETE_ARTIKEL_REQUEST,
    PENG_DELETE_ARTIKEL_SUCCESS,
    PENG_DELETE_ARTIKEL_FAIL,
    PENG_DELETE_ARTIKEL_RESET,
    PENG_PUSH_NOTIFICATION_ARTIKEL_REQUEST,
    PENG_PUSH_NOTIFICATION_ARTIKEL_SUCCESS,
    PENG_PUSH_NOTIFICATION_ARTIKEL_FAIL,
    PENG_PUSH_NOTIFICATION_ARTIKEL_RESET
  } from "../constants/artikelConstants";

  export const listPengArtikelReducer = (state = {newsList:{}}, action) => {
    switch (action.type) {
      case PENG_LIST_ARTIKEL_REQUEST:
        return { loading: true };
      case PENG_LIST_ARTIKEL_SUCCESS:
        return { loading: false, data: action.payload };
      case PENG_LIST_ARTIKEL_FAIL:
        return { loading: false, error: action.payload };
      case PENG_LIST_ARTIKEL_RESET:
        return {};
      default:
        return state;
    }
  };

  export const createArtikelReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_CREATE_ARTIKEL_REQUEST:
        return { loading: true };
      case PENG_CREATE_ARTIKEL_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_CREATE_ARTIKEL_FAIL:
        return { loading: false, error: action.payload };
      case PENG_CREATE_ARTIKEL_RESET:
        return {};
      default:
        return state;
    }
  };


  export const updateArtikelReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_UPDATE_ARTIKEL_REQUEST:
        return { loading: true };
      case PENG_UPDATE_ARTIKEL_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_UPDATE_ARTIKEL_FAIL:
        return { loading: false, error: action.payload };
      case PENG_UPDATE_ARTIKEL_RESET:
        return {};
      default:
        return state;
    }
  };

  export const deleteArtikelReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_DELETE_ARTIKEL_REQUEST:
        return { loading: true };
      case PENG_DELETE_ARTIKEL_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_DELETE_ARTIKEL_FAIL:
        return { loading: false, error: action.payload };
      case PENG_DELETE_ARTIKEL_RESET:
        return {};
      default:
        return state;
    }
  };

  export const pushNotificationArtikelReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_PUSH_NOTIFICATION_ARTIKEL_REQUEST:
        return { loading: true };
      case PENG_PUSH_NOTIFICATION_ARTIKEL_SUCCESS:
        return { loading: false, success:true, data: action.payload};
      case PENG_PUSH_NOTIFICATION_ARTIKEL_FAIL:
        return { loading: false, error: action.payload };
      case PENG_PUSH_NOTIFICATION_ARTIKEL_RESET:
        return {};
      default:
        return state;
    }
  };