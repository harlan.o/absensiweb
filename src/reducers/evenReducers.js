import {
    PENG_LIST_EVEN_REQUEST,
    PENG_LIST_EVEN_SUCCESS,
    PENG_LIST_EVEN_FAIL,
    PENG_LIST_EVEN_RESET,
    PENG_CREATE_EVEN_REQUEST,
    PENG_CREATE_EVEN_SUCCESS,
    PENG_CREATE_EVEN_FAIL,
    PENG_CREATE_EVEN_RESET,
    PENG_UPDATE_EVEN_REQUEST,
    PENG_UPDATE_EVEN_SUCCESS,
    PENG_UPDATE_EVEN_FAIL,
    PENG_UPDATE_EVEN_RESET,
    PENG_DELETE_EVEN_REQUEST,
    PENG_DELETE_EVEN_SUCCESS,
    PENG_DELETE_EVEN_FAIL,
    PENG_DELETE_EVEN_RESET,
    PENG_PUSH_NOTIFICATION_EVEN_REQUEST,
    PENG_PUSH_NOTIFICATION_EVEN_SUCCESS,
    PENG_PUSH_NOTIFICATION_EVEN_FAIL,
    PENG_PUSH_NOTIFICATION_EVEN_RESET
  } from "../constants/evenConstants";


  export const listPengEvenReducer = (state = {eventList:{}}, action) => {
    switch (action.type) {
      case PENG_LIST_EVEN_REQUEST:
        return { loading: true };
      case PENG_LIST_EVEN_SUCCESS:
        return { loading: false, data: action.payload };
      case PENG_LIST_EVEN_FAIL:
        return { loading: false, error: action.payload };
      case PENG_LIST_EVEN_RESET:
        return {};
      default:
        return state;
    }
  };

  export const createEvenReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_CREATE_EVEN_REQUEST:
        return { loading: true };
      case PENG_CREATE_EVEN_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_CREATE_EVEN_FAIL:
        return { loading: false, success:false, error: action.payload };
      case PENG_CREATE_EVEN_RESET:
        return {};
      default:
        return state;
    }
  };


  export const updateEvenReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_UPDATE_EVEN_REQUEST:
        return { loading: true };
      case PENG_UPDATE_EVEN_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_UPDATE_EVEN_FAIL:
        return { loading: false, error: action.payload };
      case PENG_UPDATE_EVEN_RESET:
        return {};
      default:
        return state;
    }
  };

  export const deleteEvenReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_DELETE_EVEN_REQUEST:
        return { loading: true };
      case PENG_DELETE_EVEN_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_DELETE_EVEN_FAIL:
        return { loading: false, error: action.payload };
      case PENG_DELETE_EVEN_RESET:
        return {};
      default:
        return state;
    }
  };

  export const pushNotificationEvenReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_PUSH_NOTIFICATION_EVEN_REQUEST:
        return { loading: true };
      case PENG_PUSH_NOTIFICATION_EVEN_SUCCESS:
        return { loading: false, success:true, data: action.payload};
      case PENG_PUSH_NOTIFICATION_EVEN_FAIL:
        return { loading: false, error: action.payload };
      case PENG_PUSH_NOTIFICATION_EVEN_RESET:
        return {};
      default:
        return state;
    }
  };