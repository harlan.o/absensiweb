import {
    USER_HAK_AKSES_REQUEST,
    USER_HAK_AKSES_SUCCESS,
    USER_HAK_AKSES_FAIL,
    USER_HAK_AKSES_LOGOUT,

  } from "../constants/hakAksesConstants";
  
  export const hakAksesReducers = (state = {}, action) => {
    
    switch (action.type) {
      case USER_HAK_AKSES_REQUEST:
        return { loading: true };
      case USER_HAK_AKSES_SUCCESS:
        return { loading: false, haks: action.payload };
      case USER_HAK_AKSES_FAIL:
        return { loading: false, error: action.payload };
      case USER_HAK_AKSES_LOGOUT:
        return {};
      default:
        return state;
    }
  };