
import {
    LIST_HARI_LIBUR_REQUEST,
    LIST_HARI_LIBUR_SUCCESS,
    LIST_HARI_LIBUR_FAIL,
    LIST_HARI_LIBUR_RESET,
    CREATE_HARI_LIBUR_REQUEST,
    CREATE_HARI_LIBUR_SUCCESS,
    CREATE_HARI_LIBUR_FAIL,
    CREATE_HARI_LIBUR_RESET,
    DELETE_HARI_LIBUR_REQUEST,
    DELETE_HARI_LIBUR_SUCCESS,
    DELETE_HARI_LIBUR_FAIL,
    DELETE_HARI_LIBUR_RESET,
  } from "../constants/hariLiburConstants";

  export const listHariLiburReducer = (state = {daftarHariLibur:{}}, action) => {
    switch (action.type) {
      case LIST_HARI_LIBUR_REQUEST:
        return { loading: true };
      case LIST_HARI_LIBUR_SUCCESS:
        return { loading: false, data: action.payload };
      case LIST_HARI_LIBUR_FAIL:
        return { loading: false, error: action.payload };
      case LIST_HARI_LIBUR_RESET:
        return {};
      default:
        return state;
    }
  };

  
  export const createHariLiburReducer = (state = {}, action) => {
    switch (action.type) {
      case CREATE_HARI_LIBUR_REQUEST:
        return { loading: true };
      case CREATE_HARI_LIBUR_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case CREATE_HARI_LIBUR_FAIL:
        return { loading: false, success:false, error: action.payload };
      case CREATE_HARI_LIBUR_RESET:
        return {};
      default:
        return state;
    }
  };

  export const deleteHariLiburReducer = (state = {}, action) => {
    switch (action.type) {
      case DELETE_HARI_LIBUR_REQUEST:
        return { loading: true };
      case DELETE_HARI_LIBUR_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case DELETE_HARI_LIBUR_FAIL:
        return { loading: false, success:false, error: action.payload };
      case DELETE_HARI_LIBUR_RESET:
        return {};
      default:
        return state;
    }
  };