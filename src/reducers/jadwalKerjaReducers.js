import {
    LIST_JADWAL_KERJA_REQUEST,
    LIST_JADWAL_KERJA_SUCCESS,
    LIST_JADWAL_KERJA_FAIL,
    LIST_JADWAL_KERJA_RESET,
    CREATE_JADWAL_KERJA_REQUEST,
    CREATE_JADWAL_KERJA_SUCCESS,
    CREATE_JADWAL_KERJA_FAIL,
    CREATE_JADWAL_KERJA_RESET,
    UPDATE_JADWAL_KERJA_REQUEST,
    UPDATE_JADWAL_KERJA_SUCCESS,
    UPDATE_JADWAL_KERJA_FAIL,
    UPDATE_JADWAL_KERJA_RESET,
    DELETE_JADWAL_KERJA_REQUEST,
    DELETE_JADWAL_KERJA_SUCCESS,
    DELETE_JADWAL_KERJA_FAIL,
    DELETE_JADWAL_KERJA_RESET,
  } from "../constants/jadwalKerjaConstants";

  export const listJadwalKerjaReducer = (state = {daftarJadwal:{}}, action) => {
    switch (action.type) {
      case LIST_JADWAL_KERJA_REQUEST:
        return { loading: true };
      case LIST_JADWAL_KERJA_SUCCESS:
        return { loading: false, data: action.payload };
      case LIST_JADWAL_KERJA_FAIL:
        return { loading: false, error: action.payload };
      case LIST_JADWAL_KERJA_RESET:
        return {};
      default:
        return state;
    }
  };

  export const createJadwalKerjaReducer = (state = {}, action) => {
    switch (action.type) {
      case CREATE_JADWAL_KERJA_REQUEST:
        return { loading: true };
      case CREATE_JADWAL_KERJA_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case CREATE_JADWAL_KERJA_FAIL:
        return { loading: false, error: action.payload };
      case CREATE_JADWAL_KERJA_RESET:
        return {};
      default:
        return state;
    }
  };

  export const updateJadwalKerjaReducer = (state = {}, action) => {
    switch (action.type) {
      case UPDATE_JADWAL_KERJA_REQUEST:
        return { loading: true };
      case UPDATE_JADWAL_KERJA_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case UPDATE_JADWAL_KERJA_FAIL:
        return { loading: false, error: action.payload };
      case UPDATE_JADWAL_KERJA_RESET:
        return {};
      default:
        return state;
    }
  };

  export const deleteJadwalKerjaReducer = (state = {}, action) => {
    switch (action.type) {
      case DELETE_JADWAL_KERJA_REQUEST:
        return { loading: true };
      case DELETE_JADWAL_KERJA_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case DELETE_JADWAL_KERJA_FAIL:
        return { loading: false, error: action.payload };
      case DELETE_JADWAL_KERJA_RESET:
        return {};
      default:
        return state;
    }
  };