import {
    LIST_JADWAL_PEGAWAI_REQUEST,
    LIST_JADWAL_PEGAWAI_SUCCESS,
    LIST_JADWAL_PEGAWAI_FAIL,
    LIST_JADWAL_PEGAWAI_RESET,
    CREATE_JADWAL_PEGAWAI_REQUEST,
    CREATE_JADWAL_PEGAWAI_SUCCESS,
    CREATE_JADWAL_PEGAWAI_FAIL,
    CREATE_JADWAL_PEGAWAI_RESET,
    UPDATE_JADWAL_PEGAWAI_REQUEST,
    UPDATE_JADWAL_PEGAWAI_SUCCESS,
    UPDATE_JADWAL_PEGAWAI_FAIL,
    UPDATE_JADWAL_PEGAWAI_RESET,
    DELETE_JADWAL_PEGAWAI_REQUEST,
    DELETE_JADWAL_PEGAWAI_SUCCESS,
    DELETE_JADWAL_PEGAWAI_FAIL,
    DELETE_JADWAL_PEGAWAI_RESET,
  } from "../constants/jadwalPegawaiConstants";


  export const listJadwalPegawaiReducer = (state = {jadwalPegawaiList:{}}, action) => {
    switch (action.type) {
      case LIST_JADWAL_PEGAWAI_REQUEST:
        return { loading: true };
      case LIST_JADWAL_PEGAWAI_SUCCESS:
        return { loading: false, data: action.payload };
      case LIST_JADWAL_PEGAWAI_FAIL:
        return { loading: false, error: action.payload };
      case LIST_JADWAL_PEGAWAI_RESET:
        return {};
      default:
        return state;
    }
  };

  export const createJadwalPegawaiReducer = (state = {}, action) => {
    switch (action.type) {
      case CREATE_JADWAL_PEGAWAI_REQUEST:
        return { loading: true };
      case CREATE_JADWAL_PEGAWAI_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case CREATE_JADWAL_PEGAWAI_FAIL:
        return { loading: false, error: action.payload };
      case CREATE_JADWAL_PEGAWAI_RESET:
        return {};
      default:
        return state;
    }
  };

  export const updateJadwalPegawaiReducer = (state = {}, action) => {
    switch (action.type) {
      case UPDATE_JADWAL_PEGAWAI_REQUEST:
        return { loading: true };
      case UPDATE_JADWAL_PEGAWAI_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case UPDATE_JADWAL_PEGAWAI_FAIL:
        return { loading: false, error: action.payload };
      case UPDATE_JADWAL_PEGAWAI_RESET:
        return {};
      default:
        return state;
    }
  };

  export const deleteJadwalPegawaiReducer = (state = {}, action) => {
    switch (action.type) {
      case DELETE_JADWAL_PEGAWAI_REQUEST:
        return { loading: true };
      case DELETE_JADWAL_PEGAWAI_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case DELETE_JADWAL_PEGAWAI_FAIL:
        return { loading: false, error: action.payload };
      case DELETE_JADWAL_PEGAWAI_RESET:
        return {};
      default:
        return state;
    }
  };