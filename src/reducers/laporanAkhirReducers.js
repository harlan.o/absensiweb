import {
    LAPORAN_AKHIR_UMUM_REQUEST,
    LAPORAN_AKHIR_UMUM_SUCCESS,
    LAPORAN_AKHIR_UMUM_FAIL,
    LAPORAN_AKHIR_UMUM_RESET,
    LAPORAN_AKHIR_KHUSUS_REQUEST,
    LAPORAN_AKHIR_KHUSUS_SUCCESS,
    LAPORAN_AKHIR_KHUSUS_FAIL,
    LAPORAN_AKHIR_KHUSUS_RESET,
  } from "../constants/laporanAkhirConstants";

  export const listLaporanAkhirUmumReducers = (state = {laporanList:{}}, action) => {
    switch (action.type) {
      case LAPORAN_AKHIR_UMUM_REQUEST:
        return { loading: true };
      case LAPORAN_AKHIR_UMUM_SUCCESS:
        return { loading: false, data: action.payload };
      case LAPORAN_AKHIR_UMUM_FAIL:
        return { loading: false, error: action.payload };
      case LAPORAN_AKHIR_UMUM_RESET:
        return {};
      default:
        return state;
    }
  };

  export const listLaporanAkhirKhususReducers = (state = {laporanList:{}}, action) => {
    switch (action.type) {
      case LAPORAN_AKHIR_KHUSUS_REQUEST:
        return { loading: true };
      case LAPORAN_AKHIR_KHUSUS_SUCCESS:
        return { loading: false, data: action.payload };
      case LAPORAN_AKHIR_KHUSUS_FAIL:
        return { loading: false, error: action.payload };
      case LAPORAN_AKHIR_KHUSUS_RESET:
        return {};
      default:
        return state;
    }
  };