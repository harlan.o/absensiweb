import {
    PENG_LIST_PEGAWAI_REQUEST,
    PENG_LIST_PEGAWAI_SUCCESS,
    PENG_LIST_PEGAWAI_FAIL,
    PENG_LIST_PEGAWAI_RESET,
    SIMPLE_LIST_PEGAWAI_REQUEST,
    SIMPLE_LIST_PEGAWAI_SUCCESS,
    SIMPLE_LIST_PEGAWAI_FAIL,
    SIMPLE_LIST_PEGAWAI_RESET,
    PENG_CREATE_PEGAWAI_REQUEST,
    PENG_CREATE_PEGAWAI_SUCCESS,
    PENG_CREATE_PEGAWAI_FAIL,
    PENG_CREATE_PEGAWAI_RESET,
    PENG_UPDATE_PEGAWAI_REQUEST,
    PENG_UPDATE_PEGAWAI_SUCCESS,
    PENG_UPDATE_PEGAWAI_FAIL,
    PENG_UPDATE_PEGAWAI_RESET,
    PENG_RESET_PASSWORD_PEGAWAI_REQUEST,
    PENG_RESET_PASSWORD_PEGAWAI_SUCCESS,
    PENG_RESET_PASSWORD_PEGAWAI_FAIL,
    PENG_RESET_PASSWORD_PEGAWAI_RESET,
    PENG_RESET_DEVICE_ID_PEGAWAI_REQUEST,
    PENG_RESET_DEVICE_ID_PEGAWAI_SUCCESS,
    PENG_RESET_DEVICE_ID_PEGAWAI_FAIL,
    PENG_RESET_DEVICE_ID_PEGAWAI_RESET,
  } from "../constants/pegawaiConstants";

  export const listPengPegawaiReducer = (state = {unitIndukList:{}}, action) => {
    switch (action.type) {
      case PENG_LIST_PEGAWAI_REQUEST:
        return { loading: true };
      case PENG_LIST_PEGAWAI_SUCCESS:
        return { loading: false, data: action.payload };
      case PENG_LIST_PEGAWAI_FAIL:
        return { loading: false, error: action.payload };
      case PENG_LIST_PEGAWAI_RESET:
        return {};
      default:
        return state;
    }
  };

  export const simpleListPengPegawaiReducer = (state = {pegawaiList:{}}, action) => {
    switch (action.type) {
      case SIMPLE_LIST_PEGAWAI_REQUEST:
        return { loading: true };
      case SIMPLE_LIST_PEGAWAI_SUCCESS:
        return { loading: false, data: action.payload };
      case SIMPLE_LIST_PEGAWAI_FAIL:
        return { loading: false, error: action.payload };
      case SIMPLE_LIST_PEGAWAI_RESET:
        return {};
      default:
        return state;
    }
  };

  export const createPegawaiReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_CREATE_PEGAWAI_REQUEST:
        return { loading: true };
      case PENG_CREATE_PEGAWAI_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_CREATE_PEGAWAI_FAIL:
        return { loading: false, error: action.payload };
      case PENG_CREATE_PEGAWAI_RESET:
        return {};
      default:
        return state;
    }
  };


  export const updatePegawaiReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_UPDATE_PEGAWAI_REQUEST:
        return { loading: true };
      case PENG_UPDATE_PEGAWAI_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_UPDATE_PEGAWAI_FAIL:
        return { loading: false, error: action.payload };
      case PENG_UPDATE_PEGAWAI_RESET:
        return {};
      default:
        return state;
    }
  };

  export const resetPasswordPegawaiReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_RESET_PASSWORD_PEGAWAI_REQUEST:
        return { loading: true };
      case PENG_RESET_PASSWORD_PEGAWAI_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_RESET_PASSWORD_PEGAWAI_FAIL:
        return { loading: false, success:false, error: action.payload };
      case PENG_RESET_PASSWORD_PEGAWAI_RESET:
        return {};
      default:
        return state;
    }
  };

  export const resetDeviceIdPegawaiReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_RESET_DEVICE_ID_PEGAWAI_REQUEST:
        return { loading: true };
      case PENG_RESET_DEVICE_ID_PEGAWAI_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_RESET_DEVICE_ID_PEGAWAI_FAIL:
        return { loading: false, success:false, error: action.payload };
      case PENG_RESET_DEVICE_ID_PEGAWAI_RESET:
        return {};
      default:
        return state;
    }
  };