import {
    LIST_PENGAMBILAN_IJIN_REQUEST,
    LIST_PENGAMBILAN_IJIN_SUCCESS,
    LIST_PENGAMBILAN_IJIN_FAIL,
    LIST_PENGAMBILAN_IJIN_RESET,
    CREATE_PENGAMBILAN_IJIN_REQUEST,
    CREATE_PENGAMBILAN_IJIN_SUCCESS,
    CREATE_PENGAMBILAN_IJIN_FAIL,
    CREATE_PENGAMBILAN_IJIN_RESET,
    DELETE_PENGAMBILAN_IJIN_REQUEST,
    DELETE_PENGAMBILAN_IJIN_SUCCESS,
    DELETE_PENGAMBILAN_IJIN_FAIL,
    DELETE_PENGAMBILAN_IJIN_RESET,
    LIST_JENIS_IJIN_REQUEST,
    LIST_JENIS_IJIN_SUCCESS,
    LIST_JENIS_IJIN_FAIL,
    LIST_JENIS_IJIN_RESET,
  } from "../constants/pengambilanIjinConstants";


  export const listPengambilanIjinReducer = (state = {pengambilanIjinList:{}}, action) => {
    switch (action.type) {
      case LIST_PENGAMBILAN_IJIN_REQUEST:
        return { loading: true };
      case LIST_PENGAMBILAN_IJIN_SUCCESS:
        return { loading: false, data: action.payload };
      case LIST_PENGAMBILAN_IJIN_FAIL:
        return { loading: false, error: action.payload };
      case LIST_PENGAMBILAN_IJIN_RESET:
        return {};
      default:
        return state;
    }
  };

  export const createPengambilanIjinReducer = (state = {}, action) => {
    switch (action.type) {
      case CREATE_PENGAMBILAN_IJIN_REQUEST:
        return { loading: true };
      case CREATE_PENGAMBILAN_IJIN_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case CREATE_PENGAMBILAN_IJIN_FAIL:
        return { loading: false, success:false, error: action.payload };
      case CREATE_PENGAMBILAN_IJIN_RESET:
        return {};
      default:
        return state;
    }
  };

//   export const updatePengambilanIjinReducer = (state = {}, action) => {
//     switch (action.type) {
//       case UPDATE_PENGAMBILAN_IJIN_REQUEST:
//         return { loading: true };
//       case UPDATE_PENGAMBILAN_IJIN_SUCCESS:
//         return { loading: false, success:true, data: action.payload };
//       case UPDATE_PENGAMBILAN_IJIN_FAIL:
//         return { loading: false, error: action.payload };
//       case UPDATE_PENGAMBILAN_IJIN_RESET:
//         return {};
//       default:
//         return state;
//     }
//   };

  export const deletePengambilanIjinReducer = (state = {}, action) => {
    switch (action.type) {
      case DELETE_PENGAMBILAN_IJIN_REQUEST:
        return { loading: true };
      case DELETE_PENGAMBILAN_IJIN_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case DELETE_PENGAMBILAN_IJIN_FAIL:
        return { loading: false, success:false, error: action.payload };
      case DELETE_PENGAMBILAN_IJIN_RESET:
        return {};
      default:
        return state;
    }
  };

  export const listJenisIjinReducer = (state = {jenisIjinList:{}}, action) => {
    switch (action.type) {
      case LIST_JENIS_IJIN_REQUEST:
        return { loading: true };
      case LIST_JENIS_IJIN_SUCCESS:
        return { loading: false, data: action.payload };
      case LIST_JENIS_IJIN_FAIL:
        return { loading: false, error: action.payload };
      case LIST_JENIS_IJIN_RESET:
        return {};
      default:
        return state;
    }
  };