import {
    LIST_MELAKSANAKAN_TUGAS_REQUEST,
    LIST_MELAKSANAKAN_TUGAS_SUCCESS,
    LIST_MELAKSANAKAN_TUGAS_FAIL,
    LIST_MELAKSANAKAN_TUGAS_RESET,
    CREATE_MELAKSANAKAN_TUGAS_REQUEST,
    CREATE_MELAKSANAKAN_TUGAS_SUCCESS,
    CREATE_MELAKSANAKAN_TUGAS_FAIL,
    CREATE_MELAKSANAKAN_TUGAS_RESET,
    UPDATE_MELAKSANAKAN_TUGAS_REQUEST,
    UPDATE_MELAKSANAKAN_TUGAS_SUCCESS,
    UPDATE_MELAKSANAKAN_TUGAS_FAIL,
    UPDATE_MELAKSANAKAN_TUGAS_RESET,
    DELETE_MELAKSANAKAN_TUGAS_REQUEST,
    DELETE_MELAKSANAKAN_TUGAS_SUCCESS,
    DELETE_MELAKSANAKAN_TUGAS_FAIL,
    DELETE_MELAKSANAKAN_TUGAS_RESET,
    LIST_TUGAS_LUAR_REQUEST,
    LIST_TUGAS_LUAR_SUCCESS,
    LIST_TUGAS_LUAR_FAIL,
    LIST_TUGAS_LUAR_RESET,
    CREATE_TUGAS_LUAR_REQUEST,
    CREATE_TUGAS_LUAR_SUCCESS,
    CREATE_TUGAS_LUAR_FAIL,
    CREATE_TUGAS_LUAR_RESET,
    UPDATE_TUGAS_LUAR_REQUEST,
    UPDATE_TUGAS_LUAR_SUCCESS,
    UPDATE_TUGAS_LUAR_FAIL,
    UPDATE_TUGAS_LUAR_RESET,
    DELETE_TUGAS_LUAR_REQUEST,
    DELETE_TUGAS_LUAR_SUCCESS,
    DELETE_TUGAS_LUAR_FAIL,
    DELETE_TUGAS_LUAR_RESET,
} from "../constants/penugasanConstants";

export const listMelaksanakanTugasReducer= (state = {melaksanakanTugasList:{}}, action) => {
    switch (action.type) {
        case LIST_MELAKSANAKAN_TUGAS_REQUEST:
          return { loading: true };
        case LIST_MELAKSANAKAN_TUGAS_SUCCESS:
          return { loading: false, data: action.payload };
        case LIST_MELAKSANAKAN_TUGAS_FAIL:
          return { loading: false, error: action.payload };
        case LIST_MELAKSANAKAN_TUGAS_RESET:
          return {};
        default:
          return state;
      }
}

export const createMelaksanakanTugasReducer= (state = {}, action) => {
    switch (action.type) {
        case CREATE_MELAKSANAKAN_TUGAS_REQUEST:
          return { loading: true };
        case CREATE_MELAKSANAKAN_TUGAS_SUCCESS:
          return { loading: false, success:true, data: action.payload };
        case CREATE_MELAKSANAKAN_TUGAS_FAIL:
          return { loading: false, success:false, error: action.payload };
        case CREATE_MELAKSANAKAN_TUGAS_RESET:
          return {};
        default:
          return state;
      }
}

export const updateMelaksanakanTugasReducer= (state = {}, action) => {
    switch (action.type) {
        case UPDATE_MELAKSANAKAN_TUGAS_REQUEST:
          return { loading: true };
        case UPDATE_MELAKSANAKAN_TUGAS_SUCCESS:
          return { loading: false, success:true, data: action.payload };
        case UPDATE_MELAKSANAKAN_TUGAS_FAIL:
          return { loading: false, success:false, error: action.payload };
        case UPDATE_MELAKSANAKAN_TUGAS_RESET:
          return {};
        default:
          return state;
      }
}

export const deleteMelaksanakanTugasReducer= (state = {}, action) => {
    switch (action.type) {
        case DELETE_MELAKSANAKAN_TUGAS_REQUEST:
          return { loading: true };
        case DELETE_MELAKSANAKAN_TUGAS_SUCCESS:
          return { loading: false, success:true, data: action.payload };
        case DELETE_MELAKSANAKAN_TUGAS_FAIL:
          return { loading: false, success:false, error: action.payload };
        case DELETE_MELAKSANAKAN_TUGAS_RESET:
          return {};
        default:
          return state;
      }
}

export const listTugasLuarReducer= (state = {tugasLuarList:{}}, action) => {
    switch (action.type) {
        case LIST_TUGAS_LUAR_REQUEST:
          return { loading: true };
        case LIST_TUGAS_LUAR_SUCCESS:
          return { loading: false, data: action.payload };
        case LIST_TUGAS_LUAR_FAIL:
          return { loading: false, error: action.payload };
        case LIST_TUGAS_LUAR_RESET:
          return {};
        default:
          return state;
      }
}

export const createTugasLuarReducer= (state = {}, action) => {
    switch (action.type) {
        case CREATE_TUGAS_LUAR_REQUEST:
          return { loading: true };
        case CREATE_TUGAS_LUAR_SUCCESS:
          return { loading: false, success:true, data: action.payload };
        case CREATE_TUGAS_LUAR_FAIL:
          return { loading: false, success:false, error: action.payload };
        case CREATE_TUGAS_LUAR_RESET:
          return {};
        default:
          return state;
      }
}

export const updateTugasLuarReducer= (state = {}, action) => {
    switch (action.type) {
        case UPDATE_TUGAS_LUAR_REQUEST:
          return { loading: true };
        case UPDATE_TUGAS_LUAR_SUCCESS:
          return { loading: false, success:true, data: action.payload };
        case UPDATE_TUGAS_LUAR_FAIL:
          return { loading: false, success:false, error: action.payload };
        case UPDATE_TUGAS_LUAR_RESET:
          return {};
        default:
          return state;
      }
}

export const deleteTugasLuarReducer= (state = {}, action) => {
    switch (action.type) {
        case DELETE_TUGAS_LUAR_REQUEST:
          return { loading: true };
        case DELETE_TUGAS_LUAR_SUCCESS:
          return { loading: false, success:true, data: action.payload };
        case DELETE_TUGAS_LUAR_FAIL:
          return { loading: false, success:false, error: action.payload };
        case DELETE_TUGAS_LUAR_RESET:
          return {};
        default:
          return state;
      }
}