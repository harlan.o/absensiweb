import {
    PENG_LIST_TPP_REQUEST,
    PENG_LIST_TPP_SUCCESS,
    PENG_LIST_TPP_FAIL,
    PENG_LIST_TPP_RESET,
    PENG_CREATE_TPP_REQUEST,
    PENG_CREATE_TPP_SUCCESS,
    PENG_CREATE_TPP_FAIL,
    PENG_CREATE_TPP_RESET,
    PENG_UPDATE_TPP_REQUEST,
    PENG_UPDATE_TPP_SUCCESS,
    PENG_UPDATE_TPP_FAIL,
    PENG_UPDATE_TPP_RESET,
    LIST_TPP_HISTORY_REQUEST,
    LIST_TPP_HISTORY_SUCCESS,
    LIST_TPP_HISTORY_FAIL,
    LIST_TPP_HISTORY_RESET,
    LIST_TPP_PEGAWAI_HISTORY_REQUEST,
    LIST_TPP_PEGAWAI_HISTORY_SUCCESS,
    LIST_TPP_PEGAWAI_HISTORY_FAIL,
    LIST_TPP_PEGAWAI_HISTORY_RESET,
    REVISI_TPP_HISTORY_REQUEST,
    REVISI_TPP_HISTORY_SUCCESS,
    REVISI_TPP_HISTORY_FAIL,
    REVISI_TPP_HISTORY_RESET
  } from "../constants/tppConstants";

  export const listPengTppReducer = (state = {tppList:{}}, action) => {
    switch (action.type) {
      case PENG_LIST_TPP_REQUEST:
        return { loading: true };
      case PENG_LIST_TPP_SUCCESS:
        return { loading: false, data: action.payload };
      case PENG_LIST_TPP_FAIL:
        return { loading: false, error: action.payload };
      case PENG_LIST_TPP_RESET:
        return {};
      default:
        return state;
    }
  };

  export const createTppReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_CREATE_TPP_REQUEST:
        return { loading: true };
      case PENG_CREATE_TPP_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_CREATE_TPP_FAIL:
        return { loading: false, error: action.payload };
      case PENG_CREATE_TPP_RESET:
        return {};
      default:
        return state;
    }
  };


  export const updateTppReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_UPDATE_TPP_REQUEST:
        return { loading: true };
      case PENG_UPDATE_TPP_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_UPDATE_TPP_FAIL:
        return { loading: false, success:false, error: action.payload };
      case PENG_UPDATE_TPP_RESET:
        return {};
      default:
        return state;
    }
  };

  export const listHistoryTppReducer = (state = {historyTppList:{}}, action) => {
    switch (action.type) {
      case LIST_TPP_HISTORY_REQUEST:
        return { loading: true };
      case LIST_TPP_HISTORY_SUCCESS:
        return { loading: false, data: action.payload };
      case LIST_TPP_HISTORY_FAIL:
        return { loading: false, error: action.payload };
      case LIST_TPP_HISTORY_RESET:
        return {};
      default:
        return state;
    }
  };

  export const listHistoryPegawaiTppReducer = (state = {historyTppList:{}}, action) => {
    switch (action.type) {
      case LIST_TPP_PEGAWAI_HISTORY_REQUEST:
        return { loading: true };
      case LIST_TPP_PEGAWAI_HISTORY_SUCCESS:
        return { loading: false, data: action.payload };
      case LIST_TPP_PEGAWAI_HISTORY_FAIL:
        return { loading: false, error: action.payload };
      case LIST_TPP_PEGAWAI_HISTORY_RESET:
        return {};
      default:
        return state;
    }
  };

  export const revisiHistoryTppReducer = (state = {}, action)=> {
    switch (action.type) {
      case REVISI_TPP_HISTORY_REQUEST:
        return { loading: true };
      case REVISI_TPP_HISTORY_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case REVISI_TPP_HISTORY_FAIL:
        return { loading: false, error: action.payload };
      case REVISI_TPP_HISTORY_RESET:
        return {};
      default:
        return state;
    }
  };