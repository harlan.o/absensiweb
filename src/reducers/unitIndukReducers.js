import {
    PENG_LIST_UNIT_INDUK_REQUEST,
    PENG_LIST_UNIT_INDUK_SUCCESS,
    PENG_LIST_UNIT_INDUK_FAIL,
    PENG_LIST_UNIT_INDUK_RESET,
    PENG_CREATE_UNIT_INDUK_REQUEST,
    PENG_CREATE_UNIT_INDUK_SUCCESS,
    PENG_CREATE_UNIT_INDUK_FAIL,
    PENG_CREATE_UNIT_INDUK_RESET,
    PENG_UPDATE_UNIT_INDUK_REQUEST,
    PENG_UPDATE_UNIT_INDUK_SUCCESS,
    PENG_UPDATE_UNIT_INDUK_FAIL,
    PENG_UPDATE_UNIT_INDUK_RESET,
  } from "../constants/unitIndukConstants";

  export const listPengUnitIndukReducer = (state = {unitIndukList:{}}, action) => {
    switch (action.type) {
      case PENG_LIST_UNIT_INDUK_REQUEST:
        return { loading: true };
      case PENG_LIST_UNIT_INDUK_SUCCESS:
        return { loading: false, data: action.payload };
      case PENG_LIST_UNIT_INDUK_FAIL:
        return { loading: false, error: action.payload };
      case PENG_LIST_UNIT_INDUK_RESET:
        return {};
      default:
        return state;
    }
  };

  export const createUnitIndukReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_CREATE_UNIT_INDUK_REQUEST:
        return { loading: true };
      case PENG_CREATE_UNIT_INDUK_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_CREATE_UNIT_INDUK_FAIL:
        return { loading: false, error: action.payload };
      case PENG_CREATE_UNIT_INDUK_RESET:
        return {};
      default:
        return state;
    }
  };


  export const updateUnitIndukReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_UPDATE_UNIT_INDUK_REQUEST:
        return { loading: true };
      case PENG_UPDATE_UNIT_INDUK_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_UPDATE_UNIT_INDUK_FAIL:
        return { loading: false, error: action.payload };
      case PENG_UPDATE_UNIT_INDUK_RESET:
        return {};
      default:
        return state;
    }
  };
