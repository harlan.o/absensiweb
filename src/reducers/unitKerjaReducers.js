import {
    PENG_LIST_UNIT_KERJA_REQUEST,
    PENG_LIST_UNIT_KERJA_SUCCESS,
    PENG_LIST_UNIT_KERJA_FAIL,
    PENG_LIST_UNIT_KERJA_RESET,
    PENG_CREATE_UNIT_KERJA_REQUEST,
    PENG_CREATE_UNIT_KERJA_SUCCESS,
    PENG_CREATE_UNIT_KERJA_FAIL,
    PENG_CREATE_UNIT_KERJA_RESET,
    PENG_UPDATE_UNIT_KERJA_REQUEST,
    PENG_UPDATE_UNIT_KERJA_SUCCESS,
    PENG_UPDATE_UNIT_KERJA_FAIL,
    PENG_UPDATE_UNIT_KERJA_RESET,
  } from "../constants/unitKerjaConstants";

  export const listPengUnitKerjaReducer = (state = {unitIndukList:{}}, action) => {
    switch (action.type) {
      case PENG_LIST_UNIT_KERJA_REQUEST:
        return { loading: true };
      case PENG_LIST_UNIT_KERJA_SUCCESS:
        return { loading: false, data: action.payload };
      case PENG_LIST_UNIT_KERJA_FAIL:
        return { loading: false, error: action.payload };
      case PENG_LIST_UNIT_KERJA_RESET:
        return {};
      default:
        return state;
    }
  };

  export const createUnitKerjaReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_CREATE_UNIT_KERJA_REQUEST:
        return { loading: true };
      case PENG_CREATE_UNIT_KERJA_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_CREATE_UNIT_KERJA_FAIL:
        return { loading: false, error: action.payload };
      case PENG_CREATE_UNIT_KERJA_RESET:
        return {};
      default:
        return state;
    }
  };


  export const updateUnitKerjaReducer = (state = {}, action) => {
    switch (action.type) {
      case PENG_UPDATE_UNIT_KERJA_REQUEST:
        return { loading: true };
      case PENG_UPDATE_UNIT_KERJA_SUCCESS:
        return { loading: false, success:true, data: action.payload };
      case PENG_UPDATE_UNIT_KERJA_FAIL:
        return { loading: false, error: action.payload };
      case PENG_UPDATE_UNIT_KERJA_RESET:
        return {};
      default:
        return state;
    }
  };
