import {
    USER_LOGIN_FAIL,
    USER_LOGIN_REQUEST,
    USER_LOGIN_SUCCESS,
    USER_LOGOUT,
    USER_DATA_PROFIL_REQUEST,
    USER_DATA_PROFIL_SUCCESS,
    USER_DATA_PROFIL_FAIL,
    USER_DATA_PROFIL_RESET,

  } from "../constants/userConstants";
  
  export const userLoginReducer = (state = {}, action) => {
    
    switch (action.type) {
      case USER_LOGIN_REQUEST:
        return { loading: true };
      case USER_LOGIN_SUCCESS:
        return { loading: false, token: action.payload };
      case USER_LOGIN_FAIL:
        return { loading: false, error: action.payload };
      case USER_LOGOUT:
        return {};
      default:
        return state;
    }
  };
  
  
  export const userDataProfileReducer = (state = { profileUser:{} }, action) => {
    switch (action.type) {
      case USER_DATA_PROFIL_REQUEST:
        return { loading: true };
      case USER_DATA_PROFIL_SUCCESS:
        return { loading: false, profileUser: action.payload };
      case USER_DATA_PROFIL_FAIL:
        return { loading: false, error: action.payload };
      case USER_DATA_PROFIL_RESET:
        return {};
      default:
        return state;
    }
  };
  
  // export const userRegisterReducer = (state = {}, action) => {
  //   switch (action.type) {
  //     case USER_REGISTER_REQUEST:
  //       return { loading: true };
  //     case USER_REGISTER_SUCCESS:
  //       return { loading: false, status: action.payload };
  //     case USER_REGISTER_FAIL:
  //       return { loading: false, status: action.payload };
  //     case USER_REGISTER_STATUS_RESET:
  //       return { loading: false, status: null };
  //     default:
  //       return state;
  //   }
  // };
  