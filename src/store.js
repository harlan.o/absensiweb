import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';
import { userLoginReducer, userDataProfileReducer } from './reducers/userReducers'
import { listPengUnitIndukReducer, createUnitIndukReducer, updateUnitIndukReducer } from './reducers/unitIndukReducers';
import { listPengUnitKerjaReducer, createUnitKerjaReducer, updateUnitKerjaReducer } from './reducers/unitKerjaReducers';
import { listPengPegawaiReducer, simpleListPengPegawaiReducer, createPegawaiReducer, updatePegawaiReducer, resetPasswordPegawaiReducer, resetDeviceIdPegawaiReducer } from './reducers/pegawaiReducers';
import { listPengTppReducer, createTppReducer, updateTppReducer, listHistoryTppReducer, listHistoryPegawaiTppReducer, revisiHistoryTppReducer} from './reducers/tppReducers';
import { listAbsensiIndukReducer, listAbsensiKerjaReducer , listAbsensiDashboardReducer, laporanAbsensiReducer} from './reducers/absensiReducers';
import {listPengArtikelReducer, createArtikelReducer, updateArtikelReducer, deleteArtikelReducer, pushNotificationArtikelReducer} from './reducers/artikelReducers';
import {listJadwalKerjaReducer, createJadwalKerjaReducer, updateJadwalKerjaReducer, deleteJadwalKerjaReducer} from './reducers/jadwalKerjaReducers';
import {listJadwalPegawaiReducer, createJadwalPegawaiReducer, updateJadwalPegawaiReducer, deleteJadwalPegawaiReducer} from './reducers/jadwalPegawaiReducers';
import {listHariLiburReducer, createHariLiburReducer, deleteHariLiburReducer} from './reducers/hariLiburReducers';
import {listPengEvenReducer, createEvenReducer, updateEvenReducer, deleteEvenReducer, pushNotificationEvenReducer} from './reducers/evenReducers';
import {listLokasiKotaReducer} from './reducers/_lokasiReducers';
import {listLaporanAkhirKhususReducers, listLaporanAkhirUmumReducers} from './reducers/laporanAkhirReducers';
import {listMelaksanakanTugasReducer, createMelaksanakanTugasReducer, updateMelaksanakanTugasReducer, deleteMelaksanakanTugasReducer, listTugasLuarReducer, createTugasLuarReducer, updateTugasLuarReducer, deleteTugasLuarReducer} from './reducers/penugasanReducers';
import {listPengambilanIjinReducer, createPengambilanIjinReducer, deletePengambilanIjinReducer, listJenisIjinReducer} from './reducers/pengambilanIjinReducers';
import {hakAksesReducers} from './reducers/hakAksesReducers';
import {listAbsensiPegawaiReducer} from './reducers/absensiPegawaiReducer';
import { decry, decryHA } from './actions/genneralAction';

const reducer = combineReducers({
  userLogin: userLoginReducer,
  hakAkses: hakAksesReducers,
  userDataProfile: userDataProfileReducer,
  listPengUnitInduk: listPengUnitIndukReducer,
  listPengUnitKerja: listPengUnitKerjaReducer,
  infoCreateUnitKerja: createUnitKerjaReducer,
  infoUpdateUnitKerja: updateUnitKerjaReducer,
  infoCreateUnitInduk: createUnitIndukReducer,
  infoUpdateUnitInduk: updateUnitIndukReducer,
  listAbsensiInduk: listAbsensiIndukReducer,
  listAbsensiKerja: listAbsensiKerjaReducer,
  listAbsensiDashboard:listAbsensiDashboardReducer,
  listAbsensiPegawai:listAbsensiPegawaiReducer,
  laporanAbsensi:laporanAbsensiReducer,
  listPengPegawai: listPengPegawaiReducer,
  simpleListPengPegawai:simpleListPengPegawaiReducer,
  infoCreatePegawai:createPegawaiReducer,
  infoUpdatePegawai:updatePegawaiReducer,
  infoResetPasswordPegawai:resetPasswordPegawaiReducer,
  infoResetDeviceIdPegawai:resetDeviceIdPegawaiReducer,
  listPengTpp: listPengTppReducer,
  listHistoryPegawaiTpp:listHistoryPegawaiTppReducer,
  infoRevisiHistoryTpp:revisiHistoryTppReducer,
  infoCreateTpp:createTppReducer,
  infoUpdateTpp:updateTppReducer,
  listHistoryTpp:listHistoryTppReducer,
  listArtikel:listPengArtikelReducer,
  infoCreateArtikel:createArtikelReducer,
  infoUpdateArtikel:updateArtikelReducer,
  infoDeleteArtikel:deleteArtikelReducer,
  pushNotificationArtikel:pushNotificationArtikelReducer,
  listJadwalKerja:listJadwalKerjaReducer,
  infoCreateJadwalKerja:createJadwalKerjaReducer,
  infoUpdateJadwalKerja:updateJadwalKerjaReducer,
  infoDeleteJadwalKerja:deleteJadwalKerjaReducer,
  listJadwalPegawai:listJadwalPegawaiReducer,
  infoCreateJadwalPegawai:createJadwalPegawaiReducer,
  infoUpdateJadwalPegawai:updateJadwalPegawaiReducer,
  infoDeleteJadwalPegawai:deleteJadwalPegawaiReducer,
  listHariLibur:listHariLiburReducer,
  infoCreateHariLibur:createHariLiburReducer,
  infoDeleteHariLibur:deleteHariLiburReducer,
  listEven:listPengEvenReducer,
  infoCreateEven:createEvenReducer,
  infoUpdateEven:updateEvenReducer,
  infoDeleteEven:deleteEvenReducer,
  pushNotificationEven:pushNotificationEvenReducer,
  listLokasiKota:listLokasiKotaReducer,
  listMelaksanakanTugas:listMelaksanakanTugasReducer,
  infoCreateMelaksanakanTugas:createMelaksanakanTugasReducer,
  infoUpdateMelaksanakanTugas:updateMelaksanakanTugasReducer,
  infoDeleteMelaksanakanTugas:deleteMelaksanakanTugasReducer,
  listTugasLuar:listTugasLuarReducer,
  infoCreateTugasLuar:createTugasLuarReducer,
  infoUpdateTugasLuar:updateTugasLuarReducer,
  infoDeleteTugasLuar:deleteTugasLuarReducer,
  listPengambilanIjin:listPengambilanIjinReducer,
  infoCreatePengambilanIjin:createPengambilanIjinReducer,
  infoDeletePengambilanIjin:deletePengambilanIjinReducer,
  listJenisIjin:listJenisIjinReducer,
  listLaporanAkhirUmum:listLaporanAkhirUmumReducers,
  listLaporanAkhirKhusus:listLaporanAkhirKhususReducers,
});


const userInfoFromStorage = localStorage.getItem('token') ? localStorage.getItem('token') : null;

const initialState = {
  userLogin: { token: userInfoFromStorage},  
};

const middleware = [thunk];

const store = createStore(
  reducer,
  initialState,
  composeWithDevTools(applyMiddleware(...middleware))
);

export default store;