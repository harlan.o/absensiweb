import React, { Component } from 'react';
import { Button, Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import BreadcrumbUser from '../../components/BreadcrumbUser';
import DataTableAbsensiPegawai from '../../components/DataTableAbsensiPegawai';
import FilterAbsensi from '../../components/FilterShowData1';



// import TabelAbsensiPegawai from '../../components/TabelAbsensiPegawai';

// const products = [
//     {"no":"1","id":"1223231233", "name":"Harlan","dinas":"Dinas Pendapatan Daerah", "group":"Divisi Humas", "tipe_absen":"Kdeatangan", "waktu":"10.05" },
//     {"no":"2","id":"1223231234", "name":"Harlan","dinas":"Dinas Pendapatan Daerah", "group":"Divisi Humas", "tipe_absen":"Kdeatangan", "waktu":"10.05" }
// ];
// const columns = [{
//   dataField: 'no',
//   text: 'No',
//   sort: true
// }, {
//     dataField: 'id',
//     text: 'NIP'
//   }, {
//   dataField: 'name',
//   text: 'Nama'
// },{
//     dataField: 'dinas',
//     text: 'Dinas'
// },{
//     dataField: 'group',
//     text: 'Group'
// }, {
//   dataField: 'tipe_absen',
//   text: 'Tipe'
// },{
//     dataField: 'waktu',
//     text: 'Waktu'
// },{
//     // terlambat, ijin
//     dataField: 'Keterangan',
//     text: 'keterangan'
//   }];

// const tableIcons = {
//     Add: forwardRef((props, ref) => <AddBox {...props} ref={ref} />),
//     Check: forwardRef((props, ref) => <Check {...props} ref={ref} />),
//     Clear: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
//     Delete: forwardRef((props, ref) => <DeleteOutline {...props} ref={ref} />),
//     DetailPanel: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
//     Edit: forwardRef((props, ref) => <Edit {...props} ref={ref} />),
//     Export: forwardRef((props, ref) => <SaveAlt {...props} ref={ref} />),
//     Filter: forwardRef((props, ref) => <FilterList {...props} ref={ref} />),
//     FirstPage: forwardRef((props, ref) => <FirstPage {...props} ref={ref} />),
//     LastPage: forwardRef((props, ref) => <LastPage {...props} ref={ref} />),
//     NextPage: forwardRef((props, ref) => <ChevronRight {...props} ref={ref} />),
//     PreviousPage: forwardRef((props, ref) => <ChevronLeft {...props} ref={ref} />),
//     ResetSearch: forwardRef((props, ref) => <Clear {...props} ref={ref} />),
//     Search: forwardRef((props, ref) => <Search {...props} ref={ref} />),
//     SortArrow: forwardRef((props, ref) => <ArrowDownward {...props} ref={ref} />),
//     ThirdStateCheck: forwardRef((props, ref) => <Remove {...props} ref={ref} />),
//     ViewColumn: forwardRef((props, ref) => <ViewColumn {...props} ref={ref} />)
//   };
class AbsensiPegawai extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            listBreadcrumbUser: [
            { path: "/absensi-pegawai", name: "Absensi Pegawai", status:true },
            ],
        };
    }

    componentDidMount(){

    }
    render(){
        return (
        <div className="container-fluid">
            <h1 className="mt-4">Absensi Pegawai</h1>
            <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />            
            <FilterShowData1/>
            <Nav fill variant="pills" className="nav3-dark" >
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/absensi-pegawai/hadir" href="/absensi-pegawai/hadir">HADIR</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/absensi-pegawai/ijin" href="/absensi-pegawai/ijin">IJIN</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/absensi-pegawai/belum" href="/absensi-pegawai/belum">BELUM DIKETAHUI</Nav.Link>
                    </Nav.Item>
                   
            </Nav>
            <DataTableAbsensiPegawai></DataTableAbsensiPegawai>
        </div>
        );
    }

}

export default AbsensiPegawai;