import React, { Component } from 'react';
import { Card, Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import BreadcrumbUser from '../../components/BreadcrumbUser';
import FilterSingleDateMultiSuperUnit from '../../components/filter/FilterSingleDateMultiSuperUnit';
import ComingSoon from '../page-coming-soon/ComingSoon';
import Hadir from './absensi-hadir/Hadir';
import Cuti from './absensi-cuti/Cuti';
import Ijin from './absensi-ijin/Ijin';
import Sakit from './absensi-sakit/Sakit';
import TanpaKeterangan from './absensi-tanpa-keterangan/TanpaKeterangan';

import {connect} from 'react-redux';
import {getListAbsenInduk, getListAbsenIndukAll} from "../../actions/absensiAction";
import { tanggalHariIndo } from '../../actions/genneralAction';
import { uIIDP } from '../../actions/userActions';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { getListAbseniPegawai } from '../../actions/absensiPegawaiAction';
import Absen from './absensi-absen/Absen';
import FilterSingleDateMultiSuperUnitKerja from '../../components/filter/FilterSingleDateMultiSuperUnitKerja';
import CetakAbsensiPegawaiHadir  from '../../components/laporan/CetakAbsensiPegawaiHadir';
import CetakAbsensiPegawaiAbsen from '../../components/laporan/CetakAbsensiPegawaiAbsen';
import CetakAbsensiPegawaiTanpaKeterangan from '../../components/laporan/CetakAbsensiPegawaiTanpaKeterangan';
import { REGISTER_FU001, REGISTER_FU010 } from '../../constants/hakAksesConstants';
const moment = require('moment');

class AbsensiPegawai extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            datasMasuk:null,
            datasPulang:null,
            loading:true,
            dataHadir:[],
            dataAlasan:[],
            dataTK:[],

            listBreadcrumbUser: [
            { path: "/absensi-pegawai", name: "Absensi Pegawai", status:true },
            ],
            filterData:{
                kodeUnitInduk: "all",
                kodeUnitKerja: "all",
                
                selectMultyUnitInduk:[],
                selectMultyUnitKerja:[],
                startDate: moment().format('YYYY-MM-DD'),
            }
        };
        this.getDataFilter = this.getDataFilter.bind(this);
        this.renderSubPage = this.renderSubPage.bind(this);
    }
    handlePrint= () => this.componentRef
    componentDidMount(){
        console.log("loading", this.state.loading)
        this.callApi();
        console.log("loading", this.state.loading)
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.totalHadir !== prevProps.totalHadir){
            this.callApi();
        }
        if(this.state.filterData !== prevState.filterData){
            this.callApi();
        }
    }

    render(){

        // let dataOlahanHadir=this.olahDataHadir(this.state.dataHadir);
        let dataOlahan ={
            hadir:this.olahDataHadir(this.state.dataHadir),
            absen:this.state.dataAlasan,
            tanpaKet: this.state.dataTK
        }
        console.log("loading", this.state.loading)
        return (
        <div className="container-fluid">
            {/* <ComingSoonTransparant/> */}
            <h1 className="mt-4">Absensi Pegawai</h1>
            <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />            
            {/* <FilterSingleDateMultiSuperUnit dataFilter={this.getDataFilter} setloading={this.state.loading}/> */}
            {cekHakAksesClient(REGISTER_FU001) ? <FilterSingleDateMultiSuperUnit kodeCetak={REGISTER_FU010} print={this.handlePrint} dataFilter={this.getDataFilter} setloading={this.state.loading} /> : <FilterSingleDateMultiSuperUnitKerja print={this.handlePrint} dataFilter={this.getDataFilter} kodeCetak={REGISTER_FU010} setloading={this.state.loading} />}

            <Nav fill variant="pills" className="nav3-dark" >
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/absensi-pegawai/hadir" href="/absensi-pegawai/hadir">HADIR</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/absensi-pegawai/absen" href="/absensi-pegawai/absen">Absen</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/absensi-pegawai/tanpa-keterangan" href="/absensi-pegawai/tanpa-keterangan">TANPA KET</Nav.Link>
                    </Nav.Item>
                   
            </Nav>
            <Card>
                {this.renderSubPage(this.props.sub_page, dataOlahan)}
            </Card>
        </div>
        );
    }

    SetBreadcrumbChild = (listBreadcrumbChild) =>{
        this.setState({listBreadcrumbUser: listBreadcrumbChild})
    }

    getDataFilter(v) {    
        this.setState(() => ({
            filterData:v,
            loading:true      
          }));  
        
    }
    
    renderSubPage(param, dataOlahan) {
        let hariTgl=tanggalHariIndo(this.state.filterData.startDate);
        switch (param) {
            case 'hadir':
                return (<div>
                        <Hadir breadcurmbChild={this.SetBreadcrumbChild} dataolahan={dataOlahan.hadir} title={hariTgl}/>
                        <div style={{ display: 'none' }}>
                            <CetakAbsensiPegawaiHadir datatable={dataOlahan.hadir} title={hariTgl} ref={(el) => (this.componentRef = el)}/>
                        </div>
                    </div>);
            case 'absen':
                    return (
                    <div>
                        <Absen breadcurmbChild={this.SetBreadcrumbChild} dataolahan={dataOlahan.absen} title={hariTgl}/>
                        <div style={{ display: 'none' }}>
                            <CetakAbsensiPegawaiAbsen datatable={dataOlahan.absen} title={hariTgl} ref={(el) => (this.componentRef = el)}/>
                        </div>
                    </div>
                    );
            case 'tanpa-keterangan':
                return (<div>
                    <TanpaKeterangan breadcurmbChild={this.SetBreadcrumbChild} dataolahan={dataOlahan.tanpaKet} title={hariTgl}/>
                    <div style={{ display: 'none' }}>
                        <CetakAbsensiPegawaiTanpaKeterangan datatable={dataOlahan.tanpaKet} title={hariTgl} ref={(el) => (this.componentRef = el)}/>
                    </div>
                </div>);
            default:
               return (<div>
                    <Hadir breadcurmbChild={this.SetBreadcrumbChild} dataolahan={dataOlahan.hadir} title={hariTgl}/>
                    <div style={{ display: 'none' }}>
                    <CetakAbsensiPegawaiHadir datatable={dataOlahan.hadir} title={hariTgl} ref={(el) => (this.componentRef = el)}/>
                    </div>
                </div>);
        }
    }

    async callApi(){
        console.log(this.state.filterData)
        
        let unitIndukArr = [
            {id:uIIDP()}
        ]
        // let tempTest=all
        console.log("idUnit Induk", uIIDP())
        if(cekHakAksesClient(REGISTER_FU001)){
            unitIndukArr = this.state.filterData?.selectMultyUnitInduk
        }  
        

        await this.props.getListAbseniPegawai(this.state.filterData?.startDate, unitIndukArr, this.state.filterData?.selectMultyUnitKerja).then( ()=> {

            // console.log("data",this.props.listAbsensiPegawai.data)
            // console.log("hadir",this.props.listAbsensiPegawai.data.data.hadirList)
            // console.log("sakitIjinCutiList",this.props.listAbsensiPegawai.data.data.sakitIjinCutiList)
            // console.log("tanpaKeteranganList",this.props.listAbsensiPegawai.data.data.tanpaKeteranganList)


            this.setState({
                dataHadir:this.groupingdatafinal(this.props.listAbsensiPegawai.data?.data?.hadirList),
                dataAlasan:this.groupingdatafinal(this.props.listAbsensiPegawai.data?.data?.sakitIjinCutiList),
                dataTK:this.groupingdatafinal(this.props.listAbsensiPegawai.data?.data?.tanpaKeteranganList),
                loading:false,  
            }) 
        })
    }

    groupingdatafinal = (_datasAll) =>{
        let groupingUnitKerja =this.state.filterData.groupingUnitKerja;
        let datasAll =_datasAll;
        if(groupingUnitKerja != null && groupingUnitKerja.length > 0){
            datasAll=_datasAll.filter((pegawai) => {
                let nilai=0;
                for(let i = 0; i < groupingUnitKerja.length; i++){
                    let [a1]=pegawai.unitKerja.split(" ");
                    let [b1]=groupingUnitKerja[i].split(" ");
                    if (a1 == b1){
                        nilai=1
                    }
                }
                if(nilai == 1){
                    return true
                }
            })
        }
        return datasAll
    }


    olahDataHadir = (daftarHadir) => {
        if(daftarHadir == null){
            return []
        }
        let _data = daftarHadir
        let output = [];
        let data= _data.sort((a, b) => {
            return a.jamAbsen?.localeCompare(b.jamAbsen)
        })
        data.forEach((item) => {
            var existing = output.filter(function(v, i) {
                return v.nip == item.nip;
              });

            if (existing.length) {
                var existingIndex = output.indexOf(existing[0]);
                let value={waktu: item.jamAbsen, type: item.tipeAbsen, keterangan:item.keterangan, geolocation: item.geolocation, fotoAbsen:item.fotoAbsen}
                // let totalKehadiran = output[existingIndex].totalKehadiran
                // let totalTerlambat = output[existingIndex].totalTerlambat           
              if(output[existingIndex].absen){
                output[existingIndex].absen = output[existingIndex].absen.concat(value);
              }
            //   if(item.keterangan === "MASUK_TEPAT_WAKTU"){
            //       output[existingIndex].totalKehadiran=totalKehadiran+1
            //   }
            //   if(item.keterangan === "MASUK_TERLAMBAT"){
            //       output[existingIndex].totalKehadiran=totalKehadiran+1
            //       output[existingIndex].totalTerlambat=totalTerlambat+1
            //   }
            } else {
                item.absen = [{waktu: item.jamAbsen, type: item.tipeAbsen, keterangan:item.keterangan, geolocation: item.geolocation, fotoAbsen:item.fotoAbsen}]
                output.push(item);
            }
        })
        return output
    }


}


const mapStateToProps = (state) => {
    return{
        listAbsensiInduk: state.listAbsensiInduk,
        listAbsensiPegawai: state.listAbsensiPegawai
    } 
  }

export default connect(mapStateToProps, {getListAbsenInduk, getListAbsenIndukAll, getListAbseniPegawai})(AbsensiPegawai);