import React, { Component } from 'react';
import AbsensiPegawaiTabelAbsen from '../../../components/absensi/AbsensiPegawaiTabelAbsen';
import AlertPengembangan from '../../../components/AlertPengembangan';

class Absen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModalEdit:false,
            showModalDelete:false,
            listBreadcrumbUser: [
            { path: "/absensi-pegawai", name: "Absensi Pegawai", status:false },
            { path: "/absensi-pegawai/absen", name: "Absen", status:true },
        ],
        };
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }
    render(){
        return(
            <>
            {/* <AlertPengembangan/> */}
            <AbsensiPegawaiTabelAbsen title={this.props.title} getFilter={this.state.filterData} dataRow={this.props.dataolahan} search="" />
            </>
        )
    }
}
export default Absen;