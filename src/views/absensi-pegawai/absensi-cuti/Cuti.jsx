import React, { Component } from 'react';
import AbsensiPegawaiTabelCuti from '../../../components/absensi/AbsensiPegawaiTabelCuti';
import AlertPengembangan from '../../../components/AlertPengembangan';

class Cuti extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModalEdit:false,
            showModalDelete:false,
            listBreadcrumbUser: [
            { path: "/absensi-pegawai", name: "Absensi Pegawai", status:false },
            { path: "/absensi-pegawai/cuti", name: "Cuti", status:true },
        ],
        };
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }
    render(){
        return(
            <>
            <AlertPengembangan/>
            <AbsensiPegawaiTabelCuti title={this.props.title} getFilter={this.state.filterData} dataRow={this.props.dataolahan} search="" />
            </>
        )
    }
}
export default Cuti;