import React, { Component } from 'react';
import AbsensiPegawaiTabelHadir from '../../../components/absensi/AbsensiPegawaiTabelHadir';


class Hadir extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModalEdit:false,
            showModalDelete:false,
            listBreadcrumbUser: [
            { path: "/absensi-pegawai", name: "Absensi Pegawai", status:false },
            { path: "/absensi-pegawai/hadir", name: "Hadir", status:true },
        ],
        };
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }
    render(){
        let datarow
        if(this.props.dataolahan != null && this.props.dataolahan!= undefined){
            console.log("here")
            console.log(this.props.dataolahan)
            datarow=this.props.dataolahan
        }
        return(
            <div>
                <AbsensiPegawaiTabelHadir title={this.props.title} getFilter={this.state.filterData} dataRow={datarow} search=""/>
            </div>
        )
    }
}
export default Hadir;