import React, { Component } from 'react';
import AbsensiPegawaiTabelIjin from '../../../components/absensi/AbsensiPegawaiTabelIjin';
import AlertPengembangan from '../../../components/AlertPengembangan';


class Ijin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModalEdit:false,
            showModalDelete:false,
            listBreadcrumbUser: [
            { path: "/absensi-pegawai", name: "Absensi Pegawai", status:false },
            { path: "/absensi-pegawai/ijin", name: "Ijin", status:true },
        ],
        };
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }
    render(){
        return(
            <>
            <AlertPengembangan/>
            <AbsensiPegawaiTabelIjin title={this.props.title} getFilter={this.state.filterData} dataRow={this.props.dataolahan} search="" />
            </>
        )
    }
}
export default Ijin;