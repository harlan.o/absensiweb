import React, { Component } from 'react';
import AbsensiPegawaiTabelSakit from '../../../components/absensi/AbsensiPegawaiTabelSakit';
import AlertPengembangan from '../../../components/AlertPengembangan';

class Sakit extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModalEdit:false,
            showModalDelete:false,
            listBreadcrumbUser: [
            { path: "/absensi-pegawai", name: "Absensi Pegawai", status:false },
            { path: "/absensi-pegawai/sakit", name: "Sakit", status:true },
        ],
        };
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }
    render(){
        return(
            <>
            <AlertPengembangan/>
            <AbsensiPegawaiTabelSakit title={this.props.title} getFilter={this.state.filterData} dataRow={this.props.dataolahan} search="" />
            </>
        )
    }
}
export default Sakit;