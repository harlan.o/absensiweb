import React, { Component } from 'react';
import AbsensiPegawaiTabelTanpaKet from '../../../components/absensi/AbsensiPegawaiTabelTanpaKet';
import AlertPengembangan from '../../../components/AlertPengembangan';


class TanpaKeterangan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModalEdit:false,
            showModalDelete:false,
            listBreadcrumbUser: [
            { path: "/absensi-pegawai", name: "Absensi Pegawai", status:false },
            { path: "/absensi-pegawai/tanpa-keterangan", name: "TanpaKeterangan", status:true },
        ],
        };
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }
    render(){
        return(
            <>
            {/* <AlertPengembangan/> */}
            <AbsensiPegawaiTabelTanpaKet title={this.props.title} getFilter={this.state.filterData} dataRow={this.props.dataolahan} search="" />
            </>
        )
    }
}
export default TanpaKeterangan;