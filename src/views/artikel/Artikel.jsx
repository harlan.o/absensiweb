import React, { Component } from 'react';
import DaftarArtikel from '../../components/artikel/DaftarArtikel';
import ModalFormArtikel from '../../components/artikel/ModalFormArtikel';
import BreadcrumbUser from '../../components/BreadcrumbUser';
import CreateFilterPanel from '../../components/filter/CreateFilterPanel';
import CreatePanel from '../../components/CreatePanel';
import FilterShowData1 from '../../components/filter/FilterShowData1';
import ComingSoon from '../page-coming-soon/ComingSoon';

// Redux
import {connect} from 'react-redux';
import { getListArtikel } from "../../actions/artikelAction";
import { getListUnitInduk } from "../../actions/unitIndukAction";
import { getListUnitKerja } from "../../actions/unitKerjaAction";
import ModalDetailArtikel from '../../components/artikel/ModalDetailArtikel';

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
import ModalDeleteFormArtikel from '../../components/artikel/ModalDeleteFormArtikel';
import CreateFilterPanelKerja from '../../components/filter/CreateFilterPanelKerja';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU001, REGISTER_FU035, REGISTER_FU036 } from '../../constants/hakAksesConstants';


class Artikel extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal:false,
            showModalDetail:false,
            showModalHapus:false,
            textCariCepat:"",
            dataRowMaster:null,
            dataRowMain:null,
            dataRowShow:null,
            dataRowUnitKerja:null,
            dataRowUnitInduk:null,
            dataOneRow:null,
            listBreadcrumbUser: [
            { path: "/artikel", name: "Artikel", status:true },
        ],
        };

        this.props.getListUnitKerja();
        this.props.getListUnitInduk();
        this.props.getListArtikel();
        this.handleTextCariCepat = this.handleTextCariCepat.bind(this);
        this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this); 
        this.handleCloseFormModalAdd = this.handleCloseFormModalAdd.bind(this); 
        this.handleSendToModalEdit = this.handleSendToModalEdit.bind(this); 
        this.getDataFilter = this.getDataFilter.bind(this);
    }

    componentDidUpdate(prevProps, prevState) {
        if(this.props.listArtikel !== prevProps.listArtikel){
            // console.log("Update")
            // console.log(this.props.listArtikel.data)
            if(this.props.listArtikel?.data !== undefined){
                this.setState(() => ({
                    // dataRow: this.props.listArtikel?.data,
                    dataRowUnitKerja: this.props.listPengUnitKerja?.data,
                    dataRowUnitInduk: this.props.listPengUnitInduk?.data,
                    dataRowMaster: this.props.listArtikel?.data,
                    dataRowMain: this.props.listArtikel?.data,
                    dataRowShow: this.props.listArtikel?.data,
                    // renderTable:false,
                    // loadingPage:true
                }));
                setTimeout(()=>{ 
                    this.setState(() => ({
                        // dataRow: this.props.listArtikel?.data,
                        dataRowUnitKerja: this.props.listPengUnitKerja?.data,
                        dataRowUnitInduk: this.props.listPengUnitInduk?.data,
                        dataRowMaster: this.props.listArtikel?.data,
                        dataRowMain: this.props.listArtikel?.data,
                        dataRowShow: this.props.listArtikel?.data,
                        // renderTable:true,
                        // loadingPage:false
                    }));
                 }, 1000);
            }
        }
    }
    
    render(){
        // console.log(this.state.dataRowUnitKerja)
        return (
        <div className="container-fluid">
            {/* <ComingSoon/> */}
            <h1 className="mt-4">Artikel</h1>
            <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />
            {cekHakAksesClient(REGISTER_FU001) ? 
            <CreateFilterPanel kodeAdd={REGISTER_FU036} kodeCetak={REGISTER_FU035} showForm={this.handleShowFormModalAdd} getTextCariCepat={this.handleTextCariCepat} dataFilter={this.getDataFilter} /> 
            :
            <CreateFilterPanelKerja kodeAdd={REGISTER_FU036} kodeCetak={REGISTER_FU035} showForm={this.handleShowFormModalAdd} getTextCariCepat={this.handleTextCariCepat} dataFilter={this.getDataFilter} />
            }


            {this.state.dataRowMaster ? <DaftarArtikel dataRow={this.state.dataRowShow} getdataedit={this.handleSendToModalEdit}/>  : "" }
            
            {this.state.showModal ?<ModalFormArtikel show={this.state.showModal} onHide={this.handleCloseFormModalAdd} dataedit={this.state.dataOneRow} dataoptionunitkerja={this.state.dataRowUnitKerja} dataoptionunitinduk={this.state.dataRowUnitInduk} /> : "" }
            {this.state.showModalDetail ?<ModalDetailArtikel show={this.state.showModalDetail} onHide={this.handleCloseFormModalAdd} dataedit={this.state.dataOneRow} dataoptionunitkerja={this.state.dataRowUnitKerja} /> : "" }
            
            {this.state.showModalHapus ? <ModalDeleteFormArtikel show={this.state.showModalHapus} onHide={this.handleCloseFormModalAdd} data={this.state.dataOneRow} /> : "" }
        </div>
        );
    }

    getDataFilter(v) {    
        //   console.log(v);
        let row;
        if(v.kodeUnitInduk === null){
            v.kodeUnitInduk="all";
        }
        if(v.kodeUnitKerja === null){
            v.kodeUnitKerja="all";
        }

        if((v.kodeUnitInduk !== "all") && (v.kodeUnitKerja !== "all")){
            row= this.state.dataRowMaster.filter(data => data.unitKerjaId === parseInt(v.kodeUnitKerja));
            console.log("unitKerja")
        }else if((v.kodeUnitInduk !== "all" )  && (v.kodeUnitKerja === "all" )){
            row= this.state.dataRowMaster.filter(data => data.unitIndukId === parseInt(v.kodeUnitInduk));
            console.log("unitInduk")
        }else if(v.kodeUnitInduk === "all"){
            row= this.state.dataRowMaster;
        }
        this.setState({
            
        })
        console.log(v)
        console.log(row)
        this.setState((state) => ({
            dataRowMain: row,
            dataRowShow: row,
        }));
          
    }

    handleTextCariCepat(v){
        let row= this.state.dataRowMain.filter(i =>
         i.judul.toLowerCase().includes(v.toLowerCase())
         );
         this.setState((state) => ({
             dataRowShow: row,
         }));
         console.log(v)
     }

    handleShowFormModalAdd(e) {    
        // console.log(e)
        this.setState(() => ({      
                showModal: e,
                dataOneRow:null,
        }));  
    }
   
    handleCloseFormModalAdd() {    
        this.setState(() => ({      
                showModal: false,
                showModalDetail: false,
                showModalHapus:false,
        }));  
    }
    async handleSendToModalEdit(dataOne, status) {
        if(status=="detail"){
            console.log("hai")
            await this.setState(({       
                dataOneRow:dataOne,
                showModalDetail: true,
            }));
        }else if(status=="ubah"){
            await this.setState(({       
                dataOneRow:dataOne,
                showModal: true,
            }));
        }else if(status=="hapus"){
            await this.setState(({       
                dataOneRow:dataOne,
                showModalHapus: true,
            }));
        }    
        console.log(status)
        // console.log(this.state.dataOneRow)
    }

}

const expDataArtikel = [{
    id:1,
    judul:"Apa Kabar Indonesia",
    tanggalBuat:"2020-12-12",
    tanggalBerakhir:"2020-12-30",
    isi:"<p>asdffdfas</p>",
},{
    id:2,
    judul:"Apa Kabar Sulut",
    tanggalBuat:"2020-12-12",
    tanggalBerakhir:"2020-12-30",
    isi:` First &middot; Second Test  the card's content.asdsad asdasd Some quick example text to <b>Sulut adalah</b> <h1>Uji Coba</h1> `,
},{
    id:2,
    judul:"Apa Kabar Sulut",
    tanggalBuat:"2020-12-12",
    tanggalBerakhir:"2020-12-30",
    isi:` First &middot; Second Test  the card's content.asdsad asdasd Some quick example text to <b>Sulut adalah</b> <h1>Uji Coba</h1> `,
}]

const mapStateToProps = (state) => {
    return{
        listPengUnitKerja: state.listPengUnitKerja,
        listPengUnitInduk: state.listPengUnitInduk,
        listArtikel: state.listArtikel,
    } 
 }
export default connect(mapStateToProps, {getListArtikel, getListUnitInduk, getListUnitKerja})(Artikel);
// export default Artikel;