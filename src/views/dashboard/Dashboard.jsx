import React, { Component, useState, useEffect } from 'react';
import { Badge } from 'react-bootstrap';
import FilterSingleDateMultiSuperUnit from '../../components/filter/FilterSingleDateMultiSuperUnit';
import TableAbsensiPegawaiHadir from '../../components/pegawai/TabelAbsensiPegawaiHadir';
import TableAbsensiPegawaiTerlambat from '../../components/pegawai/TabelAbsensiPegawaiTerlambat';
import './Dashboard.css';
import {connect} from 'react-redux';
import {getListAbsenDashboardV2} from "../../actions/absensiAction";
import TabelAbsensiPegawaiGabungan from '../../components/pegawai/TabelAbsensiPegawaiGabungan';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { uIIDP } from '../../actions/userActions';
import FilterSingleDateMultiSuperUnitKerja from '../../components/filter/FilterSingleDateMultiSuperUnitKerja';
import { REGISTER_FU001, REGISTER_FU006 } from '../../constants/hakAksesConstants';
import CetakDashboard from '../../components/laporan/CetakDashboard';
const moment = require('moment');
class Dashboard extends Component {

    constructor (props) {
        super(props)
        this.state = {
            datas:null,
            totalPegawai:0,
            totalHadir:0,
            totalTerlambat:0,
            totalIjin:0,
            totalBelumDiketahui:0,
            totalWfh:0,
            totalPenugasan:0,
            totalTugasLuar:0,  
            loading:true,
            datasHadir:null,   
            datasIjin:null,   
            datasTerlambat:null,   
            datasBelumDiketahui:null, 
          filterData:{
            groupingUnitKerja:[],
            startDate: moment().format('YYYY-MM-DD'),
            selectMultyUnitInduk:[],
            selectMultyUnitKerja:[]
          }
        };
        this.getDataFilter = this.getDataFilter.bind(this);
      }

    getDataFilter(v) {    
          this.setState({
              filterData:v      
            }, () =>{this.callApi()});
            
    }
    componentDidMount(){
        if(this.props.totalHadir !== undefined){
            this.setState(() => ({      
                totalHadir: this.props.totalHadir
            }));
        }

        this.callApi();
        this.updateTimer = setInterval(() => this.callApi(), 10000);
    }
    componentWillUnmount(){     
        clearInterval(this.updateTimer);
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.totalHadir !== prevProps.totalHadir){
            if(this.props.totalHadir !== null) 
            this.setState(() => ({      
                totalHadir: this.props.totalHadir
            }));
        }
    }

    async callApi(){

        let unitIndukArr = [
            {id:uIIDP()}
        ]
        console.log("idUnit Induk", uIIDP())
        if(cekHakAksesClient(REGISTER_FU001)){
            unitIndukArr = this.state.filterData?.selectMultyUnitInduk
        }  
        await this.props.getListAbsenDashboardV2(this.state.filterData?.startDate, unitIndukArr, this.state.filterData?.selectMultyUnitKerja).then( ()=>{
            // console.log(this.props.listAbsensiDashboard); 
            if(this.props.listAbsensiDashboard?.data !== undefined && this.props.listAbsensiDashboard?.data !== null){
                let _datasAll=this.props.listAbsensiDashboard?.data.daftarAbsen;
                let groupingUnitKerja =this.state.filterData.groupingUnitKerja;
                let datasAll =_datasAll;
                if(groupingUnitKerja.length > 0){
                    datasAll=_datasAll.filter((pegawai) => {
                        let nilai=0;
                        for(let i = 0; i < groupingUnitKerja.length; i++){
                            let [a1]=pegawai.unitKerja.split(" ");
                            let [b1]=groupingUnitKerja[i].split(" ");
                            if (a1 == b1){
                                nilai=1
                            }
                        }
                        if(nilai == 1){
                            return true
                        }
                    })
                    console.log(datasAll);
                }

                
                let totBelumDiketahui=this.props.listAbsensiDashboard?.data.tidakDiketahui
                let c_totHadir=datasAll.filter((peggawai) => {
                    return peggawai.keterangan === "MASUK_TEPAT_WAKTU" || peggawai.keterangan === "MASUK_TERLAMBAT"
                });
                let c_totIjin=datasAll.filter(peggawai => peggawai.keterangan==="IJIN");
                let c_totTerlambat=datasAll.filter(peggawai => peggawai.keterangan==="MASUK_TERLAMBAT");
                let c_totBelumDiketahui=datasAll.filter(peggawai => peggawai.keterangan==="BELUM_DIKETAHUI");
                let totHadir= c_totHadir.length;
                let totIjin= c_totIjin.length;
                let totTerlambat= c_totTerlambat.length;
                let totPegawai=this.props.listAbsensiDashboard?.data.jumlahPegawai;
                // let totPegawai=totHadir+totIjin+totBelumDiketahui;
                this.setState({
                    datas:datasAll,  
                    loading:false,
                    totalHadir:totHadir,
                    totalTerlambat:totTerlambat,
                    totalIjin:totIjin,
                    totalBelumDiketahui:totBelumDiketahui, 
                    datasHadir:c_totHadir,   
                    datasIjin:c_totIjin,   
                    datasTerlambat:c_totTerlambat,   
                    datasBelumDiketahui:c_totBelumDiketahui,
                    totalPegawai:totPegawai   
                }) 

            }
        });
    }

    handlePrint= () => this.componentRef
    render(){
        let _dataCetak = {}
        if(this.state.datas !== undefined && this.state.datas !== null){
            let _daftarHadir = this.state.datas.filter( v => v.keterangan == "MASUK_TEPAT_WAKTU")
            let _daftarTerlambat = this.state.datas.filter(v => v.keterangan == "MASUK_TERLAMBAT")
            _dataCetak = {
                 daftarHadir : _daftarHadir,
                 daftarTerlambat: _daftarTerlambat,
                 totalPegawai: this.state.totalPegawai,
                 totalWfh: this.state.totalWfh,
                 totalTugasLuar: this.state.totalTugasLuar,
                 totalMelaksanakanTugas: this.state.totalPenugasan,
                 totalHadir: this.state.totalHadir,
                 totalTerlambat: this.state.totalTerlambat,
                 totalIjin: this.state.totalIjin,
                 totalTanpaKeterangan: this.state.totalBelumDiketahui,
             }

        }

        const {loading} = this.props.listAbsensiDashboard;
        return(
         <div className="container-fluid">
            <h1 className="mt-4">Dashboard</h1> 
            <ol className="breadcrumb mb-4">
        <li className="breadcrumb-item active"><UseDate/> </li>
            </ol>
            {cekHakAksesClient(REGISTER_FU001) ? <FilterSingleDateMultiSuperUnit kodeCetak={REGISTER_FU006} dataFilter={this.getDataFilter} setloading={loading} print={this.handlePrint} /> : <FilterSingleDateMultiSuperUnitKerja dataFilter={this.getDataFilter} setloading={loading} print={this.handlePrint} kodeCetak={REGISTER_FU006}/>}
            
            {this.state.datas !== undefined && this.state.datas !== null ?
            <div style={{ display: 'none' }}>
                            <CetakDashboard filterData={this.state.filterData} dataCetak={_dataCetak} ref={(el) => (this.componentRef = el)}/>
                        </div> : ""
            }
            
            {/* <ChartLine/> */}
            <div className="row">
                <div className="col-xl-12 col-md-12">
                    <div className="card bg-secondary text-white mb-4">
                        <div className="card-body card-small-v2 them-black">
                            <h1 className="d-flex justify-content-end number-card-dark" style={{marginBottom:0}}>
                            <div className="title">TOTAL PEGAWAI</div>
                                <Badge variant="secondary" className="justify-content-start">{this.state.totalPegawai}</Badge>
                            </h1>
                        </div>
                    </div>
                </div>
                <div className="col-xl-3 col-md-6">
                    <div className="card bg-secondary text-white mb-4">
                        <div className="card-body them-red"><h3 className="d-flex justify-content-start align-items-center">HADIR</h3>
                            <h1 className="d-flex justify-content-end number-card">
                                <Badge variant="secondary" className="justify-content-start">{this.state.totalHadir}</Badge>
                            </h1>
                        </div>
                    </div>
                </div>
                <div className="col-xl-3 col-md-6">
                    <div className="card bg-secondary text-white mb-4">
                        <div className="card-body them-red"><h3 className="d-flex justify-content-start align-items-center">TERLAMBAT</h3>
                            <h1 className="d-flex justify-content-end number-card">
                                <Badge variant="secondary" className="justify-content-start">{this.state.totalTerlambat}</Badge>
                            </h1>
                        </div>
                        
                        
                    </div>
                </div>
                <div className="col-xl-3 col-md-6">
                    <div className="card bg-secondary text-white mb-4">
                        <div className="card-body them-red"><h3 className="d-flex justify-content-start align-items-center">IJIN</h3>
                            <h1 className="d-flex justify-content-end number-card">
                                <Badge variant="secondary" className="justify-content-start">{this.state.totalIjin}</Badge>
                            </h1>
                        </div>
                    </div>
                </div>
                <div className="col-xl-3 col-md-6">
                    <div className="card bg-secondary text-white mb-4">
                        <div className="card-body them-red"><h3 className="d-flex justify-content-start align-items-start">TANPA KET.</h3>
                            <h1 className="d-flex justify-content-end number-card">
                                <Badge variant="secondary" className="justify-content-start">{this.state.totalBelumDiketahui}</Badge>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>

            <div className="row">
                <div className="col-xl-4 col-md-6">
                    <div className="card bg-secondary text-white mb-4">
                        <div className="card-body card-small-v2 them-black no-margin">
                            <h1 className="d-flex justify-content-end number-card" style={{marginBottom:0}} >
                            <div className="title">WFH</div>
                                <Badge variant="secondary" className="justify-content-start">{this.state.totalWfh}</Badge>
                            </h1>
                        </div>
                    </div>
                </div>
                <div className="col-xl-4 col-md-6">
                    <div className="card bg-secondary text-white mb-4">
                        <div className="card-body card-small-v2 them-black">
                            <h1 className="d-flex justify-content-end number-card" style={{marginBottom:0}}>
                            <div className="title">PENUGASAN</div>
                                <Badge variant="secondary" className="justify-content-start">{this.state.totalPenugasan}</Badge>
                            </h1>
                        </div>
                    </div>
                </div>
                <div className="col-xl-4 col-md-6">
                    <div className="card bg-secondary text-white mb-4">
                        <div className="card-body card-small-v2 them-black">
                            {/* <h3 className="d-flex justify-content-start align-items-center">TUGAS LUAR</h3> */}
                            <h1 className="d-flex justify-content-end number-card" style={{marginBottom:0}}>
                            <div className="title">TUGAS LUAR</div>
                                <Badge variant="secondary" className="justify-content-start">{this.state.totalTugasLuar}</Badge>
                            </h1>
                        </div>
                    </div>
                </div>
            
            </div>


            
            <div className="row">
                {/* <div className=" col-md-6">
                    <TableAbsensiPegawaiHadir getFilter={this.state.filterData} dataRow={this.state.datasHadir} search="test"/>
                </div>
                <div className=" col-md-6">
                    <TableAbsensiPegawaiTerlambat getFilter={this.state.filterData} dataRow={this.state.datasTerlambat}/>
                </div> */}
                <div className=" col-md-12">
                    <TabelAbsensiPegawaiGabungan getFilter={this.state.filterData} dataRow={this.state.datas} search="test"/>
                </div>
                {/* <div className=" col-md-6">
                <TableAbsensiPegawaiIjin getFilter={this.state.filterData} dataRow={this.state.datasIjin}/>
                </div>
                <div className=" col-md-6">
                    <TableAbsensiPegawaiBelumDiketahui getFilter={this.state.filterData} dataRow={this.state.datasBelumDiketahui}/>
                </div> */}
            </div>
        
        </div>
        );
    }

}



// const moment = require('moment');
export const UseDate = () => {
    const locale = 'id';
    const [today, setDate] = useState(new Date()); // Save the current date to be able to trigger an update
  
    useEffect(() => {
        const timer = setInterval(() => { 
        setDate(new Date());
      }, 60 * 1000);
      return () => {
        clearInterval(timer);
      }
    }, []);
  
    const day = today.toLocaleDateString(locale, { weekday: 'long' });
    const date = `${day}, ${today.getDate()} ${today.toLocaleDateString(locale, { month: 'long' })}\n\n`;
  
    const hour = today.getHours();
    const wish = `Selamat ${(hour < 10 && 'Pagi') || (hour < 14 && 'Siang') || (hour < 18 && 'Sore')|| 'Malam'}, `;
  
    const time = today.toLocaleTimeString(locale, { hour: 'numeric', hour12: true, minute: 'numeric' });
    const year = today.getFullYear();
    return (
        <div>
            {/* Tanggal: {' '}{date} ---- {time} {moment().format('ss')}s, {wish} */}
            Tanggal: {' '}{date} {year} ---- {time}, {wish}...
        </div>
    );
  };

  const mapStateToProps = (state) => {
    return{
        listAbsensiDashboard: state.listAbsensiDashboard,
        userDataProfile: state.userDataProfile

    } 
  }
export default connect(mapStateToProps, {getListAbsenDashboardV2})(Dashboard);