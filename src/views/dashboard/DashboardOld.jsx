import React, { Component, useState, useEffect } from 'react';
import { Badge } from 'react-bootstrap';
import FilterShowData1 from '../../components/FilterShowData1';
import TableAbsensiPegawaiHadir from '../../components/pegawai/TabelAbsensiPegawaiHadir';
import TableAbsensiPegawaiTerlambat from '../../components/pegawai/TabelAbsensiPegawaiTerlambat';
// import TableAbsensiPegawaiIjin from '../../components/TabelAbsensiPegawaiIjin';
// import TableAbsensiPegawaiBelumDiketahui from '../../components/TabelAbsensiPegawaiBelumDiketahui';
import './Dashboard.css';
import {connect} from 'react-redux';
import {getListAbsenInduk, getListAbsenIndukAll} from "../../actions/absensiAction";

// Chart
import ChartLine from '../../components/ChartLine';

// const RenderLineChart = (
    
//   );

const moment = require('moment');
class Dashboard extends Component {

    constructor (props) {
        super(props)
        this.state = {
            datas:null,
            totalHadir:0,
            totalTerlambat:0,
            totalIjin:0,
            totalBelumDiketahui:0,  
            loading:true,
            datasHadir:null,   
            datasIjin:null,   
            datasTerlambat:null,   
            datasBelumDiketahui:null, 
          filterData:{
            endDate: moment().format('YYYY-MM-DD'),
            kodeUnitInduk: "all",
            kodeUnitKerja: "all",
            startDate: moment().format('YYYY-MM-DD'),
          }
        };
        this.getDataFilter = this.getDataFilter.bind(this);
      }

      getDataFilter(v) {    
        //   console.log(v);
          this.setState(() => ({
              filterData:v      
              // kodeUnitInduk: v.kodeUnitInduk,
              // kodeUnitKerja: v.kodeUnitKerja,
              // startDate: v.startDate,
              // endDate: v.endDate,
            }));  
          
    }
    componentDidMount(){
        
        if(this.props.totalHadir !== undefined){
            // console.log("work");
            // console.log(this.props.totalHadir);
            this.setState(() => ({      
                totalHadir: this.props.totalHadir
            }));
        }
        // if(this.state.loading !== false){
            // }
            this.callApi();
            // console.log("call api");
        this.updateTimer = setInterval(() => this.callApi(), 10000);
    }
    componentWillUnmount(){
        clearInterval(this.updateTimer);
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.totalHadir !== prevProps.totalHadir){
            // console.log("update");
            if(this.props.totalHadir !== null) 
            this.setState(() => ({      
                totalHadir: this.props.totalHadir
            }));
        }
    }
    // componentDidUpdate(prevProps, prevState) {
    //     if(prevProps.totalHadir !== this.props.totalHadir){
    //       console.log(this.props.totalHadir);
        
    //     }
    //   }

    async callApi(){
        if(this.state.filterData?.kodeUnitInduk !== null && this.state.filterData?.kodeUnitInduk !=="all"){
            await this.props.getListAbsenInduk(this.state.filterData?.kodeUnitInduk, this.state.filterData?.startDate, this.state.filterData?.endDate, "masuk").then( ()=>{
                if(this.props.listAbsensiInduk?.data !== undefined && this.props.listAbsensiInduk?.data !== null){
                    if(this.state.filterData?.kodeUnitKerja !== null && this.state.filterData?.kodeUnitKerja !=="all"){
                        let idKodeUKerja = parseInt(this.state.filterData?.kodeUnitKerja);
                        let datasAll=this.props.listAbsensiInduk?.data;
                        let c_totHadir=datasAll.filter((peggawai) => {
                            return peggawai.keterangan === "MASUK_TEPAT_WAKTU" || peggawai.keterangan === "MASUK_TERLAMBAT"
                        }).filter(peggawai =>peggawai.unitKerjaId === idKodeUKerja);
                        //   let c_totHadir=datasAll.filter(peggawai => peggawai.keterangan==="MASUK_TEPAT_WAKTU").filter(peggawai =>peggawai.unitKerjaId === idKodeUKerja);
                        let c_totIjin=datasAll.filter(peggawai => peggawai.keterangan==="IJIN").filter(peggawai =>peggawai.unitKerjaId === idKodeUKerja);
                        let c_totTerlambat=datasAll.filter(peggawai => peggawai.keterangan==="MASUK_TERLAMBAT").filter(peggawai =>peggawai.unitKerjaId === idKodeUKerja);
                        let c_totBelumDiketahui=datasAll.filter(peggawai => peggawai.keterangan==="BELUM_DIKETAHUI").filter(peggawai =>peggawai.unitKerjaId === idKodeUKerja);
                        let totHadir= c_totHadir.length;
                        let totIjin= c_totIjin.length;
                        let totTerlambat= c_totTerlambat.length;
                        let totBelumDiketahui= c_totBelumDiketahui.length;
                    //   console.log(c_totHadir);
                        this.setState({
                            datas:datasAll,  
                            loading:false,
                            totalHadir:totHadir,
                            totalTerlambat:totTerlambat,
                            totalIjin:totIjin,
                            totalBelumDiketahui:totBelumDiketahui, 
                            datasHadir:c_totHadir,   
                            datasIjin:c_totIjin,   
                            datasTerlambat:c_totTerlambat,   
                            datasBelumDiketahui:c_totBelumDiketahui,   
                        }) 
                    }else{
                        let idKodeUKerja = parseInt(this.state.filterData?.kodeUnitKerja);
                        let datasAll=this.props.listAbsensiInduk?.data;
                        let c_totHadir=datasAll.filter((peggawai) => {
                            return peggawai.keterangan === "MASUK_TEPAT_WAKTU" || peggawai.keterangan === "MASUK_TERLAMBAT"
                        });
                        let c_totIjin=datasAll.filter(peggawai => peggawai.keterangan==="IJIN");
                        let c_totTerlambat=datasAll.filter(peggawai => peggawai.keterangan==="MASUK_TERLAMBAT");
                        let c_totBelumDiketahui=datasAll.filter(peggawai => peggawai.keterangan==="BELUM_DIKETAHUI");
                        let totHadir= c_totHadir.length;
                        let totIjin= c_totIjin.length;
                        let totTerlambat= c_totTerlambat.length;
                        let totBelumDiketahui= c_totBelumDiketahui.length;
                    //   console.log(c_totHadir);
                        this.setState({
                            datas:datasAll,  
                            loading:false,
                            totalHadir:totHadir,
                            totalTerlambat:totTerlambat,
                            totalIjin:totIjin,
                            totalBelumDiketahui:totBelumDiketahui, 
                            datasHadir:c_totHadir,   
                            datasIjin:c_totIjin,   
                            datasTerlambat:c_totTerlambat,   
                            datasBelumDiketahui:c_totBelumDiketahui,   
                        }) 
                    }
                    
                }
            
            }
            );
        }else{
            await this.props.getListAbsenIndukAll(this.state.filterData?.startDate, this.state.filterData?.endDate, "masuk").then( ()=>{
                if(this.props.listAbsensiInduk?.data !== undefined && this.props.listAbsensiInduk?.data !== null){
                    // let idKodeUKerja = parseInt(this.state.filterData?.kodeUnitKerja);
                    let datasAll=this.props.listAbsensiInduk?.data;
                    let c_totHadir=datasAll.filter((peggawai) => {
                        return peggawai.keterangan === "MASUK_TEPAT_WAKTU" || peggawai.keterangan === "MASUK_TERLAMBAT"
                    });
                    //   let c_totHadir=datasAll.filter(peggawai => peggawai.keterangan==="MASUK_TEPAT_WAKTU").filter(peggawai =>peggawai.unitKerjaId === idKodeUKerja);
                    let c_totIjin=datasAll.filter(peggawai => peggawai.keterangan==="IJIN");
                    let c_totTerlambat=datasAll.filter(peggawai => peggawai.keterangan==="MASUK_TERLAMBAT");
                    let c_totBelumDiketahui=datasAll.filter(peggawai => peggawai.keterangan==="BELUM_DIKETAHUI");
                    let totHadir= c_totHadir.length;
                    let totIjin= c_totIjin.length;
                    let totTerlambat= c_totTerlambat.length;
                    let totBelumDiketahui= c_totBelumDiketahui.length;
                //   console.log(c_totHadir);
                    this.setState({
                        datas:datasAll,  
                        loading:false,
                        totalHadir:totHadir,
                        totalTerlambat:totTerlambat,
                        totalIjin:totIjin,
                        totalBelumDiketahui:totBelumDiketahui, 
                        datasHadir:c_totHadir,   
                        datasIjin:c_totIjin,   
                        datasTerlambat:c_totTerlambat,   
                        datasBelumDiketahui:c_totBelumDiketahui,   
                    }) 
                }
            
            }
            );
        }
    }
    
    render(){
        // console.log("total")
        // console.log(this.props.totalHadir)
        return(
         <div className="container-fluid">
            <h1 className="mt-4">Dashboard</h1> 
            <ol className="breadcrumb mb-4">
        <li className="breadcrumb-item active"><UseDate/> </li>
            </ol>
            <FilterShowData1 dataFilter={this.getDataFilter} />
            {/* <ChartLine/> */}
            <div className="row">
                <div className="col-xl-3 col-md-6">
                    <div className="card bg-secondary text-white mb-4">
                        <div className="card-body them-red"><h3 className="d-flex justify-content-start align-items-center">HADIR</h3>
                            <h1 className="d-flex justify-content-end number-card">
                                <Badge variant="secondary" className="justify-content-start">{this.state.totalHadir}</Badge>
                            </h1>
                        </div>
                        
                        
                    </div>
                </div>
                <div className="col-xl-3 col-md-6">
                    <div className="card bg-secondary text-white mb-4">
                        <div className="card-body them-red"><h3 className="d-flex justify-content-start align-items-center">TERLAMBAT</h3>
                            <h1 className="d-flex justify-content-end number-card">
                                <Badge variant="secondary" className="justify-content-start">{this.state.totalTerlambat}</Badge>
                            </h1>
                        </div>
                        
                        
                    </div>
                </div>
                <div className="col-xl-3 col-md-6">
                    <div className="card bg-secondary text-white mb-4">
                        <div className="card-body them-red"><h3 className="d-flex justify-content-start align-items-center">IJIN</h3>
                            <h1 className="d-flex justify-content-end number-card">
                                <Badge variant="secondary" className="justify-content-start">{this.state.totalIjin}</Badge>
                            </h1>
                        </div>
                        
                        
                    </div>
                </div>
                <div className="col-xl-3 col-md-6">
                    <div className="card bg-secondary text-white mb-4">
                        <div className="card-body them-red"><h3 className="d-flex justify-content-start align-items-start">BELUM DIKETAHUI</h3>
                            <h1 className="d-flex justify-content-end number-card">
                                <Badge variant="secondary" className="justify-content-start">{this.state.totalBelumDiketahui}</Badge>
                            </h1>
                        </div>
                        
                        
                    </div>
                </div>
                
            </div>


            
            <div className="row">
                <div className=" col-md-6">
                    <TableAbsensiPegawaiHadir getFilter={this.state.filterData} dataRow={this.state.datasHadir} search="test"/>
                </div>
                <div className=" col-md-6">
                    <TableAbsensiPegawaiTerlambat getFilter={this.state.filterData} dataRow={this.state.datasTerlambat}/>
                </div>
                {/* <div className=" col-md-6">
                <TableAbsensiPegawaiIjin getFilter={this.state.filterData} dataRow={this.state.datasIjin}/>
                </div>
                <div className=" col-md-6">
                    <TableAbsensiPegawaiBelumDiketahui getFilter={this.state.filterData} dataRow={this.state.datasBelumDiketahui}/>
                </div> */}
            </div>
        
        </div>
        );
    }

}



// const moment = require('moment');
export const UseDate = () => {
    const locale = 'id';
    const [today, setDate] = useState(new Date()); // Save the current date to be able to trigger an update
  
    useEffect(() => {
        const timer = setInterval(() => { 
        setDate(new Date());
      }, 60 * 1000);
      return () => {
        clearInterval(timer);
      }
    }, []);
  
    const day = today.toLocaleDateString(locale, { weekday: 'long' });
    const date = `${day}, ${today.getDate()} ${today.toLocaleDateString(locale, { month: 'long' })}\n\n`;
  
    const hour = today.getHours();
    const wish = `Selamat ${(hour < 10 && 'Pagi') || (hour < 14 && 'Siang') || (hour < 18 && 'Sore')|| 'Malam'}, `;
  
    const time = today.toLocaleTimeString(locale, { hour: 'numeric', hour12: true, minute: 'numeric' });
    const year = today.getFullYear();
    return (
        <div>
            {/* Tanggal: {' '}{date} ---- {time} {moment().format('ss')}s, {wish} */}
            Tanggal: {' '}{date} {year} ---- {time}, {wish}...
        </div>
    );
  };

  const mapStateToProps = (state) => {
    return{
        listAbsensiInduk: state.listAbsensiInduk
    } 
  }
export default connect(mapStateToProps, {getListAbsenInduk, getListAbsenIndukAll})(Dashboard);