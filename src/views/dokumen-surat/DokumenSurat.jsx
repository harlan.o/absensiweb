import React, { Component } from 'react';
import BreadcrumbUser from '../../components/BreadcrumbUser';
import ComingSoon from '../page-coming-soon/ComingSoon';
class DokumenSurat extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listBreadcrumbUser: [
            // { path: "/absensi-pegawai", name: "Absensi Pegawai", status:false },
            { path: "/dokumen-surat", name: "Dokumen Surat", status:true },
        ],
        };
    }
    render(){
        return (
        <div className="container-fluid">
            <h1 className="mt-4">Dokumen Surat</h1>
            <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />
            <ComingSoon/>
        </div>
        );
    }

}
export default DokumenSurat;