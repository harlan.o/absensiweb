import React, { Component } from 'react';
import { monthName } from '../../actions/genneralAction';
// import { Button, Nav } from 'react-bootstrap';
import BreadcrumbUser from '../../components/BreadcrumbUser';
import ListEven from '../../components/even/ListEven';
import CreateFilterSingleMonthMultiSuperUnit from '../../components/filter/CreateFilterSingleMonthMultiSuperUnit';
// Redux
import {connect} from 'react-redux';
import { getListEven } from "../../actions/evenAction";
import { getListUnitInduk } from "../../actions/unitIndukAction";
import { getListUnitKerja } from "../../actions/unitKerjaAction";
import CreateFilterSingleMonthSingleUnitInduk from '../../components/filter/CreateFilterSingleMonthSingleUnitInduk';
import ModalEven from '../../components/even/ModalEven';
import CreateFilterSingleMonth from '../../components/filter/CreateFilterSingleMonth';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU001, REGISTER_FU040, REGISTER_FU041 } from '../../constants/hakAksesConstants';
import { uIIDP } from '../../actions/userActions';

const moment = require('moment');


class Even extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listBreadcrumbUser: [
            { path: "/even", name: "Even Kegiatan", status:true },
            ],
            datas:[],
            dataOneRow:null,
            dataRowUnitKerja:null,
            dataRowUnitInduk:null,
            showModal:false,
            typeModal: null,
            loading:true,
            filterData:{
                selectSingleUnitInduk: null,
                startDate: moment().format('YYYY-MM'),
              }
        };

        this.getDataFilter = this.getDataFilter.bind(this);
        this.handleShowFormModal = this.handleShowFormModal.bind(this); 
        this.handleCloseFormModal = this.handleCloseFormModal.bind(this);
        this.setDataOneRow = this.setDataOneRow.bind(this);
        // this.props.getListEven(this.state.filterData.startDate, this.state.filterData.selectSingleUnitInduk);
    }

    componentDidMount(){
        this.callApi();
    }
    componentDidUpdate(prevProps, prevState){
        // console.log("call")
        if(this.state.filterData !== prevState.filterData){
            this.callApi();
        }

    }

    async callApi(){
        let unitInduk =uIIDP()
        if(cekHakAksesClient(REGISTER_FU001)){
            unitInduk =this.state.filterData.selectSingleUnitInduk
            console.log("here", cekHakAksesClient(REGISTER_FU001))
        }  
        this.props.getListEven(this.state.filterData.startDate, unitInduk).then(()=>{
            this.setState({
                datas: this.props.listEven.data,
                loading:false
            })
        })
    }
 
 
    componentDidUpdate(prevProps, prevState) {
        if(this.props.listEven !== prevProps.listEven){
            console.log("Update")
            console.log(this.props.listEven.data)
            if(this.props.listEven.data !== undefined){
                this.setState(() => ({
                    datas: this.props.listEven.data,
                }));
            }
        }
    }
    render(){
        console.log(this.props.listEven)
        return (
        <div className="container-fluid">
            {/* <ComingSoonTransparant/> */}
            <h1 className="mt-4">Even Kegiatan</h1>
            <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />            

            <h2 className="mt-4">Kegiatan Bulan {monthName(moment(this.state.filterData.startDate).format('MM'))} {moment(this.state.filterData.startDate).format('YYYY')} </h2>
            {
                cekHakAksesClient(REGISTER_FU001) ? 
                <CreateFilterSingleMonthSingleUnitInduk kodeAdd={REGISTER_FU041} kodeCetak={REGISTER_FU040}  dataFilter={this.getDataFilter} setloading={this.state.loading} showForm={this.handleShowFormModal} />
                :
                <CreateFilterSingleMonth kodeAdd={REGISTER_FU041} kodeCetak={REGISTER_FU040} dataFilter={this.getDataFilter} setloading={this.state.loading} showForm={this.handleShowFormModal} />
            }
            

            {this.state.datas != null && this.state.datas != undefined && this.state.datas.length > 0 ? (
            <ListEven data={this.state.datas} setdataonerow={this.setDataOneRow}/>
            ):(<p>Tidak ada Even Kegiatan di bulan ini</p>) }

            {this.state.showModal ? <ModalEven show={this.state.showModal} onHide={this.handleCloseFormModal} jenisModal={this.state.typeModal} dataonerow={this.state.dataOneRow}/> : "" }
        </div>
        );
    }
    getDataFilter(v) {  
        this.setState(() => ({
            filterData:v,
            loading:true      
          }), () =>{
              this.callApi()
        });  
    }
    handleShowFormModal(e, type) {    
        this.setState(() => ({      
                showModal: e,
                dataOneRow:null,
                typeModal: type,
        }));  
    }
    setDataOneRow(dataone) {  
        console.log("Detail")  
        console.log(dataone)  
        this.setState(() => ({      
                showModal: true,
                dataOneRow:dataone,
                typeModal: "DETAIL",
        }));  
    }
    handleCloseFormModal() {    
        this.setState(() => ({      
                showModal: false,
                // showModalDetail: false,
        }));  
    }
}

const mapStateToProps = (state) => {
    return{
        listPengUnitKerja: state.listPengUnitKerja,
        listPengUnitInduk: state.listPengUnitInduk,
        listEven: state.listEven,
    } 
 }

export default connect(mapStateToProps, {getListEven, getListUnitInduk, getListUnitKerja})(Even);