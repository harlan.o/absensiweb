import React, { Component } from 'react';

import { Card, Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import BreadcrumbUser from '../../components/BreadcrumbUser';
import AbsenRingkasan from './absen-ringkasan/AbsenRingkasan';
import AbsenHarian from './absen-harian/AbsenHarian';
import FilterShowData1 from '../../components/filter/FilterShowData1';
import ComingSoon from '../page-coming-soon/ComingSoon';

import ReactToPrint from 'react-to-print';

import { monthName } from '../../actions/genneralAction';

import {connect} from 'react-redux';
import {getLaporanAbsen} from "../../actions/absensiAction";
import LaporanAbsensiHarian from '../../components/laporan/LaporanAbsensiHarian';
import LaporanAbsensiBulanan from '../../components/laporan/LaporanAbsensiBulanan';
import TabelLaporanAbsen from '../../components/laporan-absen/TabelLaporanAbsen';
import TrueFilterSingleMonthSingleIndukMultipleKerja from '../../components/filter/TrueFilterSingleMonthSingleIndukMultipleKerja';
import ModalLaporanAbsen from '../../components/laporan-absen/ModalLaporanAbsen';
import LaporanAbsensiRekapan from '../../components/laporan/LaporanAbsensiRekapan';
import TrueFilterSingleMonthMultipleKerja from '../../components/filter/TrueFilterSingleMonthMultipleKerja';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU001, REGISTER_FU026 } from '../../constants/hakAksesConstants';
const moment = require('moment');
// const dataAwal= [
//     {
//         "pegawaiId": 2151,
//         "namaPegawai": "MEXVAN PUDINAUNG",
//         "nip": "198605202015031001",
//         "jabatan": "GURU PERTAMA / AHLI PERTAMA",
//         "unitIndukId": 212,
//         "unitInduk": "DINAS PENDIDIKAN",
//         "unitKerjaId": 711,
//         "unitKerja": "SMP NEGERI 4 SATU ATAP SIAU TIMUR",
//         "totalHariKerja": 19,
//         "listAbsen": [
//             {
//                 "tanggal": "2021-02-01",
//                 "jamMasuk": "07:47:25.534858",
//                 "jamPulang": "18:27:21.340462",
//                 "status": "HADIR - TERLAMBAT"
//             },
//             {
//                 "tanggal": "2021-02-02",
//                 "jamMasuk": "07:47:25.534858",
//                 "jamPulang": "18:27:21.340462",
//                 "status": "HADIR - TERLAMBAT"
//             },
//             {
//                 "tanggal": "2021-02-08",
//                 "jamMasuk": "07:47:25.534858",
//                 "jamPulang": null,
//                 "status": "HADIR - TERLAMBAT"
//             },
//             {
//                 "tanggal": "2021-02-09",
//                 "jamMasuk": null,
//                 "jamPulang": null,
//                 "status": "TIDAK HADIR - TANPA KETERANGAN"
//             },
//             {
//                 "tanggal": "2021-02-10",
//                 "jamMasuk": null,
//                 "jamPulang": null,
//                 "status": "TIDAK HADIR - TANPA KETERANGAN"
//             },
//             {
//                 "tanggal": "2021-02-11",
//                 "jamMasuk": null,
//                 "jamPulang": null,
//                 "status": "TIDAK HADIR - TANPA KETERANGAN"
//             },
//             {
//                 "tanggal": "2021-02-15",
//                 "jamMasuk": null,
//                 "jamPulang": null,
//                 "status": "TIDAK HADIR - TANPA KETERANGAN"
//             },
//             {
//                 "tanggal": "2021-02-16",
//                 "jamMasuk": null,
//                 "jamPulang": null,
//                 "status": "TIDAK HADIR - TANPA KETERANGAN"
//             },
//             {
//                 "tanggal": "2021-02-17",
//                 "jamMasuk": null,
//                 "jamPulang": null,
//                 "status": "TIDAK HADIR - TANPA KETERANGAN"
//             },
//             {
//                 "tanggal": "2021-02-18",
//                 "jamMasuk": null,
//                 "jamPulang": null,
//                 "status": "TIDAK HADIR - TANPA KETERANGAN"
//             },
//             {
//                 "tanggal": "2021-02-19",
//                 "jamMasuk": null,
//                 "jamPulang": null,
//                 "status": "TIDAK HADIR - TANPA KETERANGAN"
//             }
//         ]
//     },
//   ]
  

class LaporanAbsen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sub_page: "",
            data:null,
            loading:true,
            showModal:false,
            typeModal:"DETAIL",
            
            listBreadcrumbUser: [
                { path: "/laporan-absen", name: "Laporan Absen", status:true },
            ],
            filterData:{
                selectMultyUnitInduk: [],
                selectMultyUnitKerja: [],
                startDate: moment().format('YYYY-MM'),
            }
        };
        this.getDataFilter = this.getDataFilter.bind(this);
        this.setDataOneRow = this.setDataOneRow.bind(this);
        this.handleCloseFormModal = this.handleCloseFormModal.bind(this);
    }

    SetBreadcrumbChild = (listBreadcrumbChild) =>{
        this.setState({listBreadcrumbUser: listBreadcrumbChild})
    }

    
    handlePrint= () => this.componentRef
    
    componentDidMount(){
    }
    componentDidUpdate(prevProps, prevState) {
    }
    render(){
        let title=  " "+monthName(moment(this.state.filterData.startDate).format('MM'))+' '+moment(this.state.filterData.startDate).format('YYYY');
        let jumlahTanggal=moment(this.state.filterData.startDate).daysInMonth();

        return(
            <div className="container-fluid">
                {/* <ComingSoon/> */}
                <h1 className="mt-4">Laporan Absensi</h1>
                <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />

                { cekHakAksesClient(REGISTER_FU001) ? <TrueFilterSingleMonthSingleIndukMultipleKerja dataFilter={this.getDataFilter} setloading={this.state.loading} kodeCetak={REGISTER_FU026} print={this.handlePrint}/> : <TrueFilterSingleMonthMultipleKerja  dataFilter={this.getDataFilter} kodeCetak={REGISTER_FU026} setloading={this.state.loading} print={this.handlePrint}/> }
                <Card>
                {/* <TabelLaporanAbsen title={"Laporan Absensi"} jumlahTanggal={jumlahTanggal} dataRow={dataAwal} setdataonerow={this.setDataOneRow} /> */}
                {(this.state.data != null && this.state.data != undefined) ?
                <TabelLaporanAbsen title={title} jumlahTanggal={jumlahTanggal} dataRow={this.state.data} setdataonerow={this.setDataOneRow}  />
                  :""} 
                </Card>


                     <div style={{ display: 'none' }}>
                            <LaporanAbsensiRekapan filterData={this.state.filterData} datatable={this.state.data} ref={(el) => (this.componentRef = el)}/>
                        </div>

                 {this.state.showModal ? <ModalLaporanAbsen show={this.state.showModal} onHide={this.handleCloseFormModal} jenisModal={this.state.typeModal} dataonerow={this.state.dataOneRow} reloadTabel={()=> this.callApi()} filter={this.state.filterData}/> :"" }


            </div>
        );
    }


    async callApi(){
        this.props.getLaporanAbsen(this.state.filterData.startDate, this.state.filterData.selectMultyUnitInduk, this.state.filterData.selectMultyUnitKerja).then(() =>{
            this.setState(() => ({      
                data: this.props.laporanAbsensi.data,
                loading:false,
            })); 
        })
    }

    setDataOneRow(dataone, t) {  
        console.log(dataone)  
        this.setState(() => ({      
                showModal: true,
                dataOneRow:dataone,
                typeModal: t,
        }));  
    }

    getDataFilter(v) {  
        this.setState(() => ({
            filterData:v,
            loading:true      
          }), () =>{
              this.callApi()
            });  
    }
    handleCloseFormModal() {    
        this.setState(() => ({      
                showModal: false,
                // showModalDetail: false,
        }));  
    }




}


const mapStateToProps = (state) => {
    return{
        laporanAbsensi: state.laporanAbsensi
    } 
  }
export default connect(mapStateToProps, {getLaporanAbsen})(LaporanAbsen);
