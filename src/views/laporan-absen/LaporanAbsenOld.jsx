import React, { Component } from 'react';

import { Card, Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import BreadcrumbUser from '../../components/BreadcrumbUser';
import AbsenRingkasan from './absen-ringkasan/AbsenRingkasan';
import AbsenHarian from './absen-harian/AbsenHarian';
import FilterShowData1 from '../../components/filter/FilterShowData1';
import ComingSoon from '../page-coming-soon/ComingSoon';

import ReactToPrint from 'react-to-print';

import { monthName } from '../../actions/genneralAction';

import {connect} from 'react-redux';
import {getListAbsenInduk, getListAbsenIndukAll} from "../../actions/absensiAction";
import LaporanAbsensiHarian from '../../components/laporan/LaporanAbsensiHarian';
import LaporanAbsensiBulanan from '../../components/laporan/LaporanAbsensiBulanan';
import TabelLaporanAbsen from '../../components/laporan-absen/TabelLaporanAbsen';
const moment = require('moment');
const dataAwal= [
    {
        "pegawaiId": 2151,
        "namaPegawai": "MEXVAN PUDINAUNG",
        "nip": "198605202015031001",
        "jabatan": "GURU PERTAMA / AHLI PERTAMA",
        "unitIndukId": 212,
        "unitInduk": "DINAS PENDIDIKAN",
        "unitKerjaId": 711,
        "unitKerja": "SMP NEGERI 4 SATU ATAP SIAU TIMUR",
        "totalHariKerja": 19,
        "listAbsen": [
            {
                "tanggal": "2021-02-08",
                "jamMasuk": null,
                "jamPulang": null,
                "status": "TIDAK HADIR - TANPA KETERANGAN"
            },
            {
                "tanggal": "2021-02-09",
                "jamMasuk": null,
                "jamPulang": null,
                "status": "TIDAK HADIR - TANPA KETERANGAN"
            },
            {
                "tanggal": "2021-02-10",
                "jamMasuk": null,
                "jamPulang": null,
                "status": "TIDAK HADIR - TANPA KETERANGAN"
            },
            {
                "tanggal": "2021-02-11",
                "jamMasuk": null,
                "jamPulang": null,
                "status": "TIDAK HADIR - TANPA KETERANGAN"
            },
            {
                "tanggal": "2021-02-15",
                "jamMasuk": null,
                "jamPulang": null,
                "status": "TIDAK HADIR - TANPA KETERANGAN"
            },
            {
                "tanggal": "2021-02-16",
                "jamMasuk": null,
                "jamPulang": null,
                "status": "TIDAK HADIR - TANPA KETERANGAN"
            },
            {
                "tanggal": "2021-02-17",
                "jamMasuk": null,
                "jamPulang": null,
                "status": "TIDAK HADIR - TANPA KETERANGAN"
            },
            {
                "tanggal": "2021-02-18",
                "jamMasuk": null,
                "jamPulang": null,
                "status": "TIDAK HADIR - TANPA KETERANGAN"
            },
            {
                "tanggal": "2021-02-19",
                "jamMasuk": null,
                "jamPulang": null,
                "status": "TIDAK HADIR - TANPA KETERANGAN"
            }
        ]
    },
  ]
  

class LaporanAbsen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sub_page: "",
            datasMasuk:null,
            loading:true,
            listBreadcrumbUser: [
                { path: "/laporan-absen", name: "Laporan Absen", status:true },
            ],
            filterData:{
                endDate: moment().format('YYYY-MM-DD'),
                kodeUnitInduk: "all",
                kodeUnitKerja: "all",
                startDate: moment().format('YYYY-MM-DD'),
              }
        };
        this.getDataFilter = this.getDataFilter.bind(this);
        // this.renderSubPage = this.renderSubPage.bind(this);
    }

    SetBreadcrumbChild = (listBreadcrumbChild) =>{
        this.setState({listBreadcrumbUser: listBreadcrumbChild})
    }

    // renderSubPage(param, dataOlahan) {
    //     switch (param) {
    //         case 'absen-harian':
    //             return (<div>
    //                     <div style={{ display: 'none' }}>
    //                         <LaporanAbsensiHarian filterData={this.state.filterData} dataolahan={dataOlahan}  ref={(el) => (this.componentRef = el)}/>
    //                     </div>
    //                     <AbsenHarian breadcurmbChild={this.SetBreadcrumbChild} dataolahan={dataOlahan}/>;
    //                 </div>);
    //         case 'absen-ringkas':
    //                 return (
    //                 <div>
    //                     <div style={{ display: 'none' }}>
    //                         <LaporanAbsensiBulanan filterData={this.state.filterData} dataolahan={dataOlahan}  ref={(el) => (this.componentRef = el)}/>
    //                     </div>
    //                     <AbsenRingkasan breadcurmbChild={this.SetBreadcrumbChild} dataolahan={dataOlahan}/>
    //                 </div>
    //                 );
    //         default:
    //             return( 
    //             <div>
    //                     <div style={{ display: 'none' }}>
    //                         <LaporanAbsensiHarian  filterData={this.state.filterData} dataolahan={dataOlahan}  ref={(el) => (this.componentRef = el)}/>
    //                     </div>
    //                     <AbsenHarian  breadcurmbChild={this.SetBreadcrumbChild} dataolahan={dataOlahan}/>
    //             </div>);
    //     }
    // }

    handlePrint= () => this.componentRef
    
    componentDidMount(){
        this.callApi();
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.totalHadir !== prevProps.totalHadir){
            this.callApi();
        }
    }
    render(){
        let dataOlahan=this.olahData(this.state.datasMasuk, this.state.datasPulang);
        console.log(this.state.loading)

        let title=  "Jadwal Bulan "+monthName(moment(this.state.filterData.startDate).format('MM'))+' '+moment(this.state.filterData.startDate).format('YYYY');
    //   let jumlahTanggal=moment(this.state.filterData.startDate).daysInMonth();
      let jumlahTanggal=moment("2021-02").daysInMonth();

        return(
            <div className="container-fluid">
                {/* <ComingSoon/> */}
                <h1 className="mt-4">Laporan Absen</h1>
                <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />

                <FilterShowData1 print={this.handlePrint} loadingBtn={this.state.loading} dataFilter={this.getDataFilter}/>
                
                

                {/* <Nav fill variant="pills" className="nav3-dark" >
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/laporan-absen/absen-harian" href="/laporan-absen/absen-harian">Absen Harian</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/laporan-absen/absen-ringkas" href="/laporan-absen/absen-ringkas">Absen Ringkasan</Nav.Link>
                    </Nav.Item>
                </Nav> */}
             
                <Card>
                <TabelLaporanAbsen title={"Laporan Absensi"} jumlahTanggal={jumlahTanggal} dataRow={dataAwal} setdataonerow={this.setDataOneRow} />
                    {/* {this.renderSubPage(this.props.sub_page, dataOlahan)} */}
                </Card>


            </div>
        );
    }

    olahData = (dataMasuk, dataPulang) => {
        let datarow;
        if(dataMasuk != null && dataPulang!= null){
            let dataA_= dataMasuk.concat(dataPulang).sort((a, b) => new Date(a.jamAbsen)  - new Date(b.jamAbsen));
            let dataA= dataA_.sort((a, b) => {return a.nama.localeCompare(b.nama)})
                let output = [];
            
                dataA.forEach((item) => {
                  var existing = output.filter(function(v, i) {
                    return v.nip == item.nip;
                  });
        
                  if (existing.length) {
                      var existingIndex = output.indexOf(existing[0]);
                      let value={waktu: item.jamAbsen, type: item.tipeAbsen, keterangan:item.keterangan}
                      let totalKehadiran = output[existingIndex].totalKehadiran
                      let totalTerlambat = output[existingIndex].totalTerlambat
                    

                    if(output[existingIndex].absen){
                      output[existingIndex].absen = output[existingIndex].absen.concat(value);
                    }
                    if(item.keterangan === "MASUK_TEPAT_WAKTU"){
                        output[existingIndex].totalKehadiran=totalKehadiran+1
                    }
                    if(item.keterangan === "MASUK_TERLAMBAT"){
                        output[existingIndex].totalKehadiran=totalKehadiran+1
                        output[existingIndex].totalTerlambat=totalTerlambat+1
                    }

                  } else {
                    item.absen = [{waktu: item.jamAbsen, type: item.tipeAbsen, keterangan:item.keterangan}]
                    output.push(item);
                    if(item.keterangan === "MASUK_TEPAT_WAKTU"){
                        item.totalKehadiran=1;
                        item.totalTerlambat=0;
                    }
                    else if(item.keterangan === "MASUK_TERLAMBAT"){
                        item.totalKehadiran=1;
                        item.totalTerlambat=1;
                    }else{
                        item.totalKehadiran=0;
                        item.totalTerlambat=0;
                    }

                  }
                });
                datarow= output;
            // console.log(datarow)
        }
        return datarow;
    }

    async callApi(){
            if(this.state.filterData?.kodeUnitInduk !== null && this.state.filterData?.kodeUnitInduk !=="all"){

                await this.props.getListAbsenInduk(this.state.filterData?.kodeUnitInduk, this.state.filterData?.startDate, this.state.filterData?.endDate, "masuk").then( ()=>{
                    if(this.props.listAbsensiInduk?.data !== undefined && this.props.listAbsensiInduk?.data !== null){
                        let datasAll=this.props.listAbsensiInduk?.data;
                        if(this.state.filterData?.kodeUnitKerja !== null && this.state.filterData?.kodeUnitKerja !=="all"){
                            let idKodeUKerja = parseInt(this.state.filterData?.kodeUnitKerja);
                            datasAll=datasAll.filter(peggawai =>peggawai.unitKerjaId === idKodeUKerja);   
                        }
                        this.setState({
                        datasMasuk:datasAll,  
                        // loading:false,
                        }) 
                        // console.log(datasAll)
                        // return datasAll;
                    }
                });

                await this.props.getListAbsenInduk(this.state.filterData?.kodeUnitInduk, this.state.filterData?.startDate, this.state.filterData?.endDate, "pulang").then( ()=>{
                    if(this.props.listAbsensiInduk?.data !== undefined && this.props.listAbsensiInduk?.data !== null){
                        let datasAll=this.props.listAbsensiInduk?.data;
                        if(this.state.filterData?.kodeUnitKerja !== null && this.state.filterData?.kodeUnitKerja !=="all"){
                            let idKodeUKerja = parseInt(this.state.filterData?.kodeUnitKerja);
                            datasAll=datasAll.filter(peggawai =>peggawai.unitKerjaId === idKodeUKerja);   
                        }
                        this.setState({
                        datasPulang:datasAll,  
                        loading:false,
                        }) 
                        // console.log(datasAll)
                        // return datasAll;
                    }
                });
            }else{
                await this.props.getListAbsenIndukAll(this.state.filterData?.startDate, this.state.filterData?.endDate, "masuk").then( ()=>{
                    if(this.props.listAbsensiInduk?.data !== undefined && this.props.listAbsensiInduk?.data !== null){
                        let datasAll=this.props.listAbsensiInduk?.data;
                        this.setState({
                            datasMasuk:datasAll,
                            // loading:false,
                        }) 
                        // return datasAll;
                    }
                }
                );
                await this.props.getListAbsenIndukAll(this.state.filterData?.startDate, this.state.filterData?.endDate, "pulang").then( ()=>{
                    if(this.props.listAbsensiInduk?.data !== undefined && this.props.listAbsensiInduk?.data !== null){
                        let datasAll=this.props.listAbsensiInduk?.data;
                        this.setState({
                            datasPulang:datasAll,
                            loading:false,
                        }) 
                        // return datasAll;
                    }
                }
                );
            }
        
        
    }

    getDataFilter(v) {  
          this.setState(() => ({
              filterData:v,
              loading:true      
            }), () =>{this.callApi()});  
          
    }




}


const mapStateToProps = (state) => {
    return{
        listAbsensiInduk: state.listAbsensiInduk
    } 
  }
export default connect(mapStateToProps, {getListAbsenInduk, getListAbsenIndukAll})(LaporanAbsen);
