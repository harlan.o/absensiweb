import React, { Component } from 'react';
import TabelLaporanHarianPegawai from '../../../components/pegawai/TabelLaporanHarianPegawai';

class AbsenHarian extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listBreadcrumbUser: [
            { path: "/laporan-absen", name: "Laporan Absen", status:false },
            { path: "/laporan-absen/absen-harian", name: "Absen Harian", status:true },
        ],
        };
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
        
    }
    render(){
        let datarow
        if(this.props.dataolahan != null && this.props.dataolahan!= undefined){
            console.log("here")
            console.log(this.props.dataolahan)
            datarow=this.props.dataolahan
        }
        return (
            <div>
                <TabelLaporanHarianPegawai getFilter={this.state.filterData} dataRow={datarow} search=""/>
            </div>
        );
    }

}


export default AbsenHarian;