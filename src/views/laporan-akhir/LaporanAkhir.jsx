import React, { Component } from 'react';

import { Card, Nav } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import BreadcrumbUser from '../../components/BreadcrumbUser';
import ComingSoon from '../page-coming-soon/ComingSoon';

import ReactToPrint from 'react-to-print';

import {connect} from 'react-redux';
import {getListLaporanAkhirUmum, getListLaporanAkhirKhusus} from "../../actions/laporanAkhirAction";
import Akhir1 from './akhir-1/Akhir1';
import LaporanAkhirUmum from '../../components/laporan/LaporanAkhirUmum';
import TrueFilterSingleMonthSingleIndukMultipleKerja from '../../components/filter/TrueFilterSingleMonthSingleIndukMultipleKerja';
// import LaporanAbsensiBulanan from '../../components/laporan/LaporanAbsensiBulanan';
const moment = require('moment');

class LaporanAkhir extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sub_page: "",
            dataUmum:[],
            dataKhusus:[],
            loading:true,
            listBreadcrumbUser: [
                { path: "/laporan-absen", name: "Laporan Absen", status:true },
            ],
            filterData:{
                selectMultyUnitInduk: [],
                selectMultyUnitKerja: [],
                startDate: moment().format('YYYY-MM'),
            }
        };
        this.getDataFilter = this.getDataFilter.bind(this);
        this.renderSubPage = this.renderSubPage.bind(this);
    }

    SetBreadcrumbChild = (listBreadcrumbChild) =>{
        this.setState({listBreadcrumbUser: listBreadcrumbChild})
    }

    renderSubPage(param, dataOlahan) {
        switch (param) {
            case 'akhir-1':
                return (<div>
                        <div style={{ display: 'none' }}>
                            <LaporanAkhirUmum filterData={this.state.filterData} dataolahan={dataOlahan}  ref={(el) => (this.componentRef = el)}/>
                        </div>
                        <Akhir1 breadcurmbChild={this.SetBreadcrumbChild} dataolahan={dataOlahan}/>;
                    </div>);
            // case 'akhir-2':
            //         // return (
            //         // <div>
            //         //     <div style={{ display: 'none' }}>
            //         //         <LaporanAbsensiBulanan filterData={this.state.filterData} dataolahan={dataOlahan}  ref={(el) => (this.componentRef = el)}/>
            //         //     </div>
            //         //     <AbsenRingkasan breadcurmbChild={this.SetBreadcrumbChild} dataolahan={dataOlahan}/>
            //         // </div>
            //         // );
            default:
                return( 
                <div>
                       <div style={{ display: 'none' }}>
                            <LaporanAkhirUmum filterData={this.state.filterData} dataolahan={dataOlahan}  ref={(el) => (this.componentRef = el)}/>
                        </div>
                        <Akhir1  breadcurmbChild={this.SetBreadcrumbChild} dataolahan={dataOlahan}/>
                </div>);
        }
    }

    handlePrint= () => this.componentRef
    
    componentDidMount(){
        this.callApi();
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.totalHadir !== prevProps.totalHadir){
            this.callApi();
        }
    }
    render(){
        // let dataOlahan=this.olahData(this.state.datasMasuk, this.state.datasPulang);
        let dataOlahan=[
            {
                nama:"Test Nama",
                nip:"12312323",
                jabatan:"GURU PERTAMA / AHLI PERTAMA",
                jumlahHariKerja: 24,
                totalHadir: 20,
                totalIzin: 2,
                totalSakit: 2,
                totalTanpaKeterangan : 0,
                totalCuti: 0,
                totalTugasLuar: 0,
                nilaiTpp: 1000000
            },
            {
                nama:"Test Nama2 COSMAN ROMLY AMBALAO",
                nip:"12312323",
                jabatan:"GURU PERTAMA / AHLI PERTAMA",
                jumlahHariKerja: 24,
                totalHadir: 20,
                totalIzin: 2,
                totalSakit: 2,
                totalTanpaKeterangan : 0,
                totalCuti: 0,
                totalTugasLuar: 0,
                nilaiTpp: 1000000
            }
        ]
        ;


        console.log(this.state.loading)

        if (this.state.dataUmum != []){
            dataOlahan = this.state.dataUmum
            console.log("see this")
            console.log(dataOlahan)
        }
        return(
            <div className="container-fluid">
                {/* <ComingSoon/> */}
                <h1 className="mt-4">Laporan Akhir</h1>
                <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />
                <TrueFilterSingleMonthSingleIndukMultipleKerja dataFilter={this.getDataFilter} setloading={this.state.loading} print={this.handlePrint}/>
                
                <Nav fill variant="pills" className="nav3-dark" >
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/laporan-akhir/akhir-1" href="/laporan-akhir/akhir-1">Umum</Nav.Link>
                    </Nav.Item>
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/laporan-akhir/akhir-2" href="/laporan-akhir/akhir-2">Khusus</Nav.Link>
                    </Nav.Item>
                </Nav>
             
                <Card>
                    
                    {this.renderSubPage(this.props.sub_page, dataOlahan)}
                </Card>


            </div>
        );
    }

    async callApi(){
        this.props.getListLaporanAkhirUmum(this.state.filterData.startDate,  this.state.filterData.selectMultyUnitInduk, this.state.filterData.selectMultyUnitKerja).then(() =>{
            this.setState(() => ({      
                dataUmum: this.props.listLaporanAkhirUmum.data,
                loading:false,
            })); 
            console.log("call")
            console.log(this.props.listLaporanAkhirUmum.data)
        })
    }

    getDataFilter(v) {  
          this.setState(() => ({
              filterData:v,
              loading:true      
            }), () =>{this.callApi()});  
          
    }




}


const mapStateToProps = (state) => {
    return{
        listLaporanAkhirUmum: state.listLaporanAkhirUmum,
        listLaporanAkhirKhusus: state.listLaporanAkhirKhusus,
    } 
}
export default connect(mapStateToProps, {getListLaporanAkhirKhusus, getListLaporanAkhirUmum})(LaporanAkhir);
