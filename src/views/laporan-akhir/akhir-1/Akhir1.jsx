import React, { Component } from 'react';
import TabelLaporanHarianPegawai from '../../../components/pegawai/TabelLaporanHarianPegawai';
import TabelLaporanAkhirUmum from '../../../components/tpp/TabelLaporanAkhiriUmum';

class Akhir1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listBreadcrumbUser: [
            { path: "/laporan-akhir", name: "Laporan Akhir", status:false },
            { path: "/laporan-akhir/akhir-1", name: "Umum", status:true },
        ],
        };
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
        
    }
    render(){
        let datarow
        if(this.props.dataolahan != null && this.props.dataolahan!= undefined){
            console.log("here")
            console.log(this.props.dataolahan)
            datarow=this.props.dataolahan
        }
        return (
            <div>
                <TabelLaporanAkhirUmum getFilter={this.state.filterData} dataRow={datarow} search=""/>
            </div>
        );
    }

}


export default Akhir1;