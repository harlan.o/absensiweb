import React, { Component } from 'react';

import { Card } from 'react-bootstrap';
import BreadcrumbUser from '../../components/BreadcrumbUser';
import FilterMonthYearShowData from '../../components/filter/FilterMonthYearShowData';
import ComingSoon from '../page-coming-soon/ComingSoon';

import ReactToPrint from 'react-to-print';

import {connect} from 'react-redux';
import {getListTppHistory} from "../../actions/tppActions";
import TabelLaporanHistoryTpp from '../../components/tpp/TabelLaporanHistoryTpp';
import LaporanHistoryTpp from '../../components/laporan/LaporanHistoryTpp';
import ModalRevisiTpp from '../../components/tpp/ModalRevisiTpp';
import FilterSingleMonthMultiSuperUnit from '../../components/filter/FilterSingleMonthMultiSuperUnit';
import FilterMonthYearShowDataKerja from '../../components/filter/FilterMonthYearShowDataKerja';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU001, REGISTER_FU028 } from '../../constants/hakAksesConstants';
import { uIIDP } from '../../actions/userActions';
// import LaporanAbsensiHarian from '../../components/laporan/LaporanAbsensiHarian';
// import LaporanAbsensiBulanan from '../../components/laporan/LaporanAbsensiBulanan';
const moment = require('moment');

class LaporanTpp extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sub_page: "",
            datas:null,
            loading:true,
            showModalRevisi:false,
            dataOneRow:null,
            listBreadcrumbUser: [
                { path: "/laporan-absen", name: "Laporan Absen", status:true },
            ],
            filterData:{
                kodeUnitInduk: "all",
                kodeUnitKerja: "all",
                startDate: moment().format('YYYY-MM'),
              }
        };
        this.getDataFilter = this.getDataFilter.bind(this);
        this.handleSendToModalRevisi = this.handleSendToModalRevisi.bind(this);
        this.handleCloseFormModal = this.handleCloseFormModal.bind(this)
    }

    SetBreadcrumbChild = (listBreadcrumbChild) =>{
        this.setState({listBreadcrumbUser: listBreadcrumbChild})
    }

    

    handlePrint= () => this.componentRef
    
    handleSendToModalRevisi= async (dataOne) => {
        await this.setState(({       
            dataOneRow:dataOne,
            showModalRevisi: true,
        }));
    }
    handleCloseFormModal() {    
        this.setState(() => ({      
                showModalRevisi: false,
        }));  
    }
    componentDidMount(){
        this.callApi();
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.listHistoryTpp !== prevProps.listHistoryTpp){
            if(this.props.listHistoryTpp.data !== undefined){
                this.setState({ 
                    datas:this.props.listHistoryTpp.data,  
                    loading:false,
                })
            }else{
                this.setState({ 
                    datas:null,  
                    loading:true,
                })
            }
        }
    }
    render(){
        console.log(this.props.listHistoryTpp);
        console.log(this.state.datas);
        return(
            <div className="container-fluid">
                {/* <ComingSoon/> */}
                <h1 className="mt-4">Laporan TPP</h1>
                <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />

                {cekHakAksesClient(REGISTER_FU001) ? 
                <FilterMonthYearShowData print={this.handlePrint} dataFilter={this.getDataFilter} kodeCetak={REGISTER_FU028}/>                    
                :
                <FilterMonthYearShowDataKerja print={this.handlePrint} dataFilter={this.getDataFilter} kodeCetak={REGISTER_FU028}/> 
                }

                
                {/* <FilterSingleMonthMultiSuperUnit print={this.handlePrint} dataFilter={this.getDataFilter} setloading={this.state.loading}/> */}

                <Card>
                    <TabelLaporanHistoryTpp getFilter={this.state.filterData} dataRow={this.state.datas} search="" dataOneRow={this.handleSendToModalRevisi}/>
                </Card>
                <div style={{ display: 'none' }}>
                    <LaporanHistoryTpp filterData={this.state.filterData} datarow={this.state.datas}  ref={(el) => (this.componentRef = el)}/>
                </div>
                {this.state.showModalRevisi ?<ModalRevisiTpp show={this.state.showModalRevisi} onHide={this.handleCloseFormModal} data={this.state.dataOneRow} filterData={this.state.filterData}/> : "" }

            </div>
        );
    }


    async callApi(){
        let unitKerjaId, unitIndukId;
        if(this.state.filterData?.kodeUnitInduk === "all" || this.state.filterData?.kodeUnitInduk === null || this.state.filterData?.kodeUnitInduk === undefined){
            unitIndukId = "";
        }else{
            unitIndukId=this.state.filterData?.kodeUnitInduk;
            unitKerjaId = "";
        }
        if(this.state.filterData?.kodeUnitKerja === "all" || this.state.filterData?.kodeUnitKerja === null || this.state.filterData?.kodeUnitKerja === undefined){
            unitKerjaId = "";
        }else{
            unitIndukId = "";
            unitKerjaId=this.state.filterData?.kodeUnitKerja;
        }
        
        
        if(!cekHakAksesClient(REGISTER_FU001)){
            unitIndukId =  uIIDP()
        }

        await this.props.getListTppHistory(this.state.filterData?.startDate, unitIndukId, unitKerjaId).then(() =>{
            if(this.props.listHistoryTpp?.data !== undefined && this.props.listHistoryTpp?.data !== null){
                let datasAll=this.props.listHistoryTpp?.data
                this.setState({ 
                    datas:datasAll,  
                    loading:false,
                })
            }
        })

        
        
    }

    getDataFilter(v) {  
          this.setState(() => ({
              filterData:v      
            }), () =>{this.callApi()});  
          
    }




}


const mapStateToProps = (state) => {
    return{
        listHistoryTpp: state.listHistoryTpp
    } 
  }
export default connect(mapStateToProps, {getListTppHistory})(LaporanTpp);
