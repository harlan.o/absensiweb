import React, { Component } from 'react';
import {connect} from 'react-redux';
// import { useDispatch, useSelector } from 'react-redux';
import { Button, Col, Container, Form, Row, Media, Spinner } from 'react-bootstrap';
import "./login.css";
import Background from '../../img/img-login.jpg';
import LogoKab from '../../img/logo-kabupaten.png';
import { login } from "../../actions/userActions";
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";
var backgroundImage = {
    backgroundImage: `url(${Background})`,
    width: "100%",
    height: "100%",
    opacity: 0.9,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    backgroundPosition: "center",
    position: "relative",
  };
class Login extends Component {
    constructor(props){
        super(props);
        this.state={
            nip: "",
            password: "",
            loading:false,
        };
        this.login = this.login.bind(this); 
        this.MySwal = withReactContent(Swal);
    }
    
    login =  (e) =>  {
        e.preventDefault();
        this.setState({
            loading:true
        });
        this.props.login(this.state.nip, this.state.password);
    };

    componentDidUpdate(prevProps, prevState) {
        // console.log(this.state.loading);
       if(this.state.loading !== prevState.loading){
            this.setState({
                // loading:this.state.loading
            });
        }
        const {error, token} = this.props.userLogin;
        const {haks} = this.props.hakAkses;
        if(this.props.userLogin !== prevProps.userLogin){
            if (token && haks) {
                this.props.history.push("/dashboard");
              }

              if (error && error !== undefined) {
                this.MySwal.fire({
                  icon: "error",
                  title: error,
                }).then((result) => {
                  if (result.isConfirmed) {
                     this.setState({
                        nip:"",
                        password:""
                    });
                  }
                });
                this.setState({
                    loading:false
                });
              }
        }
    }
    render(){
        
        return(
            <Container className="d-flex justify-content-center h-100" >
            <Row className="justify-content-md-center">
                <Col md="auto" >
                    <div className="container_1">
                        <Media>
                            <img src={LogoKab} width={64} height={64} className="align-self-center mr-3" alt="Generic placeholder"/>
                            <Media.Body>
                                <p>Pemerintah Kabupaten</p>
                                <h4>Kepulauan Siau Tagulandang Biaro</h4>
                            </Media.Body>
                        </Media>
                    </div>
                </Col>
                <Col md="auto">
                    <div  className=" user_card img_container">

                        <div style={backgroundImage} >
                        <div className="img-text">
                            {/* <h1 style="font-size:50px">I am Jane Doe</h1> */}
                       <h1>SIMONGO</h1>
                        </div>
                        </div>
                    </div>
                </Col>
                <Col md="auto">
                    <Form  className=" user_card form_container">
                        <h3>E-Absensi</h3>
                        <Form.Group>
                            <Form.Label>NIP </Form.Label>
                            <Form.Control type="text" onChange={v => this.setState({nip:v.target.value})} placeholder="Masukan Nomor Induk Pegawai" />
                        </Form.Group>

                        <Form.Group>
                            <Form.Label>Password </Form.Label>
                            <Form.Control type="password" placeholder="Masukan Password" onChange={v => this.setState({password:v.target.value})}/>
                        </Form.Group>
                        <Form.Text className="text-muted">
                        <Form.Label>Lupas password? Hubungi Administartor </Form.Label>
                        </Form.Text>               

                        <Button type="button" variant="them-red" onClick={this.login} className="btn-lg btn-block"> 
                        
                        {this.state.loading ? (
                            <Spinner animation="border" className="spin" />
                        ) : ( "Login"
                        )}
                      </Button>
                        
                        
                    </Form>
                </Col>
            </Row>
            </Container>
            
        );
    }
}

const mapStateToProps = (state) => {
   return{
       userLogin: state.userLogin,
       hakAkses: state.hakAkses
   } 
}
// const mapsDispatchToProps = (dispatch) => {
//     console.log("return")
//     return {
//         handleSend: (nip, password) => dispatch(login(nip, password))
//     }
// }

export default connect(mapStateToProps, {login})(Login);