import React, { Component } from 'react';
import './Page404.css';

class Page404 extends Component {

render(){
    return(
        <div id="notfound">
		<div className="notfound-bg"></div>
		<div className="notfound">
			<div className="notfound-404">
				<h1>404</h1>
			</div>
			<h2>Mohon Maaf, Halaman Tidak Tersedia</h2>
			</div>
	</div>

    )
}
}

export default Page404;