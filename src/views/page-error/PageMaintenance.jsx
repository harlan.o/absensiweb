import React, { Component } from 'react';
import './Page404.css';

class PageMaintenance extends Component {
	render(){
		return(
			<div id="notfound">
			<div className="notfound-bg"></div>
			<div className="notfound">
				<div className="notfound-404">
					<h1>777</h1>
				</div>
				<h2>{this.props.message}</h2>
				</div>
		</div>
		)
	}
}

export default PageMaintenance;