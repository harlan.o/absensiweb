import React, { Component } from 'react';
import { tahunBulanIndo } from '../../actions/genneralAction';
import AlertPengembangan from '../../components/AlertPengembangan';
import BreadcrumbUser from '../../components/BreadcrumbUser';
import CreateFilterSingleMonthMultiSuperUnit from '../../components/filter/CreateFilterSingleMonthMultiSuperUnit';
import ModalPengambilanIjin from '../../components/pengambilan-ijin/ModalPengambilanIjin';
import TabelPengambilanIjin from '../../components/pengambilan-ijin/TabelPengambilanIjin';

import DaftarPengambilanIjin from '../../components/laporan/DaftarPengambilanIjin';

import {connect} from 'react-redux';
import {getListPengambilanIjin} from "../../actions/pengambilanIjinAction";
import CreateFilterSingleMonthMultiSuperUnitKerja from '../../components/filter/CreateFilterSingleMonthMultiSuperUnitKerja';
import { uIIDP } from '../../actions/userActions';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU001, REGISTER_FU012, REGISTER_FU013 } from '../../constants/hakAksesConstants';

const moment = require('moment');
const data=[{
	id:1,
	idPegawai:"2",
	namaPegawai:"Test",
	nip:"3434234",
	unitInduk:"unit Induk",
	unitIndukId:"1",
	unitKerja:"Unit kerja",
	unitKerjaId:"2",
	tanggalAwal:"2021-02-23",
	tanggalAkhir:"2022-03-23",
}]

class PengambilanIjin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            typeModal: "TAMBAH",
            data:null,
            dataOneRow:null,
            listBreadcrumbUser: [
            // { path: "/absensi-pegawai", name: "Absensi Pegawai", status:false },
            { path: "/pengambilan-ijin", name: "Pengambilan Ijin", status:true },
        ],
        loading:true,
        filterData:{
            groupingUnitKerja: [],
            selectMultyUnitInduk: [],
            selectMultyUnitKerja: [],
            startDate: moment().format('YYYY-MM'),
          }
        };
        this.handleShowFormModal = this.handleShowFormModal.bind(this);
        this.handleCloseFormModal = this.handleCloseFormModal.bind(this);
        this.getDataFilter = this.getDataFilter.bind(this);
        this.setDataOneRow = this.setDataOneRow.bind(this);
        this.handleSetLoading = this.handleSetLoading.bind(this);
    }
    handlePrint= () => this.componentRef


    componentDidMount(){
        this.callApi();
    }
    componentDidUpdate(prevProps, prevState) {
            // this.callApi();
    }
    render(){
        // console.log("call render")
        // console.log(this.state.showModal)
        return (
        <div className="container-fluid">
            <h1 className="mt-4">Pengambilan Ijin</h1>
            {/* <AlertPengembangan/> */}
            <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />
            {/* <ComingSoon/> */}

            {cekHakAksesClient(REGISTER_FU001) ? <CreateFilterSingleMonthMultiSuperUnit kodeCetak={REGISTER_FU012} kodeAdd={REGISTER_FU013} print={this.handlePrint} setloading={this.state.loading} dataFilter={this.getDataFilter} showForm={this.handleShowFormModal}/> : <CreateFilterSingleMonthMultiSuperUnitKerja print={this.handlePrint} kodeCetak={REGISTER_FU012} kodeAdd={REGISTER_FU013} setloading={this.state.loading} dataFilter={this.getDataFilter} showForm={this.handleShowFormModal}/>
            }
            <TabelPengambilanIjin datarow={this.state.data} getFIlter={this.state.filterData} title={tahunBulanIndo(this.state.filterData.startDate)} setdataonerow={this.setDataOneRow} actionLoading={this.handleSetLoading}/>
            <div style={{ display: 'none' }}>
                            <DaftarPengambilanIjin filterData={this.state.filterData} datatable={this.state.data} ref={(el) => (this.componentRef = el)}/>
                        </div>

            {this.state.showModal ? <ModalPengambilanIjin show={this.state.showModal} onHide={this.handleCloseFormModal} jenisModal={this.state.typeModal} dataonerow={this.state.dataOneRow} actionLoading={this.handleSetLoading}/> : "" }
        </div>
        );
    }

    async callApi(){
        let unitIndukArr = [
            {id:uIIDP()}
        ]

        if(cekHakAksesClient(REGISTER_FU001)){
            unitIndukArr = this.state.filterData?.selectMultyUnitInduk
        }  

        this.props.getListPengambilanIjin(this.state.filterData.startDate, unitIndukArr, this.state.filterData.selectMultyUnitKerja).then(() =>{
            // console.log("After Call")
            // console.log(this.props.listPengambilanIjin.data)
            let _datasAll = this.props.listPengambilanIjin.data
            let groupingUnitKerja =this.state.filterData.groupingUnitKerja;
            let datasAll =_datasAll;
            if(groupingUnitKerja.length > 0){
                datasAll=_datasAll.filter((pegawai) => {
                    let nilai=0;
                    for(let i = 0; i < groupingUnitKerja.length; i++){
                        let [a1]=pegawai.unitKerja.split(" ");
                        let [b1]=groupingUnitKerja[i].split(" ");
                        if (a1 == b1){
                            nilai=1
                        }
                    }
                    if(nilai == 1){
                        return true
                    }
                })
                console.log(datasAll);
            }
            this.setState(() => ({      
                data: datasAll,
                loading:false,
            })); 
        })
    }

    getDataFilter(v) {  
        this.setState(() => ({
            filterData:v,
            loading:true      
          }), () =>{this.callApi()});  
        
    }
    handleShowFormModal(e, type) {
        console.log(type)
        this.setState(() => ({      
            showModal: e,
            dataOneRow:null,
            typeModal: type,
        }));  
    }
    handleCloseFormModal() {    
        this.setState(() => ({      
                showModal: false,
                // showModalDetail: false,
        }));  
    }
    setDataOneRow(dataone, t) {  
        console.log(dataone)  
        console.log(t)  
        this.setState(() => ({      
                showModal: true,
                dataOneRow:dataone,
                typeModal: t,
        }));  
    }
    handleSetLoading(){
        this.setState(() => ({
            loading: true,    
          }), () =>{this.callApi()});  
    }

}

const mapStateToProps = (state) => {
    return{
        listPengambilanIjin: state.listPengambilanIjin
    } 
  }

export default connect(mapStateToProps, {getListPengambilanIjin})(PengambilanIjin);