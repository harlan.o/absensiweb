import React, { Component } from 'react';

import { Card, Nav } from 'react-bootstrap';
import { NavLink, Redirect } from 'react-router-dom';
import Pegawai from  './pegawai/Pegawai';
import AturanDisiplin from  './aturan-disiplin/AturanDisiplin';
import UnitKerja from  './unit-kerja/UnitKerja';
import LokasiAbsensi from  './lokasi-absensi/LokasiAbsensi';
import ManagementWaktu from  './management-waktu/ManagementWaktu';
import UnitInduk from './unit-induk/UnitInduk';
import BreadcrumbUser from '../../components/BreadcrumbUser';
import Tpp from './tpp/Tpp';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU057, REGISTER_FU063, REGISTER_FU067, REGISTER_FU071 } from '../../constants/hakAksesConstants';




class Pengaturan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sub_page: "",
            listBreadcrumbUser: [
                { path: "/laporan", name: "Laporan", status:true },
            ],
        };
    }
    SetBreadcrumbChild = (listBreadcrumbChild) =>{
        this.setState({listBreadcrumbUser: listBreadcrumbChild})
    }

    renderSubPage(param) {
        // console.log(param);
        switch (param) {
            case 'pegawai':
                if(cekHakAksesClient(REGISTER_FU057)){
                    return <Pegawai breadcurmbChild={this.SetBreadcrumbChild}/>;
                }
                return <Redirect to="/pengaturan/unit-induk" />
            case 'unit-induk':
                if(cekHakAksesClient(REGISTER_FU063)){
                    return <UnitInduk breadcurmbChild={this.SetBreadcrumbChild}/>;
                }
                return <Redirect to="/pengaturan/unit-kerja" />
            case 'unit-kerja':
                if(cekHakAksesClient(REGISTER_FU067)){
                    return <UnitKerja breadcurmbChild={this.SetBreadcrumbChild}/>;
                }
                return <Redirect to="/pengaturan/tpp" />
            case 'aturan-disiplin':
                    return <AturanDisiplin breadcurmbChild={this.SetBreadcrumbChild}/>;
            case 'tpp':
                if(cekHakAksesClient(REGISTER_FU071)){
                    return <Tpp breadcurmbChild={this.SetBreadcrumbChild}/>;
                }
                return <Redirect to="/*" />
            // case 'lokasi-absensi':
            //     return <LokasiAbsensi breadcurmbChild={this.SetBreadcrumbChild}/>;
            // case 'management-waktu':
            //     return <ManagementWaktu breadcurmbChild={this.SetBreadcrumbChild}/>;
            default:
                if(cekHakAksesClient(REGISTER_FU057)){
                    return <Pegawai breadcurmbChild={this.SetBreadcrumbChild}/>;
                }
                <Redirect to="/pengaturan/unit-induk" />
        }
    }

    render(){
        return(
            <div className="container-fluid">
                <h1 className="mt-4">Pengaturan</h1>
                <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />

                <Nav fill variant="pills" className="nav3-dark" >
                {cekHakAksesClient(REGISTER_FU057) ?
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/pengaturan/pegawai" href="/pengaturan/pegawai">Pegawai</Nav.Link>
                    </Nav.Item>
                :""}
                {cekHakAksesClient(REGISTER_FU063) ?
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/pengaturan/unit-induk" href="/pengaturan/unit-induk">Unit Induk</Nav.Link>
                    </Nav.Item>
                :""}
                {cekHakAksesClient(REGISTER_FU067) ?    
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/pengaturan/unit-kerja" href="/pengaturan/unit-kerja">Unit Kerja</Nav.Link>
                    </Nav.Item>
                :""}
                {cekHakAksesClient(REGISTER_FU071) ? 
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/pengaturan/tpp" href="/pengaturan/aturan-disiplin">TPP</Nav.Link>
                    </Nav.Item>
                :""}
                    {/* <Nav.Item>
                        <Nav.Link as={NavLink} to="/pengaturan/lokasi-absensi" href="/pengaturan/lokasi-absensi">Lokasi Absensi</Nav.Link>
                    </Nav.Item> */}
                    {/* <Nav.Item>
                        <Nav.Link as={NavLink} to="/pengaturan/management-waktu" href="/pengaturan/management-waktu">Management Waktu</Nav.Link>
                    </Nav.Item> */}
                </Nav>
             
                <Card>
                    {this.renderSubPage(this.props.sub_page)}
                </Card>


            </div>
        );
    }



}
export default Pengaturan;