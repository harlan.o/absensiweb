import React, { Component } from 'react';
import CreatePanel from '../../../components/CreatePanel';
import ComingSoon2 from '../../page-coming-soon/ComingSoon2';



class AturanDisiplin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listBreadcrumbUser: [
            { path: "/pengaturan", name: "Pengaturan", status:false },
            { path: "/pengaturan/aturan-disiplin", name: "Aturan Disiplin", status:true },
        ],
        };
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }
    render(){
        return (
            <div className="container-fluid">
                <CreatePanel/>
                <ComingSoon2/>
                <p>Aturan Disiplin</p>
            </div>
        )
    };
    

}
export default AturanDisiplin;