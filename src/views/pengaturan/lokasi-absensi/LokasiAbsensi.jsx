import React, { Component } from 'react';
import { Accordion, Button, Card, Col, ListGroup, Row } from 'react-bootstrap';
import CreatePanel from '../../../components/CreatePanel';
import DataTableDinasPadaLokasi from '../../../components/DataTableDinasPadaLokasi';
import GroupButtonLokasiAbsensi from '../../../components/GroupButtonLokasiAbsensi';
import ListGroupDinasPadaLokasi from '../../../components/ListGroupDinasPadaLokasi';
import SimpleMap from '../../../components/SimpleMap';
import './LokasiAbsensi.css'
class LokasiAbsensi extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listBreadcrumbUser: [
            { path: "/pengaturan", name: "Pengaturan", status:false },
            { path: "/pengaturan/lokasi-absensi", name: "Lokasi Absensi", status:true },
        ],
        };
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }
    render(){
        return (
            <Row>
    <Col xs={12} md={4}>
            <div className="container-fluid">
                <CreatePanel/>
                <h5>Lokasi Absensi</h5>

                <Accordion defaultActiveKey="0" className="daftarlokasi">
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="0">
                        Tahulandang Barat
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="0">
                        <Card.Body>
                        <GroupButtonLokasiAbsensi/>
                           Daftar Dinas
                           <ListGroupDinasPadaLokasi/>
                        </Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="1">
                        Tahulandang Timur
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="1">
                        <Card.Body>Hello! I'm another body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="2">
                        Tahulandang Timur
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="2">
                        <Card.Body>Hello! I'm another body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="3">
                        Tahulandang Timur
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="3">
                        <Card.Body>Hello! I'm another body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="4">
                        Tahulandang Timur
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="4">
                        <Card.Body>Hello! I'm another body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="5">
                        Tahulandang Timur
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="5">
                        <Card.Body>Hello! I'm another body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="6">
                        Tahulandang Timur
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="6">
                        <Card.Body>Hello! I'm another body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="7">
                        Tahulandang Timur
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="7">
                        <Card.Body>Hello! I'm another body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="8">
                        Tahulandang Timur
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="8">
                        <Card.Body>Hello! I'm another body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="9">
                        Tahulandang Timur
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="9">
                        <Card.Body>Hello! I'm another body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="10">
                        Tahulandang Timur
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="10">
                        <Card.Body>Hello! I'm another body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                    <Card>
                        <Accordion.Toggle as={Card.Header} eventKey="11">
                        Tahulandang Timur
                        </Accordion.Toggle>
                        <Accordion.Collapse eventKey="11">
                        <Card.Body>Hello! I'm another body</Card.Body>
                        </Accordion.Collapse>
                    </Card>
                </Accordion>
            </div>
    </Col>
    <Col xs={12} md={8}>
                <SimpleMap/>
    </Col>
  </Row>
                
        )
    }
    

}
export default LokasiAbsensi;