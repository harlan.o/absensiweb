import React, { Component, useState } from 'react';
import { Button, Col, Form, Modal, Row } from 'react-bootstrap';
import CreatePanel from '../../../components/CreatePanel';
import DatePicker from 'react-datepicker';
import DataTablePengaturanPegawai from '../../../components/DataTablePengaturanPegawai';
import "react-datepicker/dist/react-datepicker.css";
import "./Pegawai.css"
class Pegawai extends Component {
   

    constructor(props) {
        super(props);
        this.state = {
            showModal:false,
            listBreadcrumbUser: [
            { path: "/pengaturan", name: "Pengaturan", status:false },
            { path: "/pengaturan/pegawai", name: "Pegawai", status:true },
        ],
        };
        this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this); 
        this.handleCloseFormModalAdd = this.handleCloseFormModalAdd.bind(this); 
    }

    handleShowFormModalAdd(e) {    
        console.log(e)
        this.setState(() => ({      
                showModal: e
        }));  
    }
    handleCloseFormModalAdd() {    
        this.setState(() => ({      
                showModal: false
        }));  
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }
    render(){
        return (
            <div className="container-fluid">
                <CreatePanel showForm={this.handleShowFormModalAdd}/>
                <DataTablePengaturanPegawai />
                <p>Daftar Pegawai</p>
               
                <MyVerticallyCenteredModal
                show={this.state.showModal}
                onHide={this.handleCloseFormModalAdd}
                />

                {/* <Modal show={this.state.showModal} onHide={this.handleCloseFormModalAdd}>
                    <Modal.Header closeButton>
                    <Modal.Title>Tambah Pegawai</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>Woohoo, you're reading this text in a modal!</Modal.Body>
                    <Modal.Footer>
                    <Button variant="secondary" onClick={this.handleCloseFormModalAdd}>
                        Close
                    </Button>
                    <Button variant="primary" onClick={this.handleCloseFormModalAdd}>
                        Save Changes
                    </Button>
                    </Modal.Footer>
                </Modal> */}



            </div>
        );
    }

}
function MyVerticallyCenteredModal(props) {
    const [birth, setBrith] = useState(new Date());
    
    return (
      <Modal 
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
          Tambah Pegawai
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>

            <Form >
                {/* <Form.Group as={Row} controlId="form_nip" >
                    <Form.Label column sm="12" md="2" className="textblack">
                    NIP
                    </Form.Label>
                    <Col sm="12" md="10">
                    <Form.Control type="text" placeholder="Masukan Nomor Induk Pegawai" />
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="form_ktp" >
                    <Form.Label column sm="12" md="2" className="textblack">
                    No KTP
                    </Form.Label>
                    <Col sm="12" md="10">
                    <Form.Control type="text" placeholder="Masukan Nomor KTP" />
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="form_nama" >
                    <Form.Label column sm="12" md="2" className="textblack">
                    Nama
                    </Form.Label>
                    <Col sm="12" md="10">
                    <Form.Control type="text" placeholder="Masukan Nama Pegawai" />
                    </Col>
                </Form.Group>
                <Form.Group as={Row}>
                <Col sm="12" md="2">
                    Gelar
                </Col>
                <Col sm="12" md="10">
                    <Form.Group as={Row} controlId="form_gelar_depan" >
                        <Form.Label column sm="2" md="2" className="textblack">
                        Depan
                        </Form.Label>
                        <Col sm="4" md="4">
                        <Form.Control type="text" placeholder="Masukan Gelar Depan" />

                        </Col>
                    </Form.Group>
                    <Form.Group as={Row} controlId="form_gelar_belakang" >
                        <Form.Label column sm="2" md="2" className="textblack">
                        Belakang
                        </Form.Label>
                        <Col sm="4" md="4">
                        <Form.Control type="text" placeholder="Masukan Gelar Belakang" />
                        </Col>
                    </Form.Group>
                </Col>
                </Form.Group>
                
                <Form.Group as={Row} controlId="form_jenis_kelamin" >
                    <Form.Label column sm="12" md="2" className="textblack">
                    Jenis Kelamin
                    </Form.Label>
                    <Col sm="12" md="10">
                    <Form.Check 
                        type="radio"
                        // id={`default-${type}`}
                        label="Laki-laki"
                        name="form_jenis_kelamin"
                    />
                    <Form.Check 
                        type="radio"
                        // id={`default-${type}`}
                        label="Perempuan"
                        name="form_jenis_kelamin"
                    />
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="form_tempat_lahir" >
                    <Form.Label column sm="12" md="2" className="textblack">
                    Tempat Lahir
                    </Form.Label>
                    <Col sm="12" md="10">
                    <Form.Control type="text" placeholder="Masukan Tempat Lahir" />
                    </Col>
                </Form.Group>
                <Form.Group as={Row} controlId="form_nama" >
                    <Form.Label column sm="12" md="2" className="textblack">
                    Tanggal Lahir
                    </Form.Label>
                    <Col sm="12" md="10">
                    <Form.Control type="text" placeholder="Masukan Tanggal Lahir" />
                    </Col>
                </Form.Group> */}


    <Form.Row>
        <Form.Group as={Col} controlId="form_nip">
        <Form.Label className="textblack">NIP</Form.Label>
        <Form.Control type="text" placeholder="Masukan Nomor Induk" />
        </Form.Group>

        <Form.Group as={Col} controlId="form_ktp">
        <Form.Label className="textblack">No KTP</Form.Label>
        <Form.Control type="text" placeholder="Masukan No KTP" />
        </Form.Group>
    </Form.Row>
    <Form.Row>
        <Form.Group as={Col} controlId="form_nama">
        <Form.Label className="textblack">Nama Pegawai</Form.Label>
        <Form.Control type="text" placeholder="Masukan Nama Pegawai" />
        </Form.Group>

        <Form.Group as={Col} sm="3" controlId="form_gelar_depan">
        <Form.Label className="textblack">Gelar Depan</Form.Label>
        <Form.Control as="select" defaultValue="Pilih...">
            <option>Pilih...</option>
            <option>...</option>
        </Form.Control>
        </Form.Group>
        <Form.Group as={Col} sm="3"  controlId="form_gelar_belakang">
        <Form.Label className="textblack">Gelar Belakang</Form.Label>
        <Form.Control as="select" defaultValue="Pilih...">
            <option>Pilih...</option>
            <option>...</option>
        </Form.Control>
        </Form.Group>
    </Form.Row>

  <Form.Group>
    <Form.Label className="textblack">Jenis Kelamin</Form.Label>
    <Form>
        <Form.Check
            inline
            name="form_jenis_kelamin"
            label="Laki-Laki"
            type="radio"
        />
        <Form.Check
            inline
            name="form_jenis_kelamin"
            label="Perempuan"
            type="radio"
        />
    </Form>
  </Form.Group>

  <Form.Row>
        <Form.Group as={Col} controlId="form_tempat_lahir">
        <Form.Label className="textblack">Tempat Lahir</Form.Label>
        <Form.Control type="text" placeholder="Masukan Tempat Lahir" />
        </Form.Group>

        <Form.Group as={Col} controlId="form_tanggal_lahir">
        <Form.Label className="textblack">Tanggal Lahir</Form.Label>
        <Form >
        <DatePicker         
                                className="form-control"
                                selected={birth}
                                dateFormat="yyyy-MM-dd"
                                onChange={date => setBrith(date)}
                                placeholderText="Pilih Tanggal"
                            />
                            </Form>
        </Form.Group>
    </Form.Row>



  <Form.Row>
    <Form.Group as={Col} controlId="form_jabatan">
      <Form.Label className="textblack">Jabatan</Form.Label>
      <Form.Control as="select" defaultValue="Pilih...">
        <option>Pilih...</option>
        <option>...</option>
      </Form.Control>
    </Form.Group>

    <Form.Group as={Col} controlId="form_golongan">
      <Form.Label className="textblack">Golongan</Form.Label>
      <Form.Control as="select" defaultValue="Pilih...">
        <option>Pilih...</option>
        <option>...</option>
      </Form.Control>
    </Form.Group>

    <Form.Group as={Col} controlId="form_pendidikan_terakhir">
      <Form.Label className="textblack">Pendidikan Terakhir</Form.Label>
      <Form.Control as="select" defaultValue="Pilih...">
        <option>Pilih...</option>
        <option>...</option>
      </Form.Control>
    </Form.Group>
  </Form.Row>
            </Form>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={props.onHide}>
                Tutup
            </Button>
            <Button variant="primary" className="btn-them-red" onClick={props.onHide}>
                Simpan
            </Button>
        </Modal.Footer>
      </Modal>
    );
  }

export default Pegawai;