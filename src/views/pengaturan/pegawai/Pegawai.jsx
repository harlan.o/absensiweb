import React, { Component, } from 'react';

import CreatePanel from '../../../components/CreatePanel';

import DataTablePengaturanPegawai from '../../../components/pegawai/DataTablePengaturanPegawai';
import "react-datepicker/dist/react-datepicker.css";
import "./Pegawai.css";
import {DataPegawai} from "../../../stockdata";
import DaftarPegawai from '../../../components/laporan/DaftarPegawai';

// Redux
import {connect} from 'react-redux';
import { getListPegawai } from "../../../actions/pegawaiActions";
import { getListUnitKerja } from "../../../actions/unitKerjaAction";
import { getListTpp } from "../../../actions/tppActions";

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";


import ModalPengaturanPegawai from '../../../components/pegawai/ModalPengaturanPegawai';
import { Spinner } from 'react-bootstrap';
import { Fragment } from 'react';
import ModalPengaturanPegawaiDetail from '../../../components/pegawai/ModalPengaturanPegawaiDetail';
import ModalPengaturanPegawaiResetPass from '../../../components/pegawai/ModalPengaturanPegawaiResetPass';
import ModalPengaturanPegawaiResetMobile from '../../../components/pegawai/ModalPengaturanPegawaiResetMobile';
import { REGISTER_FU059, REGISTER_FU058 } from '../../../constants/hakAksesConstants';



class Pegawai extends Component {
   

    constructor(props) {
        super(props);
        this.state = {
            showModal:false,
            showModalDetail:false,
            showModalResetMobile:false,
            showModalResetPass:false,
            dataOneRow:null,
            dataRowUnitKerja:null,
            dataRowTpp:null,
            
            listBreadcrumbUser: [
            { path: "/pengaturan", name: "Pengaturan", status:false },
            { path: "/pengaturan/pegawai", name: "Pegawai", status:true },
        ],
            loadingPage:true,
            dataRow:[],
            renderTable:false,
        };
        this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this); 
        this.handleSetLoading = this.handleSetLoading.bind(this); 
        this.handleCloseFormModalAdd = this.handleCloseFormModalAdd.bind(this); 
        this.handleSendToModalEdit = this.handleSendToModalEdit.bind(this); 
        this.handleSendToModalDetail = this.handleSendToModalDetail.bind(this); 
        this.props.getListPegawai();
        this.props.getListUnitKerja();
        this.props.getListTpp();

        this.MySwal = withReactContent(Swal);
    }

    handleShowFormModalAdd(e) {    
        // console.log(e)
        this.setState(() => ({      
                showModal: e,
                dataOneRow:null,
        }));  
    }
    handleCloseFormModalAdd() {    
        this.setState(() => ({      
                showModal: false,
                showModalDetail: false,
                showModalResetMobile: false,
                showModalResetPass: false,
        }));  
    }
    handleSetLoading(){
        this.setState(() => ({      
            loadingPage: true,   
            renderTable: false,     
        }));
    }
    async handleSendToModalEdit(dataOne) {    
        await this.setState(({       
            dataOneRow:dataOne,
            showModal: true,
        }));
    }
    async handleSendToModalDetail(dataOne, type) {    
        console.log(type)
        switch (type) {
            case "DETAIL":
                await this.setState(({       
                    dataOneRow:dataOne,
                    showModalDetail: true,
                }));       
                break;
            case "RESET_MOBILE":
                await this.setState(({       
                    dataOneRow:dataOne,
                    showModalResetMobile: true,
                }));       
                break;
            case "RESET_PASS":
                await this.setState(({       
                    dataOneRow:dataOne,
                    showModalResetPass: true,
                }));       
                break;
        
            default:
                break;
        }

        // console.log("hai")
        // console.log(this.state.dataOneRow)
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.listPengPegawai !== prevProps.listPengPegawai){
            if(this.props.listPengPegawai?.data !== undefined){
                this.setState(() => ({
                    dataRow: this.props.listPengPegawai?.data,
                    dataRowUnitKerja: this.props.listPengUnitKerja?.data,
                    dataRowTpp: this.props.listPengTpp?.data,
                    renderTable:true,
                    loadingPage:false
                }));
            }
        }
    }
    handlePrint= () => this.componentRef

    render(){
        return (
            <div className="container-fluid">
                
                {this.state.renderTable ? 
                (<Fragment>
                    <CreatePanel kodeAdd={REGISTER_FU059} kodeCetak={REGISTER_FU058} print={this.handlePrint} showForm={this.handleShowFormModalAdd}/>
                    <DataTablePengaturanPegawai dataRow={this.state.dataRow} dataOneRow={this.handleSendToModalEdit} detail={this.handleSendToModalDetail}/>
                    <div style={{ display: 'none' }}>
                            <DaftarPegawai filterData={this.state.filterData} datatable={this.state.dataRow} ref={(el) => (this.componentRef = el)}/>
                        </div>
                </Fragment>)
                 : "" }
                {this.state.loadingPage ? (
                    <div className="loadingPage">
                        <Spinner animation="border" className="spin"  variant="light" />
                    </div>
                    ) : ("")}

               {this.state.showModal ?<ModalPengaturanPegawai show={this.state.showModal} onHide={this.handleCloseFormModalAdd} dataedit={this.state.dataOneRow} dataoptionunitkerja={this.state.dataRowUnitKerja} dataoptiontpp={this.state.dataRowTpp} actionLoading={this.handleSetLoading}/> : "" }

               {this.state.showModalDetail ?<ModalPengaturanPegawaiDetail show={this.state.showModalDetail} onHide={this.handleCloseFormModalAdd} datadetail={this.state.dataOneRow} dataoptionunitkerja={this.state.dataRowUnitKerja} dataoptiontpp={this.state.dataRowTpp}/> : "" }

               {this.state.showModalResetMobile ? <ModalPengaturanPegawaiResetMobile show={this.state.showModalResetMobile} onHide={this.handleCloseFormModalAdd} datadetail={this.state.dataOneRow} /> : "" }

               {this.state.showModalResetPass ? <ModalPengaturanPegawaiResetPass show={this.state.showModalResetPass} onHide={this.handleCloseFormModalAdd} datadetail={this.state.dataOneRow}/> : "" }

            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return{
        listPengUnitKerja: state.listPengUnitKerja,
        listPengTpp: state.listPengTpp,
        listPengPegawai: state.listPengPegawai
    } 
 }
export default connect(mapStateToProps, {getListPegawai, getListUnitKerja, getListTpp})(Pegawai);