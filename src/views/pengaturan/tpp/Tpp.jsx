import React, { Component, } from 'react';

import CreatePanel from '../../../components/CreatePanel';

import DataTablePengaturanTpp from '../../../components/tpp/DataTablePengaturanTpp';
import "react-datepicker/dist/react-datepicker.css";
import "./Tpp.css";
import {DataTpp} from "../../../stockdata";

import ReactToPrint from 'react-to-print';

// Redux
import {connect} from 'react-redux';
import { getListTpp } from "../../../actions/tppActions";

// Sweet Alert
import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";


import ModalPengaturanTpp from '../../../components/tpp/ModalPengaturanTpp';
import { Button, Spinner } from 'react-bootstrap';
import { Fragment } from 'react';
import DaftarPegawaiTpp from '../../../components/laporan/DaftarPegawaiTpp';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPrint } from '@fortawesome/free-solid-svg-icons';
import { cekHakAksesClient } from '../../../actions/hakAksesActions';
import { REGISTER_FU072 } from '../../../constants/hakAksesConstants';
class Tpp extends Component {
   

    constructor(props) {
        super(props);
        this.state = {
            showModal:false,
            dataOneRow:null,
            dataRowUnitKerja:null,
            dataRowTpp:null,
            
            listBreadcrumbUser: [
            { path: "/pengaturan", name: "Pengaturan", status:false },
            { path: "/pengaturan/pegawai", name: "Tpp", status:true },
        ],
            loadingPage:true,
            dataRow:[],
            renderTable:false,
        };
        this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this); 
        this.handleCloseFormModalAdd = this.handleCloseFormModalAdd.bind(this); 
        this.handleSendToModalEdit = this.handleSendToModalEdit.bind(this); 
        this.props.getListTpp();
  
        this.MySwal = withReactContent(Swal);
    }

    handleShowFormModalAdd(e) {    
        // console.log(e)
        this.setState(() => ({      
                showModal: e,
                dataOneRow:null,
        }));  
    }
    handleCloseFormModalAdd() {    
        this.setState(() => ({      
                showModal: false
        }));  
    }
    async handleSendToModalEdit(dataOne) {    
        await this.setState(({       
            dataOneRow:dataOne,
            showModal: true,
        }));
        // console.log(this.state.showModal)
        // console.log(this.state.dataOneRow)
    }
    handlePrint= () => this.componentRef
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.listPengTpp !== prevProps.listPengTpp){
            if(this.props.listPengTpp?.data !== undefined){
                // console.log(this.props.listPengTpp)
                // console.log("update"+this.props.listPengTpp?.data);
                this.setState(() => ({
                    dataRow: this.props.listPengTpp?.data,
                    dataRowUnitKerja: this.props.listPengUnitKerja?.data,
                    dataRowTpp: this.props.listPengTpp?.data,
                    renderTable:false,
                    loadingPage:true
                }));
                setTimeout(()=>{ 
                    this.setState(() => ({
                        dataRow: this.props.listPengTpp?.data,
                        dataRowUnitKerja: this.props.listPengUnitKerja?.data,
                        dataRowTpp: this.props.listPengTpp?.data,
                        renderTable:true,
                        loadingPage:false
                    }));
                 }, 2000);
            }
        }
    }
    render(){
        // console.log("render pegawai");
        return (
            <div className="container-fluid">
                
                {this.state.renderTable ? 
                (<Fragment>
                    {cekHakAksesClient(REGISTER_FU072) ? 
                        <ReactToPrint
                        trigger={() => {
                        
                            return <Button variant="secondary" className="btn-width"> <FontAwesomeIcon icon={faPrint} /></Button>;
                        }}
                        content={this.handlePrint}
                        />
                    :""}
                    {/* <CreatePanel showForm={this.handleShowFormModalAdd}/> */}
                    <DataTablePengaturanTpp dataRow={this.state.dataRow} dataOneRow={this.handleSendToModalEdit}/>
                    <div style={{ display: 'none' }}>
                            <DaftarPegawaiTpp filterData={this.state.filterData} datatable={this.state.dataRow} ref={(el) => (this.componentRef = el)}/>
                        </div>
               
                </Fragment>)
                 : "" }
                {/* <DataTablePengaturanTpp dataRow={DataTpp} dataOneRow={this.handleSendToModalEdit}/> */}
                {this.state.loadingPage ? (
                    <div className="loadingPage">
                        <Spinner animation="border" className="spin"  variant="light" />
                    </div>
                    ) : ("")}

               {this.state.showModal ?<ModalPengaturanTpp show={this.state.showModal} onHide={this.handleCloseFormModalAdd} dataedit={this.state.dataOneRow} dataoptionunitkerja={this.state.dataRowUnitKerja} dataoptiontpp={this.state.dataRowTpp}/> : "" }
            </div>
        );
    }

}

const mapStateToProps = (state) => {
    return{
        listPengTpp: state.listPengTpp
    } 
 }
export default connect(mapStateToProps, { getListTpp})(Tpp);