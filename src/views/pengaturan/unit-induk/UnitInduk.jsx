import React, { Component, useState } from 'react';
import CreatePanel from '../../../components/CreatePanel';
import { Button, Card, Col, Form, Modal } from 'react-bootstrap';
import DataTablePengaturanUnitInduk from '../../../components/unit-induk/DataTablePengaturanUnitInduk';

import DaftarUnitInduk from '../../../components/laporan/DaftarUnitInduk';

import {connect} from 'react-redux';
import { getListUnitInduk } from "../../../actions/unitIndukAction";
import { Spinner } from 'react-bootstrap';
import ModalPengaturanUnitInduk from '../../../components/unit-induk/ModalPengaturanUnitInduk';
import { Fragment } from 'react';
import { REGISTER_FU064, REGISTER_FU065 } from '../../../constants/hakAksesConstants';
class UnitInduk extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal:false,
            dataOneRow:null,
            listBreadcrumbUser: [
            { path: "/pengaturan", name: "Pengaturan", status:false },
            { path: "/pengaturan/UnitInduk", name: "UnitInduk", status:true },
        ],
            loadingPage:true,
            dataRow:[],
            renderTable:false,
        };
        this.handleSetLoading = this.handleSetLoading.bind(this); 
        this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this); 
        this.handleCloseFormModalAdd = this.handleCloseFormModalAdd.bind(this); 
        this.handleSendToModalEdit = this.handleSendToModalEdit.bind(this);
        this.props.getListUnitInduk();
        
        // console.log("here"+ this.props.listPengUnitInduk?.data);
    }
    
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.listPengUnitInduk !== prevProps.listPengUnitInduk){
            
            if(this.props.listPengUnitInduk?.data !== undefined){
                // console.log("update"+this.props.listPengUnitInduk?.data);
                this.setState((state) => ({
                    state,dataRow: this.props.listPengUnitInduk?.data,
                    renderTable:true,
                    loadingPage:false
                }));
            } 

        }
    }
    handleShowFormModalAdd(e) {    
        console.log(e)
        this.setState(() => ({      
                showModal: e,
                dataOneRow:null,
        }));  
    }
    handleCloseFormModalAdd() {    
        this.setState(() => ({      
                showModal: false
        }));  
    }
    handleSetLoading(){
        this.setState(() => ({      
            loadingPage: true,   
            renderTable: false,     
        }));
    }
    async handleSendToModalEdit(dataOne) {    
        await this.setState(({       
            dataOneRow:dataOne,
            showModal: true,
        }));
        console.log(this.state.showModal)
        console.log(this.state.dataOneRow)
    }
    handlePrint= () => this.componentRef

    render(){
        return (
            <div className="container-fluid">
                <CreatePanel kodeAdd={REGISTER_FU065} kodeCetak={REGISTER_FU064} print={this.handlePrint} showForm={this.handleShowFormModalAdd}/>
                {this.state.renderTable ?(<Fragment>
                    <DataTablePengaturanUnitInduk dataRow={this.state.dataRow} dataOneRow={this.handleSendToModalEdit}/>
                    <div style={{ display: 'none' }}>
                        <DaftarUnitInduk datatable={this.state.dataRow} ref={(el) => (this.componentRef = el)}/>
                    </div> 
                </Fragment>) 
                : "" }
                {this.state.loadingPage ? (
                    <div className="loadingPage">
                        <Spinner animation="border" className="spin"  variant="light" />
                    </div>
                    ) : ("")}

                {this.state.showModal ?
                <ModalPengaturanUnitInduk show={this.state.showModal} onHide={this.handleCloseFormModalAdd} dataedit={this.state.dataOneRow} actionLoading={this.handleSetLoading}/> : "" }
                
            </div>
        )
    };
    

}




  const mapStateToProps = (state) => {
    return{
        listPengUnitInduk: state.listPengUnitInduk
    } 
 }
export default connect(mapStateToProps, {getListUnitInduk})(UnitInduk);