import React, { Component, Fragment } from 'react';
import CreatePanel from '../../../components/CreatePanel';
import {connect} from 'react-redux';
import DataTablePengaturanUnitKerja from '../../../components/unit-kerja/DataTablePengaturanUnitKerja';
import { getListUnitKerja } from "../../../actions/unitKerjaAction";
import { getListUnitInduk } from "../../../actions/unitIndukAction";
import { Spinner } from 'react-bootstrap';
import ModalPengaturanUnitKerja from '../../../components/unit-kerja/ModalPengaturanUnitKerja';
import DaftarUnitKerja from '../../../components/laporan/DaftarUnitKerja';
import { REGISTER_FU068, REGISTER_FU069 } from '../../../constants/hakAksesConstants';

class UnitKerja extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal:false,
            dataOneRow:null,
            dataRowInduk:null,
            listBreadcrumbUser: [
            { path: "/pengaturan", name: "Pengaturan", status:false },
            { path: "/pengaturan/group", name: "Group", status:true },
        ],
            loadingPage:true,
            dataRow:[],
            renderTable:false,
        };
        this.handleSetLoading = this.handleSetLoading.bind(this); 
        this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this); 
        this.handleCloseFormModalAdd = this.handleCloseFormModalAdd.bind(this); 
        this.handleSendToModalEdit = this.handleSendToModalEdit.bind(this);
        this.props.getListUnitKerja();
        this.props.getListUnitInduk();
        
    }

    
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
        
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.listPengUnitKerja !== prevProps.listPengUnitKerja){
            
            if(this.props.listPengUnitKerja?.data !== undefined){
                this.setState(() => ({
                    dataRow: this.props.listPengUnitKerja?.data,
                    dataRowInduk: this.props.listPengUnitInduk?.data,
                    renderTable:true,
                    loadingPage:false
                }));
            } 
        }
    }
    handleShowFormModalAdd(e) {    
        this.setState(() => ({      
                showModal: e,
                dataOneRow:null,
        }));  
    }
    handleCloseFormModalAdd() {    
        this.setState(() => ({      
                showModal: false
        }));  
    }
    handleSetLoading(){
        this.setState(() => ({      
            loadingPage: true,   
            renderTable: false,     
        }));
    }
    async handleSendToModalEdit(dataOne) {    
        await this.setState(({       
            dataOneRow:dataOne,
            showModal: true,
        }));
    }
    handlePrint= () => this.componentRef
    render(){
        return (
            <div className="container-fluid">
                
                
                {this.state.loadingPage ? (
                    <div className="loadingPage">
                        <Spinner animation="border" className="spin"  variant="light" />
                    </div>
                    ) : ("")}
                {this.state.renderTable ? 
                (<Fragment><CreatePanel kodeAdd={REGISTER_FU069} kodeCetak={REGISTER_FU068} print={this.handlePrint} showForm={this.handleShowFormModalAdd}/>
                <DataTablePengaturanUnitKerja dataRow={this.state.dataRow} dataOneRow={this.handleSendToModalEdit}/>
                <div style={{ display: 'none' }}>
                    <DaftarUnitKerja datatable={this.state.dataRow} ref={(el) => (this.componentRef = el)}/>
                </div>
                </Fragment>) : "" }
                    

                {this.state.showModal ?
                <ModalPengaturanUnitKerja show={this.state.showModal} onHide={this.handleCloseFormModalAdd} dataedit={this.state.dataOneRow} dataoption={this.state.dataRowInduk} actionLoading={this.handleSetLoading}/> : "" }
            </div>
        )
    }
    

}

const mapStateToProps = (state) => {
    return{
        listPengUnitKerja: state.listPengUnitKerja,
        listPengUnitInduk: state.listPengUnitInduk
    } 
 }
export default connect(mapStateToProps, {getListUnitKerja, getListUnitInduk})(UnitKerja);