import React, { Component, useState } from 'react';
import CreatePanel from '../../../components/CreatePanel';
import { Button, Card, Col, Form, Modal, Row } from 'react-bootstrap';
import DataTablePengaturanUnitInduk from '../../../components/DataTablePengaturanUnitInduk';



class Dinas extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal:false,
            listBreadcrumbUser: [
            { path: "/pengaturan", name: "Pengaturan", status:false },
            { path: "/pengaturan/dinas", name: "Dinas", status:true },
        ],
        };
        this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this); 
        this.handleCloseFormModalAdd = this.handleCloseFormModalAdd.bind(this); 
    }
    
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }

    handleShowFormModalAdd(e) {    
        console.log(e)
        this.setState(() => ({      
                showModal: e
        }));  
    }
    handleCloseFormModalAdd() {    
        this.setState(() => ({      
                showModal: false
        }));  
    }
    render(){
        return (
            <div className="container-fluid">
                <CreatePanel showForm={this.handleShowFormModalAdd}/>
                <DataTablePengaturanUnitInduk />
                <MyVerticallyCenteredModal show={this.state.showModal} onHide={this.handleCloseFormModalAdd}/>
                
            </div>
        )
    };
    

}



function MyVerticallyCenteredModal(props) {
    const [kode, setKode] = useState(null);
    const [nama, setNama] = useState(null);
    const [alamat, setAlamat] = useState(null);
    const [lokasi_map, setLokasiMap] = useState(null);
    return (
      <Modal 
        {...props}
        size="lg"
        aria-labelledby="contained-modal-title-vcenter"
        centered
      >
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">
          Tambah Dinas
          </Modal.Title>
        </Modal.Header>
        <Modal.Body>
            <Form >
            <Card>
                <Card.Body>
                    <Form.Row>
                        <Form.Group as={Col} controlId="form_kode">
                        <Form.Label className="textblack">Kode</Form.Label>
                        <Form.Control type="text" placeholder="Masukan Nomor Induk" onChange={v => setKode(v.target.value)} />
                        </Form.Group>

                        <Form.Group as={Col} controlId="form_ktp">
                        <Form.Label className="textblack">Nama Dinas</Form.Label>
                        <Form.Control type="text" placeholder="Masukan No KTP" onChange={v => setNama(v.target.value)}/>
                        </Form.Group>
                    </Form.Row>
                    <Form.Row>
                        <Form.Group as={Col} controlId="form_nama">
                        <Form.Label className="textblack">Alamat</Form.Label>
                        <Form.Control type="text" placeholder="Masukan Nama Pegawai" onChange={v => setAlamat(v.target.value)}/>
                        </Form.Group>

                    </Form.Row>
                    <Form.Row>

                    <Form.Group as={Col} controlId="form_jabatan">
                        <Form.Label className="textblack">Lokasi Map</Form.Label>
                        <Form.Control as="select" defaultValue="Pilih...">
                            <option>Pilih...</option>
                            <option>...</option>
                        </Form.Control>
                        </Form.Group>
                    </Form.Row>
                  
                </Card.Body>
            </Card>
          
                </Form>
            </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={props.onHide}>
                Tutup
            </Button>
            <Button variant="primary" className="btn-them-red" onClick={props.onHide}>
                Simpan
            </Button>
        </Modal.Footer>
      </Modal>
    );
  }
export default Dinas;