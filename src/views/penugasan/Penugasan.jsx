import React, { Component } from 'react';

import { Card, Nav } from 'react-bootstrap';
import { NavLink, Redirect } from 'react-router-dom';
import BreadcrumbUser from '../../components/BreadcrumbUser';
import FilterShowData1 from '../../components/filter/FilterShowData1';
import CreateFilterSingleMonthMultiSuperUnit from '../../components/filter/CreateFilterSingleMonthMultiSuperUnit';

import ReactToPrint from 'react-to-print';

import {connect} from 'react-redux';
import {getListAbsenInduk, getListAbsenIndukAll} from "../../actions/absensiAction";
import MelaksanakanTugas from './melaksanakan-tugas/MelaksanakanTugas';
import TugasLuar from './tugas-luar/TugasLuar';
import AlertPengembangan from '../../components/AlertPengembangan';
import CreateFilterSingleMonthMultiSuperUnitKerja from '../../components/filter/CreateFilterSingleMonthMultiSuperUnitKerja';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU021, REGISTER_FU022, REGISTER_FU016, REGISTER_FU017, REGISTER_FU001, REGISTER_FU015, REGISTER_FU020 } from '../../constants/hakAksesConstants';

const moment = require('moment');

class Penugasan extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sub_page: "",
            datasMasuk:null,
            loading:true,
            modalParsing:{ showModal: false, dataOneRow:null,
                typeModal: "TAMBAH"},

            listBreadcrumbUser: [
                { path: "/penugasan", name: "Penugasan", status:true },
            ],
            filterData:{
                endDate: moment().format('YYYY-MM-DD'),
                kodeUnitInduk: "all",
                kodeUnitKerja: "all",
                startDate: moment().format('YYYY-MM-DD'),
              }
        };
        this.getDataFilter = this.getDataFilter.bind(this);
        this.renderSubPage = this.renderSubPage.bind(this);
        this.handleShowFormModal = this.handleShowFormModal.bind(this);
    }

    SetBreadcrumbChild = (listBreadcrumbChild) =>{
        this.setState({listBreadcrumbUser: listBreadcrumbChild})
    }

    renderSubPage(param) {
        
        switch (param) {
            case 'melaksanakan-tugas':
                if(cekHakAksesClient(REGISTER_FU015)){
                return (
                        <div>
                            <MelaksanakanTugas filterValue={this.state.filterData} breadcurmbChild={this.SetBreadcrumbChild} modalparsingchild={this.state.modalParsing}/>
                        </div>);
                }
                return (
                    <Redirect to="/*" />
                )
            case 'tugas-luar':
                if(cekHakAksesClient(REGISTER_FU020)){
                return (
                    <div>
                        <TugasLuar filterValue={this.state.filterData} breadcurmbChild={this.SetBreadcrumbChild} modalparsingchild={this.state.modalParsing}/>
                    </div>
                    );
                }
                return (
                    <Redirect to="/*" />
                )
            default:
                if(cekHakAksesClient(REGISTER_FU015)){
                return( 
                    <div>
                            <MelaksanakanTugas filterValue={this.state.filterData} breadcurmbChild={this.SetBreadcrumbChild} modalparsingchild={this.state.modalParsing}/>
                    </div>);
                }
                return (
                    <Redirect to="/penugasan/tugas-luar" />
                )
        }
    }

    handlePrint= () => this.componentRef
    
    componentDidMount(){
        this.callApi();
    }
    componentDidUpdate(prevProps, prevState) {
            // this.callApi();
    }
    render(){
        let k_cetak;
        let k_add;
        switch(this.props.sub_page){
            case 'melaksanakan-tugas':
                k_cetak=REGISTER_FU016
                k_add=REGISTER_FU017
                break;
            case 'tugas-luar':
                k_cetak=REGISTER_FU021
                k_add=REGISTER_FU022
                break;
            default:
                k_cetak=REGISTER_FU016
                k_add=REGISTER_FU017
                break;
        }
        return(
            <div className="container-fluid">
                {/* <ComingSoon/> */}
                <h1 className="mt-4">Penugasan</h1>
                <AlertPengembangan/>
                <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />
                { cekHakAksesClient(REGISTER_FU001) ? <CreateFilterSingleMonthMultiSuperUnit print={this.handlePrint} kodeCetak={k_cetak} kodeAdd={k_add}  setloading={this.state.loading} dataFilter={this.getDataFilter} showForm={this.handleShowFormModal}/> : <CreateFilterSingleMonthMultiSuperUnitKerja print={this.handlePrint} setloading={this.state.loading} kodeCetak={k_cetak} kodeAdd={k_add} dataFilter={this.getDataFilter} showForm={this.handleShowFormModal}/>
                }

                <Nav fill variant="pills" className="nav3-dark" >
                    {cekHakAksesClient(REGISTER_FU015) ?
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/penugasan/melaksanakan-tugas" href="/penugasan/melaksanakan-tugas">Melaksanakan Tugas</Nav.Link>
                    </Nav.Item>
                    
                    :""}
                    {cekHakAksesClient(REGISTER_FU015) ? 
                    <Nav.Item>
                        <Nav.Link as={NavLink} to="/penugasan/tugas-luar" href="/penugasan/tugas-luar">Tugas Luar</Nav.Link>
                    </Nav.Item>
                    
                    :""}
                </Nav>
             
                <Card>
                    {this.renderSubPage(this.props.sub_page)}
                </Card>


            </div>
        );
    }


    async callApi(){
    //  state.loading to false
        
    }

    getDataFilter(v) {  
          this.setState(() => ({
              filterData:v,
              loading:true      
            }), () =>{this.callApi()});  
          
    }
    handleShowFormModal(e, type) {
        console.log(type)
        let v={
            showModal: e,
            dataOneRow:null,
            typeModal: type,
            }    
        this.setState(() => ({      
            modalParsing:v
        }));  
    }



}

export default Penugasan;
