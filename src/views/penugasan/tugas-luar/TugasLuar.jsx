import React, { Component } from "react";


// Redux
import {connect} from 'react-redux';
import { cekHakAksesClient } from "../../../actions/hakAksesActions";
import { getListTugasLuar } from "../../../actions/penugasanAction";
import { uIIDP } from "../../../actions/userActions";
import ModalTugasLuar from "../../../components/penugasan/ModalTugasLuar";
import TabelTugasLuar from "../../../components/penugasan/TabelTugasLuar";
import { REGISTER_FU001 } from "../../../constants/hakAksesConstants";

const moment = require('moment');

const data=[{
	id:1,
	idPegawai:"2",
	namaPegawai:"Test",
	nip:"3434234",
	unitInduk:"unit Induk",
	unitIndukId:"1",
	unitKerja:"Unit kerja",
	unitKerjaId:"2",
	tanggalAwal:"2021-02-23",
	tanggalAkhir:"2022-03-23",
}]

class TugasLuar extends Component {
    constructor(props) {
        super(props);
        this.state ={
            showModal: false,
            typeModal: "TAMBAH",
            data:null,
            dataOneRow:null,
            listBreadcrumbUser: [
                { path: "/penugasan", name: "Penugasan", status:false },
                { path: "/penugasan/melaksanakan-tugas", name: "Tugas Luar", status:true },           
            ],
            loading:true,
            filterData:{
                selectMultyUnitInduk: [],
                selectMultyUnitKerja: [],
                startDate: moment().format('YYYY-MM'),
            }
        }
        this.getDataFilter = this.getDataFilter.bind(this);
        this.setDataOneRow = this.setDataOneRow.bind(this);
        this.handleShowFormModal = this.handleShowFormModal.bind(this); 
        this.handleCloseFormModal = this.handleCloseFormModal.bind(this);
    }
    async callApi(){

        let unitIndukArr = [
            {id:uIIDP()}
        ]

        if(cekHakAksesClient(REGISTER_FU001)){
            unitIndukArr = this.state.filterData?.selectMultyUnitInduk
        }  

        this.props.getListTugasLuar(this.state.filterData.startDate, unitIndukArr, this.state.filterData.selectMultyUnitKerja).then(() =>{
            this.setState(() => ({      
                data: this.props.listTugasLuar.data,
                loading:false,
            })); 
        })
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
        if(this.props.filterValue != null){
            this.setState(() => ({
                filterData: this.props.filterValue,
                showModal:false,
                dataOneRow:this.props.modalparsingchild?.dataOneRow,
                typeModal:this.props.modalparsingchild?.typeModal
            }), this.callApi )
        }
    }

    componentDidUpdate(prevProps, prevState){
        if(prevProps.modalparsingchild !== this.props.modalparsingchild){
            this.setState(() => ({
                showModal:this.props.modalparsingchild.showModal,
                dataOneRow:this.props.modalparsingchild.dataOneRow,
                typeModal:this.props.modalparsingchild.typeModal
            }), this.callApi )
            // console.log("a")
        }
    }

    render(){
        return(
            <>
            <TabelTugasLuar datarow={data} getFIlter={this.state.filterData} setdataonerow={this.setDataOneRow} />
            {this.state.showModal ? <ModalTugasLuar show={this.state.showModal} onHide={this.handleCloseFormModal} jenisModal={this.state.typeModal} dataonerow={this.state.dataOneRow}/> :""}
            </>
        )
    }

    getDataFilter(v) {  
        this.setState(() => ({
            filterData:v,
            loading:true      
          }), () =>{
              this.callApi()
            });   
    }
    handleShowFormModal(e, t) {    
        this.setState(() => ({      
                showModal: e,
                dataOneRow:null,
                typeModal: t,
        }));  
    }
    setDataOneRow(dataone, t) {  
        console.log(dataone)  
        this.setState(() => ({      
                showModal: true,
                dataOneRow:dataone,
                typeModal: t,
        }));  
    }
    handleCloseFormModal() {    
        this.setState(() => ({      
                showModal: false,
                // showModalDetail: false,
        }));  
    }
}

const mapStateToProps = (state) => {
    return{
        listTugasLuar: state.listTugasLuar,
    } 
 }
export default connect(mapStateToProps, {getListTugasLuar}) (TugasLuar);