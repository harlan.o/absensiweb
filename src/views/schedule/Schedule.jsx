import React, { Component } from 'react';

import { Card, Nav } from 'react-bootstrap';
import { NavLink, Redirect } from 'react-router-dom';
import BreadcrumbUser from '../../components/BreadcrumbUser';
// import Bulanan from './bulanan/Bulanan';
// import Harian from './harian/Harian';
// import Tahunan from './tahunan/Tahunan';
import FilterShowData1 from '../../components/filter/FilterShowData1';
import ComingSoon from '../page-coming-soon/ComingSoon';

import ReactToPrint from 'react-to-print';

import {connect} from 'react-redux';
import {getListAbsenInduk, getListAbsenIndukAll} from "../../actions/absensiAction";
import JadwalKerja from './jadwal-kerja/JadwalKerja';
import JadwalPegawai from './jadwal-pegawai/JadwalPegawai';
import HariLibur from './hari-libur/HariLibur';
import AlertPengembangan from '../../components/AlertPengembangan';
import { cekHakAksesClient } from '../../actions/hakAksesActions';
import { REGISTER_FU048, REGISTER_FU053, REGISTER_FU044 } from '../../constants/hakAksesConstants';

// const moment = require('moment');

class Schedule extends Component {
    constructor(props) {
        super(props);
        this.state = {
            sub_page: "",
            datasMasuk:null,
            loading:true,
            listBreadcrumbUser: [
                { path: "/schedule", name: "Schedule", status:true },
            ],
        };
        this.getDataFilter = this.getDataFilter.bind(this);
        this.renderSubPage = this.renderSubPage.bind(this);
    }

    SetBreadcrumbChild = (listBreadcrumbChild) =>{
        this.setState({listBreadcrumbUser: listBreadcrumbChild})
    }

    renderSubPage(param) {
        console.log(param);
        switch (param) {
            case 'jadwal-pegawai':
                if(cekHakAksesClient(REGISTER_FU044)){
                    return (<div>
                        <JadwalPegawai breadcurmbChild={this.SetBreadcrumbChild}/>
                    </div>);
                }
                return (
                    <Redirect to="/*" />
                )
            case 'jadwal-kerja':
                if(cekHakAksesClient(REGISTER_FU048)){
                    return (
                    <div>
                        <JadwalKerja breadcurmbChild={this.SetBreadcrumbChild}/>
                    </div>
                    );
                }
                return (
                    <Redirect to="/schedule/hari-libur" />
                )
            case 'hari-libur':
                if(cekHakAksesClient(REGISTER_FU053)){
                return (<div>
                    <HariLibur breadcurmbChild={this.SetBreadcrumbChild}/>
                </div>);
                }
                return (
                    <Redirect to="/*" />
                )
            default:
                if(cekHakAksesClient(REGISTER_FU044)){
                return (<div>
                    <JadwalPegawai breadcurmbChild={this.SetBreadcrumbChild}/>
                </div>);
                }
                return (
                    <Redirect to="/schedule/jadwal-kerja" />
                )
        }
    }

    handlePrint= () => this.componentRef
    
    componentDidMount(){
        this.callApi();
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.totalHadir !== prevProps.totalHadir){
            this.callApi();
        }
    }
    render(){
        return(
            <div className="container-fluid">
                {/* <ComingSoon/> */}
                <h1 className="mt-4">Schedule</h1>
                {/* <AlertPengembangan/> */}
                <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />

                {/* <FilterShowData1 print={this.handlePrint} dataFilter={this.getDataFilter}/> */}
                
                

                <Nav fill variant="pills" className="nav3-dark" >
                    {cekHakAksesClient(REGISTER_FU044) ?
                        <Nav.Item>
                            <Nav.Link as={NavLink} to="/schedule/jadwal-pegawai" href="/schedule/jadwal-pegawai">Jadwal Pegawai</Nav.Link>
                        </Nav.Item>
                    :""}
                    {cekHakAksesClient(REGISTER_FU048) ? 
                        <Nav.Item>
                            <Nav.Link as={NavLink} to="/schedule/jadwal-kerja" href="/schedule/jadwal-kerja">Jadwal Kerja</Nav.Link>
                        </Nav.Item>
                    :""}
                    {cekHakAksesClient(REGISTER_FU053) ?
                        <Nav.Item>
                            <Nav.Link as={NavLink} to="/schedule/hari-libur" href="/schedule/hari-libur">Hari Libur</Nav.Link>
                        </Nav.Item>
                    :""}
                </Nav>
             
                <Card>
                    {this.renderSubPage(this.props.sub_page)}
                </Card>


            </div>
        );
    }


    async callApi(){
                
    }
    getDataFilter(v) {  
          this.setState(() => ({
              filterData:v      
            }), () =>{this.callApi()});          
    }
}


const mapStateToProps = (state) => {
    return{
        listAbsensiInduk: state.listAbsensiInduk
    } 
  }
export default connect(mapStateToProps, {getListAbsenInduk, getListAbsenIndukAll})(Schedule);
