import React, { Component } from 'react';
import CreateFilterSingleYear from '../../../components/filter/CreateFilterSingleYear';
import TabelHariLibur from '../../../components/schedule/TabelHariLibur';
import { REGISTER_FU055, REGISTER_FU054 } from '../../../constants/hakAksesConstants';

import {connect} from 'react-redux';
import {getListHariLibur} from "../../../actions/hariLiburAction.js";
import ModalFormHariLibur from '../../../components/schedule/ModalFormHariLibur';
import ModalConfirmDeleteHariLibur from '../../../components/schedule/ModalConfirmDeleteHariLibur';
import CetakHariLibur from '../../../components/laporan/CetakHariLibur';

const listHoliday = [{
    "tanggal": "2021-03-11",
    "keterangan": "Isra Mi'raj Nabi Muhammad"
},
{
    "tanggal": "2021-04-02",
    "keterangan": "JUMAT AGUNG"
},
{
    "tanggal": "2021-02-12",
    "keterangan": "TAHUN BARU IMLEK"
}]

const moment = require('moment');

class HariLibur extends Component {
    constructor(props) {
        super(props);
        this.state = {
            loading:true,
            showAdd:false,
            showModalDelete:false,
            listBreadcrumbUser: [
                { path: "/schedule", name: "Schedule", status:false },
                { path: "/schedule/hari-libur", name: "Hari Libur", status:true },
            ],
            filterData: {
                startDate: moment().format('YYYY')
            },
            data:null,
            dataOneRow:{tanggal:"", keterangan:""}
        };
        // this.handleSendToModal = this.handleSendToModal.bind(this);
        // this.handleCloseFormModal = this.handleCloseFormModal.bind(this); 
        // this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this); 
        this.getDataFilter = this.getDataFilter.bind(this);
        this.handleSendToModal = this.handleSendToModal.bind(this);
        this.handleCloseFormModal = this.handleCloseFormModal.bind(this); 
        this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this);
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
        this.props.getListHariLibur(this.state.filterData.startDate)
    }

    async callApi(){
        this.props.getListHariLibur(this.state.filterData.startDate).then(() =>{
            this.setState(() => ({      
                loading:false,
            })); 
        })
    }
    handlePrint= () => this.componentRef
    render(){
        console.log("Hari Libur List", this.props.listHariLibur.data)
        console.log("Hari Libur  State", this.state.data)
        return(
            <div className="container-fluid">

                <CreateFilterSingleYear print={this.handlePrint} setloading={this.state.loading} dataFilter={this.getDataFilter} kodeAdd={REGISTER_FU055} kodeCetak={REGISTER_FU054} showForm={this.handleShowFormModalAdd}/>

            <TabelHariLibur dataRow={this.props.listHariLibur.data} dataOneRow={this.handleSendToModal} />
            <div style={{ display: 'none' }}>
                            <CetakHariLibur datatable={this.props.listHariLibur.data} ref={(el) => (this.componentRef = el)}/>
                        </div>
            {this.state.showAdd ?<ModalFormHariLibur show={this.state.showAdd} onHide={this.handleCloseFormModal} /> : "" }
            {this.state.showModalDelete ? <ModalConfirmDeleteHariLibur show={this.state.showModalDelete} onHide={this.handleCloseFormModal} dataedit={this.state.dataOneRow} /> : "" }
            </div>
        )
    }

    async handleSendToModal(dataOne, action) {    
        console.log("hadle Del", dataOne, action)
        if(action==="edit"){
            await this.setState(({       
                dataOneRow:dataOne,
                showAdd: true,
            }));
        }else if(action==="delete"){
            console.log("delete");
            await this.setState(({       
                dataOneRow:dataOne,
                showModalDelete: true,
            }));
        }
    }
    handleShowFormModalAdd(e) {    
        // console.log(e)
        this.setState(() => ({      
                showAdd: e,
                dataOneRow:null,
        }));  
    }
    handleCloseFormModal() {    
        this.setState(() => ({      
                showAdd: false,
                showModalDelete: false,
                // showAddDetail: false,
        }));  
    }

    getDataFilter(v) {  
        this.setState(() => ({
            filterData:v,
            loading:true      
          }), () =>{
              this.callApi()
            });  
    }
}

const mapStateToProps = (state) => {
    return{
        listHariLibur: state.listHariLibur
    } 
  }


export default connect(mapStateToProps, {getListHariLibur})(HariLibur);