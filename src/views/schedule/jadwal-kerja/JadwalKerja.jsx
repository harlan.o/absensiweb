import React, { Component } from 'react';
import TabelJadwalKerja from '../../../components/schedule/TabelJadwalKerja';

import {connect} from 'react-redux';
import {getListJadwalKerja} from "../../../actions/jadwalKerjaAction";
import ModalFormJadwalKerja from '../../../components/schedule/ModalFormJadwalKerja';
import CreatePanel from '../../../components/CreatePanel';
import ModalConfirmDeleteJadwalKerja from '../../../components/schedule/ModalConfirmDeleteJadwalKerja';
import { REGISTER_FU049, REGISTER_FU050 } from '../../../constants/hakAksesConstants';

class JadwalKerja extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showModalEdit:false,
            showModalDelete:false,
            listBreadcrumbUser: [
            { path: "/schedule", name: "Schedule", status:false },
            { path: "/schedule/jadwal-kerja", name: "Jadwal Kerja", status:true },
            
        ],
        };
        this.handleSendToModal = this.handleSendToModal.bind(this);
        this.handleCloseFormModal = this.handleCloseFormModal.bind(this); 
        this.handleShowFormModalAdd = this.handleShowFormModalAdd.bind(this); 
    }
    componentDidMount(){
        this.props.getListJadwalKerja();
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
        
    }
    render(){
        return (
            <div>
                <div className="container-fluid">
                <CreatePanel kodeAdd={REGISTER_FU050} kodeCetak={REGISTER_FU049} print={this.handlePrint} showForm={this.handleShowFormModalAdd}/>
                {this.props.listJadwalKerja ? <TabelJadwalKerja dataRow={this.props.listJadwalKerja.data} dataOneRow={this.handleSendToModal} />:''}
                </div>
                {this.state.showModalEdit ?<ModalFormJadwalKerja show={this.state.showModalEdit} onHide={this.handleCloseFormModal} dataedit={this.state.dataOneRow} /> : "" }

                {this.state.showModalDelete ? <ModalConfirmDeleteJadwalKerja show={this.state.showModalDelete} onHide={this.handleCloseFormModal} dataedit={this.state.dataOneRow} /> : "" }
            </div>
        );
    }

    handleShowFormModalAdd(e) {    
        // console.log(e)
        this.setState(() => ({      
                showModalEdit: e,
                dataOneRow:null,
        }));  
    }
    async handleSendToModal(dataOne, action) {    
        if(action==="edit"){
            await this.setState(({       
                dataOneRow:dataOne,
                showModalEdit: true,
            }));
        }else if(action==="delete"){
            console.log("delete");
            await this.setState(({       
                dataOneRow:dataOne,
                showModalDelete: true,
            }));
        }
    }
    handleCloseFormModal() {    
        this.setState(() => ({      
                showModalEdit: false,
                showModalDelete: false,
                // showModalEditDetail: false,
        }));  
    }

}

const mapStateToProps = (state) => {
    return{
        listJadwalKerja: state.listJadwalKerja
    } 
  }
export default connect(mapStateToProps, {getListJadwalKerja})(JadwalKerja);