import React, { Component } from 'react';

// DatePicker View List
import DatePicker, { Calendar } from "react-multi-date-picker";
import { DatePanel } from "react-multi-date-picker/plugins";
import "react-multi-date-picker/styles/colors/red.css"
import "react-multi-date-picker/styles/backgrounds/bg-dark.css";
import FilterSingleMonthMultiSuperUnit from '../../../components/filter/FilterSingleMonthMultiSuperUnit';
import { monthName } from '../../../actions/genneralAction';
import TabelJadwalPegawai from '../../../components/schedule/TabelJadwalPegawai';
import CreateFilterSingleMonthMultiSuperUnit from '../../../components/filter/CreateFilterSingleMonthMultiSuperUnit';
import ModalFormAddJadwalPegawai from '../../../components/schedule/ModalFormAddJadwalPegawai';
import TrueCreateFilterSingleMonthSingleIndukMultipleKerja from '../../../components/filter/TrueCreateFilterSingleMonthSingleIndukMultipleKerja';

// Redux
import {connect} from 'react-redux';
import { getListJadwalPegawai } from "../../../actions/jadwalPegawaiAction";
import ModalJadwalPegawai from '../../../components/schedule/ModalJadwalPegawai';
import TrueCreateFilterSingleMonthMultipleKerja from '../../../components/filter/TrueCreateFilterSingleMonthMultipleKerja';
import { cekHakAksesClient } from '../../../actions/hakAksesActions';
import { REGISTER_FU001, REGISTER_FU046, REGISTER_FU045 } from '../../../constants/hakAksesConstants';


// const data = [
//     {idPegawai: 1, namaPegawai:"Test Pegawai", nip:"5658696859", unitIndukId:212,unitInduk:"DINAS PENDIDIKAN", unitKerjaId: 756, unitKerja:"SD NEGERI INPRES BEBALI", jadwalList:[
//         {tanggalKerja:"2021-01-01", jadwalId: 3, nama: "hari H", jamMasuk: "10:00:00", jamPulang:"18:00:00"},
//         {tanggalKerja:"2021-01-04", jadwalId: 3, nama: "hari H", jamMasuk: "10:00:00", jamPulang:"18:00:00"},
//         {tanggalKerja:"2021-01-05", jadwalId: 3, nama: "hari H", jamMasuk: "10:00:00", jamPulang:"18:00:00"}
//     ]},
//     {idPegawai: 2, namaPegawai:"Test Pegawai 2", nip:"5658696860", unitIndukId:212,unitInduk:"DINAS PENDIDIKAN", unitKerjaId: 756, unitKerja:"SD NEGERI INPRES BEBALI", jadwalList:[
//         {tanggalKerja:"2021-01-01", jadwalId: 3, nama: "hari H", jamMasuk: "10:00:00", jamPulang:"18:00:00"},
//         {tanggalKerja:"2021-01-04", jadwalId: 3, nama: "hari H", jamMasuk: "10:00:00", jamPulang:"18:00:00"},
//         {tanggalKerja:"2021-01-05", jadwalId: 3, nama: "hari H", jamMasuk: "10:00:00", jamPulang:"18:00:00"}
//     ]},
// ];


const moment = require('moment');
class JadwalPegawai extends Component {
    constructor(props) {
        super(props);
        this.state = {
            value:new Date(),
            showModal:false,
            typeModal:"TAMBAH",
            data:null,
            dataOneRow:null,
            listBreadcrumbUser: [
                { path: "/schedule", name: "Schedule", status:false },
                { path: "/schedule/jadwal-pegawai", name: "Jadwal Pegawai", status:true },           
            ],
            loading:true,
            filterData:{
                selectMultyUnitInduk: [],
                selectMultyUnitKerja: [],
                startDate: moment().format('YYYY-MM'),
            }
        };
        this.getDataFilter = this.getDataFilter.bind(this);
        this.setDataOneRow = this.setDataOneRow.bind(this);
        this.handleShowFormModal = this.handleShowFormModal.bind(this); 
        this.handleCloseFormModal = this.handleCloseFormModal.bind(this);

    }
    async callApi(){
        this.props.getListJadwalPegawai(this.state.filterData.startDate, this.state.filterData.selectMultyUnitInduk, this.state.filterData.selectMultyUnitKerja).then(() =>{
            this.setState(() => ({      
                data: this.props.listJadwalPegawai.data,
                loading:false,
            })); 
        })
    }
    componentDidMount(){
        this.props.breadcurmbChild(this.state.listBreadcrumbUser);
    }
    handlePrint= () => this.componentRef
    render(){
      let title=  "Jadwal Bulan "+monthName(moment(this.state.filterData.startDate).format('MM'))+' '+moment(this.state.filterData.startDate).format('YYYY');
      let jumlahTanggal=moment(this.state.filterData.startDate).daysInMonth();
      console.log(jumlahTanggal);
        return(
            <div>

            <div className="container-fluid">
                {/* <CreateFilterSingleMonthMultiSuperUnit dataFilter={this.getDataFilter} setloading={this.state.loading} showForm={this.handleShowFormModalAdd} /> */}
                { cekHakAksesClient(REGISTER_FU001) ?

                <TrueCreateFilterSingleMonthSingleIndukMultipleKerja dataFilter={this.getDataFilter} kodeAdd={REGISTER_FU046} kodeCetak={REGISTER_FU045} setloading={this.state.loading} showForm={this.handleShowFormModal} print={this.handlePrint}/> :
                <TrueCreateFilterSingleMonthMultipleKerja dataFilter={this.getDataFilter} kodeAdd={REGISTER_FU046} kodeCetak={REGISTER_FU045} setloading={this.state.loading} showForm={this.handleShowFormModal} print={this.handlePrint} />
                }
               

            </div>
            
            {(this.state.data != null && this.state.data != undefined) ?
            (
                <TabelJadwalPegawai title={title} jumlahTanggal={jumlahTanggal} dataRow={this.state.data} setdataonerow={this.setDataOneRow} />

            )
            :""}

            {/* {this.state.showModal ? <ModalFormAddJadwalPegawai show={this.state.showModal} onHide={this.handleCloseFormModal} dataedit={this.state.dataOneRow}/> : "" } */}
            {this.state.showModal ? <ModalJadwalPegawai show={this.state.showModal} onHide={this.handleCloseFormModal} jenisModal={this.state.typeModal} dataonerow={this.state.dataOneRow} reloadTabel={()=> this.callApi()}/> : "" }
            </div>
        )
    }
    
    getDataFilter(v) {  
        this.setState(() => ({
            filterData:v,
            loading:true      
          }), () =>{
              this.callApi()
            });  
    }
    handleShowFormModal(e, t) {    
        this.setState(() => ({      
                showModal: e,
                dataOneRow:null,
                typeModal: t,
        }));  
    }
    setDataOneRow(dataone, t) {  
        console.log(dataone)  
        this.setState(() => ({      
                showModal: true,
                dataOneRow:dataone,
                typeModal: t,
        }));  
    }
    handleCloseFormModal() {    
        this.setState(() => ({      
                showModal: false,
                // showModalDetail: false,
        }));  
    }
}

const mapStateToProps = (state) => {
    return{
        listJadwalPegawai: state.listJadwalPegawai,
    } 
 }

export default connect(mapStateToProps, {getListJadwalPegawai}) (JadwalPegawai);

// https://shahabyazdi.github.io/react-multi-date-picker/