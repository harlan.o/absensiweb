import React, { Component } from 'react';
import BreadcrumbUser from '../../components/BreadcrumbUser';

import StatisticAbsensiPegawai from '../../components/pegawai/StatisticAbsensiPegawai';
import ComingSoon from '../page-coming-soon/ComingSoon';

import {connect} from 'react-redux';
import {getListAbsenInduk, getListAbsenIndukAll} from "../../actions/absensiAction";
import FilterSingleMonthMultiSuperUnit from '../../components/filter/FilterSingleMonthMultiSuperUnit';
import StatisticBar from '../../components/statistik/StatistikBar';
import { monthName } from '../../actions/genneralAction';
import ListDataStatistikBar from '../../components/statistik/ListDataStatikBar';
import AlertPengembangan from '../../components/AlertPengembangan';

const moment = require('moment');

const data = [
    {
      kode:1, nama: 'SEKRETARIAT DEWAN PERWAKILAN RAKYAT', totalPegawai:100, hadir: 95, terlambat: 24, ijin: 24, tanpaKeterangan:20
    },
    {
      kode:2, nama: '2', totalPegawai:100, hadir: 95, terlambat: 13, ijin: 22, tanpaKeterangan:20
    },
    {
      kode:3, nama: '3', totalPegawai:100, hadir: 95, terlambat: 10, ijin: 22, tanpaKeterangan:20
    },
    {
      kode:4, nama: '4', totalPegawai:100, hadir: 95, terlambat: 39, ijin: 20, tanpaKeterangan:20
    },
    {
      kode:5, nama: '5', totalPegawai:100, hadir: 95, terlambat: 48, ijin: 21, tanpaKeterangan:20
    },
    {
      kode:6, nama: '6', totalPegawai:100, hadir: 95, terlambat: 38, ijin: 25, tanpaKeterangan:20
    },
    {
      kode:7, nama: '7', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:8, nama: '8', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:9, nama: '9', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:10, nama: '10', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:11, nama: '11', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:12, nama: '12', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:13, nama: '13', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:14, nama: '14', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:15, nama: '15', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:16, nama: '16', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:17, nama: '17', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:18, nama: '18', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:19, nama: '19', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:20, nama: '20', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:21, nama: '21', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:22, nama: '22', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:23, nama: '23', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:24, nama: '24', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:25, nama: '25', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:26, nama: '26', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:27, nama: '27', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:28, nama: '28', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:30, nama: '29', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:31, nama: '30', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:32, nama: '31', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:33, nama: '32', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:34, nama: '33', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
    {
        kode:35, nama: '34', totalPegawai:100, hadir: 95, terlambat: 43, ijin: 21, tanpaKeterangan:20
    },
  ];
  

class Statistik extends Component {
    constructor(props) {
        super(props);
        this.state = {
            listBreadcrumbUser: [
            // { path: "/absensi-pegawai", name: "Absensi Pegawai", status:false },
            { path: "/statistik", name: "Statistik", status:true },
            ],
            datasMasuk:null,
            loading:true,
            filterData:{
                selectMultyUnitInduk: [],
                selectMultyUnitKerja: [],
                startDate: moment().format('YYYY-MM'),
              }
        };

        this.getDataFilter = this.getDataFilter.bind(this);
    }

    componentDidMount(){
        this.callApi();
    }
    componentDidUpdate(prevProps, prevState) {
        if(this.props.totalHadir !== prevProps.totalHadir){
            this.callApi();
        }
    }

    render(){
       let loading=false
        return (
            <div className="container-fluid">
            <h1 className="mt-4">Statistik</h1>
                <AlertPengembangan/>
           
            <BreadcrumbUser listBreadcrumbUser={this.state.listBreadcrumbUser} />
            <FilterSingleMonthMultiSuperUnit dataFilter={this.getDataFilter} setloading={this.state.loading}/>
            {/* <StatisticAbsensiPegawai dataabsen={this.state.datasMasuk}/> */}
            {/* <ComingSoon/> */}
            {/* <TestComponent/> */}
            <h2 className="mt-4">Statistik Bulan {monthName(moment(this.state.filterData.startDate).format('MM'))} {moment(this.state.filterData.startDate).format('YYYY')} </h2>
            <StatisticBar data={data}/>
            <ListDataStatistikBar dataRow={data}/>
        </div>
        );
    }
    
    async callApi(){
        if(this.state.filterData?.kodeUnitInduk !== null && this.state.filterData?.kodeUnitInduk !=="all"){

            await this.props.getListAbsenInduk(this.state.filterData?.kodeUnitInduk, this.state.filterData?.startDate, this.state.filterData?.endDate, "masuk").then( ()=>{
                if(this.props.listAbsensiInduk?.data !== undefined && this.props.listAbsensiInduk?.data !== null){
                    let datasAll=this.props.listAbsensiInduk?.data;
                    if(this.state.filterData?.kodeUnitKerja !== null && this.state.filterData?.kodeUnitKerja !=="all"){
                        let idKodeUKerja = parseInt(this.state.filterData?.kodeUnitKerja);
                        datasAll=datasAll.filter(peggawai =>peggawai.unitKerjaId === idKodeUKerja);   
                    }
                    this.setState({
                    datasMasuk:datasAll,  
                    loading:false,
                    }) 
                }
            });

        }else{
            await this.props.getListAbsenIndukAll(this.state.filterData?.startDate, this.state.filterData?.endDate, "masuk").then( ()=>{
                if(this.props.listAbsensiInduk?.data !== undefined && this.props.listAbsensiInduk?.data !== null){
                    let datasAll=this.props.listAbsensiInduk?.data;
                    this.setState({
                        datasMasuk:datasAll,
                        loading:false,
                    }) 
                    // return datasAll;
                }
            }
            );

        }
    }

    getDataFilter(v) {  
        this.setState(() => ({
            filterData:v,
            loading:true      
          }), () =>{this.callApi()});  
        
  }
}


const mapStateToProps = (state) => {
    return{
        listAbsensiInduk: state.listAbsensiInduk
    } 
  }

export default connect(mapStateToProps, {getListAbsenInduk, getListAbsenIndukAll})(Statistik);